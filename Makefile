include Makefile.top

SUBDIRS=include src init.d doc librhcm

#
# Main build.  We build static-libs in librhcm first because we do not want
# the cluster to be detached from the API library - that is, changing the
# API library should not break the cluster's internal stuff.
#
all: librhcm/sizes.h
	make -C librhcm dynamic-clean static-libs
	for subdir in $(SUBDIRS); do make -C $$subdir || exit 1; done


#
# Build the clumanager.spec file from clumanager.spec.in.  Just a sed
# operation.
# Uses "VERSION" define from Makefile.top.
#
clumanager.spec: clumanager.spec.in Makefile.top
	@[ -n "$(RELEASE)" ] || echo "RELEASE not defined; aborting"
	@[ -n "$(RELEASE)" ] || exit 1
	sed -e s/"##VERSION##"/$(VERSION)/g \
	    -e s/"##RELEASE##"/$(RELEASE)/g \
		clumanager.spec.in > clumanager.spec


#
# RPM operations
#
srpm: rpmcommon 
	rm -f clumanager-$(VERSION)*src.rpm
	[ -z "`/bin/ls *patch`" ] || cp *patch `pwd`/rpm_build
	rpmbuild --define "_sourcedir `pwd`/rpm_build" \
		--define "_srcrpmdir `pwd`/rpm_build" \
		--define "_builddir `pwd`/rpm_build" \
		--define "_rpmdir `pwd`/rpm_build" --nodeps -bs \
		rpm_build/clumanager.spec
	mv rpm_build/*rpm .

rpm: rpmcommon
	rm -f clumanager-$(VERSION)*$(ARCH).rpm
	rpmbuild --define "_sourcedir `pwd`/rpm_build" \
		--define "_srcrpmdir `pwd`/rpm_build" \
		--define "_builddir `pwd`/rpm_build" \
		--define "_rpmdir `pwd`/rpm_build" --nodeps -bb \
		rpm_build/clumanager.spec
	mv rpm_build/*/*rpm .

gitrpms:
	rm -f clumanager.spec
	make RELEASE=0.1git`git log -1 | head -1 | cut -f2 -d' '` rpms

rpms: srpm rpm
	rm -rf rpm_build

rpmcommon: librhcm/sizes.h rpm_build

rpm_build: tarball clumanager.spec librhcm/sizes.h
	rm -rf $@
	mkdir $@
	cp clumanager-$(VERSION).tar.gz rpm_build
	cp clumanager.spec rpm_build


#
# Tarball operations.
#
tarball: clumanager-$(VERSION).tar.gz librhcm/sizes.h

tarball-clean:
	rm -f clumanager-*.tar.gz

clumanager-$(VERSION).tar.gz: librhcm/sizes.h
	rm -rf clumanager-$(VERSION)
	mkdir clumanager-$(VERSION)
	cp Makefile* clumanager-$(VERSION)
	cp COPYING ChangeLog clumanager.doxygen clumanager.spec.in \
		clumanager-$(VERSION)
	for subdir in $(SUBDIRS); do cp -a $$subdir clumanager-$(VERSION); done	
	make -C clumanager-$(VERSION) purge gitpurge
	tar -czf clumanager-$(VERSION).tar.gz clumanager-$(VERSION)
	rm -rf clumanager-$(VERSION)

#
# Spec file operations.
#
spec: clumanager.spec librhcm/sizes.h


#
# Pool cleaning operations.
#
spec-clean:
	rm -f clumanager.spec

gitpurge:
	find . -type d -name ".git" | xargs rm -rf

clean:
	find . -name "*buildtest*" | xargs rm -f
	rm -rf rpm_build
	rm -f librhcm/sizes.h
	for subdir in $(SUBDIRS); do make -C $$subdir clean || exit 1; done

maintainer-clean: dist-clean

distclean: purge

purge: clean tarball-clean spec-clean
	rm -rf clumanager-api 
	rm -f ./clumanager*rpm


#
# Installation!
#
install: 
	for subdir in $(SUBDIRS); do make -C $$subdir install || exit 1; done
	mkdir -p $(DOCDIR)
	install -m 0644 COPYING $(DOCDIR)
	install -m 0644 ChangeLog $(DOCDIR)
	mkdir -p $(LOCKDIR)
	echo "Member-wide lock files are stored here - do not remove" > \
		$(LOCKDIR)/README.locks

#
# We generate this at build time.
#
librhcm/sizes.h:
	make -C librhcm sizes.h
