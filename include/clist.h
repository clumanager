/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Header for clist.c
 */
#ifndef __CLIST_H
#define __CLIST_H

#include <sys/select.h>

int clist_insert(int fd, int flags);
int clist_delete(int fd);
int clist_fill_fdset(fd_set *set);
int clist_next_set(fd_set *set);
int clist_set_flags(int fd, int flags);
int clist_multicast(void *msg, int len, int flags);
int clist_broadcast(void *msg, int len);
int clist_init(void);

#endif
