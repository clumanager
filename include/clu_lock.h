/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Header for clulock.c
 */
/*
 *  $Revision: 1.14 $
 *
 *  Author: Dave Winchell <winchell@missioncriticallinux.com>
 *	    Lon Hohberger <lhh at redhat.com>
 *  
 */

#ifndef CLU_LOCK_H
#define CLU_LOCK_H

#include <errno.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <clushared.h>
#include <sharedstate.h>
#include <msgsvc.h>


/* ... check errno for reason it failed */
#define LOCK_SUCCESS 0
#define LOCK_FAILURE -1

/*
 * Lock offsets (numbers). 
 * MAX_LOCKS = MAX_POWER_CONTROLLERS + MAX_SERVICES + MAX_NODES + 1
 */
#define LOCK_IDX_SERVICE	0
#define LOCK_IDX_NODE		(LOCK_IDX_SERVICE+MAX_SERVICES)
#define LOCK_IDX_CONFIG		(LOCK_IDX_NODE+MAX_NODES)

/* 
 * GLM stuff
 */
#define LOCK_LOCK		0x9000
#define LOCK_TRYLOCK		0x9001
#define LOCK_UNLOCK		0x9002
#define LOCK_ACK		0x9003
#define LOCK_NACK		0x9004
#define LOCK_NOT_MASTER		0x9005
#define LOCK_INVALID		0x9006
#define LOCK_NO_QUORUM		0x9007
#define LOCK_PID_QUERY		0x9008
#define LOCK_MASTER_QUERY	0x9009
#define LOCK_DUMP		0x900a
#define LOCK_LOST_QUORUM	0x900b

typedef struct __attribute__ ((packed)) {
	generic_msg_hdr lr_hdr;
	SharedLockBlock lr_lock;
} lock_request_t;

#define swab_lock_request_t(ptr) \
{ \
	swab_generic_msg_hdr(&(ptr)->lr_hdr); \
	swab_SharedLockBlock(&(ptr)->lr_lock); \
}

/* Expand to use quorum view. */
int clu_lock(int lockid);
int clu_try_lock(int lockid);
int clu_unlock(int lockid);
void lock_set_quorum_view(uint64_t);

#define clu_svc_lock(svcid)		clu_lock(LOCK_IDX_SERVICE + svcid)
#define clu_node_lock(nodeid)		clu_lock(LOCK_IDX_NODE + nodeid)
#define clu_config_lock()		clu_lock(LOCK_IDX_CONFIG)

#define clu_try_svc_lock(svcid)		clu_try_lock(LOCK_IDX_SERVICE + svcid)
#define clu_try_node_lock(nodeid)	clu_try_lock(LOCK_IDX_NODE + nodeid)
#define clu_try_alias_lock()		clu_try_lock(LOCK_IDX_ALIAS)

#define clu_svc_unlock(svcid)		clu_unlock(LOCK_IDX_SERVICE + svcid)
#define clu_node_unlock(nodeid)		clu_unlock(LOCK_IDX_NODE + nodeid)
#define clu_config_unlock()		clu_unlock(LOCK_IDX_CONFIG)

#define SHADOW_SUCCESS 0
#define SHADOW_NO_ACCESS 1
#define SHADOW_BAD_CHECKSUM 2

#define FAULT() {							    \
	clulog_and_print(LOG_ALERT,"Unhandled Exception at %s:%d in %s\n",  \
			 __FILE__, __LINE__, __FUNCTION__);		    \
	raise(SIGSEGV);							    \
}

#endif


