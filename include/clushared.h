/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
 */
/** @file
 * Header for ClusterStorageDriver plugin handling.
 *
 * This defines cluster storage driver plugin type, as well as global function
 * pointers which can be used as standard APIs throughout the cluster
 */
#ifndef _CLUSHARED_H
#define _CLUSHARED_H

#include <sys/types.h>
#include <stdint.h>

#define SHARED_API_VERSION (double)1.0
#define FIELD_SIZE 128


#define SHARED_HEADER_MAGIC		0x00DEBB1E

/*
 * The user data is stored with this header prepended.
 * The header ONLY contains CRC information and the length of the data.
 * The data blocks themselves contain their own respective magic numbers.
 */
typedef struct __attribute__ ((packed)) {
	uint32_t h_magic;		/* Header magic	       */
	uint32_t h_hcrc;		/* Header CRC          */
	uint32_t h_dcrc;		/* CRC32 of data       */
	uint32_t h_length;		/* Length of real data */
	uint64_t h_view;		/* View # of real data */
	uint64_t h_timestamp;		/* Timestamp           */
} SharedHeader;

#define SHAREDHEADER_INITIALIZER = {0, 0, 0, 0, 0, 0}


#define swab_SharedHeader(ptr) \
{\
	swab32((ptr)->h_magic);\
	swab32((ptr)->h_hcrc);\
	swab32((ptr)->h_dcrc);\
	swab32((ptr)->h_length);\
	swab64((ptr)->h_view);\
	swab64((ptr)->h_timestamp);\
}


/**
 * ClusterStorageDriver "object".
 */
typedef struct __ClusterStorageDriver {
	char cs_desc[FIELD_SIZE];		/**< Module description */
	char cs_author[FIELD_SIZE];		/**< Module author */

	struct {
		/**
		 * No-op function.  This isn't required, but is nice
		 * for testing purposes. Generally, this should contain
		 * something on the order of a printf().
		 */
		int (*s_null)(void);

		/** 
		 * Return the metadata of an object within the confines of
		 * our cluster namespace.
		 */
		int (*s_stat)(const char *pathname, SharedHeader *hdr);

		/**
		 * Open an object in the driver's namespace.
		 * No drivers in the pool implement this yet.
		 */
		int (*s_open)(const char *pathname, int flags);

		/**
		 * Read part of an open object (fd) in our cluster namespace.
		 * No drivers in the pool implement this yet.
		 */
		int (*s_read)(int fd, void *buf, size_t count);

		/**
		 * Write to part of an open object (fd) in our cluster
		 * namespace. No drivers in the pool implement this yet.
		 */
		int (*s_write)(int fd, const void *buf, size_t count);

		/**
		 * Open, read, close.  This must currently be used to
		 * read an entire object, so calling sh_size() is usually
		 * preferred prior to calling sh_read_atomic.
		 */
		int (*s_read_atomic)(const char *pathname, void *buf,
				     size_t count);

		/**
		 * Open, write, close.  This must currently be used to
		 * write an entire object.
		 */
		int (*s_write_atomic)(const char *pathname, const void *buf,
				      size_t count);

		/**
		 * Close a given object.  No drivers implement this.
		 */
		int (*s_close)(int fd);

		/**
		 * Returns a pointer to MODULE_DESCRIPTION.
		 * This is so a developer can use the csd_set_default()
		 * function below and still have some way of determining
		 * the 
		 */
		char *(*s_version)(void);
	} cs_fops;

	struct {
		/**
		 * This is the handle we were returned when we called
		 * dlsym().  Similarly, it is used when we unload the
		 * driver and call dlclose() from within csd_unload().
		 */
		void *p_dlhandle;

		int (*p_load_func)(struct __ClusterStorageDriver *);
		
		/**
		 * This is our initialization function.  Generally,
		 * this isn't used after initialization.
		 */
		int (*p_init_func)(struct __ClusterStorageDriver *,
				   const void *, size_t);

		/**
		 * This is our close-down function.  Here, we clean up
		 * any resources, file descriptors, memory, etc. that we
		 * allocated during the course of use.  This is called
		 * by csd_unload().
		 */
		int (*p_deinit_func)(struct __ClusterStorageDriver *);

		/**
		 * Our private data field.  Implementation dependent.
		 */
		void *p_data;

		/**
		 * The size of our private data field.  Implementation
		 * dependent.
		 */
		size_t p_datalen;
	} cs_private;

} ClusterStorageDriver;


/*
 * Handy function names.
 * A module must implement these functions as a bare minimum.  These are
 * stored in the p_init_func and p_deinit_func above.
 */
#define CLU_STORAGE_LOAD        cluster_storage_module_load
#define CLU_STORAGE_INIT        cluster_storage_module_init
#define CLU_STORAGE_UNLOAD	cluster_storage_module_unload
#define CLU_STORAGE_MODULE      cluster_storage_module_version
#define CLU_STORAGE_LOAD_SYM    "cluster_storage_module_load"
#define CLU_STORAGE_INIT_SYM    "cluster_storage_module_init"
#define CLU_STORAGE_UNLOAD_SYM  "cluster_storage_module_unload"
#define CLU_STORAGE_VERSION_SYM "cluster_storage_module_version"

/*
 * Handy macro; this should appear near the top of your module.
 */
#define IMPORT_API_VERSION() \
double cluster_storage_module_version (void) { return SHARED_API_VERSION; }

/*
 * Load the libaray with filename "libpath" and try to initialize the cluster
 * storage driver contained within.  Returns NULL on error or a pointer to a
 * ClusterStorageDriver on success.  Libraries MUST implement
 * CLU_STORAGE_INIT_SYM - which is defined above.  The simplest init function
 * does nothing.
 *
 * Note:
 *
 * priv and privlen are driver-specific.  For instance, the shared SCSI driver
 * will assume that priv is an array of two ints representing the shared file
 * descriptor set...  Generally, it is better to have the driver configure
 * itself from /etc/cluster.xml.
 */
ClusterStorageDriver *csd_load(const char *libpath);
int csd_init(ClusterStorageDriver *driver, const void *priv, size_t privlen);

/* 
 * Unload and stop the driver.  The memory in the pointer is freed, and if the
 * driver being unloaded was the default (as set by csd_set_default), the
 * sh_... global functions are set to unimplemented versions.
 */
int csd_unload(ClusterStorageDriver *driver);

/*
 * Reassign the sh_... functions to the functions contained within the given
 * ClusterStorageDriver.
 */
void csd_set_default(ClusterStorageDriver *driver);

/*
 * Clear all the default functions out.  This does NOT kill loaded csd
 * modules!
 */
void csd_reset(void);

/*
 * These are the global APIs which are used by the cluster proper.  They are
 * set by csd_set_default.  They are set as 'extern' because they are variables
 * which are initialized in one of the source files (io_driver.c)
 *
 * Note that one doesn't _have_ to use these functions.  It just makes life
 * easier when compared to calling csdp->cs_fops.s_FUNCTION(args...)
 */
extern int (*sh_null)(void);
extern int (*sh_stat)(const char *pathname, SharedHeader *hdr);
extern int (*sh_open)(const char *pathname, int flags);
extern int (*sh_read)(int fd, void *buf, size_t count);
extern int (*sh_write)(int fd, const void *buf, size_t count);
extern int (*sh_read_atomic)(const char *pathname, void *buf, size_t count);
extern int (*sh_write_atomic)(const char *pathname, const void *buf,
			      size_t count);
extern int (*sh_close)(int fd);
extern char *(*sh_version)(void);

/*
 * Globals to our description/author pointers.
 */
extern char *sh_desc;
extern char *sh_author;

/*
 * Macros to make it easy to tell what's implemented and what isn't.  These
 * should only really be checked after a call to csd_set_default(), as 
 * prior to this, they are _all_ unimplemented.
 */
#define SH_IMP(func)         (func != unimplemented_##func)
#define SH_UNIMP(func)       (func == unimplemented_##func)
#define SET_UNIMP(func)      (func = unimplemented_##func)

/*
 * Tell us if, given a pointer to a driver object and the name of a
 * function, a function is implemented within a driver object.
 */
#define CSD_IMP(ptr, func)   (ptr->cs_fops.s_##func != unimplemented_##func)
#define CSD_UNIMP(ptr, func) (ptr->cs_fops.s_##func == unimplemented_##func)

/*
 * Drivers must set their member functions to these when they do not implement
 * them.  Initially, the only ones we will care about is sh_read_atomic and
 * sh_write_atomic.  These generally shouldn't be called by user drivers.
 */
int unimplemented_sh_null(void);
int unimplemented_sh_stat(const char *, SharedHeader *);
int unimplemented_sh_open(const char *, int);
int unimplemented_sh_read(int, void *buf, size_t);
int unimplemented_sh_write(int, const void *, size_t);
int unimplemented_sh_read_atomic(const char *, void *, size_t);
int unimplemented_sh_write_atomic(const char *, const void *, size_t);
int unimplemented_sh_close(int);
char *unimplemented_sh_version(void);

#endif
