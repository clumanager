/*
  Copyright Red Hat, Inc. 2002-2003
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Header for Common Cluster Definitions.
 * $Revision: 1.19 $
 */

#ifndef	_CLUSTERDEFS_H
#define	_CLUSTERDEFS_H	1

#ifdef __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdint.h>
#include <cm_api.h>

#define CLU_CONFIG_SEPARATOR	'%'
#define SM_COMMAND_NAME		"clusvcmgrd"

/*
 * Note: these defines dictate the on-disk formats of the node and service
 * 	 descriptions.  Consequently, changing these values can result in
 *	 "rolling upgrade" type issues.
 */
#define MAX_POWER_CONTROLLERS_PER_NODE   3	// Max power controllers

#define MAX_POWER_CONTROLLERS   MAX_POWER_CONTROLLERS_PER_NODE*MAX_NODES
						// Max power controllers
#define NODE_ID_NONE		999		// ID representing "None"
#define MAX_DESC		100		// Descriptive comment
#define MIN_SERVICE		0		// First available svc number
#define MAX_SERVICES		100		// Max # of cluster services
#define MAX_SERVICE_NAMELEN	64		// Max service name length
#define SERVICE_ID_NONE	999		// service ID not set yet

/**
 * Number of locks: One per node (not used currently).  One per service.  
 * One per power controller (not used currently).
 * And... one for the cluster config database.
 */
#define MAX_LOCKS		(MAX_NODES+MAX_POWER_CONTROLLERS+MAX_SERVICES+1)

/*
 * Node States
 */
#define NODE_DOWN		0
#define NODE_UP		1

/*
 * Strings corresponding to the states to allow printing for debug purposes.
 */
extern char *nodeStateStrings[];

/*
 * Service states
 */
#define SVC_UNINITIALIZED       0
#define SVC_STARTED     	1
#define SVC_STOPPED     	2
#define SVC_DISABLED    	3
#define SVC_PENDING		4
#define SVC_FAILED		5
#define SVC_STOPPING		6
#define SVC_LAST_STATE    	6

/*
 * Service state strings
 */
#define SVC_UNINITIALIZED_STR	"uninitialized"
#define SVC_STARTED_STR	"started"
#define SVC_STOPPED_STR	"stopped"
#define SVC_DISABLED_STR	"disabled"
#define SVC_PENDING_STR	"pending"
#define SVC_FAILED_STR		"failed"
#define SVC_STOPPING_STR	"stopping"

/*
 * Service action string table
 */
extern char *serviceStateStrings[];

/**
 * Service state as represented on disk.
 *
 * This structure represents a service description.  This data structure
 * represents the in-memory service description.  (There is a separate
 * description of the on-disk format.)
 */
typedef struct __attribute__ ((packed)) {
	uint32_t	sb_id;		/**< Service ID */
	uint32_t	sb_owner;	/**< Member ID running service. */
	uint32_t	sb_last_owner;	/**< Last member to run the service. */
	uint32_t	sb_state;	/**< State of service. */
	uint32_t	sb_restarts;	/**< Number of cluster-induced 
					     restarts */
	uint16_t	sb_checks;	/**< Successful check count. */
	uint16_t	sb_false_starts; /**< Failure in first check interval */
	uint64_t	sb_transition;	/**< Last service transition time */
} ServiceBlock;

#define swab_ServiceBlock(ptr) \
{\
	swab32((ptr)->sb_id);\
	swab32((ptr)->sb_owner);\
	swab32((ptr)->sb_last_owner);\
	swab32((ptr)->sb_state);\
	swab32((ptr)->sb_restarts);\
	swab16((ptr)->sb_checks);\
	swab16((ptr)->sb_false_starts);\
	swab64((ptr)->sb_transition);\
}
	

/*
 * Port definitions
 */

#define CFG_CLUMEMBD_PORT      "clumembd%port"
#define CFG_CLUSVCMGRD_PORT    "clusvcmgrd%port"
#define CFG_CLUQUORUMD_PORT    "cluquorumd%port"
#define CFG_CLULOCKD_PORT	"clulockd%port"

#define DFLT_CLUMEMBD_PORT	"34001" // Should eq. librhcm's CM_MEMB_PORT
#define DFLT_CLUSVCMGRD_PORT	"34002"
#define DFLT_CLUQUORUMD_PORT	"34003" // Should eq. librhcm's CM_QUORUM_PORT
#define DFLT_CLULOCKD_PORT	"34004"


/*
 * Quorum Daemon default intervals
 */
#define DEFAULT_PING_INTERVAL	2	// Delay in seconds between iterations

void daemon_init(char *);

#ifdef __cplusplus
}
#endif

#endif /* clusterdefs.h */
