#include <quorum.h>

#ifndef _DISK_TIE_H
#define _DISK_TIE_H

/* from cluquorumd_disk.c */
int disk_create_quorum_thread(pthread_t * thread);
int disk_cancel_quorum_thread(void);
int disk_tiebreaker_init(int my_node_id);
int disk_tiebreaker(void);
int disk_other_status(void);
int disk_my_status(void);
int disk_other_node(void);
int disk_in_use(void);
int disk_other_lost(void);
int disk_other_gained(void);
int disk_need_shoot(void);
void disk_reset(void);
int disk_memb_up(quorum_view_t *quorum_status);
int disk_other_memb_down(quorum_view_t *quorum_status,
			 memb_mask_t exit_mask, int need_shoot);
int disk_local_memb_down(quorum_view_t *quorum_status);
void disk_check_states(quorum_view_t *quorum_status, memb_mask_t exit_mask);
/* from cluquorumd.c */
int start_vf(memb_mask_t curr_mask, quorum_view_t *qv);

#endif
