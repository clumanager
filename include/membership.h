/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
 */
/** @file
 * Header for internal membership functions (not part of the API package).
 */
#ifndef __MEMBERSHIP_H
#define __MEMBERSHIP_H

#include <clusterdefs.h>
#include <stdint.h>
#include <msgsvc.h>
#include <clulog.h>
#include <cm_api.h>

/*
 * We use this to initiate the VF protocol.  This doesn't really belong here.
 */
typedef struct __attribute__ ((packed)) _memb_msg {
	generic_msg_hdr	mm_hdr;
	uint32_t	mm_coordinator; /* Node ID of who coordinates */
	uint64_t	mm_view;
	memb_mask_t	mm_mask;
} memb_msg_t;


/*
 * semi-private membershipfunctions
 */
int memb_register(void);

/*
 * Membership message types.
 */
#define MEMB_JOIN_VIEW		0x1001
#define MEMB_VOTE		0x1002
#define MEMB_ABORT		0x1004
#define MEMB_VIEW_FORMED	0x1005
#define MEMB_REGISTER		0x1008
#define MEMB_UNREGISTER		0x1009
#define MEMB_QUERY_PARAMS	0x100A

#define MEMB_MSG_MIN		0x1001
#define MEMB_MSG_MAX		0x100A

#define MEMB_COORD_TIMEOUT	5	/* 5 seconds MAX timeout */
#define MAX_FDS			1024

#define swab_memb_msg_t(ptr, counter) \
{\
	swab_generic_msg_hdr(&(ptr)->mm_hdr);\
	swab32((ptr)->mm_coordinator);\
	swab64((ptr)->mm_view);\
	for (counter = 0; counter < MEMB_MASK_LEN; counter++) \
		swab32(((uint32_t *)(ptr)->mm_mask)[counter]);\
}

int clear_bit(uint32_t *mask, uint32_t bitidx, uint32_t masklen);
int set_bit(uint32_t *mask, uint32_t bitidx, uint32_t masklen);
int is_bit_set(uint32_t *mask, uint32_t bitidx, uint32_t masklen);

/* 
 * VF Stuff
 */
int view_form_start(int32_t my_node_id, memb_mask_t new_membership);
int view_form_end(void);
int vote_yes(int fd);
int vote_no(int fd);
int buffer_join_msg(int fd, memb_msg_t *hdr, struct timeval *timeout);
int buffer_commit(int fd);
uint64_t view_current(void);

int view_try_commit(memb_mask_t mask);

int view_timeout_check(void);
int view_abort(int fd);

int clumembd_sw_watchdog_start(struct timeval *fo_period, int mynodeid);
int clumembd_sw_watchdog_reset(void);
int clumembd_sw_watchdog_stop(void);

#define ALL (-1) /** Send to all */

/*
 * Defines we use later
 */
#define PROC_NET		"/proc/net"

/*
 * stuff in /etc/cluster.xml which gives us our parameters
 */
#define CFG_MEMB_ADAPTIVE	"clumembd%adaptive"
#define CFG_MEMB_BCAST		"clumembd%broadcast"
#define CFG_MEMB_PRIMARY_ONLY	"clumembd%broadcast_primary_only"
#define CFG_MEMB_INTERVAL	"clumembd%interval"
#define CFG_MEMB_LOGLEVEL	"clumembd%logLevel"
#define CFG_MEMB_MCAST		"clumembd%multicast"
#define CFG_MEMB_MCADDR		"clumembd%multicast_ipaddress"
#define CFG_MEMB_PORT		"clumembd%port"
#define CFG_MEMB_RTP		"clumembd%rtp"
#define CFG_MEMB_SMOOTH		"clumembd%smooth"
#define CFG_MEMB_STRICT		"clumembd%strict"
#define CFG_MEMB_THREAD		"clumembd%thread"
#define CFG_MEMB_TKO_COUNT	"clumembd%tko_count"

/*
 * Defaults for our behavior modifying parameters
 */
#define DFLT_MCADDR		"225.0.0.11" /* "0" = disabled */
#define DFLT_ADAPTIVE		1	/* Per-node flexibility (ON) */
#define DFLT_BCAST		0	/* Broadcast */
#define DFLT_PRIMARY_ONLY	0	/* Broadcast on primary NIC + lo only */
#define DFLT_INTERVAL		{0,750000}	/* 0.75 seconds */
#define DFLT_MCAST		1	/* Multicast */
#define DFLT_PORT		1228	/* Arbitrarily chosen */
#define DFLT_RTP		-1	/* RT Priority, 0..100 (-1 = OFF) */
#define DFLT_SMOOTH		0	/* Time-based smoothing (OFF) */
#define DFLT_STRICT		1	/* Strict voting (ON) */
#define DFLT_THREAD		1	/* Use transmit thread (ON) */
#define DFLT_TKO_COUNT		20	/* 20*0.75s = 15.0s failure detect */

#endif
