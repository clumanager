/** @file
 * Header for msgsimple.c.
 */
/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/*
 * Author: Jeff Moyer <moyer@missioncriticallinux.com>
 * Modified: Lon Hohberger <lhh at redhat.com>
 *   - Removed unused types in RHCM-1.1.x
 *   - Added byte-swapping/etc. to network-message types.
 *   - Changed authentication mechanism.
 */

#ifndef __MSG_H
#define __MSG_H
#ifdef __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unistd.h>
#include <clusterdefs.h>
#include <time.h>
#include <sys/types.h>
#include <platform.h>
#include <cm_api.h>

typedef int msg_handle_t;
typedef unsigned int msg_addr_t;

#define MSGSVC_VERSION           1   /* Consistency check */
#define MAX_MSGBACKLOG          15   /* Maximum backlog of connections.   */
#define MSGSVC_CONNECT_TIMEOUT  {5,0} /* Connect timeout */
#define MSGSVC_MAX_MSGSIZE	(1<<20)	/* 1MB */

#define MAX_DAEMON_NAMELEN 255  /* Used to convert procid to daemon name */
/*
 * Method for identifying a process for the message subsystem.
 * NOTE: these identifiers correspond to the elements in the proc_id_array.
 * If you change the order, or add an element, be sure to update this array
 * in msg.c.
 */
#define PROCID_CLUMEMBD   0
#define PROCID_CLUSVCMGRD 1
#define PROCID_CLUQUORUMD 2
#define PROCID_CLULOCKD   3

#define MAX_PROCID        3

#define PROCID_NONE       (MAX_PROCID+1)

/*
 *  Service specific messages.
 */

/*
 * Svcmgrd
 */
#define SVC_ACTION_REQUEST	2  /* Service action request to svcmgrd */					   /* Can't collide with DISK_NODESTATECHANGE */

/*
 * Data structures
 */
#define GENERIC_HDR_MAGIC CLUSTER_MAGIC /* match librhcm!  */

typedef struct __attribute__ ((packed)) {
    uint32_t gh_magic; 
    uint32_t gh_length;
    uint32_t gh_command;
    uint32_t gh_arg1;
    uint32_t gh_arg2;
} generic_msg_hdr;

#define swab_generic_msg_hdr(ptr)\
{\
	swab32((ptr)->gh_magic);\
	swab32((ptr)->gh_length);\
	swab32((ptr)->gh_command);\
	swab32((ptr)->gh_arg1);\
	swab32((ptr)->gh_arg2);\
}


typedef struct __attribute__ ((packed)) {
    generic_msg_hdr	sm_hdr;
    struct {
        uint32_t	d_action;
        uint32_t	d_svcID;
        uint32_t	d_svcState;
        uint32_t	d_svcOwner;
        int32_t		d_ret;
    } sm_data;
} SmMessageSt;

#define swab_SmMessageSt(ptr) \
{\
	swab_generic_msg_hdr(&((ptr)->sm_hdr));\
	swab32((ptr)->sm_data.d_action);\
	swab32((ptr)->sm_data.d_svcID);\
	swab32((ptr)->sm_data.d_svcState);\
	swab32((ptr)->sm_data.d_svcOwner);\
	swab32((ptr)->sm_data.d_ret);\
}

/*
 * API For manipulating file descriptor based communications channels.
 */
/*
 * int msg_svc_init(int)
 *
 * DESCRIPTION
 *   Clear out any current connections and (re)initialize the message service.
 *   This is generally not called by user programs, as calling any of the other
 *   APIs calls this if necessary.  Also, (re)read the cluster's disk session
 *   ID.
 *
 * ARGUMENTS
 *   Reload.  Set to '1' if you are reconfiguring an on-line node's view
 *   of the message subsystem.  This will prevent the removal of open file
 *   descriptors from the fdlist.
 *
 * RETURN VALUES
 *   -1 on failure; 0 on success.
 */
int msg_svc_init(int);


/*
 * msg_handle_t msg_open(msg_addr_t dest, int nodeid)
 *
 * DESCRIPTION
 *   Open a communication path to the daemon associated with dest on
 *   node 'nodeid.'
 *
 * ARGUMENTS
 *   dest  Address to which the connection is made.  Currently, one of:
 *
 *         PROCID_CLUHBD
 *         PROCID_CLUSVCMGRD
 *         PROCID_CLUQUORUMD
 *         PROCID_CLUPOWERD
 *
 *   nodeid  Node where the daemon you wish to contact lives.  This value
 *           corresponds to that returned from get_clu_cfg(), in the lid field.
 *
 * RETURN VALUES
 *   Upon success, a valid file descriptor is returned.  For failure, -1 is
 *   returned.
 */
msg_handle_t msg_open(msg_addr_t dest, int nodeid);
msg_handle_t msg_open_timeout(msg_addr_t dest, int nodeid,
			      struct timeval *timeout);
msg_handle_t msg_open_local(msg_addr_t dest, int tout);

/*
 * void msg_close(msg_handle_t handle)
 *
 * DESCRIPTION
 *   Close an open msg_handle_t.  It is _required_ that you call this, as
 *   the msg svc keeps internal tables of file descriptors and associated
 *   states.
 *
 * ARGUMENTS
 *   handle  Handle returned by msg_open or msg_accept which you will no
 *           longer use.
 *
 * RETURN VALUES
 *   none
 */
void msg_close(msg_handle_t handle);

/*
 * msg_handle_t msg_listen(msg_addr_t my_proc_id)
 *
 * DESCRIPTION
 *   Create a msg_handle_t which is ready to accept incoming connections.
 *
 * ARGUMENTS
 *   my_proc_id  Address on which to listen.  PROCID_XXX corresponds to a
 *               TCP/IP port on which this daemon will listen.
 *
 * RETURN VALUES
 *   On success, a valid file descriptor is returned.  On error, -1 is 
 *   returned.
 */
msg_handle_t msg_listen(msg_addr_t my_proc_id);
msg_handle_t msg_listen_local(msg_addr_t my_proc_id);

/*
 * msg_handle_t msg_accept(msg_handle_t handle)
 *
 * DESCRIPTION
 *   Call accept on a file descriptor returned from msg_listen.
 *
 * ARGUMENTS
 *   handle  Valid handle returned from msg_listen.
 *
 * RETURN VALUES
 *   On success, a valid file descriptor is returned which describes the new
 *   communication channel.  On error, -1 is returned.
 */
msg_handle_t msg_accept(msg_handle_t handle);
msg_handle_t msg_accept_local(msg_handle_t handle);

/*
 * msg_handle_t msg_accept_timeout(msg_handle_t handle, int timeout)
 *
 * DESCRIPTION
 *   Call accept on a file descriptor.  If no connections are pending within
 *   timeout seconds, the function returns.
 *
 * ARGUMENTS
 *   handle   valid handle returned by msg_listen.
 *   timeout  time in seconds to wait for a connection.
 *
 * RETURN VALUES
 *   If a connection is pending, a valid file descriptor for that connection
 *   is returned.  If no connections are pending, 0 is returned.  On error,
 *   -1 is returned.
 */
msg_handle_t msg_accept_timeout(msg_handle_t handle, int timeout);

/*
 * int msg_send(msg_handle_t handle, void *buf, ssize_t count)
 *
 * DESCRIPTION
 *   Send a message over the communications channel described by handle.
 *
 * ARGUMENTS
 *   handle  Valid handle returned from msg_open or msg_accept[_timeout].
 *   buf     Pointer to data to be sent.
 *   count   Number of bytes to send.
 *
 * RETURN VALUES
 *   On success, the number of bytes successfully written is returned.  On
 *   error, -1 is returned, and errno is set according to write(2).
 */
int msg_send(msg_handle_t handle, void *buf, ssize_t count);
int msg_send_simple(int handle, int cmd, int arg1, int arg2);

/*
 * int __msg_send(msg_handle_t handle, void *buf, ssize_t count)
 *
 * DESCRIPTION
 *   Send a message over the communications channel described by handle.  This
 *   call differs from msg_send in that it does no sanity checking of internal
 *   file descriptor tables.  Use this call if you hand craft a connection,
 *   and would like the message service to take care of the communications.
 *
 * ARGUMENTS
 *   handle  Valid file descriptor.
 *   buf     Pointer to data to be sent.
 *   count   Number of bytes to send.
 *
 * RETURN VALUES
 *   On success, the number of bytes successfully written is returned.  On
 *   error, -1 is returned, and errno is set according to write(2).
 */
int __msg_send(msg_handle_t handle, void *buf, ssize_t count);

/*
 * ssize_t msg_receive(msg_handle_t handle, void *buf, ssize_t count)
 *
 * DESCRIPTION
 *   Receive a message off of the communications channel described by handle.
 *
 * ARGUMENTS
 *   handle  Valid handle returned by a call to msg_accept or msg_open.
 *   buf     Buffer into which the received data is copied.
 *   count   Number of bytes to read from msg_handle_t.
 *
 * RETURN VALUES
 *   On success, the number of bytes successfully read is returned.  On error, 
 *   -1 is returned, and errno is set according to read(2).
 */
ssize_t msg_receive(msg_handle_t handle, void *buf, ssize_t count);
int msg_receive_simple(int handle, generic_msg_hdr **buf, int timeout);

/*
 * ssize_t __msg_receive(msg_handle_t handle, void *buf, ssize_t count)
 *
 * DESCRIPTION
 *   Receive a message off of the communications channel described by handle.
 *   This call differs from msg_receive in that it does no sanity checking
 *   of internal file descriptor tables.  Use this call if you hand craft a 
 *   connection, and would like the message service to take care of
 *   communications.
 *
 * ARGUMENTS
 *   handle  Valid file descriptor.
 *   buf     Buffer into which the received data is copied.
 *   count   Number of bytes to read from file descriptor.
 *   tv      If NULL, nothing, otherwise, specifies max timeout.
 *
 * RETURN VALUES
 *   On success, the number of bytes successfully read is returned.  On error, 
 *   -1 is returned, and errno is set according to read(2).
 */
ssize_t __msg_receive(msg_handle_t handle, void *buf, ssize_t count,
		      struct timeval *tv);

/*
 * ssize_t msg_receive_timeout(msg_handle_t handle, void *buf,
 *			   ssize_t count, unsigned int timeout);
 *
 * DESCRIPTION
 *   Receive a message on a given communications channel.  If no message
 *   is available within timeout seconds, the call returns.
 *
 * ARGUMENTS
 *   handle  Valid file descriptor returned from a call to msg_open or
 *           msg_accept[_timeout].
 *   buf     Buffer into which the received data is copied.
 *   count   Number of bytes to read from handle.
 *   timeout Time in seconds to wait for a message to arrive.
 *
 * RETURN VALUES
 *   If there is data to be read within timeout seconds, the number
 *   of bytes read is returned.  If the timeout expires before data
 *   is ready, 0 is returned.  On error, -1 is returned and errno is
 *   set according to one of select(2) or read(2).
 */
ssize_t msg_receive_timeout(msg_handle_t handle, void *buf,
			   ssize_t count, unsigned int timeout);

/*
 * ssize_t msg_peek(msg_handle_t handle, void *buf, ssize_t count)
 *
 * DESCRIPTION
 *   Check to see if there is data to read from the socket, but do not
 *   retrieve the data.  For a more detailed explanation, see man 2 recv and
 *   search for MSG_PEEK.
 *
 * ARGUMENTS
 *   handle  Handle returned by msg_open or msg_accept.
 *   buf     Buffer into which the available data is copied
 *   count   Number of bytes to read.
 *
 * RETURN VALUES
 *   On success, the number of bytes available for reading is returned, and
 *   buf is filled with that number of bytes from the connection.  0 is 
 *   returned if there is nothing available for reading.  On error, -1 is
 *   returned.
 */
ssize_t msg_peek(msg_handle_t handle, void *buf, ssize_t count);

/*
 * ssize_t __msg_peek(msg_handle_t handle, void *buf, ssize_t count)
 *
 * DESCRIPTION
 *   Check to see if there is data to read from the socket, but do not
 *   retrieve the data.  For a more detailed explanation, see man 2 recv and
 *   search for MSG_PEEK.  This call differs from msg_peek in that it does
 *   no sanity checks on the input parameters.
 *
 * ARGUMENTS
 *   handle  Handle returned by msg_open or msg_accept.
 *   buf     Buffer into which the available data is copied
 *   count   Number of bytes to read.
 *
 * RETURN VALUES
 *   On success, the number of bytes available for reading is returned, and
 *   buf is filled with that number of bytes from the connection.  0 is 
 *   returned if there is nothing available for reading.  On error, -1 is
 *   returned.
 */
ssize_t __msg_peek(msg_handle_t handle, void *buf, ssize_t count);


#ifdef __cplusplus
}
#endif
#endif /* __MSG_H */
