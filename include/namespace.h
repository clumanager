/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
 */
/** @file
 * Cluster shared state namespace
 */
/* host file system cluster namespace root */
#define NAMESPACE_ROOT "/var/cluster"

#define NS_D_CLUSTER   "/cluster"
#define NS_D_SERVICE   "/service"
#define NS_D_PARTITION "/partition"
#define NS_D_LOCK      "/lock"

/* Name of "status" file */
#define NS_F_STATUS    "/status"

/* cluster "subdirectory" */
#define NS_F_CONFIG    "config.xml"
#define NS_F_CLUHDR    "header"

/*
 * /cluster/header		Config file header. Stores checksum, etc.
 * /cluster/config.xml		plain text (XML)
 * /partition/0/status		Partition 0 status
 * /partition/1/status		Partition 1 status
 * /service/0/status
 * /service/1/status
 * ...
 * /service/99/status
 * /lock/0/status...
 */
