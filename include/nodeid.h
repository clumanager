/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
 */
/** @file
 * Header for nodeid.c
 */
#ifndef __NODEID_H
#define __NODEID_H

#include <xmlconfig.h>
#include <sys/types.h>
#include <net/if.h>

/*
 * "Cached" and "Uncached" are only to be used during initialization before
 * shared storage can be trusted.
 */

/*
 * Local Node ID
 */
int getLocalNodeIDCached(void);
int getLocalNodeIDUncached(void);

/*
 * Local Node ID and ethernet interface corresponding to hostname/IP
 */
int getLocalNodeInfo(struct ifreq *if_r);

/*
 * Remote Node ID
 */
int getNodeIDCached(const char *name);
int getNodeIDUncached(const char *nodename);

int cluGetLocalNodeId(void);

#endif
