/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
 */
/** @file
 * Header for Internal Quorum stuff (not part of user API).
 */
#ifndef __QUORUM_H
#define __QUORUM_H

#include <clusterdefs.h>
#include <stdint.h>
#include <membership.h>

#define QUORUM_REGISTER		0x2002
#define	QUORUM_UNREGISTER	0x2003
#define QUORUM_EXIT		0x2004
#define QUORUM_CLEAN_EXIT	0x2005
#define QUORUM_PSWITCH_BAD	0x2006
#define QUORUM_PSWITCH_GOOD	0x2007
#define QUORUM_FORCE		0x2008

#define QUORUM_MSG_MIN		0x2001
#define QUORUM_MSG_MAX		0x2008

typedef struct __attribute__ ((packed)) _quorum_view {
	uint8_t qv_status;		/** Majority? */
	memb_mask_t qv_mask;		/** Mask of online nodes */
	memb_mask_t qv_stonith_mask;	/** Mask of nodes needing to be shot */
	memb_mask_t qv_panic_mask;	/** Mask of nodes in panic state:
					    'Panic' state -> failed to shoot */
} quorum_view_t;

#define swab_quorum_view_t(ptr, c) \
{\
	for (c=0; c < MEMB_MASK_LEN; c++)\
		swab32((ptr)->qv_mask[c]);\
	for (c=0; c < MEMB_MASK_LEN; c++)\
		swab32((ptr)->qv_stonith_mask[c]);\
	for (c=0; c < MEMB_MASK_LEN; c++)\
		swab32((ptr)->qv_panic_mask[c]);\
}

int quorum_register(void);

#endif
