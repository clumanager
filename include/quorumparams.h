/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Definitions related to the shared state disk subsystem.
 *
 * Author: Tim Burke (tburke at redhat.com)
 * $Revision: 1.7 $
 *
 */
#ifndef	_QUORUMPARAMS_H
#define	_QUORUMPARAMS_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <time.h>
#include <sys/param.h>
#include <clu_lock.h>

/* 
 * The following defines describe the set of configurable parameters which
 * can be specified in the cluster configuration file.
 *
 * "cluquorumd%sameTimeNetup" & "cluquorumd%sameTimeNetdown"
 * The main mechanism used to detect if a partner node has died is to
 * read in the timestamp to see if it has changed.  Its probably not
 * prudent to assume that a partner is dead merely after missing only one
 * timestamp update.  Perhaps they hit an IO activity spike or something like
 * that.  The "cluquorumd%sameTimeNetdown" parameter defines the number of 
 * consecutive polls that the timestamp is allowed to remain unchanging 
 * before initiating a shootdown of the failed partner cluster node.  
 * If the heartbeat daemon believes the other node to
 * be up, we will give it longer to update its disk timestamp based on the
 * "cluquorumd%sameTimeNetup" parameter.  This allows
 * us to have quicker failover times for the case where a node is truly down;
 * while allowing us to not prematurely suspect a node is down due to an
 * IO activity spike. Therefore the value of the "cluquorumd%sameTimeNetup"
 * parameter should be greater than the "cluquorumd%sameTimeNetdown" parameter.
 * These 2 parameters are optional.
 *                                      
 * "cluquorumd%pingInterval"
 * This parameter specifies the number of seconds to delay between
 * each iteration of the cluquorumdd process.  It controls how often the
 * interval timer is updated on disk, as well as how often the
 * partner node's interval timer is read in.
 * This parameter is optional.
 *                                                          
 * "cluquorumd%logLevel"
 * Controls the level of diagnostic messages.  
 */                      

#define CFG_QUORUM_PING_INTERVAL	"cluquorumd%pinginterval"
#define CFG_QUORUM_POWER_CHECK_INTERVAL	"cluquorumd%powercheckinterval"
#define CFG_QUORUM_LOGLEVEL		"cluquorumd%loglevel"
#define CFG_QUORUM_RTP			"cluquorumd%rtp"
#define CFG_QUORUM_DISK_QUORUM		"cluquorumd%disk_quorum"
#define CFG_QUORUM_ALLOW_SOFT		"cluquorumd%allow_soft"
#define CFG_QUORUM_IGNORE_GULM		"cluquorumd%ignore_gulm"

#define SHAREDSTATE_DRIVER		"sharedstate%driver"
#define KEY_QUORUM 0x12345678		/** Private quorum key ID */
#define MAX_MEMB_RETRIES 10		/** Number of retries to connect to
					  membership daemon before giving
					  up. */
#define SIMPLE_OPERATION		"cluster%simple_operation"


#endif /* diskstate.h */
