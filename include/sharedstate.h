/** @file
 * Header file for shared state/status structures.
 *
 * XXX Needs Doxygenification.
 */
/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/*
 * Authors: Tim Burke     (tburke at redhat.com)
 *	    Lon Hohberger (lhh at redhat.com)
 */
#ifndef	_SHAREDSTATE_H
#define	_SHAREDSTATE_H	1


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <clusterdefs.h>
#include <clushared.h>
#include <crc32.h>
#include <stdint.h>
#include <sys/param.h>
#include <platform.h>
#include <sys/types.h>

/*
 * Definitions of on disk formats for data structures representing
 * shared cluster state.
 */
#define SPACE_NET_BLOCK_DATA 450
#define SHARED_STATE_MAGIC_NUMBER	0x39119FCD	// Arbitrarily chosen
#define SHARED_STATE_LATEST_VERSION	2		// Header version #
#define STATUS_BLOCK_MAGIC_NUMBER	0xF1840DCE	// Arbitrarily chosen
#define STATUS_BLOCK_LATEST_VERSION	2		// Header version #
#define SERVICE_BLOCK_MAGIC_NUMBER	0x19FAD022	// Arbitrarily chosen
#define SERVICE_BLOCK_LATEST_VERSION	2		// Header version #
#define LOCK_BLOCK_MAGIC_NUMBER		0xFEEDFACE	// Arbitrarily chosen
#define LOCK_BLOCK_LATEST_VERSION	2		// Header version #
#define CONFIG_DB_MAGIC_NUMBER		0x0122345F	// Arbitrarily chosen
#define CONFIG_DB_LATEST_VERSION	2		// Header version #


/*
 * Shared state disk header.  Describes cluster global information.
 */
typedef struct __attribute__ ((packed)) {
	uint32_t	ss_magic;
	uint32_t	ss_align;	   // 64-bit-ism: alignment fixer.
	uint64_t	ss_timestamp;	   // time of last update
	char 		ss_updateHost[128];// Hostname who put this here...
} SharedStateHeader;

#define swab_SharedStateHeader(ptr) \
{\
	swab32((ptr)->ss_magic); \
	swab32((ptr)->ss_align); \
	swab64((ptr)->ss_timestamp); \
}


/*
 * Under normal operating circumstances, this status structure is 
 * write-only by one partition and read-only to the other partition.
 *
 * I wanted to try to keep the size of this to be under 512 bytes in
 * length in order to enable it to be written atomicaly to the disk.
 * Initially I had the services as an array of character (strings) but
 * that ended up taking up too much space.
 */
typedef struct __attribute__ ((packed)) {
	uint32_t	ps_magic;
	uint32_t	ps_state;		// running or stopped
	uint32_t	ps_updateNodeNum;	// ID of node doing last update
	uint32_t	ps_substate;		// more details on state, 
	uint64_t	ps_timestamp;		// time of last update
	uint64_t	ps_incarnationNumber;	// cluster startup time
						// eg panic-ing
	char		ps_nodename[MAXHOSTNAMELEN];
} PartStatusBlock;

#define swab_PartStatusBlock(ptr) \
{\
	swab32((ptr)->ps_magic);\
	swab32((ptr)->ps_state);\
	swab32((ptr)->ps_substate);\
	swab32((ptr)->ps_updateNodeNum);\
	swab64((ptr)->ps_timestamp);\
	swab64((ptr)->ps_incarnationNumber);\
}


/* DiskLockBlock defined in clu_lock.h */

/*
 * DiskServiceBlock
 *
 * This structure is the on-disk representation of a service.
 * Contained within it is the memory resident version of a service.
 */
typedef struct __attribute__ ((packed)) {
	uint32_t	sb_magic;
	uint32_t	sb_align;	// 64-bit-ism fixer.
	ServiceBlock	sb_svcblk;	// defined in clusterdefs.h
} SharedServiceBlock;

#define swab_SharedServiceBlock(ptr) \
{\
	swab32((ptr)->sb_magic);\
	swab32((ptr)->sb_align);\
	swab_ServiceBlock(&((ptr)->sb_svcblk));\
}


/*
 * DiskNetBlock
 *
 * Starts with the usual boilerplate header, followed by the actual data
 * payload itself.  
 * Non-ideal in that the data portion isn't block aligned.
 */
typedef struct __attribute__ ((packed)) {
	uint32_t	nb_magic;		
	uint32_t	nb_state;		// valid or uninitialized
	char		nb_data[SPACE_NET_BLOCK_DATA];
} SharedNetBlock;

#define swab_SharedNetBlock(ptr) \
{\
	swab32((ptr)->nb_magic);\
	swab32((ptr)->nb_state);\
}


/*
 * SharedLockBlock
 *
 * Lock block.
 */
#define MAX_DAEMON_NAME_LEN 128
typedef struct __attribute__ ((packed)) {
	uint32_t lb_magic;
	uint32_t lb_lockid;
	uint32_t lb_state;
	uint32_t lb_incarnation;
	struct {
		uint32_t h_node;
		uint32_t h_pid;
		uint32_t h_crc;		/* CRC of holder program */
		uint32_t h_master;	/* Lock master ID who wrote last state */
		uint64_t h_pc;		/* For debugging purposes */
		uint8_t  h_exe[MAX_DAEMON_NAME_LEN];
	} lb_holder;
} SharedLockBlock;

#define swab_SharedLockBlock(ptr) \
{\
	swab32((ptr)->lb_magic);\
	swab32((ptr)->lb_lockid);\
	swab32((ptr)->lb_state);\
	swab32((ptr)->lb_incarnation);\
	swab32((ptr)->lb_holder.h_node);\
	swab32((ptr)->lb_holder.h_pid);\
	swab32((ptr)->lb_holder.h_crc);\
	swab32((ptr)->lb_holder.h_master);\
	swab64((ptr)->lb_holder.h_pc);\
}


void header_encode(SharedHeader *hdr);
void header_decode(SharedHeader *hdr);
int  header_generate(SharedHeader *, const char *, size_t);
int  header_verify(SharedHeader *, const char *, size_t);

#define PRINT_SHARED_HEADER(hdr) \
{ \
	fprintf(stderr,"%p->h_magic  -> 0x%08x\n", hdr, (hdr)->h_magic); \
	fprintf(stderr,"%p->h_hcrc   -> 0x%08x\n", hdr, (hdr)->h_hcrc); \
	fprintf(stderr,"%p->h_dcrc   -> 0x%08x\n", hdr, (hdr)->h_dcrc); \
	fprintf(stderr,"%p->h_length -> 0x%08x\n", hdr, (hdr)->h_length); \
}

int shared_storage_init(void);
int shared_storage_deinit(void);


void do_reboot(int how, const char *func, const char *file, int line);
#define REBOOT(arg) do_reboot(arg, __FUNCTION__, __FILE__, __LINE__)


#endif /* sharedstate.h */
