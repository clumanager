/*
  Copyright Red Hat, Inc. 2002-2003
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Function prototypes for routines exported by the stonith library.
 *
 * Authors: Tim Burke <tburke at redhat.com>
 *          Lon Hohberger <lhh at redhat.com>
 * $Revision: 1.6 $
 */
#ifndef __STONITHAPIS_H
#define __STONITHAPIS_H 
#ifdef __cplusplus
extern "C" {
#endif

/* power_stonith.c */
extern int clu_stonith_init(void);		/* Initialize */
extern void clu_stonith_deinit(void);		/* Free memory */

extern int clu_stonith_fence_cycle(int node);	/* Cycle fenced status */
extern int clu_stonith_fence(int node);		/* Fence a node */
extern int clu_stonith_unfence(int node);	/* Unfence a node */

extern int clu_stonith_check(void);		/* Check ALL nodes' switches */
extern int clu_stonith_status(int node);	/* Check node's switch(es) */
extern int clu_stonith_count(int node);		/* Count # of devices */

extern void clu_stonith_sighup_handler(int signum);
extern int clu_stonith_gulm(int nodeid);

/* stonith_config.c */
int stonith_config_init(char *stonithdir, char *conf_file);
int stonith_config_load(char *optfile, char *type, char *IPaddr, 
		char *login, char *passwd, char *portname);

#ifdef __cplusplus
}
#endif

#endif // __STONITHAPIS_H 
