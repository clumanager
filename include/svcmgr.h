/** @file
 * Service Manager String Definitions for svc_db.c
 */
/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/*
 *
 * Author: Gregory P. Myrdal <Myrdal@MissionCriticalLinux.Com>
 *         Xudong Tang       <tang@MissionCriticalLinux.Com>
 *         Brian Stevens     <bstevens at redhat.com>
 *
 * svcmgr.h
 * $Revision: 1.13 $
 */
 
#ifdef __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdint.h>
#include <membership.h>

/*
 * Function return status
 */
#define SUCCESS			0
#define FAIL			-1
#define WARNING                 -2
#define ABORT			-3
#define NOT_FOUND		-4
#define YES			1
#define NO			0

/*
 * Fail-over domain states
 */
#define FOD_ILLEGAL		0
#define FOD_GOOD		1
#define FOD_BETTER		2
#define FOD_BEST		3


#define LOG_DEFAULT             LOG_NOTICE
#define MAX_LINELEN             2048
#define MAX_ERROR_BUFLEN        256

/*
 * Cluster database
 */
#define DB_INIT_SIZE		102400	// size of cluster database
#define MIN_TOKEN_ID		0	// first token ID
#define MAX_TOKEN_LEN		2048	// max length of a token string
#define MAX_INTERFACE           50      // lo,eth0,etc. The # of interfaces   
#define MAX_LINE_LEN            100

/*
 * Service actions
 */
#define SVC_NONE		0
#define SVC_ADD			1
#define SVC_REMOVE		2
#define SVC_START		3
#define SVC_STOP		4
#define SVC_CHECK		5
#define SVC_DISABLE		6
#define SVC_RELOCATE		7
#define SVC_STATUS_INQUIRY	8
#define SVC_FAILBACK            9
#define SVC_START_PENDING	10
#define SVC_START_RELOCATE	11
#define SVC_CONFIG_UPDATE	12
#define SVC_RESTART		13
#define SVC_LOCK		14	/* user->svcmgr message */
#define SVC_UNLOCK		15	/* user->svcmgr message */
#define SVC_LOCKALL		16	/* svcmgr->svcmgr message */
#define SVC_UNLOCKALL		17	/* svcmgr->svcmgr message */
#define SVC_QUERY_LOCK		18	/* User->svcmgr message */

/*
 * Service action strings
 */
#define SVC_NONE_STR		"no action"
#define SVC_ADD_STR		"add"
#define SVC_REMOVE_STR		"remove"
#define SVC_START_STR		"start"
#define SVC_STOP_STR		"stop"
#define SVC_CHECK_STR		"status"
#define SVC_DISABLE_STR		"disable"
#define SVC_RELOCATE_STR	"relocate"
#define SVC_STATUS_INQUIRY_STR	"inquiry"
#define SVC_FAILBACK_STR	"fail-back"
#define SVC_START_PENDING_STR   "pending start"
#define SVC_START_RELOCATE_STR  "remote start"
#define SVC_CONFIG_UPDATE_STR	"configuration update"
#define SVC_RESTART_STR		"restart"

/*
 * Service database entry strings.  These are the strings that are entered
 * in the service database to describe the service.  Some of these entries
 * are labels where the real values are in sub-sections.  Refer to the
 * service database description file for more information.
 */
#define SVC_SERVICES_LIST_STR		"services"
#define SVC_SERVICE_STR			"service"
#define SVC_NAME_STR 			"name"
#define SVC_PREFERRED_NODE_STR		"preferrednode"
#define SVC_RELOCATE_ON_BOOT_STR	"relocateonpreferrednodeboot"
#define SVC_USER_SCRIPT_STR		"userscript"

#define SVC_NETWORK_LIST_STR		"networks"
#define SVC_NETWORK_STR			"network"
#define SVC_IP_ADDR_STR			"ipaddress"
#define SVC_NETMASK_STR			"netmask"
#define SVC_BROADCAST_STR		"broadcast"

#define SVC_DEVICE_LIST_STR		"devices"
#define SVC_DEVICE_STR			"device"
#define SVC_DEVICE_NAME_STR		"name"
#define SVC_MOUNT_STR			"mount"
#define SVC_MOUNT_NAME_STR		"name"
#define SVC_MOUNT_OPTIONS_STR		"options"
#define SVC_MOUNT_FSTYPE_STR            "fstype"
#define SVC_FORCE_UNMOUNT_STR		"forceunmount"
#define SVC_NFS_STR			"nfs"
#define SVC_NFS_SVR_DIR_STR		"serverdir"
#define SVC_NFS_SVR_DEV_STR		"serverdev"
#define SVC_EXPORT_LIST_STR		"nfsexports"
#define SVC_EXPORT_STR			"nfsexport"
#define SVC_EXPORT_NAME_STR		"name"
#define SVC_EXPORT_CLIENT_STR		"client"
#define SVC_EXPORT_CLIENT_NAME_STR	"name"
#define SVC_EXPORT_CLIENT_OPTIONS_STR	"options" 
#define SVC_DEVICE_OWNER_STR		"owner"
#define SVC_DEVICE_GROUP_STR		"group"
#define SVC_DEVICE_MODE_STR		"mode"
#define SVC_CHECK_INTERVAL_STR		"checkinterval"
#define SVC_SHARE_NAME_STR		"sharename"

/*
 * Node database entry strings.
 */
#define NODE_LIST_STR			"members"
#define NODE_STR			"member"
#define NODE_ID_STR			"id"
#define NODE_NAME_STR			"name"

/*
 * Service Manager database entry strings
 */
#define SVCMGR_STR			"clusvcmgrd"
#define SVCMGR_LOGLEVEL_STR		"loglevel"

/*
 * Possible values for entries in the service section of the database
 */
#define YES_STR				"yes"
#define NO_STR				"no"

/*
 * Service Manager prototypes
 */

/* svc_db.c */
int getDatabaseToken(char *token, char **value);
int setDatabaseToken(char *token, char *value);
int findNextFreeEntry(char *tokenPath, int *freeEntry);

int getSvcNameTokenStr(int svcID, char *tokenStr, int tokenlen);
int getSvcScriptTokenStr(int svcID, char *tokenStr, int tokenlen);
int getSvcRelocateOnPreferredNodeBootTokenStr(int svcID, char *tokenStr, int tokenlen);
int getSvcPreferredNodeNameTokenStr(int svcID, char *tokenStr, int tokenlen);
int getSvcIPaddressTokenStr(int svcID, int N, char *tokenStr, int tokenlen);
int getSvcNetmaskTokenStr(int svcID, int N, char *tokenStr, int tokenlen);
int getSvcBroadcastTokenStr(int svcID, int N, char *tokenStr, int tokenlen);
int getSvcDeviceTokenStr(int svcID, int N, char *tokenStr, int tokenlen);
int getSvcMountPointTokenStr(int svcID, int N, char *tokenStr, int tokenlen);
int getSvcMountOptionsTokenStr(int svcID, int N, char *tokenStr, int tokenlen);
int getSvcMountFstypeTokenStr(int svcID, int N, char *tokenStr, int tokenlen);
int getSvcForceUnmountTokenStr(int svcID, int N, char *tokenStr, int tokenlen);
int getSvcDeviceOwnerTokenStr(int svcID, int N, char *tokenStr, int tokenlen);
int getSvcDeviceGroupTokenStr(int svcID, int N, char *tokenStr, int tokenlen);
int getSvcDeviceModeTokenStr(int svcID, int N, char *tokenStr, int tokenlen);
int getSvcCheckIntervalTokenStr(int svcID, char *tokenStr, int tokenlen);
int getSvcExportDirTokenStr(int svcID, int deviceID, int exportID, char *tokenStr, int tokenlen);
int getSvcExportClientTokenStr(int svcID, int deviceID, int exportID, int clientID, char *tokenStr, int tokenlen);
int getSvcExportOptionsTokenStr(int svcID, int deviceID, int exportID, int clientID, char *tokenStr, int tokenlen);
int getSvcShareNameTokenStr(int svcID, int N, char *tokenStr, int tokenlen);

int getSvcID(char *svcName, int *svcID);
int getSvcName(int svcID, char **svcName);
int getSvcMaxFalseStarts(int svcID, char **ret);
int getSvcMaxRestarts(int svcID, char **ret);
int getSvcScript(int svcID, char **svcScript);
int getSvcRelocateOnPreferredNodeBoot(int svcID, char **relocate);
int getSvcPreferredNodeName(int svcID, char **nodeName) ;
int getSvcPreferredNodeID(int svcID, int *nodeID);
int getSvcIPaddress(int svcID, int N, char **IPaddr);
int getSvcNetmask(int svcID, int N, char **netmask);
int getSvcBroadcast(int svcID, int N, char **broadcast);
int getSvcDevice(int svcID, int N, char **device);
int getSvcMountPoint(int svcID, int N, char **mountPoint);
int getSvcMountFstype(int svcID, int N, char **mountFstype);
int getSvcMountOptions(int svcID, int N, char **mountOptions);
int getSvcForceUnmount(int svcID, int N, char **forceUnmount);
int getSvcDeviceOwner(int svcID, int N, char **deviceOwner);
int getSvcDeviceGroup(int svcID, int N, char **deviceGroup);
int getSvcDeviceMode(int svcID, int N, char **deviceMode);
int getSvcCheckInterval(int svcID, char **intervalStr);
int getSvcExportDir(int svcID, int deviceID, int exportID, char **tokenStr);
int getSvcExportClient(int svcID, int deviceID, int exportID, int clientID, char **tokenStr);
int getSvcExportOptions(int svcID, int deviceID, int exportID, int clientID, char **tokenStr);
int getSvcShareName(int svcID, int deviceID, char **shareName);

int ckSvcName(int current_svcID, char *svcName, char *errMsg, int msgLen);
int ckSvcScript(char *svcScript, char *errMsg, int msgLen);
int ckSvcRelocateOnPreferredNodeBoot(char *relocate, char *errMsg, int msgLen);
int ckSvcPreferredNodeName( char *nodeName, char *errMsg, int msgLen);
int ckSvcPreferredNodeID( int nodeID, char *errMsg, int msgLen);
int ckSvcIPaddress(int current_svcID, int current_ipN, char *IPaddr,char *errMsg, int msgLen);
int ckSvcBroadcast(char *Broadcast, char *errMsg, int msgLen);
int ckSvcNetmask(char *Netmask, char *errMsg, int msgLen);
int ckSvcDevice(int current_svcID, int current_devN, char *device, char *errMsg, int msgLen);
int ckSvcMountPoint(int current_svcID, int current_devN, char *mountPoint, char *errMsg, int msgLen);
int ckSvcMountOptions(char *mountOptions, char *errMsg, int msgLen);
int ckSvcMountFstype(char *mountFstype, char *errMsg, int msgLen);
int ckSvcForceUnmount(char *forceUnmount, char *errMsg, int msgLen);
int ckSvcDeviceOwner(char *deviceOwner, char *errMsg, int msgLen);
int ckSvcDeviceGroup(char *deviceGroup, char *errMsg, int msgLen);
int ckSvcDeviceMode(char *deviceMode, char *errMsg, int msgLen);
int ckSvcCheckInterval(char *intervalStr, char *errMsg, int msgLen);
int checkFormatOfIP(char *String, char *errMsg, int msgLen);
int checkIsInDataset(char *data,char **dataset, char *errMsg, int msgLen, int flag);
int ckIsMemberIP(char *IP, char *errMsg, int msgLen);
int ckSvcExportClient(char *clientName, char *errMsg, int msgLen);
int ckSvcExportOptions(char *exportOptions, char *errMsg, int msgLen);
int ckSvcShareName(char *shareName);
int ckSvcExportDir(int svcID, int deviceID, int exportID, char *mount, char *exportDir, char *errMsg, int msgLen);
int subdirectory(char *dir, char *subdir);

int setSvcName(int svcID, char *svcName, char *errMsg, int msgLen);
int setSvcScript(int svcID, char *svcScript, char *errMsg, int msgLen);
int setSvcRelocateOnPreferredNodeBoot(int svcID, char *relocate, char *errMsg, int msgLen);
int setSvcPreferredNodeName(int svcID, char *nodeName, char *errMsg, int msgLen);
int setSvcPreferredNodeID(int svcID, int nodeID, char *errMsg, int msgLen);
int setSvcIPaddress(int svcID, int N, char *IPaddr, char *errMsg, int msgLen);
int setSvcNetmask(int svcID, int N, char *netmask, char *errMsg, int msgLen);
int setSvcBroadcast(int svcID, int N, char *broadcast, char *errMsg, int msgLen);
int setSvcDevice(int svcID, int N, char *device, char *errMsg, int msgLen);
int setSvcCheckInterval(int svcID, char *intervalStr, char *errMsg, int msgLen);
int setSvcMountPoint(int svcID, int N, char *mountPoint, char *errMsg, int msgLen);
int setSvcMountFstype(int svcID, int N, char *mountFstype, char *errMsg, int msgLen);	
int setSvcMountOptions(int svcID, int N, char *mountOptions, char *errMsg, int msgLen);
int setSvcForceUnmount(int svcID, int N, char *forceUnmount, char *errMsg, int msgLen);
int setSvcDeviceOwner(int svcID, int N, char *deviceOwner, char *errMsg, int msgLen);
int setSvcDeviceGroup(int svcID, int N, char *deviceGroup, char *errMsg, int msgLen);
int setSvcDeviceMode(int svcID, int N, char *deviceMode, char *errMsg, int msgLen);
int setSvcExportDir(int svcID, int deviceID, int dirNum, char *exportDir, char *errMsg, int msgLen);
int setSvcExportClient(int svcID, int deviceID, int dirNum, int clientNum, char *exportClient, char *errMsg, int msgLen);
int setSvcExportOptions(int svcID, int deviceID, int dirNum, int clientNum, char *exportOptions, char *errMsg, int msgLen);
int setSvcShareName(int svcID, int deviceID, char *shareName, char *errMsg, int msgLen);

int getNextFreeSvcID(void);
int getNextFreeSvcIPaddress(int svcID, int *N);
int getNextFreeSvcDevice(int svcID, int *N);

int serviceExists(int svcID);
int unmountsForcefully(int svcID, int N);
int reReadDatabase(char *database);
int getSvcMgrLogLevel(int *logLevel);
int getNodeName(int nodeID, char **nodeName);
int getNodeID(char *nodeName);

void clean_string(char *p, int flag);

/*
 * fo_domain.c
 */
int svc_has_domain(int);
int node_should_start(uint32_t,memb_mask_t,uint32_t);

#ifdef __cplusplus
}
#endif
