/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
 */
/** @file
 * Header for vf.c.
 */
#ifndef __VF_H
#define __VF_H

#include <clusterdefs.h>
#include <stdint.h>
#include <msgsvc.h>
#include <clulog.h>
#include <cm_api.h>

/*
 * We use this to initiate the VF protocol.  This doesn't really belong here.
 */
typedef struct __attribute__ ((packed)) _vf_msg_info {
	uint32_t	vf_command;
	uint32_t	vf_keyid;
	uint32_t	vf_coordinator; /* Node ID of who coordinates */
	uint64_t	vf_view;
	uint32_t	vf_datalen;
	char		vf_data[0];
} vf_msg_info_t;

#define swab_vf_msg_info_t(ptr) \
{\
	swab32((ptr)->vf_command);\
	swab32((ptr)->vf_keyid);\
	swab32((ptr)->vf_coordinator);\
	swab64((ptr)->vf_view);\
	swab32((ptr)->vf_datalen);\
}


typedef struct __attribute__ ((packed)) _vf_msg {
	generic_msg_hdr	vm_hdr;
	vf_msg_info_t	vm_msg;
} vf_msg_t;

#define swab_vf_msg_t(ptr) \
{\
	swab_generic_msg_hdr(&((ptr)->vm_hdr));\
	swab_vf_msg_info_t(&((ptr)->vm_msg));\
}
 
/*
 * Exp: Callback function proto definitions.
 */
typedef int32_t (*vf_vote_cb_t)(uint32_t, uint64_t, void *, uint32_t);
typedef int32_t (*vf_commit_cb_t)(uint32_t, uint64_t, void *, uint32_t);

/*
 * INTERNAL VF STRUCTURES
 */
 
 /**
 * A view node.  This holds the data from a VF_JOIN_VIEW message until it
 * is committed.
 */
typedef struct _view_node {
	struct _view_node *
			vn_next;	/**< Next pointer. */
	int		vn_fd;		/**< Associated file descriptor. */
	uint32_t	vn_nodeid;	/**< Node ID of coordinator. */
	struct timeval  vn_timeout;	/**< Expiration time. */
	uint64_t	vn_viewno;	/**< View Number. */
	uint32_t	vn_datalen;	/**< Length of included data. */
	char		vn_data[0];	/**< Included data. */
} view_node_t;


/**
 * A commit node.  This holds a commit message until it is possible to
 * resolve it with its corresponding view_node_t.
 */
typedef struct _commit_node {
	struct _commit_node *
			vc_next;	/**< Next pointer. */
	int		vc_fd;		/**< File descriptor. */
} commit_node_t;


/**
 * A key node.  For each type of data used, a key node is created
 * and managed by the programmer.
 */
typedef struct _key_node {
	struct _key_node *kn_next;	/**< Next pointer. */
	uint32_t kn_keyid;		/**< Key ID this key node refers to. */
	uint32_t kn_pid;		/**< PID. Child process running
					  View-Formation on this key. */
	view_node_t *kn_jvlist;		/**< Buffered join-view list. */
	commit_node_t *kn_clist;	/**< Buffered commit list. */
	uint64_t kn_viewno;		/**< Current view number of data. */
	uint32_t kn_datalen;		/**< Current length of data. */
	char *kn_data;			/**< Current data. */
	int kn_tsec;			/**< Default timeout (in seconds */
	vf_vote_cb_t kn_vote_cb;	/**< Voting callback function */
	vf_commit_cb_t kn_commit_cb;	/**< Commit callback function */
} key_node_t;




/*
 * VF message types.
 */
/* Main programs handle this */
#define VF_MESSAGE		0x3000

#define VF_JOIN_VIEW		0x3001
#define VF_VOTE			0x3002
#define VF_ABORT		0x3004
#define VF_VIEW_FORMED		0x3005
#define VF_CURRENT		0x3006
#define VF_ACK			0x3007
#define VF_NACK			0x3008


#define VF_COORD_TIMEOUT	5	/* 5 seconds MAX timeout */
#define MAX_FDS			1024

/* Return codes for vf_handle_msg... */
#define VFR_ERROR	-1
#define VFR_OK		0
#define VFR_YES		VFR_OK
#define VFR_NO		1
#define VFR_COMMIT	2
#define VFR_ABORT	3

/*
 * Operational flags for vf_start
 */
#define VFF_RETRY		0x1
#define VFF_IGNORE_ERRORS	0x2


/* 
 * VF Stuff.  VF only talks to peers.
 */
int vf_init(uint32_t my_node_id, uint32_t my_daemon_id);

/*
 * Returns a file descriptor on which the caller can select().
 *
 * This is a pipe which is used to notify the parent process that
 * the child has exited
 */
int vf_start(memb_mask_t curr_memb_mask, uint32_t flags,
	     uint32_t keyid, void *data, uint32_t datalen);
int vf_process_msg(int handle, generic_msg_hdr *msgp, int nbytes);
int vf_current(uint32_t keyid, uint64_t *view, void **data,
	       uint32_t *datalen);
int vf_key_init(uint32_t keyid, int timeout, vf_vote_cb_t vote_cb,
		vf_commit_cb_t commit_cb);
int vf_end(uint32_t keyid);
int vf_purge(uint32_t keyid, int *fd);
int vf_running(uint32_t keyid);
int getuptime(struct timeval *tv);

#endif
