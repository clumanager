/*
  Copyright Red Hat, Inc. 2002-2003

  The Red Hat Cluster Manager API Library is free software; you can
  redistribute it and/or modify it under the terms of the GNU Lesser
  General Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any later
  version.

  The Red Hat Cluster Manager API Library is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
*/
/** @file
 * Main user/client API driver routines.
 *
 * This contains the API routines which user/client applications use to
 * talk directly to Red Hat Cluster Manager.
 */
#include <cm_api.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <apitcp.h>

int memb_register(void);		/** Defined in membership.c */
int quorum_register(void);		/** Defined in quorum.c */

/**
 * Read a membership event from a file descriptor.  The idea here is that the
 * event itself is opaque to the user.
 *
 * @param fd		The File descriptor we selected.
 * @return		The event, if any, present on fd.
 */
cm_event_t *
cm_ev_read(int fd)
{
	fd_set set;
	cm_event_t *msg = NULL;
	int rv, c;
	struct timeval tv;

	if (fd < 0) {
		errno = EINVAL;
		return NULL;
	}

	FD_ZERO(&set);
	FD_SET(fd, &set);

	tv.tv_sec = 3;
	tv.tv_usec = 0;

	rv = select(fd + 1, &set, NULL, NULL, &tv);
	if (!rv)
		return NULL;

	msg = malloc(sizeof(cm_event_t));
	if (!msg)
		return NULL;

	rv = tcp_receive(fd, msg, sizeof(cm_event_t));
	if ((rv != sizeof(cm_event_t)) || (rv == -1)) {
		free(msg);
		return NULL;
	}

	swab_cm_event_hdr_t(&msg->em_header);

	if ((cm_ev_type(msg) != EC_MEMBERSHIP) &&
	    (cm_ev_type(msg) != EC_QUORUM)) {
		free(msg);
		errno = EINVAL;
		return NULL;
	}

	if (msg->em_header.eh_length < sizeof(cm_event_t)) {
		free(msg);
		errno = EINVAL;
		return NULL;
	}

	switch(cm_ev_event(msg)) {
	case EV_MEMB_UPDATE:
		swab_cm_memb_event_t(&msg->u.ev_memb, c);
		break;
	case EV_QUORUM:
	case EV_NO_QUORUM:
	case EV_QUORUM_GAINED:
	case EV_QUORUM_LOST:
		swab_cm_quorum_event_t(&msg->u.ev_quorum, c);
		break;
	default:
		break;
	}

	return msg;
}


/**
 * Free an event.  Because the contents of a cluster event may change, we don't
 * want users freeing them by hand.
 *
 * @param eventp	The cluster event to free.
 */
void
cm_ev_free(cm_event_t *eventp)
{
	free(eventp);
}

/**
 * Register for event(s).  This contacts the appropriate daemon(s) and sends
 * an EV_REGISTER with the appropriate flags to the daemon(s).
 *
 * @param event_class	The class of events to register for. 
 * @return		-1 on failure, 0 on success.
 */
int
cm_ev_register(int event_class)
{
	errno = 0;

	if (event_class == EC_MEMBERSHIP) {
		return memb_register();
	} else if (event_class == EC_QUORUM) {
		return quorum_register();
	}

	errno = EINVAL;
	return -1;
}


/**
 * Unregister for events on a given file descriptor.  Right now, this simply
 * closes the file descriptor.
 *
 * @param fd		The file descriptor to unregister/close.
 * @return		0
 */
int
cm_ev_unregister(int fd)
{
	close(fd);
	return 0;
}
