/*
  Copyright 2003 Red Hat, Inc.

  The Red Hat Cluster Manager API Library is free software; you can
  redistribute it and/or modify it under the terms of the GNU Lesser
  General Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any later
  version.

  The Red Hat Cluster Manager API Library is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA. 
 */
/** @file
 * Thin TCP Socket Functions for Cluster Manager API Library
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <errno.h>
#include <apitcp.h>
#include <platform.h>
#include <auth_md5.h>
#include <xmlwrap.h>

int hex2bin(char *, int, char *, int);

/**
 * Connect to localhost on the specified port.
 *
 * @param port	Port to which we intend to connect.
 * @return	fd, or -1 on failure. See socket(2) and connect(2) for 
 *		possible errors.
 */
int
tcp_localconnect(int port)
{
	int fd, j;
	struct sockaddr_in addr;
	char *key;
	char keybuf[256];

	fd = socket(PF_INET, SOCK_STREAM, 0);
	if (fd < 0)
		return -1;

	if (!CFG_Loaded() && (CFG_ReadFile(CLU_CONFIG_FILE) != CFG_OK))
		auth_md5_init(NULL, 0);

	if (CFG_Loaded() &&
	    CFG_Get("cluster%key", NULL, &key) == CFG_OK) {
		j = hex2bin(key, strlen(key), keybuf, sizeof(keybuf));
		if (j == -1)
			auth_md5_init(NULL, 0);
		else
			auth_md5_init(keybuf, j);
	} else
		auth_md5_init(NULL, 0);

#if 0
	j = 1;
	setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &j, sizeof(j));
#endif
	
	memset(&addr,0,sizeof(addr));
	addr.sin_family = PF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	if (port == 0 || port > 65535)
		port = 34001;
	addr.sin_port = htons(port);
	
	j = connect(fd, (struct sockaddr *)&addr, sizeof(addr));
	if ((j == 0) && (auth_md5(fd) == 0))
		return fd;

	j = errno;
	close(fd);
	errno = j;
	return -1;
}


/**
 * Send a message to a socket.
 *
 * @param fd
 * @param msg
 * @param len
 * @return		Number of bytes send, or -1 on failure.
 */
ssize_t
tcp_send(int fd, void *msg, uint32_t len)
{
	ssize_t ret;
	uint32_t truesize;
	void *truemsg;
	fd_set set;
	struct timeval tv;

	if (fd == -1)
		return -1;

	if (len == 0)
		return 0;

	/*
	 * Send the # of bytes we intend to send as first 4 bytes.
	 */
	truesize = len + sizeof(uint32_t);
	truemsg = malloc(truesize);
	if (!truemsg)
		return -1;

	memset(truemsg, 0, truesize);
	((uint32_t *)(truemsg))[0] = len;
	swab32(((uint32_t *)(truemsg))[0]);
	memcpy(truemsg + sizeof(truesize), msg, len);

	FD_ZERO(&set);
	FD_SET(fd, &set);
	tv.tv_sec = TCP_TIMEOUT;
	tv.tv_usec = 0;

	ret = (ssize_t)select(fd+1, NULL, &set, NULL, &tv);
	if (ret <= 0) {
		if (ret == 0)
			errno = ETIMEDOUT;
		free(truemsg);
		return -1;
	}

	ret = write(fd, truemsg, truesize);
	free(truemsg);

	if (ret == -1)
		return -1;
	
	if (ret < truesize) {
		errno = EAGAIN;
		ret = -1;
	}

	return len;
}


/**
 * Send a message to a socket.
 *
 * @param fd
 * @param msg
 * @param len
 * @return		Number of bytes send, or -1 on failure.
 */
ssize_t
tcp_receive(int fd, void *msg, uint32_t len)
{
	ssize_t ret;
	uint32_t length, truesize;
	struct timeval tv;
	fd_set set;

	if (fd < 0)
		return -1;

	FD_ZERO(&set);
	FD_SET(fd, &set);
	tv.tv_sec = TCP_TIMEOUT;
	tv.tv_usec = 0;

	ret = (ssize_t)select(fd+1, NULL, &set, NULL, &tv);
	if (ret <= 0) {
		if (ret == 0)
			errno = ETIMEDOUT;
		return -1;
	}

	ret = read(fd, (void *)&length, sizeof(length));
	if (ret != sizeof(length))
		return -1;

	/* Fix byte order */
	swab32(length);
	if (length > TCP_MAXLEN)
		return -1;

	truesize = (length < len) ? length : len;
	ret = read(fd, msg, truesize);
	if (ret == -1)
		return -1;
	
	if (ret < truesize) {
		errno = EAGAIN;
		ret = -1;
	}

	return truesize;
}
