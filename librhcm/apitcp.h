/*
  Copyright Red Hat, Inc. 2003

  The Red Hat Cluster Manager API Library is free software; you can
  redistribute it and/or modify it under the terms of the GNU Lesser
  General Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any later
  version.

  The Red Hat Cluster Manager API Library is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
 */
/** @file
 * Header file for RHCM API TCP functions.
 */
#include <sys/types.h>
#include <stdint.h>

#define TCP_TIMEOUT 5
#define TCP_MAXLEN 1024

int tcp_localconnect(int port);
ssize_t tcp_send(int fd, void *msg, uint32_t len);
ssize_t tcp_receive(int fd, void *msg, uint32_t length);
