/*
  Copyright Red Hat, Inc. 2003

  The Red Hat Cluster Manager API Library is free software; you can
  redistribute it and/or modify it under the terms of the GNU Lesser
  General Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any later
  version.

  The Red Hat Cluster Manager API Library is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
 */
/** @file
 * Very simple challenge-response, based on MD5 + shared-secret.
 * Uses Colin Plumb's MD5 code.
 */
#include <sys/mman.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>
#include <md5.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define AUTH_TIMEOUT 5
#define KEY_SIZE 16

static pthread_mutex_t auth_mutex = PTHREAD_MUTEX_INITIALIZER;
static int auth_enabled = 0;
static struct MD5Context auth_md5_ctx;
static int urfd = -1;
#if 0
static int rfd = -1;
static char __rdata[KEY_SIZE*4];
#endif

typedef struct _random_data {
	int rd_size;
	void *rd_data;
} random_data_t;

#ifdef DEBUG
#include <stdio.h>

/**
 * Dump a buffer of bytes in human-readable hexadecimal to output.
 *
 * @param buf		Buffer to dump
 * @param len		Length of buf
 */
static void
hex_dump(char *buf, int len)
{
	int x;

	printf("0x");
	for(x=0; x<len; x++)
		printf("%02x", (int)(buf[x]&0x000000ff));
}
#endif


/**
 * Mash pseudo random data with real (entropy) random data.  This is so we
 * don't run the system out of entropy when lots of authentication packets
 * are being sent back and forth.
 *
 * @param pd		Pseudo random data set.
 * @param pdsize	Pseudo random data set size.
 * @param rd		Random data set (based on system entropy).
 * @param rdsize	Random data set size.
 */
#if 0
void
mash(char *pd, int psize, char *rd, int rsize)
{
	int x;
	int start = 0;

	/* Pass 1: Generate pseudorandom start offset */
	for (x = 0; x < psize; x++)
		start ^= (int)(pd[x]&0xff);
	start %= (rsize - psize + 1);
	read(rfd, rd + (start % rsize), 1);

	/*
	 * Pass 2: XOR pseudo w/ random data starting
	 * at pseudorandom offset...
	 */
	for (x = 0; x < psize; x++)
		pd[x] ^= rd[x+start];
}
#endif


/**
 * Fudge random data based on /dev/urandom and previously
 * read-in data from /dev/random.
 *
 * @param data		random_data_t info to initialize.
 * @param size		Size of buffer (or amount to fill).
 * @return		-1 on failure; 0 on success.
 */
static int
alloc_random_data(random_data_t *data, int size)
{
	pthread_mutex_lock(&auth_mutex);
#if 0
	if (rfd == -1) {
		rfd = open("/dev/random", O_RDONLY);
		if (rfd == -1) {
			pthread_mutex_unlock(&auth_mutex);
			return -1;
		}
		read(rfd, __rdata, sizeof(__rdata));
	}
#endif

	if (urfd == -1) {
		urfd = open("/dev/urandom", O_RDONLY);
		if (urfd == -1) {
#if 0
			close(rfd);
			rfd = -1;
#endif
			pthread_mutex_unlock(&auth_mutex);
			return -1;
		}
		pthread_mutex_unlock(&auth_mutex);
	}

	data->rd_size = size;
	data->rd_data = malloc(size);
	pthread_mutex_unlock(&auth_mutex);

	if (!data->rd_data)
		return -1;

	if (size > 0) {
		/* Kind of depends on cryptographic strength
		   of /dev/urandom this way */
		read(urfd, (char *)(data->rd_data), size);
#if 0
		mash((char *)data->rd_data, size, __rdata,
		     sizeof(__rdata));
#endif
	}
	return 0;
}


/**
 * Munmap/free data stored in a random_data_t structure.
 *
 * @param data		random_data_t to clear out.
 */
static void
free_random_data(random_data_t *data)
{
	free(data->rd_data);
}


/**
 * Initialize our global MD5 context.
 *
 * @param key		Key we're starting with.
 * @param keylen	Length of our key.
 * @return 		0
 */
int
auth_md5_init(char *key, size_t keylen)
{
	pthread_mutex_lock(&auth_mutex);
	MD5Init(&auth_md5_ctx);

	if (key && keylen) {
		MD5Update(&auth_md5_ctx, (uint8_t *)key, keylen);
		auth_enabled = 1;
	} else {
		auth_enabled = 0;
	}

	pthread_mutex_unlock(&auth_mutex);
	return 0;
}


/**
 * Close our /dev/random FD & clear up our MD5 Context.
 */
int
auth_md5_deinit(void)
{
	pthread_mutex_lock(&auth_mutex);
#if 0
	if (rfd != -1) {
		close(rfd);
		rfd = -1;
	}
#endif
	if (urfd != -1) {
		close(urfd);
		urfd = -1;
	}
	MD5Init(&auth_md5_ctx);
	auth_enabled = 0;
	pthread_mutex_unlock(&auth_mutex);
	return 0;
}


/**
 * Issue MD5 challenge.  We basically just send some number of bytes read
 * from /dev/random to the specified file descriptor.  After we're done, we
 * update a copy of our global key-MD5 context, and compare what the
 * client sends back with what we expect it to send back, based on our
 * view of the secret key.  If the client sends back the proper message,
 * we report 'success'; otherwise, 'permission denied'.
 *
 * @param fd		File descriptor to authenticate.
 * @return		-1 on fail/not authentic, 0 on success/authentic
 */
int
auth_md5_challenge(int fd)
{
	int remain = KEY_SIZE, n;
	uint8_t expected_md5[16], client_md5[16];
	unsigned char *p, reply = 0;
	random_data_t challenge_data;
	struct MD5Context ch_ctx;
	struct timeval tv;
	fd_set rfds;

	pthread_mutex_lock(&auth_mutex);
	if (!auth_enabled) {
		pthread_mutex_unlock(&auth_mutex);
		return 0;
	}

	memcpy(&ch_ctx, &auth_md5_ctx, sizeof(ch_ctx));
	pthread_mutex_unlock(&auth_mutex);

	/* No random data, no authentication */
	if (alloc_random_data(&challenge_data, KEY_SIZE) < 0)
		return -1;

	/* Figure out what we expect to receive */
	MD5Update(&ch_ctx, (uint8_t *)challenge_data.rd_data,
		  challenge_data.rd_size);
	MD5Final(expected_md5, &ch_ctx);
	
#ifdef DEBUG
	printf("Issuing challenge: ");
	hex_dump(challenge_data.rd_data, challenge_data.rd_size);
	printf("\n");
	printf("Expecting: ");
	hex_dump(expected_md5, 16);
	printf("\n");
#endif

	p = challenge_data.rd_data;
	while (remain) {
		n = write(fd, p, remain);
		if (n == -1) {
			if (errno == EINTR)
				continue;
			free_random_data(&challenge_data);
			return -1;
		}
		if (n == 0) {
			free_random_data(&challenge_data);
			return -1;
		}

		p += n;
		remain -= n;
	}
	free_random_data(&challenge_data);

	tv.tv_sec = AUTH_TIMEOUT;
	tv.tv_usec = 0;
	remain = KEY_SIZE;
#ifdef DEBUG
	printf("Challenge sent, waiting for response\n");
#endif

	p = client_md5;
	while (remain && (tv.tv_sec || tv.tv_usec)) {
		FD_ZERO(&rfds);
		FD_SET(fd, &rfds);

		n = select(fd+1, &rfds, NULL, NULL, &tv);
		if (n == -1) {
			if (errno == EINTR)
				continue;
			return -1;
		}
		if (n == 0) {
			errno = ETIMEDOUT;
			return -1;
		}

		n = read(fd, p, remain);
		if (n == -1) {
			if (errno == EINTR)
				continue;
			return -1;
		}
		if (n == 0) {
			errno = EPIPE;
			return -1;
		}

		p += n;
		remain -= n;
	}

#ifdef DEBUG
	printf("Received: ");
	hex_dump(client_md5, 16);
	printf("\n");
#endif

	if (remain || memcmp(expected_md5, client_md5, 16)) {
		/* Notify client */
		reply = (char)(0xff);
		write(fd, &reply, 1);
		errno = EACCES;
		return -1;
	}

#ifdef DEBUG
	printf("Authentication successful\n");
#endif
	write(fd, &reply, 1);
	return 0;
}


/**
 * Handle an MD5 challenge from a server.  This is generally done
 * immediately following a connect() call.  This handles the challenge
 * data from the server, updates an MD5 context based on our "secret"
 * key, and sends the resulting MD5 back to the server.  If the server
 * accepts our response, it writes a null byte to us; anything else
 * is assumed to be a rejection.
 *
 * @param fd		File descriptor to authenticate.
 * @return		-1 on failure/auth-failed, 0 on success
 */
int
auth_md5(int fd)
{
	int remain = KEY_SIZE, n;
	unsigned char key[KEY_SIZE];
	unsigned char *p;
	unsigned char response[16];
	struct MD5Context ch_ctx;
	struct timeval tv;
	fd_set rfds;

	pthread_mutex_lock(&auth_mutex);
	if (!auth_enabled) {
		pthread_mutex_unlock(&auth_mutex);
		return 0;
	}

	memcpy(&ch_ctx, &auth_md5_ctx, sizeof(ch_ctx));
	pthread_mutex_unlock(&auth_mutex);

#ifdef DEBUG
	printf("Awaiting challenge\n");
#endif

	/* Grab the challenge from the server */
	tv.tv_sec = 5;
	tv.tv_usec = 0;
	remain = KEY_SIZE;
	p = key;

	while (remain && (tv.tv_sec || tv.tv_usec)) {
		FD_ZERO(&rfds);
		FD_SET(fd, &rfds);

		n = select(fd+1, &rfds, NULL, NULL, &tv);
		if (n == -1) {
			if (errno == EINTR)
				continue;
			return -1;
		}
		if (n == 0) {
			errno = ETIMEDOUT;
			return -1;
		}

		n = read(fd, p, remain);
		if (n == -1) {
			if (errno == EINTR)
				continue;
			return -1;
		}
		if (n == 0) {
			errno = EPIPE;
			return -1;
		}

		remain -= n;
		p += n;
	}

	if (remain)
		return -1;

#ifdef DEBUG
	printf("Received: ");
	hex_dump(key, KEY_SIZE);
	printf("\n");
#endif
	/* Build response */
	MD5Update(&ch_ctx, (uint8_t *)key, KEY_SIZE);
	MD5Final(response, &ch_ctx);

        remain = 16;
	p = response;
	while (remain) {
		n = write(fd, p, remain);
		if (n == -1) {
			if (errno == EINTR)
				continue;
			return -1;
		}
		if (n == 0)
			return -1;

		p += n;
		remain -= n;
	}

#ifdef DEBUG
	printf("Responded with: ");
	hex_dump(response, 16);
	printf("\n");
#endif

	/* wait for reply from server as to whether or not we
	   are authenticated */
	p = key; /* Reuse */
	while (tv.tv_sec || tv.tv_usec) {
		FD_ZERO(&rfds);
		FD_SET(fd, &rfds);

		n = select(fd+1, &rfds, NULL, NULL, &tv);
		if (n == -1) {
			if (errno == EINTR)
				continue;
			return -1;
		}
		if (n == 0) {
			errno = ETIMEDOUT;
			return -1;
		}

		n = read(fd, p, 1);
		if (n == -1) {
			if (errno == EINTR)
				continue;
			return -1;
		}
		if (n == 0) {
			errno = EPIPE;
			return -1;
		}
		break;
	}

	if (*p == 0) {
		return 0;
	}

	/* Permission denied */
	errno = EACCES;
	return -1;
}


#ifdef STANDALONE
#include <arpa/inet.h>
#include <stdio.h>

#ifdef SERVER
int
main(int argc, char **argv)
{
        char buf[256];
        int fd, newfd, len, flag, pid;
        struct sockaddr_in sai;
        fd_set rfds;

	printf("TCP auth_md5 Echo Server\n");
        if (argc < 2) {
		fprintf(stderr,"usage: %s <port> [password]\n", argv[0]);
		return 1;
	}

	if (argc >= 3)
        	auth_md5_init(argv[2], strlen(argv[2]));
	else
        	auth_md5_init(NULL, 0);

        fd = socket(PF_INET, SOCK_STREAM, 0);
        memset(&sai, 0, sizeof(sai));
        sai.sin_family = PF_INET;
        sai.sin_port = htons(atoi(argv[1]));
        sai.sin_addr.s_addr = htonl(INADDR_ANY);

        flag = 1;
        setsockopt(fd, SOL_SOCKET, SO_REUSEADDR,
                   &flag, sizeof(flag));

        if (bind(fd, (struct sockaddr *)&sai, sizeof(sai)) == -1) {
                perror("bind");
                return(-1);
        }

        if (listen(fd, 5) == -1) {
                perror("listen");
                return(-1);
        }

        len = sizeof(sai);
        while (1) {
		newfd = accept(fd, (struct sockaddr *)&sai, &len);

		if (newfd == -1) {
		perror("accept");
			return(-1);
		}

		if (auth_md5_challenge(newfd) == -1) {
			perror("auth_md5_challenge");
			close(newfd);
			continue;
		}

		pid = fork();
		if (pid == 0) {
			close(fd);
			break;
		}
		
		if (pid == -1) {
			perror("fork");
			continue;
		}
		
		printf("New Child: PID%d\n", pid);
		close(newfd);
	}

	pid = getpid();
        while(1) {
                FD_ZERO(&rfds);
                FD_SET(newfd, &rfds);

                if (select(newfd + 1, &rfds, NULL, NULL, NULL) == 1) {
                        memset(buf,0,sizeof(buf));
                        switch(len = read(newfd, buf, sizeof(buf))) {
                        case 0:
                                printf("[%d] Connection closed by remote host\n",
                                       pid);
                                exit(0);
                        case -1:
                                fprintf(stderr, "[%d] read: %s", pid,
                                	strerror(errno));
                                return(0);
                        default:
                                write(newfd, buf, len);
                        }
                }
        }
}
#else

int
main(int argc, char **argv)
{
	char buf[256];
	int fd, len;
	struct sockaddr_in sai;
	fd_set rfds;

	printf("TCP auth_md5 Echo Client\n");

	if (argc < 2) {
		fprintf(stderr, "usage: %s <port> [password]\n",
			argv[0]);
		return 1;
	}

	if (argc >= 3)
        	auth_md5_init(argv[2], strlen(argv[2]));
	else
        	auth_md5_init(NULL, 0);

	fd = socket(PF_INET, SOCK_STREAM, 0);
	memset(&sai, 0, sizeof(sai));
	sai.sin_family = PF_INET;
	sai.sin_port = htons(atoi(argv[1]));
	sai.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	if (connect(fd, (struct sockaddr *)&sai, sizeof(sai)) == -1) {
		perror("connect");
		return(-1);
	}

	if (auth_md5(fd) != 0) {
		perror("auth_md5");
		return(-1);
	}
	
	printf("Authenticated.  Type away.\n");
	while(1) {
		FD_ZERO(&rfds);
		FD_SET(STDIN_FILENO,&rfds);
		FD_SET(fd,&rfds);

		select(1024, &rfds, NULL, NULL, NULL);

		if (FD_ISSET(STDIN_FILENO,&rfds)) {
			len = read(STDIN_FILENO, buf, sizeof(buf));

			if (!len) {
				printf("Connection closed by remote host\n");
				return(0);
			}

			if (write(fd, buf, len) == -1) {
				perror("write");
				return(-1);
			}
		}

		if (FD_ISSET(fd, &rfds)) {
			memset(buf,0,sizeof(buf));
			if (read(fd, buf, sizeof(buf)) == -1) {
				perror("read");
				return(-1);
			}

			printf("%s", buf);
		}
	}

	return(0);
}
#endif
#endif
