/**
  Copyright Red Hat, Inc. 2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Very simple challenge-response, based on sha1 + shared-secret (header)
 */
#ifndef __AUTH_SHA1_H
#define __AUTH_SHA1_H
int auth_sha1_init(char *key, size_t keylen);
int auth_sha1_deinit(void);
int auth_sha1_challenge(int fd);
int auth_sha1(int fd);
#endif
