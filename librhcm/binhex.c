/*
  Copyright 2003 Red Hat, Inc.

  The Red Hat Cluster Manager API Library is free software; you can
  redistribute it and/or modify it under the terms of the GNU Lesser
  General Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any later
  version.

  The Red Hat Cluster Manager API Library is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
 */
/** @file
 * "fast?" Binary <-> ASCII Hexadecimal conversion functions.
 */
#include <string.h>
 
static char tohex[16] = {
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
	'a', 'b', 'c', 'd', 'e', 'f'
};

static char tobin[256] = {
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 0..23 */
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 23..47 */
	0,1,2,3,4,5,6,7,8,9,0,0,0,0,0,0,0,10,11,12,13,14,15,0, /* 48..71 */
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 72..95 */
	0,10,11,12,13,14,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 96..119 */
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 120..143 */
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 144..167 */
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 168..191 */
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 192..215 */
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 216..239 */
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0  /* 239..255 */
};


/**
 * Convert binary array of bytes to human-readable hexadecimal.
 *
 * @param in		Input buffer
 * @param inlen		Input buffer length
 * @param out		Output buffer
 * @param outlen	Output buffer length (>=2 * inlen)
 * @return		-1 on failure, # of bytes in out on success.
 */
int
bin2hex(char *in, int inlen, char *out, int outlen)
{
	char *inp = in, *outp = out;
	int x;

	if (inlen <= 0 || outlen <= 0)
		return -1;
	
	if (outlen < (inlen * 2))
		return -1;

	memset(out,0,outlen);

	for (x=0; x<inlen; x++) {
		outp[0] = tohex[(inp[0] >> 4 & 0xf)];
		outp[1] = tohex[(inp[0]      & 0xf)];
		outp+=2;
		inp++;
	}

	return (outp - out);
}


/**
 * Convert human-readable hexadecimal to binary array of bytes
 *
 * @param in		Input buffer
 * @param inlen		Input buffer length
 * @param out		Output buffer
 * @param outlen	Output buffer length (>=inlen / 2)
 * @return		-1 on failure, # of bytes in out on success.
 */
int
hex2bin(char *in, int inlen, char *out, int outlen)
{
	char *inp = in, *outp = out;
	int x;
	
	if (inlen <= 0 || outlen <= 0)
		return -1;

	if (inlen > (outlen * 2))
		return -1;

	memset(out,0,outlen);

	for (x=0; x<inlen; x+=2) {
		outp[0] = (tobin[(int)inp[0]] << 4) | tobin[(int)inp[1]];
		outp++;
		inp+=2;
	}

	return (outp - out);
}

#ifdef STANDALONE
#include <stdio.h>

int
main(int argc, char **argv)
{
	char buf[256], buf2[128];
	int o;

	if (argc < 2) {
		printf("usage: %s <string>\n", argv[0]);
		return 1;
	}

	o = bin2hex(argv[1], strlen(argv[1]), buf, sizeof(buf));
	printf(" -> hex -> %s (%d)\n", buf, o);
	o = hex2bin(buf, strlen(buf), buf2, sizeof(buf2));
	printf(" -> bin -> %s (%d)\n", buf2, o);
}
#endif
