/*
  Copyright Red Hat, Inc. 2002-2003

  The Red Hat Cluster Manager API Library is free software; you can
  redistribute it and/or modify it under the terms of the GNU Lesser
  General Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any later
  version.

  The Red Hat Cluster Manager API Library is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
 */
/** @file
 * Bitmap and membership mask handling routines.
 */
#include <stdio.h>
#include <cm_api.h>
#include <string.h>
#include <stdint.h>


/**
 * Clear a bit in a bitmap / bitmask.
 *
 * @param mask		Bitmask to modify.
 * @param bitidx	Bit to modify.
 * @param masklen	Bitmask length (in uint32_t units).
 * @return		-1 if the index exceeds the number of bits in the
 *			bitmap, otherwise 0.
 */
int
clear_bit(uint32_t *mask, uint32_t bitidx, uint32_t masklen)
{
	uint32_t idx;
	uint32_t bit;

	/* Index into array */
	idx = bitidx >> 5;
	bit = 1 << (bitidx & 0x1F);

	if (idx >= masklen)
		return -1;

	mask[idx] &= ~bit;

	return 0;
}


/**
 * Set a bit in a bitmap / bitmask.
 *
 * @param mask		Bitmask to modify.
 * @param bitidx	Bit to modify.
 * @param masklen	Bitmask length (in uint32_t units).
 * @return		-1 if the index exceeds the number of bits in the
 *			bitmap, otherwise 0.
 */
int
set_bit(uint32_t *mask, uint32_t bitidx, uint32_t masklen)
{
	uint32_t idx;
	uint32_t bit;

	/* Index into array */
	idx = bitidx >> 5;
	bit = 1 << (bitidx & 0x1F);

	if (idx >= masklen)
		return -1;

	mask[idx] |= bit;

	return 0;
}


/**
 * Check the status of a bit in a bitmap / bitmask.
 *
 * @param mask		Bitmask to check.
 * @param bitidx	Bit to to check.
 * @param masklen	Bitmask length (in uint32_t units).
 * @return		-1 if the index exceeds the number of bits in the
 *			bitmap, 0 if not set, or 1 if set.
 */
int
is_bit_set(uint32_t *mask, uint32_t bitidx, uint32_t masklen)
{
	uint32_t idx;
	uint32_t bit;

	/* Index into array */
	idx = bitidx >> 5;
	bit = 1 << (bitidx & 0x1F);

	if (idx >= masklen)
		return -1;

	return !!(mask[idx]&bit);
}


/**
 * Check to see if a member is online within a given mask.
 *
 * @param mask		Membership mask.
 * @param node		Member number/ID to check.
 * @return		See is_bit_set.
 * @see is_bit_set
 */
int
memb_online(memb_mask_t mask, int node)
{
	return (is_bit_set((uint32_t *)mask, node, MEMB_MASK_LEN));
}


/**
 * Return the lowest-numbered member in a given membership mask
 *
 * @param mask		Membership mask.
 * @return		Index of lowest bit set in mask.
 * @see is_bit_set
 */
int
memb_low_id(memb_mask_t mask)
{
	int x;

	for (x = 0; x < MAX_NODES; x++)
		if (is_bit_set((uint32_t *)mask, x, MEMB_MASK_LEN) == 1)
			return x;
	return -1;
}


/**
 * Return the highest-numbered member in a given membership mask
 *
 * @param mask		Membership mask.
 * @return		Index of highest bit set in mask.
 * @see is_bit_set
 */
int
memb_high_id(memb_mask_t mask)
{
	int x;

	for (x = MAX_NODES-1; x >= 0; x--)
		if (is_bit_set((uint32_t *)mask, x, MEMB_MASK_LEN) == 1)
			return x;
	return -1;
}


/**
 * Return the the number of bits ON in a given membership mask.
 *
 * @param mask		Membership mask.
 * @return		Number of bits ON in mask.
 * @see is_bit_set
 */
int
memb_count(memb_mask_t mask)
{
	int x, rv = 0;

	for (x = 0; x < MAX_NODES; x++)
		if (is_bit_set((uint32_t *)mask, x, MEMB_MASK_LEN) == 1)
			rv++;

	return rv;
}


/**
 * Constructs a formatted string out of a membership mask and returns it.
 *
 * @param mask		Membership mask.
 * @return		Pointer to static format string, or NULL if
 *			mask is NULL.
 */
char *
memb_mask_str(memb_mask_t mask)
{
	static char bufferstr[MEMB_MASK_LEN * 8 + 3];
	char tmpstr[9];
	int x;

	if (!(uint32_t *)mask)
		return NULL;

	snprintf(bufferstr, sizeof(bufferstr), "0x");
	for (x=MEMB_MASK_LEN-1; x>=0; x--) {
		snprintf(tmpstr,sizeof(tmpstr),"%08x",((uint32_t *)mask)[x]);
		strcat(bufferstr,tmpstr);
	}

	return bufferstr;
}


#ifdef STANDALONE
int
main(int argc, char **argv)
{
	int a;
	memb_mask_t membership;

	memset(membership, 0, sizeof(membership));

	a = atoi(argv[1]);
	printf("bit[%d] = %d\n", a, is_bit_set(membership, a, MEMB_MASK_LEN));
	set_bit(membership, a, MEMB_MASK_LEN);
	printf("bit[%d] = %d\n", a, is_bit_set(membership, a, MEMB_MASK_LEN));
	clear_bit(membership, a, MEMB_MASK_LEN);
	printf("bit[%d] = %d\n", a, is_bit_set(membership, a, 64));
	set_bit(membership, a, MEMB_MASK_LEN);

	printf("Mask = %s\n", memb_mask_str(membership));

	return 0;


}
#endif
