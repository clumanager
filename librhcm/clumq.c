/*
  Copyright Red Hat, Inc. 2002-2003

  Red Hat Cluster Manager is free software; you can redistribute it
  and/or modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * API Demo Program.
 *
 * $Revision: 1.1 $
 */
#include <cm_api.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

void
print_info(int me, void *result)
{
	int total;

	printf("Membership View: %d\n", (int)cm_memb_view(result));
	printf("Membership Mask: %s\n", memb_mask_str(cm_memb_mask(result)));

	if (memb_online(cm_memb_mask(result), me)) {
		total = memb_count(cm_memb_mask(result));

		if (total == 1)
			printf("* I am the only participating member.\n");
		else {
			if (memb_low_id(cm_memb_mask(result)) == me)
				printf("* I have the lowest participating "
				       "member ID.\n");
			if (memb_high_id(cm_memb_mask(result)) == me)
				printf("* I have the highest participating "
				       "member ID.\n");
			printf("* %d members are participating.\n", total);
		}
	} else {
		printf("* I am not participating.\n");
	}

	printf("* This cluster has a maximum of %d members.\n", MAX_NODES);
}


void
print_quorum(int me, void *result)
{
	switch (cm_ev_event(result)) {
	case EV_QUORUM:
		printf("* Partition has a quorum.\n");
		break;
	case EV_QUORUM_GAINED:
		printf("* Partition has gained a quorum.\n");
		break;
	case EV_NO_QUORUM:
		printf("* Partition does not have a quorum.\n");
		break;
	case EV_QUORUM_LOST:
		printf("* Partition no longer has a quorum.\n");
		break;
	default:
		printf("* Unhandled event: %08x\n", cm_ev_event(result));
	}
}


int
main(int argc, char **argv)
{
	cm_event_t *result = NULL;
	int me, membfd, quorumfd;
	fd_set rfds;

	if ((me = memb_local_id()) == -1) {
		printf("Failed getting local member ID: %s\n",
		       strerror(errno));
		return 1;
	}

	printf("Cluster Member ID: %d\n", me);

	if (argc == 1) {
		if (memb_query(&result) == -1) {
			printf("memb_query: %s\n", strerror(errno));
			return 1;
		}

		print_info(me, result);
		cm_ev_free(result);

		if (quorum_query(&result) == -1) {
			printf("quorum_query: %s\n", strerror(errno));
			return 1;
		}

		print_quorum(me, result);
		cm_ev_free(result);

		return 0;
	}

	membfd = cm_ev_register(EC_MEMBERSHIP);
	if (membfd == -1) {
		perror("cm_ev_register");
		return -1;
	}

	quorumfd = cm_ev_register(EC_QUORUM);
	if (quorumfd == -1) {
		perror("cm_ev_register");
		return -1;
	}

	while (1) {
		FD_ZERO(&rfds);
		FD_SET(membfd, &rfds);
		FD_SET(quorumfd, &rfds);

		select(1024, &rfds, NULL, NULL, NULL);

		if (FD_ISSET(membfd, &rfds)) {
			result = cm_ev_read(membfd);
			if (!result) {
				perror("cm_ev_read");
				return -1;
			}
		}

		if (!result && FD_ISSET(quorumfd, &rfds)) {
			result = cm_ev_read(quorumfd);
			if (!result) {
				perror("cm_ev_read");
				return -1;
			}
		}

		if (cm_ev_event(result) == EV_MEMB_UPDATE) {
			printf("\n\n=== Event from Membership Daemon ===\n");
			print_info(me, result);
			cm_ev_free(result);
			result = NULL;
		} else {
			printf("\n\n=== Event from Quorum Daemon ===\n");
			print_quorum(me, result);
			cm_ev_free(result);
			result = NULL;
		}
	}

	return 0;
}
