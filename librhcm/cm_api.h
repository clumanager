/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
 */
/** @file
 * User API include file.
 *
 * Author: Lon Hohberger <lhh at redhat.com>
 */
#ifndef _CM_API_H
#define _CM_API_H

#include <stdint.h>
#include <platform.h>		/** swab32/etc. */

/*
 * If you change these, be sure to change the others in RHCM!
 */
#define CM_MEMB_PORT 34001
#define CM_QUORUM_PORT 34003

/*
 * Membership type definition.  Just a bitmask for now.
 */
#define MIN_NODE_ID		0
#define MIN_NODES		1
#define MAX_NODES		16	/** At ship date, this will be 16 */
#define MAX_NODES_DISK		2	/** Max # of nodes for disk 
					    tiebreaker */
#define MAX_PARTS_DISK		2
#define MAX_NODE_ID		(MAX_NODES - 1)
#define MEMB_MASK_LEN		((MAX_NODES / 32) + (!!(MAX_NODES % 32)))

/** The membership bitmask type */
typedef uint32_t memb_mask_t [MEMB_MASK_LEN];

/*
 * APIs for manipulating membership masks.  These don't actually do anything
 * special; they manipulate bitmasks.
 */

/**
 * Is a specified member online?
 */
int memb_online(memb_mask_t mask, int member);

/**
 * Lowest member ID# online.
 */
int memb_low_id(memb_mask_t mask);

/**
 * Highest member ID# online.
 */
int memb_high_id(memb_mask_t mask);

/**
 * Number of members online in a given membership mask.
 */
int memb_count(memb_mask_t mask);

/**
 * Local member ID
 */
int memb_local_id(void);

/**
 * Convert a nodemask into a static string, suitable for printf/syslog/etc.
 * Destroys previous contents (think strerror()).
 * XXX Not thread safe.
 */
char *memb_mask_str(memb_mask_t mask);

/**
 * Mark a member online in a bitmask...  This, of course, only modifies an
 * application's private view of the membership mask - and doesn't cause
 * member transitions, etc.
 */
#define memb_mark_down(mask, member) \
	clear_bit((uint32_t *)mask, member, MEMB_MASK_LEN)

/**
 * Mark a member offline in a bitmask.  Same rule as above.
 */
#define memb_mark_up(mask, member) \
	set_bit((uint32_t *)mask, member, MEMB_MASK_LEN)


/**
 * What members were lost between an old view and a new (more recent) view?
 *
 * @param dest		Destination membership mask.
 * @param old_view	Current membership mask.
 * @param new_view	New membership mask. 
 */
int memb_mask_lost(memb_mask_t dest, memb_mask_t old_view,
		    memb_mask_t new_view);

/**
 * What members were gained between an old view and a new (more recent) view?
 *
 * @param dest		Destination membership mask.
 * @param old_view	Current membership mask.
 * @param new_view	New membership mask. 
 */
int memb_mask_gained(memb_mask_t dest, memb_mask_t old_view,
       		     memb_mask_t new_view);


/**
 * Register for events.  Returns a file descriptor, or -1 with 
 * errno set appropriately.  If errno == ECONNREFUSED, it generally means
 * that the cluster software isn't running.
 *
 * Registration for events may not be changed.  Unregister and reregister.
 * This is because membership messages without quorum messages may sometimes
 * be desired (by, for instance, the quorum daemon ;) ), and will then bypass
 * the quorum daemon (alleviating the quorum daemon from being the sole
 * provider of messages) - whereas with both membership messages and quorum
 * messages (or simply quorum messages), events are passed from the quorum
 * daemon.
 *
 * @param event_class	The class of events to which we would like to
 *			subscribe.
 * @return		-1 on failure, or a file descriptor (>= 0) on success.
 */
int cm_ev_register(int event_class);

/** 
 * Close a registration descriptor.
 *
 * @param fd		File descriptor to close.
 * @return		See man close (2)
 */
int cm_ev_unregister(int fd);

/** 
 * Cluster event magic #
 */
#define CLUSTER_MAGIC 0x0bad0ace

/**
 * Event classes: Important(tm) cluster events.  Membership changes, quorum
 * changes, and of course - local-member shutdown events. 
 */
#define EC_NONE		0	/** ... */
#define EC_MEMBERSHIP	1	/** Membership change events */
#define EC_QUORUM	2	/** Quorum events */

/** You can't actually register for these... */
#define EC_QUORUM_TB	4	/** Quorum tiebreaker events */


/*
 * Important(tm) cluster events.
 */
#define EV_NULL			0xCC00	/* Debug... */
#define EV_REGISTER		0xCC01	/** Register for events. */
#define EV_UNREGISTER		0xCC02	/**
					 * Kill registration for events.
					 * ... this is currently not 
					 * implemented, as close(2) works
					 * just fine.
					 */

#define EV_MEMB_UPDATE		0xCC04	/** [Committed] change in membership */
#define EV_NACK			0xCC05	/** NACK. */
#define EV_ACK			0xCC06	/** ACK. */

#define EV_QUORUM_GAINED	0x600D	/** Quorum gained.  Good. */
#define EV_QUORUM_LOST		0x0BAD	/** Quorum lost.  Bad. */
#define EV_QUORUM		0xCC07	/** Quorum maintained */
#define EV_NO_QUORUM		0xCC08	/** Quorum still not reached */
#define EV_SHUTDOWN		0xDEAD	/** Local member going down (cleanly) */

/**
 * Synchronous query messages.
 */
#define MEMB_QUERY		0x1313  /** Local membership query req. */
#define MEMB_QUERY_NODEID	0x1314  /** Local member ID query */
#define QUORUM_QUERY		0x1413  /** Local quorum query */
#define QUORUM_QUERY_TB		0x1414	/** Local tiebreaker query */


/**
 * Results of quorum_tb_query
 */
#define EV_TBF_ONLINE		(1<<0)	/** Flagged as online */
#define EV_TBF_NONE		(1<<1)	/** No tiebreaker in use */
#define EV_TBF_DISK		(1<<4)	/** Disk in use */
#define EV_TBF_DISK_OTHER	(1<<5)	/** Other online, disk only */
#define EV_TBF_NET		(1<<8)	/** Net in use */
#define EV_TBF_NET_SOFT		(1<<9)	/** Soft quorum, net only */


/**
 * Internal structure defining the header of a cluster manager event.
 * This should be the same size as generic_msg_hdr for now.
 */
typedef struct __attribute__ ((packed)) _event_hdr {
	uint32_t	eh_magic;
	uint32_t	eh_length;	/**< Payload length */
	uint32_t	eh_type;	/**< Event type. */
	uint32_t	eh_event;	/**< Actual event */
	uint32_t	eh_memberid;	/**< Member ID of sender */
} cm_event_hdr_t;

#define swab_cm_event_hdr_t(ptr) {\
	swab32((ptr)->eh_magic);\
	swab32((ptr)->eh_length);\
	swab32((ptr)->eh_type);\
	swab32((ptr)->eh_event);\
	swab32((ptr)->eh_memberid);\
}


/**
 * Membership event (data only).
 */
typedef struct __attribute__ ((packed)) _membership_msg {
	uint64_t	mm_view;
	memb_mask_t	mm_mask;
} cm_memb_event_t;


/**
 * cm_ev_read() handles un-swabbing of membership events for us, but
 * here is what it does (for completeness):
 */
#define swab_cm_memb_event_t(ptr, c) {\
	swab64((ptr)->mm_view);\
	for (c=0; c<MEMB_MASK_LEN; c++)\
		swab32((ptr)->mm_mask[c]);\
}


/**
 * Quorum event (data only; requires a header)
 */
typedef struct __attribute__ ((packed)) _quorum_msg {
	uint64_t	qm_view;		/**< View number */
	memb_mask_t	qm_mask;		/**< Membership mask */
	memb_mask_t	qm_mask_panic;		/**< Mask of members in panic
						     state */
} cm_quorum_event_t;


/**
 * cm_ev_read() handles un-swabbing of quorum events for us, but
 * here is what it does (for completeness):
 */
#define swab_cm_quorum_event_t(ptr, c) {\
	swab64((ptr)->qm_view);\
	for (c=0; c<MEMB_MASK_LEN; c++)\
		swab32((ptr)->qm_mask[c]);\
	for (c=0; c<MEMB_MASK_LEN; c++)\
		swab32((ptr)->qm_mask_panic[c]);\
}


/**
 * API Event (all types).  Any new event types should be included in the
 * union.  This is subject to change; use the macros and such to handle
 * these.
 */
typedef struct __attribute__ ((packed)) _event_msg {
	cm_event_hdr_t	em_header;
	union {
		cm_memb_event_t		ev_memb;
		cm_quorum_event_t	ev_quorum;
	} u;
} cm_event_t;

cm_event_t *cm_ev_read(int fd);
void cm_ev_free(cm_event_t *eventp);

/*
 * Handy macros for getting at our data when we receive an event.  These
 * should not be assumed to be macros; and may be functions at some point.
 * The structure of cm_event_t is subject to change, so do not access
 * its members directly.
 */

/**
 * Determine the event type of the message.
 */
#define cm_ev_type(msgp)	((cm_event_t *)(msgp))->em_header.eh_type

/**
 * Determine the actual event of the message 
 */
#define cm_ev_event(msgp)	((cm_event_t *)(msgp))->em_header.eh_event

/**
 * Find out the member ID of the sender (for now, this is always the local
 * member)
 */
#define cm_ev_memberid(msgp)	((cm_event_t *)(msgp))->em_header.eh_memberid

/*
 * ... event specific stuff ...
 */

/**
 * Point to the memb_mask_t membership view contained within a 
 * membership update.
 */
#define cm_memb_mask(msgp)	((cm_event_t *)(msgp))->u.ev_memb.mm_mask

/**
 * Point to the membership view number contained within a 
 * membership update.
 */
#define cm_memb_view(msgp)	((cm_event_t *)(msgp))->u.ev_memb.mm_view


/**
 * Point to the memb_mask_t membership view contained within a 
 * quorum event.
 */
#define cm_quorum_mask(msgp)	((cm_event_t *)(msgp))->u.ev_quorum.qm_mask
#define cm_quorum_mask_panic(msgp) \
	((cm_event_t *)(msgp))->u.ev_quorum.qm_mask_panic

/**
 * Point to the quorum view number contained within a quorum event.
 */
#define cm_quorum_view(msgp)	((cm_event_t *)(msgp))->u.ev_quorum.qm_view

/** Synchronous local query for membership */
int memb_query(cm_event_t **result);

/** Synchronous local query for quorum status */
int quorum_query(cm_event_t **result);

/** Synchronous local tiebreaker query for TB status */
int quorum_query_tb(cm_event_t **result);

#endif /* _CM_API_H */
