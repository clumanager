#ifndef MD5_H
#define MD5_H

#include <stdint.h>

struct MD5Context {
	uint32_t buf[4];
	uint32_t bits[2];
	uint8_t  in[64];
};

void MD5Init(struct MD5Context *context);
void MD5Update(struct MD5Context *context, uint8_t const *buf,
	       unsigned len);
void MD5Final(uint8_t digest[16], struct MD5Context *context);
void MD5Transform(uint32_t buf[4], uint32_t const in[16]);
void clu_md5(char *buf, uint32_t buflen, char *md5sum);

/*
 * This is needed to make RSAREF happy on some MS-DOS compilers.
 */
typedef struct MD5Context MD5_CTX;
#define md5_init MD5Init
#define md5_state_t MD5_CTX 
#define md5_append MD5Update
#define md5_finish MD5Final

#endif /* !MD5_H */

