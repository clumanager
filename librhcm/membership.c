/*
  Copyright Red Hat, Inc. 2002-2003

  The Red Hat Cluster Manager API Library is free software; you can
  redistribute it and/or modify it under the terms of the GNU Lesser
  General Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any later
  version.

  The Red Hat Cluster Manager API Library is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
 */
/** @file
 * Membership API functions.
 */
#include <cm_api.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <string.h>
#include <apitcp.h>
#include <platform.h>

static int __local_member_id = -1;
static pthread_mutex_t __local_member_id_lock = PTHREAD_MUTEX_INITIALIZER;

/**
 * Register for membership events 
 *
 * @return		File descriptor, or -1 if clumembd couldn't be
 *			reached.
 */
int
memb_register(void)
{
	int fd;
	cm_event_hdr_t msg;

	/*
	 * Talks to the local member.  If the user hasn't read the
	 * configuration file, it talks to the loopback interface,
	 * since the membership daemon will also be listening on it.
	 */
	fd = tcp_localconnect(CM_MEMB_PORT);

	if (fd == -1)
		return -1;

	msg.eh_magic = CLUSTER_MAGIC;
	msg.eh_type = EV_REGISTER;
	msg.eh_length = sizeof(msg);

	swab_cm_event_hdr_t(&msg);

	/*
	 * Send query
	 */
	if (tcp_send(fd, &msg, sizeof(msg)) != sizeof(msg)) {
		close(fd);
		return -1;
	}

	/*
	 * Receive response
	 */
	if (tcp_receive(fd, (void *)&msg, sizeof(msg)) != sizeof(msg)) {
		close(fd);
		errno = ETIMEDOUT;
		return -1;
	}

	swab_cm_event_hdr_t(&msg);

	if (msg.eh_type != EV_ACK) {
		close(fd);
		errno = EAGAIN;
		return -1;
	}

	return fd;
}


/**
 * Query the membership daemon synchronously for the current status o
 * membership. This ALWAYS talks to the local member, and so running it from
 * remote is not possible.  This does not require the caller to know anything
 * about the cluster configuration, but DOES require the cluster be running
 * on the local member.
 *
 * @param result	Unallocated space which will be allocated within
 *			and returned to the user.
 * @return		-1 on failure, 0 on success.
 */
int
memb_query(cm_event_t **result)
{
	int fd, counter;
	cm_event_hdr_t msg;
	cm_event_t *view;

	if (!result)
		return -1;

	/*
	 * Talks to the local member over loopback
	 */
	fd = tcp_localconnect(34001);

	if (fd == -1)
		return -1;

	msg.eh_magic = CLUSTER_MAGIC;
	msg.eh_type = MEMB_QUERY;
	msg.eh_length = sizeof(msg);
	swab_cm_event_hdr_t(&msg);

	/*
	 * Send query
	 */
	if (tcp_send(fd, &msg, sizeof(msg)) != sizeof(msg)) {
		close(fd);
		return -1;
	}

	*result = malloc(sizeof(*view));
	view = (cm_event_t *)*result;

	/*
	 * Receive response
	 */
	if (tcp_receive(fd, (void *)view, sizeof(*view)) !=
	    sizeof(*view)) {
		close(fd);
		free(view);
		errno = ETIMEDOUT;
		return -1;
	}

	swab_cm_event_hdr_t(&view->em_header);
	swab_cm_memb_event_t(&view->u.ev_memb, counter);

	pthread_mutex_lock(&__local_member_id_lock);
	if (__local_member_id == -1)
		__local_member_id = cm_ev_memberid(view);
	pthread_mutex_unlock(&__local_member_id_lock);

	close(fd);
	return 0;
}


/**
 * Query the local membership daemon for the local member id. This doesn't
 * require any knowledge of configuration information on the part of the 
 * caller.
 *
 * @return		-1 on failure, or the member ID on success.
 */
int
memb_local_id(void)
{
	int fd, rv;
	cm_event_hdr_t msg;

	/*
	 * See if we have it locally...
	 */
	pthread_mutex_lock(&__local_member_id_lock);
	if (__local_member_id != -1) {
		rv = __local_member_id;
		pthread_mutex_unlock(&__local_member_id_lock);
		return rv;
	}
	pthread_mutex_unlock(&__local_member_id_lock);

	/*
	 * Fallback: Ask the membership daemon for the information we require.
	 */
	fd = tcp_localconnect(34001);
	if (fd == -1)
		return -1;

	msg.eh_magic = CLUSTER_MAGIC;
	msg.eh_type= MEMB_QUERY_NODEID;
	msg.eh_length = sizeof(msg);

	swab_cm_event_hdr_t(&msg);

	/*
	 * Send query
	 */
	if (tcp_send(fd, &msg, sizeof(msg)) != sizeof(msg)) {
		close(fd);
		return -1;
	}

	/*
	 * Receive response
	 */
	if (tcp_receive(fd, (void *)&msg, sizeof(msg)) !=
	    sizeof(msg)) {
		close(fd);
		errno = ETIMEDOUT;
		return -1;
	}

	swab_cm_event_hdr_t(&msg);
	close(fd);
	if (msg.eh_type != EV_ACK) {
		return -1;
	}

	pthread_mutex_lock(&__local_member_id_lock);
	__local_member_id = msg.eh_memberid;
	rv = __local_member_id;
	pthread_mutex_unlock(&__local_member_id_lock);
	return rv;
}


/**
 * Determine the members lost (went down) between two membership views.
 *
 * @param dest		Will be filled with mask of all memberss which went 
 *			down.
 * @param old_view	The old membership view.
 * @param new_view	The new membership view.
 * @return		The number of members which were lost between the two
 *			membership masks.
 */
int
memb_mask_lost(memb_mask_t dest, memb_mask_t old_view, memb_mask_t new_view)
{
	memb_mask_t tmp;
	int x;

	memset(tmp, 0, sizeof(memb_mask_t));
	for (x=0; x<MEMB_MASK_LEN; x++)
		tmp[x] = (old_view[x] ^ new_view[x]) & old_view[x];
	memcpy(dest, tmp, sizeof(memb_mask_t));

	return memb_count(tmp);
}


/**
 * Determine the members gained (came up) between two membership views.
 *
 * @param dest		Will be filled with mask of all members which came up.
 * @param old_view	The old membership view.
 * @param new_view	The new membership view.
 * @return		The number of members which were gained between the two
 *			membership masks.
 */
int
memb_mask_gained(memb_mask_t dest, memb_mask_t old_view, memb_mask_t new_view)
{
	memb_mask_t tmp;
	int x;

	memset(tmp, 0, sizeof(memb_mask_t));
	for (x=0; x<MEMB_MASK_LEN; x++)
		tmp[x] = (old_view[x] ^ new_view[x]) & new_view[x];
	memcpy(dest, tmp, sizeof(memb_mask_t));
	
	return memb_count(tmp);
}
