/*
  Copyright Red Hat, Inc. 2003

  The Red Hat Cluster Manager API Library is free software; you can
  redistribute it and/or modify it under the terms of the GNU Lesser
  General Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any later
  version.

  The Red Hat Cluster Manager API Library is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
*/
/** @file
 * Quorum API functions.
 */
#include <cm_api.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <apitcp.h>

static int __local_member_id = -1;
static pthread_mutex_t __local_member_id_lock = PTHREAD_MUTEX_INITIALIZER;

/**
 * Register for quorum and/or quorum-proxied membership events.
 *
 * @return		File descriptor, or -1 if clumembd couldn't be
 *			reached.
 */
int
quorum_register(void)
{
	int fd;
	cm_event_hdr_t msg;

	/*
	 * Talks to the local node.  If the user hasn't read the configuration
	 * file, it talks to the loopback interface, since the quorum
	 * daemon will also be listening on it.
	 */
	fd = tcp_localconnect(CM_QUORUM_PORT);
	if (fd == -1)
		return -1;

	msg.eh_magic = CLUSTER_MAGIC;
	msg.eh_type = EV_REGISTER;
	msg.eh_length = sizeof(msg);

	swab_cm_event_hdr_t(&msg);

	/*
	 * Send query
	 */
	if (tcp_send(fd, &msg, sizeof(msg)) != sizeof(msg)) {
		close(fd);
		return -1;
	}

	/*
	 * Receive response
	 */
	if (tcp_receive(fd, (void *)&msg, sizeof(msg)) !=
	    sizeof(msg)) {
		close(fd);
		errno = ETIMEDOUT;
		return -1;
	}

	swab_cm_event_hdr_t(&msg);

	if (msg.eh_type != EV_ACK) {
		close(fd);
		errno = EAGAIN;
		return -1;
	}

	return fd;
}


/**
 * Query the quorum daemon synchronously for the current status of
 * quorum. This ALWAYS talks to the local node, and so running it from
 * remote is not possible.  This does not require the caller to know anything
 * about the cluster configuration.
 *
 * @param result	Unallocated space which will be allocated within
 *			and returned to the user.
 * @return		-1 on failure, 0 on success.
 */
int
quorum_query(cm_event_t **result)
{
	int fd, counter;
	cm_event_hdr_t msg;
	cm_event_t *view;

	if (!result)
		return -1;

	/*
	 * Talks to the local node.  If the user hasn't read the configuration
	 * file, it talks to the loopback interface, since the quorum
	 * daemon will also be listening on it.
	 */
	fd = tcp_localconnect(34003);

	if (fd == -1)
		return -1;

	msg.eh_magic = CLUSTER_MAGIC;
	msg.eh_type = QUORUM_QUERY;
	msg.eh_length = sizeof(msg);

	swab_cm_event_hdr_t(&msg);

	/*
	 * Send query
	 */
	if (tcp_send(fd, &msg, sizeof(msg)) != sizeof(msg)) {
		close(fd);
		return -1;
	}

	*result = malloc(sizeof(*view));
	view = (cm_event_t *)*result;

	/*
	 * Receive response
	 */
	if (tcp_receive(fd, (void *)view, sizeof(*view)) !=
	    sizeof(*view)) {
		close(fd);
		free(view);
		errno = ETIMEDOUT;
		return -1;
	}

	swab_cm_event_hdr_t(&view->em_header);
	swab_cm_quorum_event_t(&view->u.ev_quorum, counter);

	pthread_mutex_lock(&__local_member_id_lock);
	if (__local_member_id == -1)
		__local_member_id = cm_ev_memberid(view);
	pthread_mutex_unlock(&__local_member_id_lock);

	close(fd);
	return 0;
}


/**
 * Query the quorum daemon synchronously for the current status of
 * quorum tiebreaker. This ALWAYS talks to the local node, and so running it
 * from remote is not possible.  This does not require the caller to know
 * anything about the cluster configuration.
 *
 * @param result	Unallocated space which will be allocated within
 *			and returned to the user.
 * @return		-1 on failure, 0 on success.
 */
int
quorum_query_tb(cm_event_t **result)
{
	int fd;
	cm_event_hdr_t msg;
	cm_event_t *view;

	if (!result)
		return -1;

	/*
	 * Talks to the local node.  If the user hasn't read the configuration
	 * file, it talks to the loopback interface, since the quorum
	 * daemon will also be listening on it.
	 */
	fd = tcp_localconnect(34003);

	if (fd == -1)
		return -1;

	msg.eh_magic = CLUSTER_MAGIC;
	msg.eh_type = QUORUM_QUERY_TB;
	msg.eh_length = sizeof(msg);

	swab_cm_event_hdr_t(&msg);

	/*
	 * Send query
	 */
	if (tcp_send(fd, &msg, sizeof(msg)) != sizeof(msg)) {
		close(fd);
		return -1;
	}

	*result = malloc(sizeof(*view));
	view = (cm_event_t *)*result;

	/*
	 * Receive response
	 */
	if (tcp_receive(fd, (void *)view, sizeof(view->em_header)) !=
	    sizeof(view->em_header)) {
		close(fd);
		free(view);
		errno = ETIMEDOUT;
		return -1;
	}

	swab_cm_event_hdr_t(&view->em_header);

	pthread_mutex_lock(&__local_member_id_lock);
	if (__local_member_id == -1)
		__local_member_id = cm_ev_memberid(view);
	pthread_mutex_unlock(&__local_member_id_lock);

	close(fd);
	return 0;
}

