/*
  Copyright Red Hat, Inc. 2002-2003

  The Red Hat Cluster Manager API Library is free software; you can
  redistribute it and/or modify it under the terms of the GNU Lesser
  General Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any later
  version.

  The Red Hat Cluster Manager API Library is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
 */
/** @file
 * Configuration Library using libxml2 Doc Tree.
 *
 * XXX Needs Doxygenification.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <sys/queue.h>
#include <signal.h>
#include <sys/param.h>
#include <sys/types.h>
#include <regex.h>			/* Regular Expression Matching */
#include <fnmatch.h>			/* Glob pattern matching */
#include <xmlconfig.h>
#include <fcntl.h>
#include <errno.h>


struct _uint_stack {
	struct _uint_stack *uis_next;
	unsigned int uis_val;
};


static inline int
_uis_push(struct _uint_stack **stack, int val)
{
	struct _uint_stack *new;

	if (!stack)
		return -1;
       
	if (!(new = malloc(sizeof(*new))))
		return -1;

	new->uis_next = *stack;
	new->uis_val = val;
	*stack = new;

	return 0;
}


static inline int
_uis_pop(struct _uint_stack **stack)
{
	struct _uint_stack *del;
	int rv;

	if (!*stack)
		return -1;

	del = *stack;
	*stack = (*stack)->uis_next;
	rv = del->uis_val;
	free(del);

	return rv;
}


static inline void
_uis_kill(struct _uint_stack **stack)
{
	while (_uis_pop(stack) != -1);
}


static inline xmlAttrPtr
_xmlGetAttrPtr(xmlNodePtr cur, const char *prop)
{
	xmlAttrPtr attr = cur->properties;

	for (; attr; attr = attr->next)
		if (!strcasecmp((char *)attr->name, prop))
			return attr;

	return NULL;
}


static inline int
_is_regex(const char *token)
{
	return (strcspn(token,"([.*|+])") != strlen(token));
}


/**
 * construct a list of all tokens in our XML tree in the form:
 * "tag%subtag%subtag1%abc123%property"
 * TODO optimize, optimize, optimize...
 */
int
xtree_tl_build(xmlDocPtr xtree, struct _token_list_head *head,
	       const char *m_pat, int m_type)
{
	xmlNodePtr cur = xmlDocGetRootElement(xtree);
	xmlAttrPtr attr = NULL;
	char token[MAX_TOKEN_LEN], *prop;
	unsigned int pos = 0, tmp;
	struct _uint_stack *stack = NULL;
	struct _token_list_node *tnode;
	int count = 0;

	MATCH_INIT(m_pat, m_type);

	if (!head || !cur) {
		MATCH_FREE(m_pat, m_type);
		return -1;
	}

	TAILQ_INIT(head);

	cur = cur->xmlChildrenNode;
	while (cur) {
		if (cur->type == XML_TEXT_NODE)
			goto next;

		strncpy(token + pos, (char *)cur->name, sizeof(token) - pos);
		prop = (char *)xmlGetProp(cur, (xmlChar *)"id");
		if (prop) {
			strncat(token + strlen(token), prop,
				MIN(strlen(prop) + 1,
				    sizeof(token) - strlen(token)));
			xmlFree(prop);
		}

		if (cur->xmlChildrenNode) {
			_uis_push(&stack, pos);
			cur = cur->xmlChildrenNode;
			strncat(token + strlen(token), "%",
				MIN(2, sizeof(token) - strlen(token)));
			pos = strlen(token);
			continue;
		}

		/* Here we tack on the properties */
		tmp = strlen(token);
		strncat(token + tmp, "%", MIN(2, sizeof(token) - tmp));
nochild:
		tmp = strlen(token);
		attr = cur->properties;
		while (attr) {
			if (!strcasecmp((char *)attr->name,"id")) {
				attr = attr->next;
				continue;
			}

			strncpy(token + tmp, (char *)attr->name,
				MIN(strlen((char *)attr->name) + 1,
				    sizeof(token) - tmp));

			if (!MATCH(token, m_pat, m_type)) {
				attr = attr->next;
				continue;
			}

			if (!(tnode = malloc(sizeof(*tnode)))) {
				perror("malloc");
				MATCH_FREE(m_pat, m_type);
				_uis_kill(&stack);
				return -1;
			}

			memset(tnode,0,sizeof(*tnode));

			if (!(tnode->tl_token = strdup(token))) {
				perror("strdup");
				MATCH_FREE(m_pat, m_type);
				_uis_kill(&stack);
				free(tnode);
				return -1;
			}

			if (!(tnode->tl_value = 
			      strdup((char *)attr->children->content))) {
				perror("strdup");
				MATCH_FREE(m_pat, m_type);
				_uis_kill(&stack);
				free(tnode->tl_token);
				free(tnode);
				return -1;
			}

			tnode->tl_terminal = attr;

			TAILQ_INSERT_TAIL(head, tnode, tl_chain);
			count++;
			attr = attr->next;
		}
		token[tmp] = 0;

next:
		if (cur->next) {
			cur = cur->next;
			continue;
		}

		if (pos) {
			token[pos] = 0;
			pos = _uis_pop(&stack);
			cur = cur->parent;
			goto nochild;
		}

		break;
	}

	_uis_kill(&stack);
	MATCH_FREE(m_pat,m_type);
	return count;
}


void
xtree_tl_free(struct _token_list_head *head)
{
	struct _token_list_node *cur, *back;

	cur = head->tqh_first;
	while (cur) {
		back = cur;
		cur = cur->tl_chain.tqe_next;

		TAILQ_REMOVE(head, back, tl_chain);
		free(back->tl_token);
		free(back->tl_value);
		free(back);
	}

	head->tqh_first = NULL;
}


static char *
_extract_int(char *chunk, char *rv, int rv_size)
{
	int id_loc;
	char *id;

	if ((id_loc = strcspn(chunk,"0123456789")) < strlen(chunk)) {
		id = chunk + id_loc;
		strncpy(rv, id, rv_size);
		*id = 0;
		return rv;
	}

	return NULL;
}


static int
_has_attributes(xmlNodePtr node)
{
	xmlAttrPtr attr = node->properties;

	while (attr) {
		if ((attr->type == XML_ATTRIBUTE_NODE) &&
		    strcasecmp((char *)attr->name,"id"))
			return 1;
		attr = attr->next;
	}

	return 0;
}


static int
_has_children(xmlNodePtr node)
{
	xmlNodePtr cur = node->xmlChildrenNode;

	while (cur) {
		if (node->type == XML_ELEMENT_NODE)
			return 1;
		cur = cur->next;
	}

	return 0;
}


int
_xtree_del(xmlAttrPtr attr)
{
	xmlNodePtr cur, pp;

	if (!attr)
		return 0;

	cur = attr->parent;
	xmlRemoveProp(attr);

	/* Ok, check if that was our last property/child */
	while (!_has_children(cur) && !_has_attributes(cur)) {
		pp = cur->parent;
		xmlUnlinkNode(cur);
		xmlFreeNode(cur);
		cur = pp;
	}

	return 0;
}


xmlAttrPtr
_xtree_find(xmlDocPtr xtree, const char *token)
{
	char		*tok = NULL,
			*part = NULL,
			*nextpart = NULL,
			*id = NULL,
			*id_check = NULL,
			buf[16];
	xmlNodePtr	cur = xmlDocGetRootElement(xtree),
			last_parent = NULL;
	xmlAttrPtr	rv = NULL;

	/* duplicate our inbound token, so we can tokenize it */
	if (!cur || (!(tok = malloc(strlen(token)+1))))
		return NULL;

	/* Set up */
	strcpy(tok,token);
	last_parent = cur;
	cur = cur->xmlChildrenNode;
	part = tok;

	while ((nextpart = strchr(part,'%')) && cur) {
		*nextpart = 0;			/* remove '%' */
		nextpart++;
		id = _extract_int(part, buf, sizeof(buf));

		while (cur) {			/* check for drop-to-child */
			if (xmlStrcmp(cur->name, (xmlChar *)part) != 0) {
				cur = cur->next;
				continue;
			}

			if (id) {
				/* compare the ID */
				id_check = (char *)xmlGetProp(cur,
							      (xmlChar *)"id");

				if (id_check && strcasecmp(id, id_check)) {
					cur = cur->next;
					xmlFree(id_check);
					continue;
				}

				if (id_check) 
					xmlFree(id_check);
				id = NULL;
			}

			last_parent = cur;
			cur = cur->xmlChildrenNode;
			break;
		}

		/* 
		 * if id is till set here, we have an id from above, but it
		 * wasn't found.
		 */
		if (id)
			break;

		part = nextpart;
	}

	/* clean up, etc. */
	if (!nextpart) {
		if (id)			/* ID not found! */
			rv = NULL;
		else			/* xml node property */ 
			rv = _xmlGetAttrPtr(last_parent, part);
	}

	free(tok);

	return rv;
}


/*
 * bottom-up deletion... ;)
 */
int
xtree_del(xmlDocPtr xtree, const char *token)
{
	return _xtree_del(_xtree_find(xtree,token));
}


int
xtree_set(xmlDocPtr xtree, const char *token, char *value)
{
	char		*tok = NULL,
			*part = NULL,
			*nextpart = NULL,
			*id_check = NULL,
			*id = NULL,
			child = 0,
			buf[16],
			sv;
	xmlNodePtr	cur = xmlDocGetRootElement(xtree),
			last_parent = NULL,
			newnode = NULL;

	/* Reject regexps in this case */
	if (_is_regex(token))
		return -1;

	/* duplicate our inbound token, so we can hack it up */
	if (!cur || !strlen(token) || (!(tok = malloc(strlen(token)+1))))
		return -1;

	/* Set up */
	strncpy(tok, token, strlen(token)+1);
	last_parent = cur;
	cur = cur->xmlChildrenNode;
	part = tok;

	nextpart = strchr(part, '%');
	while (nextpart) {
		*nextpart = 0;			/* remove '%' */
		nextpart++;

		id = _extract_int(part, buf, sizeof(buf));

		while (cur) {			/* check for drop-to-child */

			if (strcasecmp((char *)cur->name, part)) {
				cur = cur->next;
				continue;
			}

			if (id) {
				/* compare the ID */
				id_check = (char *)xmlGetProp(cur,
							      (xmlChar *)"id");

				if (strcasecmp(id, id_check)) {
					/* not the same */
					cur = cur->next;
					xmlFree(id_check);
					continue;
				}

				id = NULL;
				xmlFree(id_check);
			}

			last_parent = cur;
			cur = cur->xmlChildrenNode;
			part = nextpart;
			child = 1;
			break;
		}

		/*
		 * Ok, we dropped to child successfully.  No IDs needed,
		 * just continue
		 */
		if (child) {
			child = 0;
			nextpart = strchr(part, '%');
			continue;
		}

		/*
		 * ok, so we've run off the end of our token or the
		 * end of the XML tree.
		 */
		if (!nextpart)
			/* Attribute! */
			break;

		/* New regular node.  Add it. */
		sv = *nextpart;
		*nextpart = 0;
		newnode = xmlNewNode(NULL, (xmlChar *)part);
		*nextpart = sv;

		cur = xmlAddChild(last_parent, newnode);
		if (!cur) {
			xmlFreeNode(newnode);
			free(tok);
			return -1;
		}

		last_parent = cur;
		cur = newnode->xmlChildrenNode; /* same as NULL */

		/* Don't forget to continue eating the token */
		part = nextpart;
		nextpart = strchr(part, '%');

		/* set id property if it exists */
		if (id)
			xmlSetProp(newnode, (xmlChar *)"id", (xmlChar *)id);
	}

	/* Write out the property */
	if (!nextpart)
		xmlSetProp(last_parent, (xmlChar *)part, (xmlChar *)value);

	free(tok);
	return 0;
}


int
xtree_get(xmlDocPtr xtree, const char *token, char *dflt, char **value)
{
	xmlAttrPtr	attr;

	/* Reject regexps in this case */
	if (_is_regex(token)) {
		printf("token is regex\n");
		return -1;
	}

	attr = _xtree_find(xtree, token);

	if (attr && attr->children->content && 
	    strlen((char*)attr->children->content)) {
		if (value)
			*value = (char *)attr->children->content;
		return 0;
	}

	if (dflt) {
		if (value)
			*value = dflt;
		return 0;
	}

	return 1;
}


int
xtree_readfile(const char *filename, xmlDocPtr *xtreep)
{
	xmlNodePtr cur;

	xmlKeepBlanksDefault(0);
	xmlIndentTreeOutput = 1;

	*xtreep = xmlParseFile(filename);

	if (!*xtreep)
		return -1;

	if (!((cur = xmlDocGetRootElement(*xtreep)))) {
		xmlFreeDoc(*xtreep);
		*xtreep = NULL;
		return -1;
	}

	return 0;
}


int
xtree_readbuffer(const char *buffer, size_t size, xmlDocPtr *xtreep)
{
	xmlNodePtr cur;

	xmlKeepBlanksDefault(0);
	xmlIndentTreeOutput = 1;

	*xtreep = xmlParseMemory(buffer, size);

	if (!*xtreep) {
		printf("parse failure %p %d\n", buffer, (int)size);
		return -1;
	}

	if (!((cur = xmlDocGetRootElement(*xtreep)))) {
		printf("root element failure\n");
		xmlFreeDoc(*xtreep);
		*xtreep = NULL;
		return -1;
	}

	return 0;
}


int
xtree_writefile(const char *filename, xmlDocPtr xtree)
{
	char tmpfn[1024];
	int fd, tmpfd;
	xmlChar *buffer;
	struct flock flock;
	int n, remain, written, size = 0;

	snprintf(tmpfn, sizeof(tmpfn), "%s.XXXXXX", filename);
	tmpfd = mkstemp(tmpfn);
	if (tmpfd == -1)
		return -1;

	memset(&flock, 0, sizeof(flock));
	flock.l_type = F_WRLCK;

	fd = open(filename, O_WRONLY | O_CREAT | O_SYNC);
	if (fd == -1) {
		n = errno;
		close(tmpfd);
		unlink(tmpfn);
		errno = n;
		return -1;
	}

	while (fcntl(fd, F_SETLKW, &flock) == -1) {
		if (errno == EINTR)
			continue;
		n = errno;
		close(fd);
		close(tmpfd);
		unlink(tmpfn);
		errno = n;
		return -1;
	}

	xmlDocDumpFormatMemory(xtree, (xmlChar **)&buffer, (int *)&size, 1);

	written = 0;
	remain = size;
	while (remain) {
		n = write(tmpfd, buffer + written, remain);

		if (n == -1) {
			if (errno == EINTR)
				continue;
				
			free(buffer);
			n = errno;
			close(fd);
			close(tmpfd);
			unlink(tmpfn);
			errno = n;
			return -1;
		}
			
		written += n;
		remain -= n;
	}

	xmlFree(buffer);
	if (rename(tmpfn, filename) == -1) {
		n = errno;
		close(fd);
		close(tmpfd);
		unlink(tmpfn);
		errno = n;
		return -1;
	}

	close(fd);
	fsync(tmpfd);
	close(tmpfd);
	return 0;
}


int
xtree_writebuffer(xmlDocPtr xtree, char **buffer, size_t *size)
{
	*size = 0;
	xmlDocDumpFormatMemory(xtree, (xmlChar **)buffer, (int *)size, 1);
	return 0;
}


void
tlist_dump(struct _token_list_head *lh)
{
	struct _token_list_node *cur;

	cur = lh->tqh_first;
	while (cur) {
		printf("%s => %s\n", cur->tl_token,
		       cur->tl_terminal->children->content);
		cur = cur->tl_chain.tqe_next;
	}
}
