/*
  Copyright Red Hat, Inc. 2002-2003

  The Red Hat Cluster Manager API Library is free software; you can
  redistribute it and/or modify it under the terms of the GNU Lesser
  General Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any later
  version.

  The Red Hat Cluster Manager API Library is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
 */
/** @file
 * Header for xmlconfig.c.
 */
#ifndef _XMLCONFIG_H
#define _XMLCONFIG_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <sys/queue.h>
#include <signal.h>
#include <sys/param.h>
#include <sys/types.h>
#include <regex.h>			/* Regular Expression Matching */
#include <fnmatch.h>			/* Glob pattern matching */

#define MAX_TOKEN_LEN 2048

/**
 * A node in a list of token,value pairs.
 *
 * This is used to extract information from the database using a regex or
 * glob pattern match.
 */
typedef struct _token_list_node {
	TAILQ_ENTRY(_token_list_node) tl_chain; /**< See sys/queue.h */
	char *tl_token;			/**< Actual config token */
	char *tl_value;			/**< for thread safety, create a copy */
	xmlAttrPtr tl_terminal;		/**< INTERNAL use only; use tl_value */
} token_list_node;

/**
 * Define token list head.
 */
TAILQ_HEAD(_token_list_head,_token_list_node);	

typedef struct _token_list_head token_list_head;

#define MATCH_EXACT 0
#define MATCH_REGEX 1
#define MATCH_GLOB 2

#define MATCH(str, pattern, type) \
	( !pattern || \
	 ((type == MATCH_REGEX) && \
	  !regexec(&rx##pattern, str, 0, NULL, 0)) ||\
	 ((type == MATCH_GLOB) && \
	  !fnmatch(pattern, str, 0)) || \
	 ((type == MATCH_EXACT) && \
	  !strcmp(pattern, str)))

#define MATCH_INIT(pattern, type) \
	regex_t rx##pattern; \
	if (pattern && (type == MATCH_REGEX)) \
		regcomp(&rx##pattern, pattern, REG_EXTENDED | REG_NOSUB | REG_ICASE)

#define MATCH_FREE(pattern, type) \
	if (pattern && (type == MATCH_REGEX)) \
		regfree(&rx##pattern)

/* XML Get/Set/etc. APIs */
int xtree_del(xmlDocPtr xtree, const char *token);
int xtree_set(xmlDocPtr xtree, const char *token, char *value);
int xtree_get(xmlDocPtr xtree, const char *token, char *dflt, char **value);
int xtree_readfile(const char *filename, xmlDocPtr *xtreep);
int xtree_readbuffer(const char *buffer, size_t size, xmlDocPtr *xtreep);
int xtree_writefile(const char *filename, xmlDocPtr xtree);
int xtree_writebuffer(xmlDocPtr xtree, char **buf, size_t *bufsize);

/*
 * Token List Operations
 */
int xtree_tl_build(xmlDocPtr xtree, struct _token_list_head *head,
		   const char *m_pat, int m_type);
void xtree_tl_free(struct _token_list_head *head);

#define xtree_tl_walk(ptr, head) \
	for(ptr = head->lh_first; ptr; ptr = ptr->tl_chain.le_next)
#define xtree_tl_token(ptr) ptr->tl_token
#define xtree_tl_value(ptr) ptr->tl_value


#ifdef DEBUG
void tlist_dump(struct _token_list_head *lh);
#endif

#endif
