/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * CFG_xxx API -> XML Config API Wrapper.
 *
 * XXX NOT THREAD SAFE - need to return copies of strings!
 * XXX Needs Doxygenification.
 */
#include <xmlconfig.h>
#include <xmlwrap.h>

#ifndef NO_SHARED_IO
#include <clushared.h>
#include <sharedstate.h>
#endif

#include <pthread.h>
#include <namespace.h>
#include <clushared.h>
#include <errno.h>

static xmlDocPtr config_db = NULL;
static pthread_mutex_t config_db_mutex = PTHREAD_MUTEX_INITIALIZER;

int CFG_Initialized(void);
int CFG_Loaded(void);
int CFG_Set(const char *token, char *value);
int CFG_Get(const char *token, char *dflt, char **value);
int CFG_ReadFile(const char *filename);
int CFG_Read(void);
int CFG_WriteFile(const char *filename);
int CFG_Write(void);
int CFG_Remove(const char *token);
int CFG_RemoveMatch(const char *token, int mtype);
void CFG_Destroy(void);
xmlDocPtr CFG_Get_Pointer(void);

/*
 * Non-locked internals so we don't have to worry about recursive nonportabtle
 * pthread_mutex_t...
 */
static int cfg_readfile_nt(const char *filename);
#ifndef NO_SHARED_IO
static int cfg_readbuffer_nt(void);
#endif
static void cfg_destroy_nt(void);


/**
 * Is there an initialized config db?
 * @return	1 if a config database is present, 0 if not
 * @see CFG_Initialized
 */
int
CFG_Initialized(void)
{
	pthread_mutex_lock(&config_db_mutex);
	if (config_db) {
		pthread_mutex_unlock(&config_db_mutex);
		return 1;
	}
	pthread_mutex_unlock(&config_db_mutex);
	return 0;
}


/**
 * Is the subsystem initialized?
 * XXX this probably should do something different from CFG_Initialized()
 * @return	1 if a config database is present, 0 if not
 * @see CFG_Initialized
 */
int
CFG_Loaded(void)
{
	pthread_mutex_lock(&config_db_mutex);
	if (config_db) {
		pthread_mutex_unlock(&config_db_mutex);
		return 1;
	}
	pthread_mutex_unlock(&config_db_mutex);
	return 0;
}

/**
 * Sets a token to a value in a config database database.
 *
 * @param token 	The token in the form of "tag%sub1%subsub1%attribute"
 * @param value		The value (string) to set.
 * @return		CFG_BAD_KEY if the key is invalid, CFG_FALSE if
 *			it wasn't possible to store the value, or CFG_OK on
 *			success.
 */
int
CFG_Set(const char *token, char *value)
{
	int rv;

	pthread_mutex_lock(&config_db_mutex);
	if (!config_db) {
		pthread_mutex_unlock(&config_db_mutex);
		return CFG_FALSE;
	}

	rv = xtree_set(config_db, token, value);
	pthread_mutex_unlock(&config_db_mutex);

	switch (rv) {
	case -1:
		return CFG_BAD_KEY;
	case 0:
		return CFG_OK;
	}

	return CFG_FALSE;
}


/**
 * Retrieve a value from the configuration database, given a token.
 *
 * @param token		The token in the form of "tag%sub1%subsub1%attribute"
 * @param dflt		The default value, if any, to return.
 * @param value		A pointer to a string pointer.  Do not free the memory
 * 			returned! XXX This is broken and NOT thread safe. ;(
 *			It should return a newly allocated string which can
 *			be freed.
 * @return		CFG_BAD_KEY if the key was invalid, CFG_DEFAULT if no
 *			match was found but a default was specified, CFG_OK
 			on success, or CFG_FALSE if no value was found.
 */
int
CFG_Get(const char *token, char *dflt, char **value)
{
	int rv;
	char *val_priv;

	pthread_mutex_lock(&config_db_mutex);
	rv = xtree_get(config_db, token, dflt, &val_priv);
	pthread_mutex_unlock(&config_db_mutex);

	switch(rv) {
	case -1:
		return CFG_BAD_KEY;
	case 0:
		if (!strcasecmp(val_priv, "none")) {
			if (dflt) {
				*value = dflt;
				/* "none" is a reserved word here */
				return CFG_DEFAULT;
			}
			*value = NULL;
			return CFG_FALSE;
		}

		if (value) {
			*value = val_priv;
			if (*value == dflt)
				return CFG_DEFAULT;
		}

		return CFG_OK;
	case 1:
		return CFG_DEFAULT;
	}

	return CFG_FALSE;
}


static int
cfg_readfile_nt(const char *filename)
{
	cfg_destroy_nt();
	return xtree_readfile(filename, &config_db);
}


/**
 * Read a local config file into memory.
 *
 * @param filename	File to read.
 * @return		CFG_OK on success, CFG_PARSE_FAILED if the file could
 *			could not be parsed, or CFG_FALSE for other errors (ie,
 *			the file didn't exist)
 * @see CFG_Read
 */
int
CFG_ReadFile(const char *filename)
{
	int rv;
	char *realfn;

	pthread_mutex_lock(&config_db_mutex);
	if (filename && strlen(filename))
		realfn = (char *)filename;
	else
		realfn = CLU_CONFIG_FILE;

	rv = cfg_readfile_nt(realfn);
	pthread_mutex_unlock(&config_db_mutex);

	switch(rv) {
	case 0:
		return CFG_OK;
	case -1:
		return CFG_PARSE_FAILED;
	}
	return CFG_FALSE;
}


#ifndef NO_SHARED_IO
static int
cfg_readbuffer_nt(void)
{
	char *buffer;
	int sz, rv;
	SharedHeader hdr;

	if (shared_storage_init() == -1)
		return -1;

	rv = sh_stat(NS_D_CLUSTER "/" NS_F_CONFIG, &hdr);
	if (rv == -1)
		return -1;

	sz = (int)hdr.h_length;
	buffer = malloc(sz+1);
	if (!buffer)
		return -1;

	cfg_destroy_nt();
	memset(buffer,0,sz+1);
	if (sh_read_atomic(NS_D_CLUSTER "/" NS_F_CONFIG, buffer, sz) == -1) {
		printf("failed reading " NS_D_CLUSTER "/" NS_F_CONFIG ": %s\n",
		       strerror(errno));

		free(buffer);
		return -1;
	}

	rv = xtree_readbuffer(buffer, sz+1, &config_db);

	free(buffer);
	return rv;
}
#endif


/**
 * Read the configuration file from shared state into memory.  This requires
 * shared_storage_init() to be called first!
 *
 * @return		CFG_OK on success, CFG_PARSE_FAILED if the file could
 *			could not be parsed.
 */
int
CFG_Read(void)
{
#ifndef NO_SHARED_IO
	int rv;

	pthread_mutex_lock(&config_db_mutex);
	rv = cfg_readbuffer_nt();
	pthread_mutex_unlock(&config_db_mutex);

	if (rv == -1)
		return CFG_PARSE_FAILED;
	return CFG_OK;
#else
	errno = -ENOSYS;
	return CFG_FALSE;
#endif
}


static void
cfg_destroy_nt(void)
{
	if (config_db) {
		xmlFreeDoc(config_db);
		config_db = NULL;
	}
}


/**
 * Nuke the in-memory configuration database.
 */
void
CFG_Destroy(void)
{
	pthread_mutex_lock(&config_db_mutex);
	cfg_destroy_nt();
	pthread_mutex_unlock(&config_db_mutex);
}


/**
 * Write the contents of the in-memory configuration database to a file.
 *
 * @param filename	File to write.
 * @return		CFG_FALSE on failure, CFG_OK on success.
 * @see CFG_WriteBuffer, CFG_Write
 */
int
CFG_WriteFile(const char *filename)
{
	int rv, esv;
	pthread_mutex_lock(&config_db_mutex);
	rv = xtree_writefile(filename, config_db);
	esv = errno;
	pthread_mutex_unlock(&config_db_mutex);

	if (rv == -1) {
		errno = esv;
		return CFG_FALSE;
	}

	return CFG_OK;
}


/**
 * Write the contents of the in-memory configuration database to a buffer.
 * The buffer is allocated in-line, and must be freed by the caller..
 *
 * @param buf		Return buffer pointer.
 * @param buflen	Return buffer size.
 * @return		CFG_FALSE on failure, CFG_OK on success.
 * @see CFG_WriteFile, CFG_Write
 */
int
CFG_WriteBuffer(char **buf, size_t *buflen)
{
	int rv; 

	pthread_mutex_lock(&config_db_mutex);
	rv = xtree_writebuffer(config_db, buf, buflen);
	pthread_mutex_unlock(&config_db_mutex);

	if (rv == -1)
		return CFG_FALSE;

	return CFG_OK;
}


/**
 * Writes the configuration database from memory to shared state.
 *
 * @return		CFG_FALSE on failure, CFG_OK on success.
 * @see CFG_WriteFile
 */
int
CFG_Write(void)
{
#ifndef NO_SHARED_IO
	int rv;
	size_t bufsize;
	char *buf = NULL;

	pthread_mutex_lock(&config_db_mutex);
	rv = xtree_writebuffer(config_db, &buf, &bufsize);
	pthread_mutex_unlock(&config_db_mutex);

	if (rv == -1) {
		if (buf)
			free(buf);
		return CFG_FALSE;
	}
	
	rv = sh_write_atomic(NS_D_CLUSTER "/" NS_F_CONFIG, buf, bufsize);
	if (rv == -1) {
		rv = errno;
		free(buf);
		errno = rv;
		return CFG_FALSE;
	}
	free(buf);

	return CFG_OK;
#else
	errno = -ENOSYS;
	return CFG_FALSE;
#endif
}


/**
 * Remove a token from the in-memory config database.
 *
 * @param token		Token to remove.
 * @return 		CFG_FALSE on failure, or CFG_OK on success.
 */
int
CFG_Remove(const char * token)
{
	int rv;
	pthread_mutex_lock(&config_db_mutex);
	rv = xtree_del(config_db, token);
	pthread_mutex_unlock(&config_db_mutex);

	return (rv == -1)?CFG_FALSE:CFG_OK;
}


int _xtree_del(xmlAttrPtr attr);
int
CFG_RemoveMatch(const char *pattern, int mtype)
{
	token_list_head head;
	token_list_node *cur;

	pthread_mutex_lock(&config_db_mutex);

	/* Build the list of tokens... */
	if (xtree_tl_build(config_db, &head, pattern, mtype) == -1) {
		pthread_mutex_unlock(&config_db_mutex);
		return CFG_FALSE;
	}

	/* Remove them! */
	for (cur = head.tqh_first; cur; cur = cur->tl_chain.tqe_next) {
		if (_xtree_del(cur->tl_terminal) == -1) {
			xtree_tl_free(&head);
			pthread_mutex_unlock(&config_db_mutex);
			return CFG_FALSE;
		}
	}

	xtree_tl_free(&head);
	pthread_mutex_unlock(&config_db_mutex);

	return CFG_OK;
}


CFG_Struct *
CFG_MemBackup(void)
{
	CFG_Struct *cs = NULL;

	cs = malloc(sizeof(*cs));
	if (!cs)
		return NULL;

	pthread_mutex_lock(&config_db_mutex);
	cs->cs_tree = config_db;
	config_db = NULL;
	pthread_mutex_unlock(&config_db_mutex);

	return cs;
}


void
CFG_MemRestore(CFG_Struct *cs)
{
	if (!cs)
		return;

	pthread_mutex_lock(&config_db_mutex);
	if (config_db)
		cfg_destroy_nt();

	config_db = cs->cs_tree;
	pthread_mutex_unlock(&config_db_mutex);
	free(cs);
}


void
CFG_MemBackupKill(CFG_Struct *cs)
{
	if (!cs)
		return;

	xmlFreeDoc(cs->cs_tree);
	free(cs);
}


/*
 * List operations  Totally the opposite of thread safe.
 */
CFG_List *
CFG_ListCreate(const char *m_pat, int m_type)
{
	CFG_List *list = NULL;
	token_list_head *head = NULL;

	head = malloc(sizeof(*head));
	if (!head)
		return NULL;

	list = malloc(sizeof(*list));
	if (!list) {
		free(head);
		return NULL;
	}

	if (xtree_tl_build(config_db, head, m_pat, m_type) <= 0) {
		free(list);
		free(head);
		return NULL;
	}

	list->cl_head = head;
	list->cl_cur = head->tqh_first;

	return list;
}


void
CFG_ListDestroy(CFG_List *list)
{
	if (!list)
		return;

	if (list->cl_head) {
		xtree_tl_free(list->cl_head);
		free(list->cl_head);
	}
	free(list);
}


int
CFG_ListNext(CFG_List *list)
{
	if (list->cl_cur->tl_chain.tqe_next) {
		list->cl_cur = list->cl_cur->tl_chain.tqe_next;
		return 1;
	}
	return 0;
}


int
CFG_ListMore(CFG_List *list)
{
	if (!list || !list->cl_cur)
		return 0;

	return (!!(list->cl_cur->tl_chain.tqe_next));
}


void
CFG_ListRewind(CFG_List *list)
{
	list->cl_cur = list->cl_head->tqh_first;
}
