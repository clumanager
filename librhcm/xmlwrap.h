/*
  Copyright Red Hat, Inc. 2002-2003

  The Red Hat Cluster Manager API Library is free software; you can
  redistribute it and/or modify it under the terms of the GNU Lesser
  General Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any later
  version.

  The Red Hat Cluster Manager API Library is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
 */
/** @file
 * Header for xmlwrap.c.
 */
#ifndef _XMLWRAP_H
#define _XMLWRAP_H
#include <xmlconfig.h>

/**
 * Backup structure so we don't have to reload the database from disk.
 */
typedef struct _cfg_struct {
	xmlDocPtr cs_tree;  	/**< The XML Document Root of current
				     config database */
} CFG_Struct;

/**
 * We create this when we try to do a pattern match on the database,
 * and use it when we cycle through said list of patterns.
 */
typedef struct _cfg_list {
	token_list_head *cl_head;/**< Pointer to the head node */
	token_list_node *cl_cur; /**< Pointer to the current node */
} CFG_List;


typedef int CFG_status;

#define CFG_OK                1
#define CFG_FALSE             0

#define CFG_NO_SUCH_SECTION  -1
#define CFG_DEFAULT          -2
#define CFG_FAILED           -3
#define CFG_LINE_TOO_LONG    -4
#define CFG_PARSE_FAILED     -5
#define CFG_INIT_FAILURE     -6
#define CFG_BAD_KEY          -7
#define CFG_TOO_BIG          -8

#define CFG_EXACT	     MATCH_EXACT
#define CFG_REGEX	     MATCH_REGEX
#define CFG_GLOB	     MATCH_GLOB

int CFG_Loaded(void);
int CFG_Set(const char *token, char *value);
int CFG_Get(const char *token, char *dflt, char **value);
int CFG_ReadFile(const char *filename);
int CFG_Read(void);
int CFG_WriteFile(const char *filename);
int CFG_WriteBuffer(char **buffer, size_t *buflen);
int CFG_Write(void);
int CFG_Remove(const char *token);
int CFG_RemoveMatch(const char *token, int mtype);
void CFG_Destroy(void);

/* Memory back up */
CFG_Struct *CFG_MemBackup(void); 
void CFG_MemRestore(CFG_Struct *);
void CFG_MemBackupKill(CFG_Struct *);

/*
 * These are list ops; not thread safe.  TODO - make thread safe!
 */
/* 
 * Create a list based on a given match type + pattern
 */
CFG_List *CFG_ListCreate(const char *m_pat, int m_type);

/*
 * Destroy a list.  *list is freed.
 */
void CFG_ListDestroy(CFG_List *list);

/*
 * Traverse to the next pointer in the list.
 */
int CFG_ListNext(CFG_List *list);

/*
 * More in list?
 */
int CFG_ListMore(CFG_List *list);

/*
 * Put list back to top.
 */
void CFG_ListRewind(CFG_List *list);

/*
 * Return the current token/value.
 */
#define CFG_ListToken(list) (list->cl_cur->tl_token)
#define CFG_ListValue(list) (list->cl_cur->tl_value)

#endif
