/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Connection list handling routines.
 *
 * This is kind of redundant with fdlist - except not.
 */
#include <sys/queue.h>
#include <pthread.h>
#include <sys/select.h>
#include <msgsvc.h>
#include <clulog.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

static inline int clist_delete_nt(int fd);

/**
 * Node in connection list.
 */
typedef struct __conn_node {
	TAILQ_ENTRY(__conn_node) cn_entries;	/**< sys/queue tailq entri */
	uint32_t cn_fd;			/**< File descriptor */
	uint32_t cn_flags;		/**< Application-specific flags */
} conn_node_t;

typedef TAILQ_HEAD(__conn_list_head, __conn_node) conn_list_head_t;

static pthread_mutex_t conn_list_mutex = PTHREAD_MUTEX_INITIALIZER;
static conn_list_head_t conn_list_head = { NULL, NULL };


/**
 * Insert a file descriptor with the given flags into our list.
 *
 * @param fd		File descriptor to add
 * @param flags		Flags.  Application specific.
 * @see			fdlist_add
 * @return		0
 */
int
clist_insert(int fd, int flags)
{
	conn_node_t *node;

	node = malloc(sizeof(*node));
	/* ASSERT(node); */
	memset(node,0,sizeof(*node));

	node->cn_fd = fd;
	node->cn_flags = flags;

	pthread_mutex_lock(&conn_list_mutex);
	clist_delete_nt(fd);
	TAILQ_INSERT_TAIL(&conn_list_head, node, cn_entries);
	pthread_mutex_unlock(&conn_list_mutex);

	return 0;
}


static inline int
clist_delete_nt(int fd)
{
	conn_node_t *curr;

	for (curr = conn_list_head.tqh_first; curr;
	     curr = curr->cn_entries.tqe_next) {

		if (curr->cn_fd == fd) {
			TAILQ_REMOVE(&conn_list_head, curr, cn_entries);
			free(curr);
			return 0;
		}
	}

	return 1;
}


/**
 * Delete a file descriptor from our connection list.
 *
 * @param fd		The file descriptor to delete.
 * @return		1 if not found, 0 if successful.
 */
int
clist_delete(int fd)
{
	int rv;

	pthread_mutex_lock(&conn_list_mutex);
	rv = clist_delete_nt(fd);
	pthread_mutex_unlock(&conn_list_mutex);

	return rv;
}


/**
 * Set all file descriptors in the connection list in a given fd_set.
 * We close any file descriptors which have gone bad for any reason and remove
 * them from our list.
 *
 * @param set		The file descriptor set to modify.
 * @return		0
 */
int
clist_fill_fdset(fd_set *set)
{
	conn_node_t *curr;
	fd_set test_fds;
	struct timeval tv;

	pthread_mutex_lock(&conn_list_mutex);

top:
	for (curr = conn_list_head.tqh_first; curr;
	     curr = curr->cn_entries.tqe_next) {

		/* Kill all negative FDs */
		if (curr->cn_fd < 0) {
			clist_delete_nt(curr->cn_fd);
			goto top;
		}

		FD_ZERO(&test_fds);
		FD_SET(curr->cn_fd, &test_fds);

		tv.tv_sec = 0;
		tv.tv_usec = 0;
		if (select(curr->cn_fd + 1, &test_fds, &test_fds, NULL,
			   &tv) == -1) {
			if (errno == EBADF || errno == EINVAL) {
				clist_delete_nt(curr->cn_fd);
				goto top;
			}
		}
		
		FD_SET(curr->cn_fd, set);
	}

	pthread_mutex_unlock(&conn_list_mutex);

	return 0;
}


/**
 * Determine the next set file descriptor in our connection list, given a 
 * set of file descriptors.  O(n), but works.
 *
 * @param set		File descriptor set to check into.
 * @return		-1 on failure or the number of the next set file
 *			descriptor if successful.
 */
int
clist_next_set(fd_set *set)
{
	int rv;
	conn_node_t *curr;

	pthread_mutex_lock(&conn_list_mutex);

	for (curr = conn_list_head.tqh_first; curr;
	     curr = curr->cn_entries.tqe_next) {

		if (FD_ISSET(curr->cn_fd, set)) {
			FD_CLR(curr->cn_fd, set);
			rv = curr->cn_fd;
			pthread_mutex_unlock(&conn_list_mutex);
			return rv;
		}
	}

	pthread_mutex_unlock(&conn_list_mutex);

	return -1;
}


/**
 * Set a given file descriptor's flags.
 *
 * @param fd		File descriptor.
 * @param flags		Application specific flags to set on the file
 *			descriptor.
 * @return		-1 if not found, or 0 on success.
 * @see			fdlist_setstate
 */
int
clist_set_flags(int fd, int flags)
{
	int rv = -1;
	conn_node_t *curr;

	pthread_mutex_lock(&conn_list_mutex);

	for (curr = conn_list_head.tqh_first; curr;
	     curr = curr->cn_entries.tqe_next) {

		if (curr->cn_fd == fd) {
			curr->cn_flags = flags;
			rv = 0;
		}
	}

	pthread_mutex_unlock(&conn_list_mutex);

	return rv;
}
	

/**
 * Send a message to file descriptors in our list with certain flags.
 *
 * @param msg		The message buffer.
 * @param len		Length of the message buffer.
 * @param flags		Flags to check for prior to sending.
 * @return		0
 */
int
clist_multicast(void *msg, int len, int flags)
{
	conn_node_t *curr;

	pthread_mutex_lock(&conn_list_mutex);

	for (curr = conn_list_head.tqh_first; curr;
	     curr = curr->cn_entries.tqe_next) {

		if (flags && ((flags & curr->cn_flags) != flags))
			continue;

		if (msg_send(curr->cn_fd, msg, len) < 0) {
			if (errno != EINTR) {
				/* Don't delete the entry here - just close
				   the socket */
				msg_close(curr->cn_fd);
			}
		}
	}

	pthread_mutex_unlock(&conn_list_mutex);

	return 0;
}
	

/**
 * Send a message to ALL file descriptors in our connection list.
 *
 * @param msg		The message buffer.
 * @param len		Length of the message buffer.
 * @return		0
 */
int
clist_broadcast(void *msg, int len)
{
	return clist_multicast(msg, len, 0);
}


/**
 * Initialize the connection list.
 *
 * @see			fdlist_init
 * @return		0
 */
int
clist_init(void)
{
	pthread_mutex_lock(&conn_list_mutex);
	TAILQ_INIT(&conn_list_head);
	pthread_mutex_unlock(&conn_list_mutex);
	return 0;
}
