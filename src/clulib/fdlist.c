/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * File descriptor list handling for msgsimple communication layer.
 */
/*
 *  $Revision: 1.6 $
 *
 *  author: Jeff Moyer <moyer@mclinux.com>
 *	    Lon Hohberger <lhh at redhat.com>
 *
 * - lhh - Added doxygen markup.
 */
#ifdef DEBUG

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/types.h>
#include <fcntl.h>

#include "fdlist.h"

/*
 * Local prototypes
 */
struct fd_list *fdlist_lookup(int fd);

/*
 * Global list variable
 */
struct fd_list_head fdlist = { NULL, NULL };

/**
 * Initialize message subsystem file descriptor list.
 *
 * @see		clist_init
 */
void
fdlist_init(void)
{
	/* Warning - closes EVERYTHING */
	while (fdlist.head) {
		close(fdlist.head->fd);
		fdlist_delete(fdlist.head->fd);
	}

	fdlist.head = fdlist.tail = NULL;
}


/**
 * Set message subsystem status flags on a given fd in our list.
 *
 * @param fd		File descriptor.
 * @param state		Status flags.
 * @see			clist_set_flags
 */
int
fdlist_setstate(int fd, int state)
{
	struct fd_list *fdnode;

	fdnode = fdlist_lookup(fd);
	if (fdnode == NULL)
		return -1;

	fdnode->state |= state;
	return 0;
}


/**
 * Retrieve the state flags for a given file descriptor.
 *
 * @return		-1 on failure, or the state on success.
 */
int
fdlist_getstate(int fd)
{
	struct fd_list *fdnode;

	fdnode = fdlist_lookup(fd);
	if (fdnode == NULL)
		return -1;

	return fdnode->state;
}


/**
 * Add a file descriptor/address pair to our file descriptor list.
 * 
 * @param fd		The file descriptor to add.
 * @param state		The current state of the file descriptor.
 * @param local		The local address the file descriptor is connected to.
 * @return		-1 on failure, 0 on success.
 * @see			clist_insert
 */
int
fdlist_add(int fd, int state, struct sockaddr_in *local)
{
	struct fd_list *node;

	node = (struct fd_list *) malloc(sizeof (struct fd_list));
	if (node == NULL) {
		printf("Unable to allocate memory.  Error: %s\n",
			strerror(errno));
		return -1;
	}

	node->fd = fd;
	node->state = state;
	node->local_addr = local;
	node->next = NULL;

	if (fdlist.tail) {
		fdlist.tail->next = node;
	}
	fdlist.tail = node;

	if (fdlist.head == NULL)
		fdlist.head = node;

	return 0;
}


/**
 * Retrieve the local address of a file descriptor in our list.
 *
 * @param fd		The file descriptor.
 * @return		NULL on failure (not found), the pointer
 *			to the address on success.
 */
struct sockaddr_in *
fdlist_getaddr(int fd)
{
	struct fd_list *fdnode;

	fdnode = fdlist_lookup(fd);
	if (fdnode == NULL)
		return NULL;

	return fdnode->local_addr;
}


/**
 * Delete a file descriptor from our list.
 *
 * @param fd		The file descriptor to delete.
 * @return		-1 on failure, 0 on success.
 * @see			clist_delete
 */
int
fdlist_delete(int fd)
{
	struct fd_list *curr, *prev;

	if (!fdlist.head) {
		return -1;
	}

	if (fdlist.head->fd == fd) {
		curr = fdlist.head;
		fdlist.head = fdlist.head->next;
		if (fdlist.tail == curr) {
			fdlist.tail = fdlist.head;	/* NULL */
		}
		free(curr);
		return 0;
	}

	prev = fdlist.head;
	for (curr = fdlist.head->next; curr != NULL; curr = curr->next) {
		if (curr->fd == fd) {
			prev->next = curr->next;
			if (curr == fdlist.tail)
				fdlist.tail = prev;
			free(curr);
			return 0;
		}
		prev = curr;
	}
	/* Entry not found */
	return -1;
}


/**
 * Find the fd_list structure for a given file descriptor.
 * 
 * @param fd		File descriptor to look up.
 * @return		NULL if not found or the address to the fd_list
 *			structure containing fd.
 */
struct fd_list *
fdlist_lookup(int fd)
{
	struct fd_list *curr;

	for (curr = fdlist.head; curr != NULL; curr = curr->next) {
		if (curr->fd == fd) {
			return curr;
		}
	}
	return NULL;
}

#endif /* #ifdef DEBUG */
