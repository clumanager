/** @file
 * Header for fdlist.c.
 */
/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/*
 *  $Revision: 1.2 $
 *
 *  author: Jeff Moyer <moyer@mclinux.com>
 */


#ifndef _FD_LIST_H
#define _FD_LIST_H

#ifdef cplusplus
extern "C" {
#endif

/*
 * FD States (socket states)
 */
#define MSG_CONNECTED      0x01
#define MSG_UNCONNECTED    0x02
#define MSG_LISTENING      0x04
#define MSG_ACCEPTED       0x08
#define MSG_AUTHENTICATED  0x10

#define MSG_CANREAD(x)     ((x)&(MSG_CONNECTED|MSG_ACCEPTED))
#define MSG_CANWRITE(x)    ((x)&(MSG_CONNECTED|MSG_ACCEPTED))
#define MSG_SECURE(x)      ((x)&(MSG_AUTHENTICATED))

/**
 *  This structure represents a communications endpoint.
 *  We do sanity checking on file descriptors passed into the messaging
 *  subsystem based on the values stored in this linked list.  It
 *  is worth noting that local_addr does not accurately represent the 
 *  connection associated with the given file descriptor. It is
 *  simply a pointer back to the proc_id_array representation of the 
 *  available communications endpoints.  This field is used primarily
 *  by the logger to determine which daemon is requesting a message be logged
 *  for inclusion in the log message string.
 */
struct fd_list {
    int                 fd;	/**< File descriptor */
    int                 state;  /**< State flags. See states above */
    struct sockaddr_in  *local_addr; /**< Address of connected socket */
    struct fd_list      *next;	/**< Next pointer */
};

/**
 * List head for the fd_list structure.
 */
struct fd_list_head {
	struct fd_list *head;	/**< Head pointer */
	struct fd_list *tail;	/**< Tail pointer */
};

extern struct fd_list_head fdlist;

void fdlist_init(void);
int  fdlist_add(int fd, int state, struct sockaddr_in *local);
int  fdlist_delete(int fd);
int  fdlist_getstate(int fd);
int  fdlist_setstate(int fd, int state);

#ifdef cplusplus
}
#endif
#endif
/*
 * Local variables:
 *  c-basic-offset: 4
 *  c-indent-level: 4
 *  tab-width: 8
 * End:
 */
