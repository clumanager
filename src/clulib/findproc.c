/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Find a process given the name of the executable.
 */
#include <fcntl.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <libgen.h>
#include <stdio.h>
#include <signal.h>

#define PROC_DIR "/proc"

/**
 * Find a process given the name of its executable.  This walks the /proc
 * directory, looking for a match in /proc/[pid]/exe - then sends an optional
 * signal to each PID found as well as optionally storing the each PID in a
 * pre-allocated array.
 *
 * @param processname		The name of the process sought.
 * @param pids			A pre-allocated array of pid_t.  If NULL,
 *				then we only return the number of PIDs.
 *				Otherwise, each PID which matches
 *				processname is stored.
 * @param pids_len		The maximum number of slots PIDs to report.
 *				If zero, then we only return the number of
 *				pids.
 * @param sig			Send this signal to each PID found.
 * @return			-1 on error or the number of pids found/killed
 */
int
findkillproc(char *processname, pid_t *pids, size_t pids_len, int sig)
{
	int x = 0, nbytes;
	pid_t pid;
	DIR *procdir;
	struct dirent *de;
	char filename[NAME_MAX+1];
	char buf[4096];

	procdir = opendir(PROC_DIR);
	if (!procdir) {
		return -1;
	}

	while ((!pids || !pids_len || (x < pids_len)) &&
	       ((de = readdir(procdir)))) {

		snprintf(filename, sizeof(filename), "%s/%s/exe",
			 PROC_DIR, de->d_name);

		memset(buf,0,sizeof(buf));
		nbytes = readlink(filename, buf, sizeof(buf));

		/* chop */
		if (nbytes == sizeof(buf))
			buf[sizeof(buf)-1] = 0;

		if ((nbytes <= 1) || (strcmp(basename(buf), processname)))
			continue;

		if (de->d_name[0] != '.') {
			pid = atoi(de->d_name);
			x++;
			
			if (pids && pids_len)
				pids[x] = pid;
			if (sig >= 0)
				kill(pid, sig);
		}
	}

	closedir(procdir);
	return x;
}


/**
 * Find a process given the name of its executable.  Do not send a signal to
 * each process found.
 *
 * @param processname		The name of the process sought.
 * @param pids			A pre-allocated array of pid_t.  If NULL,
 *				then we only return the number of PIDs.
 *				Otherwise, each PID which matches
 *				processname is stored.
 * @param pids_len		The maximum number of slots PIDs to report.
 *				If zero, then we only return the number of
 *				pids.
 * @return			-1 on error or the number of pids found/killed
 * @see findkillproc killall
 */
int
findproc(char *processname, pid_t *pids, size_t pids_len)
{
	return findkillproc(processname, pids, pids_len, -1);
}


/**
 * Find a process given the name of its executable.  Send a signal to
 * each process found.
 *
 * @param processname		The name of the process sought.
 * @param sig			Send this signal to each PID found.
 * @return			-1 on error or the number of pids found/killed
 * @see findkillproc killall
 */
int
killall(char *processname, int sig)
{
	return findkillproc(processname, NULL, 0, sig);
}


#ifdef STANDALONE
int
main(int argc, char **argv)
{
	pid_t pids[128];
	int npids, x;

	if (argc < 2) {
		printf("usage: %s <program>\n", argv[0]);
		return -1;
	}

	npids = findproc(argv[1], pids, 128);

	if (npids > 0) {
		for (x = 0; x < npids; x++) {
			printf("%d\n", pids[x]);
		}
	}
	return 0;
}

#endif


