/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Find a network interface, given an IP address or hostname.
 *
 * Author: Brian Stevens <bstevens at redhat.com>
 */

static const char *version __attribute__ ((unused)) = "$Revision: 1.6 $";

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/param.h>
#include <errno.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <if_lookup.h>

/**
 * Find a network interface, given an IP address or hostname.
 *
 * @param ip_name	IP address or hostname to check against.
 */
int
if_lookup(char *ip_name, struct ifreq *req)
{
	int sockfd, len, lastlen;
	char *cptr;
	struct ifconf ifc;
	struct ifreq *ifr, flags;
	struct hostent *hp;
	int i;

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0)
		return (1);

	lastlen = 0;
	len = 100 * sizeof (struct ifreq);
	for (;;) {
		ifr = (struct ifreq *) malloc(len);
		ifc.ifc_len = len;
		ifc.ifc_buf = (char *) ifr;
		if (ioctl(sockfd, SIOCGIFCONF, &ifc) < 0) {
			if (errno != EINVAL || lastlen != 0) {
				free(ifr);
				close(sockfd);
				return (1);
			}
		} else {
			if (ifc.ifc_len == lastlen)
				break;	/* success, len has not changed */
			lastlen = ifc.ifc_len;
		}
		len += 10 * sizeof (struct ifreq);
		free(ifr);
	}

	/*
	 * Get the address for the ip name we are looking up
	 */
	if ((hp = gethostbyname(ip_name)) == NULL) {
		close(sockfd);
		return (1);
	}

	len = (int) ifc.ifc_len / sizeof (struct ifreq);
	for (i = 0; i < len; i++) {

		if (ifr[i].ifr_addr.sa_family != AF_INET)
			continue;

		/*
		 * Ignore channel-bonded slave interfaces.
		 */
		memset(&flags, 0, sizeof (flags));
		strcpy(flags.ifr_name, ifr->ifr_name);
		if (ioctl(sockfd, SIOCGIFFLAGS, &flags)) {
			close(sockfd);
			return (1);
		}
		if (flags.ifr_flags & IFF_SLAVE)
			continue;
		if (!(flags.ifr_flags & IFF_UP))
			continue;

		/*
		 * Deal with aliases
		 */
		if ((cptr = strchr(ifr[i].ifr_name, ':')) != NULL)
			*cptr = 0;

		if (memcmp(&((struct sockaddr_in *) &ifr[i].ifr_addr)->sin_addr,
			   hp->h_addr, sizeof (struct in_addr)))
			continue;

		memcpy(req, &ifr[i], sizeof (struct ifreq));
		free(ifr);
		close(sockfd);
		return (0);
	}

	free(ifr);
	close(sockfd);
	return (1);
}
