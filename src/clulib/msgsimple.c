/*
  Copyright Red Hat, Inc. 2002-2003
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Intra-Cluster Messaging Interface.
 *
 * XXX Needs Doxygenification.
 */
/*
 *  $Id: msgsimple.c,v 1.31 2005/11/11 20:33:17 lhh Exp $
 *
 *  author: Jeff Moyer <moyer@mclinux.com>
 *          Lon H. Hohberger <lhh at redhat.com>
 *          - Read-retry, IP/Ether authentication,
 *          - MD5 CRAC (challenge-response auth code)
 *  description: Inter-process messaging interface.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/socket.h>
#include <malloc.h>
#include <linux/limits.h>
#include <sys/time.h>
#include <sys/un.h>
#include <dirent.h>
#include <string.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <libgen.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <sys/syslog.h>
#include <sys/ioctl.h>
#include <auth_md5.h>

#include "fdlist.h"
#include <msgsvc.h>
#include <xmlwrap.h>
#include <clusterdefs.h>
#include <clulog.h>
#include <clushared.h>
#include <sharedstate.h>
#include <clu_lock.h>
#include <nodeid.h>
#include <net/if.h>
#include <net/if_arp.h>

#define SOCK_TEMPLATE "/tmp/clumanager.%d" /* port to listen on */

/*
 * Global Variables
 */
static int msgsvc_noarp = 0;
static int msgsvc_initialized = 0;
static int local_node = -1;
static char *local_name = NULL;
static char use_md5 = 0;

/*
 * Process ID for running process.  Needed for msg_receive, set in msgInit.
 */
static int proc_id = -1;

static struct ifreq local_interface;

/*
 * ARP info for each member.
 */
typedef struct _arp_info {
	char ai_set;
	struct arpreq ai_ar;
} arp_info_t;
arp_info_t node_arp_array[MAX_NODES];


struct sockaddr_in node_addr_array[MAX_NODES];
struct sockaddr_in msg_clumembd_addr,
    msg_clusvcmgrd_addr,
    msg_cluquorumd_addr,
    msg_clulockd_addr;

struct msg_id_struct {
	int msg_fd;		/* Used for the current process to listen on */
	struct sockaddr_in *msg_ins;	/* Unix Domain Socket associated with proc */
	char *config_file_port_ent;
	char *dflt_port;
} proc_id_array[] = {
	{ -1, &msg_clumembd_addr, CFG_CLUMEMBD_PORT, DFLT_CLUMEMBD_PORT},
	{ -1, &msg_clusvcmgrd_addr, CFG_CLUSVCMGRD_PORT, DFLT_CLUSVCMGRD_PORT},
	{ -1, &msg_cluquorumd_addr, CFG_CLUQUORUMD_PORT, DFLT_CLUQUORUMD_PORT},
	{ -1, &msg_clulockd_addr, CFG_CLULOCKD_PORT, DFLT_CLULOCKD_PORT}
};

struct __attribute__ ((packed)) msg_struct {
	uint32_t ms_count;	/* number of bytes in payload */
};


/*
 *  Local prototypes
 */
int msg_svc_init(int);
static void msg_setup_sockin(char *host, int port, struct sockaddr_in *ins);
static unsigned long msg_create(void *payload, ssize_t count, void **msg);
static void msg_destroy(void *msg);

/*
 * External prototypes from fdops.c
 */
int __select_retry(int fdmax, fd_set * rfds, fd_set * wfds, fd_set * xfds,
		   struct timeval *timeout);
ssize_t __read_retry(int fd, void *buf, int count, struct timeval *timeout);
ssize_t __write_retry(int fd, void *buf, int count, struct timeval *timeout);

int hex2bin(char *, int, char *, int);


/**
 * Initialize the TCP/IP message subsystem.
 *
 * @param reconfigure	If set to 1, treat this call as a reconfiguration.
 * @return 0
 */
int
msg_svc_init(int reconfigure)
{
	int i;
	char *port;
	char qstr[256];
	char *ret;
	
	/*
	 * Determine local node ID.  PRE: CFG_Read() or CFG_ReadFile()!!!
	 */
	if (!CFG_Loaded())
		return -1;

	memset(&local_interface, 0, sizeof (local_interface));
	local_node = getLocalNodeInfo(&local_interface);
	clulog(LOG_DEBUG, "Cluster I/F: %s [%s]\n", local_interface.ifr_name,
	       inet_ntoa(((struct sockaddr_in *) &local_interface.ifr_addr)->
			 sin_addr));

	if (local_node == -1)
		clulog(LOG_INFO, "Local Member ID unavailable\n");

	if (!reconfigure && (CFG_Get("cluster%msgsvc_noarp", NULL, &ret) ==
			     CFG_OK)) {
		if (ret && strlen(ret) &&
		    (!strcasecmp(ret, "yes") ||
		     !strcasecmp(ret, "1"))) { 
			clulog(LOG_DEBUG,
			       "Internal ARP checking disabled\n");
			msgsvc_noarp = 1;
		}
	}

	/* 
	 * Multinode... 
	 */
	for (i = 0; i < MAX_NODES; i++) {
		snprintf(qstr, sizeof (qstr), "members%%member%d%%name", i);
		if (CFG_Get(qstr, NULL, &ret) != CFG_OK)
			continue;

		if (!reconfigure) 
			memset(&node_arp_array[i], 0,
			       sizeof (node_arp_array[i]));

		msg_setup_sockin(ret, 0, &node_addr_array[i]);
	}

	/*
	 *  Now we get communication endpoints from the config file.
	 */
	for (i = 0; i <= MAX_PROCID; i++) {
		CFG_Get(proc_id_array[i].config_file_port_ent,
			proc_id_array[i].dflt_port, &port);

		msg_setup_sockin(local_name, atoi(port),
				 proc_id_array[i].msg_ins);
	}

	/*
	 * (re)configure authentication data
	 */
	if (CFG_Get("cluster%key", NULL, &ret) == CFG_OK) {
		i = hex2bin(ret, strlen(ret), qstr, sizeof(qstr));
		if (i == -1) {
			auth_md5_init(NULL, 0);
		} else {
			use_md5 = 1;
			auth_md5_init(qstr, i);
		}
	} else {
		use_md5 = 0;
		auth_md5_init(NULL, 0);
		clulog(LOG_NOTICE, "Challenge-Response disabled\n");
	}

	msgsvc_initialized = 1;
	return 0;
}


/**
 * Create a listen socket based on the daemon ID of the caller.
 *
 * @param my_proc_id	Daemon ID of the caller.
 * @return		-1 on failure, or a listening file descriptor.
 */
msg_handle_t
msg_listen(msg_addr_t my_proc_id)
{
	const int on = 1;
	int val;		/* for F_GETFL */

	if (!msgsvc_initialized) {
		if (msg_svc_init(0) < 0) {
			clulog(LOG_ERR, "msg_listen: Unable to initialize "
			       "messaging subsystem.\n");
			return -1;
		}
	}

	proc_id = my_proc_id;

	if ((proc_id_array[proc_id].msg_fd =
	     socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		clulog(LOG_ERR, "msg_listen: Unable to create socket\n");
		close(proc_id_array[proc_id].msg_fd);
		return -1;
	}

	setsockopt(proc_id_array[proc_id].msg_fd, SOL_SOCKET,
		   SO_REUSEADDR, &on, sizeof (on));

	val = fcntl(proc_id_array[proc_id].msg_fd, F_GETFD, 0);
	if (val < 0) {
		/*
		 * Error.
		 */
		close(proc_id_array[proc_id].msg_fd);
		return -1;
	}
	val |= FD_CLOEXEC;
	if (fcntl(proc_id_array[proc_id].msg_fd, F_SETFD, val) < 0) {
		clulog(LOG_ERR,
		       "msg_listen: Unable to set the FD_CLOEXEC flag on sock\n");
		close(proc_id_array[proc_id].msg_fd);
		return -1;
	}

	/*
	 * Allow connections from anywhere
	 */
	proc_id_array[proc_id].msg_ins->sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(proc_id_array[proc_id].msg_fd,
		 (struct sockaddr *) proc_id_array[proc_id].msg_ins,
		 (socklen_t) sizeof (struct sockaddr_in)) < 0) {

		clulog(LOG_ERR, "msg_listen: Unable to bind to socket %s\n",
		       strerror(errno));
		close(proc_id_array[proc_id].msg_fd);
		return -1;
	}

	listen(proc_id_array[proc_id].msg_fd, MAX_MSGBACKLOG);

	return proc_id_array[proc_id].msg_fd;
}


/*
   local message interface
 */
msg_handle_t
msg_listen_local(msg_addr_t my_proc_id)
{
	int sock = -1;
      	struct sockaddr_un su;
	mode_t om;

	sock = socket(PF_LOCAL, SOCK_STREAM, 0);
	if (sock < 0)
		goto fail;
		
	su.sun_family = PF_LOCAL;
	snprintf(su.sun_path, (sizeof(su.sun_path)), SOCK_TEMPLATE, 
		 my_proc_id);

	unlink(su.sun_path);
	om = umask(077);

	if (bind(sock, (struct sockaddr *)&su, sizeof(su)) < 0) {
		umask(om);
		goto fail;
	}
	umask(om);

	if (listen(sock, SOMAXCONN) < 0)
		goto fail;
	return sock;
fail:
      	if (sock >= 0)
		close(sock);
	return -1;
}


/**
 *
 *
 */
msg_handle_t
msg_open_local(msg_addr_t daemonid, int tout)
{
	struct timeval timeout = {tout, 0};
	int sock, flags, error, ret;
	socklen_t len;
	struct sockaddr_un sun;
	fd_set rfds, wfds;
	
	sock = socket(PF_LOCAL, SOCK_STREAM, 0);
	if (sock < 0)
		return -1;
		
	sun.sun_family = PF_LOCAL;
	snprintf(sun.sun_path, (sizeof(sun.sun_path)), SOCK_TEMPLATE, 
		 daemonid);
	
	flags = fcntl(sock, F_GETFL, 0);
	fcntl(sock, F_SETFL, flags | O_NONBLOCK);

        ret = connect(sock, (struct sockaddr *) &sun, sizeof(sun));
        
        if (ret < 0 && (errno != EINPROGRESS)) {
		close(sock);
		return -1;
	}

	if (ret == 0)           /* connect completed immediately */
		goto done;

	FD_ZERO(&rfds);
	FD_SET(sock, &rfds);
	wfds = rfds;
	
	ret = select(sock + 1, &rfds, &wfds, NULL, &timeout);
	if (ret == 0) {
		close(sock);
		errno = ETIMEDOUT;
		return -1;
	}
	
	if (FD_ISSET(sock, &rfds) || FD_ISSET(sock, &wfds)) {
		len = sizeof (error);
		if (getsockopt(sock, SOL_SOCKET, SO_ERROR, &error, &len) < 0) {
			close(sock);
			return -1;
		}
	} else {
		clulog(LOG_ERR, "msg_open: select error: sockfd not set\n");
		close(sock);
		return -1;
	}

	clulog(LOG_DEBUG, "locally connected\n");

done:
	
	return sock;        
}


msg_handle_t
msg_accept_local(msg_handle_t handle)
{
	int sockfd = handle, acceptfd;

	/*
	 *  Do some sanity checks on the handle passed in.
	 */
	if (sockfd < 0) {
		clulog(LOG_ERR, "msg_accept called with bad handle %d\n",
		       handle);
		errno = EBADF;
		return -1;
	}

	while ((acceptfd =
		accept(sockfd, (struct sockaddr *) NULL, NULL)) < 0) {
		if (errno == EINTR) {
			continue;
		}

		clulog(LOG_ERR, "msg_accept: accept error %s.\n",
		       strerror(errno));
		return -1;
	}

	clulog(LOG_DEBUG, "accepted local connection\n");

	return acceptfd;
}


/**
 * Open a socket to the specified daemon on the specified node.  Give up
 * after the specified time (if there is one).
 *
 * @param daemonid	Daemon we are trying to reach.
 * @param nodeid	Member we are trying to reach.
 * @param timeout	Specified timeout value, or NULL.
 * @return		-1 on failure, or a connected file descriptor on
 *			success.
 */
msg_handle_t
__msg_open(msg_addr_t daemonid, int nodeid, struct timeval * timeout)
{
	int sockfd, flags, error;
	int ret;
	socklen_t ins_len = sizeof (struct sockaddr_in), len;
	fd_set rfds, wfds;
	struct sockaddr_in dest;

	if (!msgsvc_initialized) {
		if (msg_svc_init(0) < 0) {
			clulog(LOG_ERR,
			       "msg_open: unable to initialize msg subsystem.\n");
			return -1;
		}
	}

	if (nodeid < 0)
		nodeid = local_node;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		clulog(LOG_ERR,
		       "msg_open: Unable to create socket. Error: %s\n",
		       strerror(errno));
		return -1;
	}

	/*
	 * Set the socket up for a non-blocking connect.  Normal TCP connects
	 * have a timeout of 75 seconds or more.  This is not acceptable for
	 * our clustering software, so we'll make the timeout 5 seconds.  This
	 * is very reasonable, considering these are local connects, for the most
	 * part.
	 */
	flags = fcntl(sockfd, F_GETFL, 0);
	fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);

	/*
	 * Use TCP Keepalive
	 */
	flags = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, &flags,
	    sizeof(flags)) == -1)
		clulog(LOG_WARNING, "Couldn't set keepalive flag on fd%d\n",
		       sockfd);

#if 0
	flags = 1;
	setsockopt(sockfd, SOL_TCP, TCP_NODELAY, &flags, sizeof(flags));
#endif

	/*
	 * Set up the destination socket.
	 */
	if (nodeid == -1) {
		dest.sin_family = PF_INET;
		dest.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	} else
		memcpy(&dest, &node_addr_array[nodeid], ins_len);

	dest.sin_port = proc_id_array[daemonid].msg_ins->sin_port;

	ret = connect(sockfd, (struct sockaddr *) &dest, ins_len);

	if (ret < 0) {
		if (errno != EINPROGRESS) {
			clulog(LOG_ERR,
			       "msg_open: Unable to connect. Error %s\n",
			       strerror(errno));
			close(sockfd);
			return -1;
		}
	}

	if (ret == 0)		/* connect completed immediately */
		goto done;

	FD_ZERO(&rfds);
	FD_SET(sockfd, &rfds);
	wfds = rfds;

	ret = select(sockfd + 1, &rfds, &wfds, NULL, timeout);
	if (ret == 0) {
		close(sockfd);
		errno = ETIMEDOUT;
		return -1;
	}

	if (FD_ISSET(sockfd, &rfds) || FD_ISSET(sockfd, &wfds)) {
		len = sizeof (error);
		if (getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &len) < 0) {
			close(sockfd);
			return -1;
		}
	} else {
		clulog(LOG_ERR, "msg_open: select error: sockfd not set\n");
		close(sockfd);
		return -1;
	}

      done:
	fcntl(sockfd, F_SETFL, flags);

	if (error) {
		close(sockfd);
		errno = error;
		return -1;
	}

	/* Get the ARP info so we can store the hardware ethernet address. */

	if (auth_md5(sockfd) == -1) {
		error = errno;
		close(sockfd);
		errno = error;
		return -1;
	}

	return sockfd;
}


/**
 * Open a socket to the specified daemon on the specified node.  Gives up
 * after the default timeout has expired.
 *
 * @param daemonid	Daemon we are trying to reach.
 * @param nodeid	Member we are trying to reach.
 * @return		-1 on failure, or a connected file descriptor on
 *			success.
 */
msg_handle_t
msg_open(msg_addr_t daemonid, int nodeid)
{
	struct timeval timeout = MSGSVC_CONNECT_TIMEOUT;

	return __msg_open(daemonid, nodeid, &timeout);
}


/**
 * @see __msg_open
 */
msg_handle_t
msg_open_timeout(msg_addr_t daemonid, int nodeid, struct timeval * timeout)
{
	return __msg_open(daemonid, nodeid, timeout);
}


/**
 * Ask the kernel for the ARP information about a given IP address.
 * @param ar		Structure which will be filled in on return.
 * @param ipaddr	IP address (struct sockaddr_in) to check.
 * @return		0 on success, -1 on failure/error.
 */
int
do_arpreq(struct arpreq *ar, struct sockaddr_in *ipaddr)
{
	/* char loglog[80]; */
	struct sockaddr_in *sinp;
	int fd;

	if (!strlen(local_interface.ifr_name))
		return -1;

	fd = socket(PF_INET, SOCK_DGRAM, 0);
	if (fd == -1)
		return -1;

	memset(ar, 0, sizeof (*ar));
	sinp = (struct sockaddr_in *) &ar->arp_pa;
	sinp->sin_family = PF_INET;
	memcpy(&sinp->sin_addr, &ipaddr->sin_addr, sizeof (sinp->sin_addr));
	strncpy(ar->arp_dev, local_interface.ifr_name, sizeof (ar->arp_dev));

	if (ioctl(fd, SIOCGARP, ar) == -1) {
		close(fd);
		clulog(LOG_DEBUG, "ioctl(fd,SIOCGARP,ar [%s]): %s\n",
		local_interface.ifr_name,
		strerror(errno));
		return -1;
	}
	close(fd);

	/*
	snprintf(loglog, 80, "0x%02x%02x%02x%02x%02x%02x",
		 (int) (ar->arp_ha.sa_data[0]) & 0xff,
		 (int) (ar->arp_ha.sa_data[1]) & 0xff,
		 (int) (ar->arp_ha.sa_data[2]) & 0xff,
		 (int) (ar->arp_ha.sa_data[3]) & 0xff,
		 (int) (ar->arp_ha.sa_data[4]) & 0xff,
		 (int) (ar->arp_ha.sa_data[5]) & 0xff);

	clulog(LOG_DEBUG, "ARP: %s -> %s\n", inet_ntoa(ipaddr->sin_addr),
	       loglog);
	 */
	return 0;
}


/**
 * Blocking call to accept.
 *
 * @param handle	Listening file descriptor with a waiting connection.
 * @return 		-1 on failure, or a new connected file descriptor on
 *			success.
 */
msg_handle_t
msg_accept(msg_handle_t handle)
{
	int sockfd = handle, acceptfd, x;
	struct sockaddr_in cliaddr;
	socklen_t clilen;
	struct arpreq ar;

	/*
	 *  Do some sanity checks on the handle passed in.
	 */
	if (sockfd < 0) {
		clulog(LOG_ERR, "msg_accept called with bad handle %d\n",
		       handle);
		errno = EBADF;
		return -1;
	}

	memset(&cliaddr, 0, sizeof (cliaddr));
	clilen = sizeof (cliaddr);

	while ((acceptfd =
		accept(sockfd, (struct sockaddr *) &cliaddr, &clilen)) < 0) {
		if (errno == EINTR) {
			continue;
		}

		clulog(LOG_ERR, "msg_accept: accept error %s.\n",
		       strerror(errno));
		return -1;
	}

	if (clilen != sizeof (cliaddr)) {
		clulog(LOG_ERR, "msg_accept: Invalid address length!\n");
		close(acceptfd);
		return -1;
	}

	/*
	 * Ensure we know who's calling... TODO break auth. up into another
	 * function.
	 */
	if ((cliaddr.sin_addr.s_addr == htonl(INADDR_LOOPBACK)) ||
	    (cliaddr.sin_addr.s_addr == 
	     node_addr_array[local_node].sin_addr.s_addr)) {
		clulog(LOG_DEBUG, "Verified connect from member #%d (%s)\n",
		       local_node, inet_ntoa(cliaddr.sin_addr));
		goto md5auth;
	}

	/*
	 * If there's no ARP request and it's not on loopback, deny access.
	 * Assume it's an IP spoof.  
	 */
	/* This started breaking around RHEL3 U4; I don't know why */
	if (!msgsvc_noarp && do_arpreq(&ar, &cliaddr) < 0) {
		clulog(LOG_WARNING,
		       "Dropping connect from %s: Not in subnet!\n",
		       inet_ntoa(cliaddr.sin_addr));
		close(acceptfd);
		return -1;
	}

	for (x = 0; x < MAX_NODES; x++) {
		if (cliaddr.sin_addr.s_addr ==
		    node_addr_array[x].sin_addr.s_addr) {
			clulog(LOG_DEBUG,
			       "Connect: Member #%d (%s) [IPv4]\n", x,
			       inet_ntoa(cliaddr.sin_addr));

			if (msgsvc_noarp)
				break;

			/*
			 * Add the source hardware address to our table.
			 */
			memcpy(&node_arp_array[x].ai_ar, &ar,
			       sizeof (struct arpreq));
			node_arp_array[x].ai_set = 1;
			break;
		}

		if (msgsvc_noarp)
			break;

		/*
		 * Check against our internal arp table...  This can be caused
		 * by services with a different netmask somehow taking precedence
		 * over the main interface address.  For members we don't
		 * have entries for, query the kernel for them.  If we still
		 * don't get an entry, oh-well...
		 */
		if (!node_arp_array[x].ai_set) {
			if (do_arpreq(&ar, &node_addr_array[x]) == -1)
				/*
				 * Here, we allow a failure: if the member is
				 * not online, it will NOT have an ARP entry!
				 */
				continue;
			memcpy(&node_arp_array[x].ai_ar, &ar,
			       sizeof (struct arpreq));
			node_arp_array[x].ai_set = 1;
		}

		if (!memcmp(node_arp_array[x].ai_ar.arp_ha.sa_data,
			    ar.arp_ha.sa_data, 6)) {
			clulog(LOG_DEBUG,
			       "Connect: Member #%d (%s) [ETHER]\n",
			       x, inet_ntoa(cliaddr.sin_addr));
			break;
		}
	}

	if (x >= MAX_NODES) {
		clulog(LOG_WARNING, "Dropping connect from %s: Unauthorized\n",
		       inet_ntoa(cliaddr.sin_addr));
		close(acceptfd);
		return -1;
	}

md5auth:
#if 0
	x = 1;
	setsockopt(acceptfd, SOL_TCP, TCP_NODELAY, &x, sizeof(x));
#endif

	if (auth_md5_challenge(acceptfd) != 0) {
		if (!use_md5)
			return acceptfd;
		clulog(LOG_WARNING, "Denied %s: %s",
		       inet_ntoa(cliaddr.sin_addr), strerror(errno));
		close(acceptfd);
		return -1;
	}

	return acceptfd;
}


/**
 * Call accept, but time out after the specified timeout has expired.
 *
 * @param handle	Handle to accept from.
 * @param timeout	Amount of time to wait (in seconds)
 * @return		-1 on failure, or a file descriptor on success.
 */
msg_handle_t
msg_accept_timeout(msg_handle_t handle, int timeout)
{
	int sockfd = handle;
	struct timeval tv;
	fd_set fdset;
	int ret;

	/*
	 *  Do some sanity checks on the handle passed in.
	 */
	if (sockfd < 0) {
		clulog(LOG_ERR,
		       "msg_accept_timeout called with bad handle %d\n",
		       handle);
		return -1;
	}

	tv.tv_sec = timeout;
	tv.tv_usec = 0;
	FD_ZERO(&fdset);
	FD_SET(sockfd, &fdset);

	while ((ret = select(sockfd + 1, &fdset, NULL, NULL, &tv)) < 0) {
		if (errno != EINTR)
			clulog(LOG_ERR,
			       "msg_accept_timeout: select returned error "
			       "status %s\n", strerror(errno));
		return -1;
	}

	/* Timeout - set errno so calling function knows we've timed out */
	if (ret == 0) {
		errno = ETIMEDOUT;
		return -1;
	}

	return msg_accept(sockfd);
}


int
__msg_send(msg_handle_t handle, void *buf, ssize_t count)
{
	void *msg;
	int msg_len = -1, bytes_written = 0;

	msg_len = msg_create(buf, count, &msg);
	if ((bytes_written = write(handle, msg, msg_len)) < msg_len) {
		clulog(LOG_ERR,
		       "__msg_send: Incomplete write to %d. Error: %s\n",
		       handle, strerror(errno));
		return -1;
	}
	msg_destroy(msg);
	return (bytes_written - sizeof (struct msg_struct));
}


/*
 *  Send a message to another process.
 */
int
msg_send(msg_handle_t handle, void *buf, ssize_t count)
{
	int sockfd = handle;

	if (!msgsvc_initialized) {
		if (msg_svc_init(0) < 0) {
			clulog(LOG_ERR,
			       "msg_send: unable to initialize msg subsystem.\n");
			return -1;
		}
	}

	if (sockfd < 0) {
		clulog(LOG_ERR, "msg_send called with bad handle %d\n", handle);
		return -1;
	}

	return (__msg_send(sockfd, buf, count));
}


ssize_t
__msg_receive(msg_handle_t handle, void *buf, ssize_t count,
	      struct timeval *tv)
{
	struct msg_struct msg_hdr;
	int sockfd = handle;
	ssize_t retval = 0;

	if ((retval = __read_retry(sockfd, &msg_hdr, sizeof (msg_hdr), tv)) <
	    (ssize_t) sizeof (msg_hdr)) {
		return -1;
	}

	swab32(msg_hdr.ms_count);
	if (!msg_hdr.ms_count) {
		clulog(LOG_ERR, "__msg_receive: empty response?\n");
		return 0;
	}

	return (__read_retry(sockfd, buf, count, tv));
}


ssize_t
msg_receive(msg_handle_t handle, void *buf, ssize_t count)
{
	int sockfd = handle;

	/*
	 * Sanity checks
	 */
	if (!msgsvc_initialized) {
		if (msg_svc_init(0) < 0) {
			clulog(LOG_ERR,
			       "msg_receive: unable to initialize msg subsystem.\n");
			return -1;
		}
	}

	if (sockfd < 0) {
		clulog(LOG_DEBUG, "msg_receive called with bad handle %d\n",
		       handle);
		return -1;
	}

	return (__msg_receive(sockfd, buf, count, NULL));
}


ssize_t
msg_receive_timeout(msg_handle_t handle, void *buf, ssize_t count,
		    unsigned int timeout)
{
	int sockfd = handle;
	struct timeval tv;

	/*
	 * Sanity checks
	 */
	if (!msgsvc_initialized) {
		if (msg_svc_init(0) < 0) {
			clulog(LOG_DEBUG,
			       "msg_receive_timeout: unable to initialize"
			       " msg subsystem.\n");
			return -1;
		}
	}

	if (sockfd < 0) {
		clulog(LOG_DEBUG, "msg_receive_timeout called with bad "
		       "handle %d\n", handle);
		return -1;
	}

	tv.tv_sec = timeout;
	tv.tv_usec = 0;

	return (__msg_receive(sockfd, buf, count, &tv));
}


/*
 * msg_send_simple
 */
int
msg_send_simple(int fd, int cmd, int arg1, int arg2)
{
	generic_msg_hdr msg;

	msg.gh_magic = GENERIC_HDR_MAGIC;
	msg.gh_length = sizeof (msg);
	msg.gh_command = cmd;
	msg.gh_arg1 = arg1;
	msg.gh_arg2 = arg2;
	swab_generic_msg_hdr(&msg);

	return msg_send(fd, (void *) &msg, sizeof (msg));
}


/*
 * receive_message
 *
 * Read a message and the corresponding buffer off of the file descriptor
 * indicated by fd.  This allocates **buf; so the user must free it when 
 * [s]he is done with it.  Also returns the length of the full buffer in
 * *buf_size.
 *
 * Returns 0 on success or -1 on failure.
 */
int
msg_receive_simple(int fd, generic_msg_hdr ** buf, int timeout)
{
	int ret;
	generic_msg_hdr peek_msg;

	/*
	 * Peek at the header.  We need the size of the inbound buffer!
	 */
	ret = msg_peek(fd, &peek_msg, sizeof (generic_msg_hdr));
	if (ret != sizeof (generic_msg_hdr)) {
		if (ret == -1) {
			if (errno != ECONNRESET)
				clulog(LOG_ERR, "fd%d peek: %s\n", fd,
				       strerror(errno));
		} else if (ret != 0)	/* Blank message = probably closed socket */
			clulog(LOG_ERR, "fd%d peek: %d/%d bytes\n", fd,
			       ret, sizeof (generic_msg_hdr));
		return -1;
	}

	swab_generic_msg_hdr(&peek_msg);
	if (peek_msg.gh_magic != GENERIC_HDR_MAGIC) {
		clulog(LOG_ERR, "Invalid magic: Wanted 0x%08x, got 0x%08x\n",
		       GENERIC_HDR_MAGIC, peek_msg.gh_magic);
		return -1;
	}

	/* 
	 * Ensure someone didn't just try to crash us
	 */
	if (peek_msg.gh_length > MSGSVC_MAX_MSGSIZE) {
		clulog(LOG_ERR,
		       "%s: Message rejected: Maximum size exceeded!\n");
		return -1;
	}

	/*
	 * allocate enough memory to receive the header + diff buffer
	 */
	*buf = malloc(peek_msg.gh_length);
	memset(*buf, 0, peek_msg.gh_length);

	if (!*buf) {
		clulog(LOG_CRIT, "%s: malloc: %s", __FUNCTION__,
		       strerror(errno));
		return -1;
	}

	/*
	 * Now, do the real receive.  2 second timeout, if none specified.
	 */
	ret = msg_receive_timeout(fd, (void *) (*buf), peek_msg.gh_length,
				  timeout ? timeout : 2);

	if (ret == -1) {
		clulog(LOG_ERR, "msg_receive_timeout: %s\n", strerror(errno));
		return -1;
	}

	if (ret != peek_msg.gh_length) {
		clulog(LOG_ERR, "short read: %d/%d\n", ret, peek_msg.gh_length);
		return -1;
	}

	return ret;
}


ssize_t
__msg_peek(msg_handle_t handle, void *buf, ssize_t count)
{
	char *bigbuf;
	ssize_t ret;
	int sockfd = handle, bigbuf_sz;
	int hdrsz = sizeof (struct msg_struct);

	bigbuf_sz = count + hdrsz;
	bigbuf = (char *) malloc(bigbuf_sz);
	if (bigbuf == NULL) {
		clulog(LOG_DEBUG, "msg_peek: Out of memory\n");
		return -1;
	}

	/*
	 * We need to account for the msg header.  So we skip past it
	 * and decrement the return value by the number of bytes eaten
	 * up by the header.
	 */
	while (((ret = recv(sockfd, bigbuf, bigbuf_sz, MSG_PEEK)) < 0) &&
	       errno == EINTR) {
		usleep(10000);
	}
	if (ret < 0) {
		free(bigbuf);
		return -1;
	}
	if (ret - hdrsz > 0) {
		ret -= hdrsz;
		if (ret > count)
			ret = count;
		memcpy(buf, bigbuf + hdrsz, ret);
		free(bigbuf);
	} else {
		clulog(LOG_DEBUG, "Truncating %d to 0...\n", ret);
		ret = 0;
	}

	return ret;
}


ssize_t
msg_peek(msg_handle_t handle, void *buf, ssize_t count)
{
	int sockfd = handle;

	if (sockfd < 0) {
		clulog(LOG_ERR, "msg_peek: called with bad handle %d\n",
		       sockfd);
		return -1;
	}

	return (__msg_peek(sockfd, buf, count));
}


void
msg_close(msg_handle_t handle)
{
	if (handle < 0) {
		clulog(LOG_ERR, "msg_close called with bad handle %d\n",
		       handle);
		return;
	}

	close(handle);
}


/*
 *  Internal Helper Functions
 */
static void
msg_setup_sockin(char *host, int port, struct sockaddr_in *ins)
{
	struct hostent *hp;

	bzero(ins, sizeof (struct sockaddr_in));
	ins->sin_family = AF_INET;
	ins->sin_port = htons(port);

	if (!host || (!(hp = gethostbyname(host)))) {
		ins->sin_addr.s_addr = htonl(INADDR_ANY);
		return;
	}
	memcpy(&ins->sin_addr, hp->h_addr, hp->h_length);
}


static unsigned long
msg_create(void *payload, ssize_t len, void **msg)
{
	unsigned long ret;
	struct msg_struct msg_hdr;

	memset(&msg_hdr, 0, sizeof (msg_hdr));
	msg_hdr.ms_count = len;
	swab32(msg_hdr.ms_count);

	if (!len || !payload)
		return sizeof (msg_hdr);

	*msg = (void *) malloc(sizeof (msg_hdr) + len);
	if (*msg == NULL) {
		clulog(LOG_ERR,
		       "msg_create: unable to allocate memory.  error %s\n",
		       strerror(errno));
		return -1;
	}
	memcpy(*msg, &msg_hdr, sizeof (msg_hdr));
	memcpy(*msg + sizeof (msg_hdr), payload, len);

	ret = sizeof (msg_hdr) + len;
	return ret;
}


static void
msg_destroy(void *msg)
{
	if (msg != NULL)
		free(msg);
}
