/*
  Copyright Red Hat, Inc. 2002-2003
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Functions for determining local node/member ID number.  There are way
 * too many ways to call this.
 */
#include <xmlwrap.h>
#include <sys/types.h>
#include <net/if.h>
#include <clusterdefs.h>
#include <if_lookup.h>


static int localid = -1;

int getLocalNodeIDCached(void);
int getLocalNodeIDUncached(void);
int getNodeIDCached(const char *name);
int getNodeIDUncached(const char *nodename);
int getNodeInfoCached(const char *name, struct ifreq *if_r);


int
getLocalNodeIDCached(void)
{
	return getNodeIDCached(NULL);
}


int
getLocalNodeIDUncached(void)
{
	return getNodeIDUncached(NULL);
}


/**
 * Finds the member ID and the corresponding ethernet interface.
 *
 * @param if_r		Will be filled with ethernet interface info
 * @return 		member ID, or -1 if unsuccessful.
 */
int
getLocalNodeInfo(struct ifreq *if_r)
{
	return getNodeInfoCached(NULL, if_r);
}


int
getNodeInfoCached(const char *name, struct ifreq *if_r)
{
	int x;
	char qstr[80];
	char *value;
	struct ifreq ifr;
	struct ifreq *ifrp = &ifr;

	if (if_r)
		ifrp = if_r;

	/* !name -> get local node ID */
	if (!name && !if_r && (localid != -1))
		return localid;

	if (!CFG_Loaded())
		return -1;

	for (x=0; x<MAX_NODES; x++) {
		snprintf(qstr, sizeof(qstr), "members%%member%d%%name",
			 x);

		if (CFG_Get(qstr, NULL, &value) != CFG_OK)
			continue;

		if (name) {
			/* non-local */
			if (strcmp(value, name) == 0) {
				return x;
			}
		} else {
			/* Local */
			if (if_lookup(value, ifrp) == 0) {
				localid = x;
				return x;
			}
		}
	}

	return -1;
}


int
getNodeIDCached(const char *name)
{
	return getNodeInfoCached(name, NULL);
}


int
getNodeIDUncached(const char *nodename)
{
	int nodeid = -1;
	int bkid = localid;
	CFG_Struct *bk;

	bk = CFG_MemBackup();

	if (CFG_ReadFile(CLU_CONFIG_FILE) != CFG_OK) {
		fprintf(stderr, "Couldn't read %s\n", CLU_CONFIG_FILE);
		return -1;
	}

	localid = -1;
	nodeid = getNodeIDCached(nodename);
	localid = bkid;

	CFG_MemRestore(bk);
	return nodeid;
}


int
cluGetLocalNodeId(void)
{
	if (CFG_Loaded())
		return getLocalNodeIDCached();
	return getLocalNodeIDUncached();
}
