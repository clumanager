/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Shared Service Block read/write functions.
 */
/*
 *  $Id: shared_services.c,v 1.12 2008/01/10 17:32:35 lhh Exp $
 *
 *  author: Tim Burke <tburke at redhat.com>
 *  description: Interface to Service descriptions.
 *
 * diskservices.c
 *
 * This file implements the routines used to represent the set of 
 * services being served by a node.  Its main job is to control the
 * "ServiceBlock" structures on-disk.
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/reboot.h>
#include <sched.h>
#include <signal.h>
#include <errno.h>
#include <clu_lock.h>

#include <clulog.h>
#include <namespace.h>
#include <clushared.h>
#include <sharedstate.h>
#include <xmlwrap.h>
#include <findproc.h>


char *serviceStateStrings[] = {     /* strings for service states */
    SVC_UNINITIALIZED_STR,
    SVC_STARTED_STR,
    SVC_STOPPED_STR,
    SVC_DISABLED_STR,
    SVC_PENDING_STR,
    SVC_FAILED_STR,
    SVC_STOPPING_STR
};




/*
 * Forward routine declarations.
 */
static void initServiceBlock(SharedServiceBlock *svcblk, int svcNum);
static int writeServiceBlock(SharedServiceBlock *svcblk);
void printServiceBlock(SharedServiceBlock *svcblk);

static const char *version __attribute__ ((unused)) = "$Id: shared_services.c,v 1.12 2008/01/10 17:32:35 lhh Exp $";
/*
 * .............Configurable Parameters...................
 *
 * The following tuning knobs are intended to allow customization.
 */
/*
 * We tolerate a few IO errors before reacting.
 * This parameter defines how many consecutive errors are needed
 * to declare a true IO failure condition.  It is intended to avoid
 * over-reacting to an intermittent error.
 */
#define MAX_CONSECUTIVE_IO_ERRORS 5
static int max_consecutive_io_errors = MAX_CONSECUTIVE_IO_ERRORS;

/*
 * Called to initialize a service block.  Setting the state to 
 * SVC_UNINITIALIZED means that the service does not represent a service
 * which is currently in the service description file, rather it is an
 * unused or deleted service.
 * During the course of normal operation, various fields within this block
 * will be updated accordingly.
 */
static void initServiceBlock(SharedServiceBlock *svcblk, int svcNum) {
	bzero((void *)svcblk, sizeof(SharedServiceBlock));
	svcblk->sb_magic = SERVICE_BLOCK_MAGIC_NUMBER;
	svcblk->sb_svcblk.sb_state = SVC_UNINITIALIZED;
	svcblk->sb_svcblk.sb_id = svcNum;
}


void
do_reboot(int how, const char *func, const char *file, int line)
{
	char *val = NULL;
	int fd;
	char buf[128];
	time_t now = time(NULL);
	struct sched_param s;

	CFG_Get("cluster%reboot_debug", NULL, &val);

	if (!val || !strlen(val) || val[0]=='n' ||
	    ((val[0] != 'y' && atoi(val)==0))) {
		reboot(how);
		return;
	}
	
	/* Pre-empt EVERYTHING */
	memset(&s,0, sizeof(s));
	s.sched_priority = 100;
	sched_setscheduler(0, SCHED_RR, &s);

	fd = open("/tmp/clumanager-reboot-debug",
		  O_RDWR|O_SYNC|O_TRUNC|O_CREAT);
	if (fd < 0) {
		printf("Failed to create file\n");
	} else {
		snprintf(buf, sizeof(buf),
		 	 "%s-> reboot(%d) @ %s:%d in function %s()\n", 
		 	 ctime(&now), how, file, line, func);

		write(fd, buf, strlen(buf));
		fdatasync(fd);
		fsync(fd);
		close(fd);
		sync(); sync(); sync();
	}

	reboot(how);
	/* notreached */
	s.sched_priority = 0;
	sched_setscheduler(0, SCHED_OTHER, &s);
}


/**
 * Take action on read/write error from shared state.
 */
void
disk_error_action(void)
{
	char *val = NULL;
	
	CFG_Get("cluster%disk_error_action", NULL, &val);

	if (!val || !strlen(val))
		goto reboot;

	if (!strcasecmp(val, "ignore")) {
		clulog(LOG_CRIT, "Shared State Error: Ignoring");
		return;
	}
		
	if (!strcasecmp(val, "halt")) {
		clulog(LOG_ALERT, "Shared State Error: HALTING");
		REBOOT(RB_HALT_SYSTEM);
		return;
	}
	
	if (!strcasecmp(val, "stop")) {
		clulog(LOG_ALERT,
		       "Shared State Error: Stopping Cluster Daemons");
		killall("cluquorumd", SIGTERM);
		return;
	}
	
reboot:	
	clulog(LOG_ALERT, "Shared State Error: REBOOTING");
	REBOOT(RB_AUTOBOOT);
}


int
consider_shutdown(char *reason)
{
	clulog(LOG_ERR, "%s\n", reason);
	disk_error_action();
	return 0;
}



/*
 * Write the service block out to disk.
 * Returns: -1 on IO error, -1 on parameter error, 0 on success.
 */
static int
writeServiceBlock(SharedServiceBlock *svcblk) {
	/*off_t offsetService;*/
	int svcNum;
	char filename[256];
	SharedServiceBlock sb;
	int n;

	svcNum = svcblk->sb_svcblk.sb_id;
	// Paranoia checks
	if ((svcNum < 0) || (svcNum >= MAX_SERVICES)) {
		clulog(LOG_ERR, "writeServiceBlock: Invalid service number %d.\n",
			svcNum);
		return(-1);
	}
	if(svcblk->sb_svcblk.sb_state < 0 || svcblk->sb_svcblk.sb_state > SVC_LAST_STATE) {
		clulog(LOG_ERR, "writeServiceBlock: Invalid state %d.\n",
			svcblk->sb_svcblk.sb_state);
		return(-1);
	}
	if ((svcblk->sb_svcblk.sb_owner != NODE_ID_NONE) &&
	    ((svcblk->sb_svcblk.sb_owner < 0) || 
	    (svcblk->sb_svcblk.sb_owner >= MAX_NODES))) {
		clulog(LOG_ERR, "writeServiceBlock: Invalid owner number %d.\n",
			svcblk->sb_svcblk.sb_owner);
		return(-1);
	}
	if (svcblk->sb_magic != SERVICE_BLOCK_MAGIC_NUMBER) {
		clulog(LOG_ERR, "writeServiceBlock: invalid magic# 0x%lx\n",
			svcblk->sb_magic);
		return(-1);
	}

	/* LHH -> disk */
	snprintf(filename, sizeof(filename), NS_D_SERVICE "/%d" NS_F_STATUS,
		 svcNum);

	memcpy(&sb, svcblk, sizeof(sb));
	swab_SharedServiceBlock((&sb));
	n = sh_write_atomic(filename, &sb, sizeof(sb));

	if (n >= 0)
		return 0;
	disk_error_action();
	return -1;
}


/*
 * Reads in the service block from the shared partition.
 * Stuffing the results into the passed data struct.
 * Returns: -1 on IO error, -1 on parameter error, 0 on success.
 */
static int
readServiceBlock(int svcNum, SharedServiceBlock *svcblk) {
	int ret;
	int retries = 0;
	char filename[256];
	SharedServiceBlock sb;

top:

        bzero((void *)svcblk, sizeof(SharedServiceBlock)); // paranoia
	if ((svcNum < 0) || (svcNum >= MAX_SERVICES)) {
	    clulog(LOG_ERR, "readServiceBlock: Invalid service number %d.\n",
			svcNum);
	    return(-1);
	}

	snprintf(filename,sizeof(filename), NS_D_SERVICE "/%d" NS_F_STATUS,
		 svcNum);

	ret = sh_read_atomic(filename, (char *)&sb, sizeof(sb));

	if (ret == -1) {
		clulog(LOG_ERR,
		       "readServiceBlock: bad ret %d sh_read_atomic\n", ret);
		return(ret);
	}		

	swab_SharedServiceBlock(&sb);
	memcpy(svcblk, &sb, sizeof(*svcblk));

	/*
         * Do at least a primitive level of validation to see if it looks like
	 * a viable service block.
	 */
	if (svcblk->sb_magic != SERVICE_BLOCK_MAGIC_NUMBER) {
		clulog(LOG_ERR, "readServiceBlock: Invalid magic # 0x%lx.\n",
		       svcblk->sb_magic);
		return(-1);
	} 
	if (svcblk->sb_svcblk.sb_id != svcNum && retries < 3) {
	    clulog(LOG_DEBUG, "Service number mismatch %d, %d.  Retrying.\n",
		   svcNum, svcblk->sb_svcblk.sb_id);
	    ++retries;
	    goto top;
	}
	if (svcblk->sb_svcblk.sb_id != svcNum) {
	    clulog(LOG_EMERG, "readServiceBlock: Service number mismatch %d, %d.\n",
		   svcNum, svcblk->sb_svcblk.sb_id);
	    return(-1);
	}
	return(0);
}


/*
 * Initialize the on-disk data structures representing services being served.
 * This will later be overwritten when the disk service status subsystem
 * is initialized.  Its main purpose is to wipe the disk to a clean slate.
 * Returns: 0 on success.
 */
int initializePartitionServiceBlocks(void) {
    char filename[256];
    SharedServiceBlock servb;
    int retval;
    int i;

    /*
     * Just wiping out any prior settings.
     */
    for (i=0; i < MAX_SERVICES; i++) {
	initServiceBlock(&servb, i);

	snprintf(filename, sizeof(filename), NS_D_SERVICE "/%d" NS_F_STATUS,
		 i);

	retval = sh_write_atomic(filename, (char *)&servb, sizeof(servb));

        if (retval == -1) {
	    clulog(LOG_CRIT,
		   "initializePartitionServiceBlocks: unable to initialize "
		   "partition service blocks.\n");
	    return(retval);
        }
    }
    clulog(LOG_DEBUG, "initializePartitionServiceBlocks: successfully initialized %d service blocks.\n", MAX_SERVICES);
    return(0);
}


/*
 * Read the on-disk data structures representing all services.
 * This is called periodically as part of the read/repair service.
 * Returns: 0 on success.
 */
int
readAllServiceBlocks(void) {
    	SharedServiceBlock servb;
	int retval;
	int i;
	
	/*
	 * Just wiping out any prior settings.
	 */
	for (i=0; i < MAX_SERVICES; i++) {
		retval = readServiceBlock(i, &servb);
		if (retval != 0) {
			clulog(LOG_ERR, "readAllServiceBlocks: unable to "
			       "read partition service blocks.\n");
			return(retval);
		}
	}
	return(0);
}


/*
 * Read the on-disk data structures representing some of the services.
 * This is called periodically as part of the read/repair service.
 * Returns: 0 on success.
 */
int
readSomeServiceBlocks(int start, int count) {
    	SharedServiceBlock servb;
	int retval;
	int i;
	
	/*
	 * Just wiping out any prior settings.
	 */
	for (i=start; i < MIN(MAX_SERVICES, start+count); i++) {
		retval = readServiceBlock(i, &servb);
		if (retval != 0) {
			clulog(LOG_ERR, "%s: unable to read service block %d\n",
			       __FUNCTION__, i);
			return(retval);
		}
	}
	return(0);
}


/*
 * Externally accessible API used to retrieve the memory resident version
 * of a service description.  Note: this differs from the on-disk version!
 *
 * Returns: 0 - success
 *	   -1 - service description is not active, meaning that it was never
 *		added before, or if so it has been since deleted. Or an 
 *		invalid parameter was passed.
 * Side Effect: Reboots on inability to access shared disk.
 */
int
getServiceStatus(int svcNum, ServiceBlock *svcblk) {
	SharedServiceBlock diskSvcBlk;
	int retval;
	int retries = 0;
	
	if ((svcNum < 0) || (svcNum >= MAX_SERVICES)) {
		clulog(LOG_ERR,
		       "getServiceStatus: Invalid service number %d.\n",
		       svcNum);
		return(-1);
	}
	while (retries++ < max_consecutive_io_errors) {
		retval = readServiceBlock(svcNum, &diskSvcBlk);
		if (retval == 0) {
			if (diskSvcBlk.sb_svcblk.sb_state ==
			    SVC_UNINITIALIZED) {
				return(-1);
			}
			bcopy(&diskSvcBlk.sb_svcblk, svcblk,
			      sizeof(ServiceBlock));
			return(0);
		}
	}
	/*
	 * Inability to write to the shared state partition constitutes
	 * unsafe operation.  Initiate a clean shutdown in the hopes that
	 * some cleanup can be done before we inevitably get shot.
	 */
	consider_shutdown("Cluster Instability: "
			  "can't read service status block.");
	return(-1);
}

/*
 * Externally accessible API used to write the memory resident version
 * of a service description out to disk.  
 *
 * Returns: 0 - success
 *	   -1 - invalid service description
 * Side Effect: Reboots on inability to access shared disk.
 */
int
setServiceStatus(ServiceBlock *svcblk) {
    	SharedServiceBlock diskSvcBlk;
	int retval;
	int svcNum;
	int retries = 0;
	
	svcNum = svcblk->sb_id;
	if ((svcNum < 0) || (svcNum >= MAX_SERVICES)) {
		clulog(LOG_ERR,
		       "setServiceStatus: Invalid service number %d.\n",
		       svcNum);
		return(-1);
	}
	if (svcblk->sb_state == SVC_UNINITIALIZED) {
		clulog(LOG_ERR, "setServiceStatus: Invalid state %d, "
		       "service number %d.\n", svcblk->sb_state, svcNum);
		return(-1);
	}
	initServiceBlock(&diskSvcBlk, svcNum);
	bcopy(svcblk,&diskSvcBlk.sb_svcblk, sizeof(ServiceBlock));
	while (retries++ < max_consecutive_io_errors) {
		retval = writeServiceBlock(&diskSvcBlk);
		if (retval == 0) {
			return(0);
		}
		if (retval == -1) { // invalid parameter
			return(-1);
		}
	}
	/*
	 * Inability to write to the shared state partition constitutes
	 * unsafe operation.  Initiate a clean shutdown in the hopes that
	 * some cleanup can be done before we inevitably get shot.
	 */
	consider_shutdown("Cluster Instability: "
			  "can't write service status block.");
	return(-1);
}


/*
 * Externally accessible API used to delete a service description from the
 * on-disk representation.
 *
 * Returns: 0 - success
 *	   -1 - service description on disk does not represent an active service
 *	        or invalid parameter.
 * Side Effect: Reboots on inability to access shared disk.
 */
int
removeService(int svcNum) {
    	SharedServiceBlock svcblk;
	int retval;
	int retries = 0;
	
	clulog(LOG_DEBUG,
	       "removeService: removing service number %d.\n",svcNum);

	if ((svcNum < 0) || (svcNum >= MAX_SERVICES)) {
		clulog(LOG_ERR, "removeService: Invalid service number %d.\n",
		       svcNum);
		return(-1);
	}
	// Set state to uninitialized
	initServiceBlock(&svcblk, svcNum);
	while (retries++ < max_consecutive_io_errors) {
		retval = readServiceBlock(svcNum, &svcblk);
		if (retval == 0) {
			break;
		}
		if (retval == -1) { // invalid parameter
			return(-1);
		}
	}
	if (retval != 0) { 
		/*
		 * Inability to write to the shared state partition constitutes
		 * unsafe operation.  Initiate a clean shutdown in the hopes that
		 * some cleanup can be done before we inevitably get shot.
		 */
		consider_shutdown("Cluster Instability: can't read service "
				  "status block to remove service.");
		return(-1);
	}
	if (svcblk.sb_svcblk.sb_state == SVC_UNINITIALIZED) {
		clulog(LOG_ERR,
		       "removeService: service %d already removed.\n", svcNum);
		return(-1);
	}
	// Set state to uninitialized
	initServiceBlock(&svcblk, svcNum);
	retries = 0;
	while (retries++ < max_consecutive_io_errors) {
		retval = writeServiceBlock(&svcblk);
		if (retval == 0) {
			return(0);
		}
		if (retval == -1) { // invalid parameter
			return(-1);
		}
	}
	/*
	 * Inability to write to the shared state partition constitutes
	 * unsafe operation.  Initiate a clean shutdown in the hopes that
	 * some cleanup can be done before we inevitably get shot.
	 */
	consider_shutdown("Cluster Instability: "
			  "can't remove service status block.");
	return(-1);
}


