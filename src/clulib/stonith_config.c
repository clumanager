/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Functions which manager the STONITH configuration file which is
 * accessed by the individual STONITH driver modules.
 *
 * XXX Needs Doxygenification.
 *
 *  $Id: stonith_config.c,v 1.12 2005/02/07 20:43:34 lhh Exp $
 *
 *  author: Tim Burke <tburke at redhat.com>
 *
 * stonith_config.c
 *
 * This file implements the routines used to manage the configuration
 * file (rpc.cfg) which is accessed by the stonith driver modules.
 *
 * Rather than require the users to manually specify power related config
 * parameters in rpc.cfg and the remainder of cluster config params in
 * cluster.conf, we automatically do the work for them.  Specifically, all
 * cluster params including power settings are in cluster.conf.  Then when
 * the power subystem is initialized, we dynamically create rpc.cfg.
 *
 * In order to facilitate the addition of new stonith driver types in
 * the field without re-releasing, provisions exist to allow manual custom
 * stonith config settings to be placed in rpc.cfg.custom.
 */
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/syslog.h>
#include <sys/mman.h>
#include <signal.h>
#include <stdlib.h>
#include <errno.h>
#include <clulog.h>
#include <stonithapis.h>
#include <stonith.h>
#include <clu_lock.h>
#include <xmlwrap.h>

static int stonith_config_add_option(char *optfile, char *newstuff);

/**
 * Create the stonith configuration directory if it doesn't already exist.
 * @param dirname	Directory to create.
 * @return		0 on success, -1 on failure.
 */
static int 
stonith_config_create_dir(char *dirname)
{
	int retval;
	struct stat statbuf;

	retval = stat(dirname, &statbuf);
	if (retval == 0) {
		if (!(S_ISDIR(statbuf.st_mode))) {
			clulog(LOG_ERR, "stonith_config_create_dir: %s exists,"
				       " but is not a directory.\n", dirname);
			return(-1);
		}
	} else {
		if (errno == ENOTDIR) {
			clulog(LOG_ERR, "stonith_config_create_dir: %s exists,"
				       " and is not a directory.\n", dirname);
			return(-1);
		}
	}
	
	// Uses standard umask for owner, group file permissions,
	// but preclude world access.
	retval = mkdir(dirname, 0770);
	if (retval != 0) {
		if (errno == EEXIST)
			return(0);
			
		clulog(LOG_ERR, "stonith_config_create_dir: Unable to create %s"
				", errno = %d.\n", dirname, errno);
		return(-1);
	}
	return(0);
}


/**
 * Truncates the configuration file that the stonith drivers read thier
 * configuration information from.
 *
 * @param optfile	Filename to truncate.
 * @return		0 on success, -1 on error.
 */
static int
stonith_config_truncate(char *optfile)
{
	FILE *file;

	file = fopen(optfile, "w+");
	if(!file) {
		clulog(LOG_CRIT, 
		"stonith_config_truncate: config file %s, erron=%d\n",
			optfile, errno);
		return -1;
	}
	fclose(file);
	return 0 ;
}


/**
 * Sets file permissions on the specified file to disallow world 
 * read/write/execute permission.  Principally used to ensure that
 * cluster.conf isn't world readable.
 *
 * @param filename	Filename on which permissions should be set.
 * @return		0 on success, -1 on failure.
 */
static int
set_file_permissions(const char *filename)
{
	int fd = -1, esv, ret = -1;
	struct stat statbuf;

	fd = open(filename, O_RDWR);
	if (fd == -1) {
		esv = errno;
		clulog(LOG_ERR, 
		"set_file_permissions: can't open %s, errno=%d\n",
			filename, errno);
		errno = esv;
		return -1;
	}

	ret = fstat(fd, &statbuf);
	if (ret != 0) {
		esv = errno;
		clulog(LOG_ERR, 
		"set_file_permissions: can't get permissions of %s, errno=%d\n",
			filename, errno);
		errno = esv;
		goto out;
	}
	/*
	 * Strip off r,w,x permissions for "others".
	 */
	statbuf.st_mode &= ~(S_IRWXO);
	ret = fchmod(fd, statbuf.st_mode);
	if (ret == -1) {
		esv = errno;
		clulog(LOG_ERR, 
		"set_file_permissions: can't set permissions of %s, errno=%d\n",
			filename, errno);
		errno = esv;
	}
out:
	esv = errno;
	close(fd);
	errno = esv;
	return(ret);
}


/**
 * Checks to see if a given line is already present in a file. 
 * Used to avoid having duplicate entries in the stonith configuration file.
 *
 * @param optfile	File to check.
 * @param newstuff	Line to check for.
 * @return		0 - newstuff is not in optfile, 1 - newstuff is in
 *			optfile, -1 - error/failure.
 */
static int
stonith_config_line_present(char *optfile, char *newstuff) {
	FILE *file;
	char *buf;

	buf = malloc(1024);
	if (buf == NULL) {
		clulog(LOG_ERR, "stonith_config_line_present: can't malloc.");
		return -1;
	}

	file = fopen(optfile, "r");
	if(!file) {
		// If the file isn't there, the string isn't in it.
		free(buf);
		return 0;
	}
	
	while (fgets(buf, 1024, file)) {
		if (strcmp(buf, newstuff) == 0) {
			free(buf);
			fclose(file);
			return 1;
		}
	}
	fclose(file);
	return 0 ;
}


/**
 * Initialize the configuration directory and files.  This consists of
 * creating the directory if it doesn't already exist.  Then there is an 
 * individual configuration file per switch.  Here we don't do anything related
 * to the individual files because:
 * - For known switch types these files will be automatically created based
 *   on the config settings in cluster.conf.
 * - For "unknown" switch types (those which were not initially supported at
 *   release time).  You simply create the appropriate switch config file
 *   by hand which a name based on the switch number.
 *
 * @param stonithdir	Directory to initialize.
 * @param conf_file	Not used.
 * @return		0 on success, -1 on error.
 */
int
stonith_config_init(char *stonithdir,
                    char * __attribute__ ((unused)) conf_file)
{
	int retval;

	retval = stonith_config_create_dir(stonithdir);
	if (retval != 0)
		return(retval);
	return 0 ;
}


/**
 * Append this string onto the stonith config file.
 *
 * @param optfile	File which should be appended.
 * @param newstuff	String which we should append to optfile.
 * @return 0
 */
static int
stonith_config_add_option(char *optfile, char *newstuff)
{
	FILE *file;

	if (stonith_config_line_present(optfile, newstuff) == 1)
		return 0;

	file = fopen(optfile, "a+");
	if(!file) {
		clulog(LOG_CRIT, "stonith_config_add_option: config "
			       "file error %s, errno=%d\n", optfile, errno);
		return -1;
	}
	fprintf(file, "%s", newstuff);
	fclose(file);
	return 0 ;
}


/**
 * Creates an individual entry in the configuration file used by the
 * stonith modules.
 */
int 
stonith_config_load(char *optfile, char *type, char *IPaddr, 
		char *login, char *passwd, char *portname)
{
	char newstuff[256];
	int retval;

	if ((optfile == NULL) || (type == NULL) ||
	    ((IPaddr == NULL) && (portname == NULL))) {
		clulog(LOG_ERR, "stonith_config_load: null parameter.\n");
		return -1;
	}
	memset(newstuff, 0, 256);

	if(strcmp(type, "wti_nps") == 0)
		snprintf(newstuff, sizeof(newstuff), "%s %s\n", IPaddr,
			 passwd);
	else if(strcmp(type, "gulm-bridge") == 0) 
		snprintf(newstuff, sizeof(newstuff), "%s\n", portname);
	else if(strcmp(type, "rps10") == 0)
		/*
		 * The format for the rps10 is:
		 * <serial device>
		 * The outlet parameter is principaly used for chaining
		 * them in a master/slave configuration; and is provided
		 *
		 * and the "IPaddr" parameter to represent the port.
		 */
		snprintf(newstuff, sizeof(newstuff), "%s\n", IPaddr);
	else {
#if 0
		/*
		Old behavior: determine for EVERY TYPE, and if we do not
		recognize a particular entry, produce a warning and do
		nothing.

		((strcmp(type, "baytech") == 0) || 
		 (strcmp(type, "apcmaster") == 0) ||
		 (strcmp(type, "servertech") == 0) ||
		 (strcmp(type, "pap") == 0) ||
		 (strcmp(type, "nova4") == 0))
		 */

		/*
	 	 * Doesn't return an error status for unsupported switch types
	 	 * as that would preclude new ones.  Rather it creates the 
	 	 * stonith config file for those switches that it knows about.
	 	 */
		clulog(LOG_WARNING,
		       "stonith_config_load: not creating %s entry"
		       " for unknown switch type %s.\n", optfile, type);
#endif
		/*
	   	   New behavior: assume id, login, password triple for
		   		un-special network-controlled power
				switches.
	   	 */
		clulog(LOG_DEBUG, "Using {dev login pass} for type %s\n",
		       type);
		snprintf(newstuff, sizeof(newstuff), "%s %s %s\n", IPaddr,
			 login, passwd);
	}

	if (strlen(newstuff) > 0) {
		retval = stonith_config_truncate(optfile);
		if (retval != 0)
			return(retval);
			
		retval = set_file_permissions(optfile);
		if (retval != 0)
			return(retval);

		if (stonith_config_add_option(optfile, newstuff) != 0)
			return -1;
	}

	return 0;
}
