/** @file
 * Stonith API infrastructure.
 *
 * Copyright (c) 2000 Alan Robertson <alanr@unix.sh>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <syslog.h>
#include <libintl.h>
#include <sys/wait.h>
#include <sys/param.h>
#include <dlfcn.h>
#include <dirent.h>
#include <stonith.h>

#include <clulog.h>

#define MAX_FUNC_NAME 30

#define	MALLOC(n)	malloc(n)
#define MALLOCT(t)	(t*)(malloc(sizeof(t)))
#define FREE(p)		{free(p); (p) = NULL;}

#ifndef RTLD_GLOBAL
#	define RTLD_GLOBAL	0
#endif
#ifndef RTLD_LAZY
#	define RTLD_LAZY	0
#endif

struct symbol_str {
    char name[MAX_FUNC_NAME];
    void** function;
};

/* BSD wants us to cast the select parameter to scandir */
#ifdef BSD
#	define SCANSEL_C	(void *)
#else
#	define SCANSEL_C	/* Nothing */
#endif


static int so_select (const struct dirent *dire);

static int symbol_load(struct symbol_str symbols[], int len, void **handle);
char ** stonith_types(void);

static int
symbol_load(struct symbol_str symbols[], int len, void **handle)
{
	int  a;
	char *error;

	for(a = 0; a < len; a++) {
		struct symbol_str *sym = &symbols[a];

		*sym->function = dlsym(*handle, sym->name);

		if ((error = dlerror()) != NULL)  {
			syslog(LOG_ERR, "%s", error);
			dlclose(*handle); 
			return 1;
		}
	}
	return 0;
}


/*
 *	Create a new Stonith object of the requested type.
 */
Stonith *
stonith_new(const char * type)
{
	int	ret;
	Stonith *s;
	struct symbol_str syms[NR_STONITH_FNS];
	char *obj_path;
	size_t  obj_path_size;


#ifdef TIMXXX
	// These are supposed to be -D in CFLAGS in Makefile.am
	bindtextdomain(ST_TEXTDOMAIN, LOCALEDIR);
#endif //TIMXXX

	s = MALLOCT(Stonith);

	if (s == NULL) {
		return(NULL);
	}

	s->s_ops = MALLOCT(struct stonith_ops);

	if (s->s_ops == NULL) {
		FREE(s);
		return(NULL);
	}

	/* lhh - sizeof(char) == 1 on all architectures; it's a C standard. */
	obj_path_size = strlen(STONITHDIR) + strlen(type) + 5;
	obj_path = (char*) MALLOC(obj_path_size);

	if (obj_path == NULL) {
		FREE(s->s_ops);
		FREE(s);
		return(NULL);
	}
	
	snprintf(obj_path, obj_path_size, "%s/%s.so", STONITHDIR, type);

	if ((s->dlhandle = dlopen(obj_path, RTLD_LAZY|RTLD_GLOBAL)) == NULL) {
		clulog(LOG_ERR, "%s: %s\n", __FUNCTION__, dlerror());
		FREE(s->s_ops);
		FREE(s);
		FREE(obj_path);
		return(NULL);
	}

    	strncpy(syms[0].name, "st_new", sizeof(syms[0].name));
	syms[0].function = (void **) &s->s_ops->new;
    	strncpy(syms[1].name, "st_destroy", sizeof(syms[1].name));
	syms[1].function = (void **) &s->s_ops->destroy;
    	strncpy(syms[2].name, "st_setconffile", sizeof(syms[2].name));
	syms[2].function = (void **) &s->s_ops->set_config_file;
    	strncpy(syms[3].name, "st_setconfinfo", sizeof(syms[3].name));
	syms[3].function = (void **) &s->s_ops->set_config_info;
    	strncpy(syms[4].name, "st_getinfo", sizeof(syms[4].name));
	syms[4].function = (void **) &s->s_ops->getinfo;
    	strncpy(syms[5].name, "st_status", sizeof(syms[5].name));
	syms[5].function = (void **) &s->s_ops->status;
    	strncpy(syms[6].name, "st_reset", sizeof(syms[6].name));
	syms[6].function = (void **) &s->s_ops->reset_req;

	ret = symbol_load(syms, NR_STONITH_FNS, &s->dlhandle);
	
	if (ret != 0) {
		clulog(LOG_ERR, "stonith_new: unable to load symbols for %s.\n", 
				obj_path);
		FREE(s->s_ops);
		FREE(s);
		FREE(obj_path);
		return(NULL);
	}

	s->pinfo = s->s_ops->new();

	return(s);
}

/*
 *	Return the list of Stonith types which can be given to stonith_new()
 */
char **
stonith_types(void)
{
	char ** list;
	struct dirent **namelist;
	int n, i;
	static char **	lastret = NULL;
	static int	lastcount = 0;


	n = scandir(STONITHDIR, &namelist, SCANSEL_C &so_select, 0);
	if (n < 0) {
		clulog(LOG_ERR, "%s: scandir failed.", __FUNCTION__);
		return(NULL);
	}

	/* Clean up from the last time we got called. */
	if (lastret != NULL) {
		char **	cp = lastret;
		for (;*cp; ++cp) {
			FREE(*cp)
		}
		if (lastcount != n) {
			FREE(lastret);
			lastret = NULL;
		}
	}
	if (lastret) {
		list = lastret;
	}else{
		list = (char **)MALLOC((n+1)*sizeof(char *));
	}

	if (list == NULL) {
		clulog(LOG_ERR, "%s: malloc failed.", __FUNCTION__);
		return(NULL);
	}

	for(i=0; i<n; i++) { 
		int len = strlen(namelist[i]->d_name);

		list[i] = (char*)  MALLOC((len + 1) * sizeof(char));
		if (list[i] == NULL) {
			clulog(LOG_ERR, "%s: malloc/1 failed.", __FUNCTION__);
			return(NULL);
		}
		strncpy(list[i], namelist[i]->d_name, len + 1);

		/* strip ".so" */
		list[i][len - 3] = '\0';
		clulog(LOG_DEBUG, "Adding %s to list of power switches.",
				list[i]);

		FREE(namelist[i]);
	}

	list[i] = NULL;
	lastret = list;
	lastcount = n;

	return list;
}


static int
so_select (const struct dirent *dire)
{
	const char *end = &dire->d_name[strlen(dire->d_name) - 3];
	const char *obj_end = ".so";

	if (strcmp(end, obj_end) == 0){
		return 1;
	}

	return 0;
}

