/*
  Copyright Red Hat, Inc. 2002-2003
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.

  author: Dave Winchell <winchell at missioncriticallinux.com>
  author: Tim Burke <tburke at redhat.com>
  author: Lon Hohberger <lhh at redhat.com>
 */
/** @file
 * STONITH Fencing Routines.
 *
 * Power management code which is layered above the stonith drivers.
 * The drivers themselves are dynamically loaded at runtime as shared
 * libraries.  Principal tasks here involove configuring the stonith
 * drivers.
 */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/reboot.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/syslog.h>
#include <sys/mman.h>
#include <signal.h>
#include <stdlib.h>
#include <errno.h>
#include <clulog.h>
#include <sharedstate.h>
#include <stonithapis.h>
#include <stonith.h>
#include <clu_lock.h>
#include <xmlwrap.h>
#include <nodeid.h>
#include <pthread.h>
#include <svcmgr.h>

int getNodeName(int, char  **);

#define POWER_OP_SHOOT  0
#define POWER_OP_STATUS 1

/*
 * The set of stonith drivers is represented as a linked list.
 */
#define IPADDR_LEN 32
typedef struct stonithel {
	Stonith *stonith;
	char devtype[64];
	char ipaddr[IPADDR_LEN];
	uint32_t nodeid;
	char port[64];
	struct stonithel *next;
	int	num;
} stonith_el;

/*
 * APIs defined in this file, used to call stonith functions.
 */
int clu_stonith_init(void);
int clu_stonith_fence_cycle(int node);
void clu_stonith_deinit(void);
void clu_stonith_sighup_handler(int signum);
/* End stonith APIs defined in this file. */

/*
 * Forward routine declarations for internal functions.
 */
static stonith_el * add_new_stonith(int num, char *type, char *IPaddr, 
				    char *portname, uint32_t nodeid,
				    char *login, char *passwd);
static int clu_stonith_op(int node, int optype);
static int stonith_modules_check(void);
/* End forward routine declarations */

stonith_el *stoniths = NULL;
int clu_node_id = -1;


static char * stonith_errlist[] = {
	"Success",		/* S_OK */
	"Bad configuration",	/* S_BADCONFIG */
	"Access denied",	/* S_ACCESS */
	"Illegal argument",	/* S_INVAL */
	"Bad host name",	/* S_BADHOST */
	"Reset failed",		/* S_RESETFAIL */
	"Timed out",		/* S_TIMEOUT */
	"Already fenced off",	/* S_ISOFF */
	"Unknown error"
};


/**
 * Release all stonith related resources.
 * - First call each of the individual stonith modules so they can
 *   release any resources they've allocated.
 * - Then release all of the linked list data structures representing 
 *   stonith modules.
 */
void 
clu_stonith_deinit(void)
{
	stonith_el *ston_el, *trailer;

	for(ston_el = stoniths; ston_el != NULL; 
			ston_el = ston_el->next)
		ston_el->stonith->s_ops->destroy(ston_el->stonith);

	for(ston_el = stoniths; ston_el != NULL;) {
		trailer = ston_el;
		ston_el = ston_el->next;
		free(trailer);
	}
	stoniths = NULL;

	return;
}


/**
 * Re-reads configuration to see if a new stonith switch or cluster member
 * has just been added.
 */
void 
clu_stonith_sighup_handler(int __attribute__ ((unused)) signum)
{
	clu_stonith_init();
	return;
}


/**
 * Calls the stonith driver module associated with the specified node
 * to shoot it.  Notes: clu_stonith_op will *not* work (currently) with 
 * SCSI reservations or Fibre Channel Zoning - this is because all it does
 * is turn off all switches, then turn them all back on...  This is
 * thread safe only for use by quorumd_disk.c.
 *
 * @param node		The node to power cycle.
 * @return		0 on success, -1 on failure.
 */
int 
clu_stonith_fence_cycle(int node)
{
	int rv;
	static pthread_mutex_t cycle_mutex = PTHREAD_MUTEX_INITIALIZER;

	pthread_mutex_lock(&cycle_mutex);
	rv = clu_stonith_op(node, POWER_OP_SHOOT);
	pthread_mutex_unlock(&cycle_mutex);

	return rv;
}


/**
 * Fence off a given node.  This connects to all power switches associated
 * with a node (MAX_POWER_CONTROLLERS_PER_NODE), and turns them all off.
 *
 * @param node		The node ID to fence off.
 * @return		0 on success, or the number of failed switches if
 *			failure.
 */
int
clu_stonith_fence(int node)
{
	stonith_el *ston_el;
	int n = 0, rv = 0, ns = 0;
	char *name;

	getNodeName(node, &name);
       
	for (ston_el = stoniths; ston_el; ston_el = ston_el->next) {
		if (ston_el->nodeid == node) {
			ns++;
			n = ston_el->stonith->s_ops->reset_req(
				ston_el->stonith, ST_POWEROFF, ston_el->port);
			if (n != S_OK) {
				clulog(LOG_ERR, "STONITH: Device at %s FAILED"
				       " to fence off %s: %s!\n", 
				       ston_el->ipaddr, name, 
				       stonith_errlist[n]);
				rv++;
			}
		}
	}

	if (rv)
		return rv;

	if (!ns) {
		clulog(LOG_WARNING, "STONITH: Falsely claiming "
		       "that %s has been fenced\n", name);
		clulog(LOG_CRIT, "STONITH: Data integrity may be "
		       "compromised!\n");
	} else
		clulog(LOG_NOTICE, "STONITH: %s has been fenced!\n", name);

	return 0;
}


/**
 * Release fencing on a given node.  This connects to all power switches
 * associated with a node (MAX_POWER_CONTROLLERS_PER_NODE), and turns them
 * all off.
 *
 * @param node		The node ID to fence off.
 * @return		0 on success, or the number of failed switches if
 *			failure.
 */
int
clu_stonith_unfence(int node)
{
	stonith_el *ston_el;
	int n = 0, rv = 0, nd = 0;
	char *name;

	getNodeName(node, &name);
       
	for (ston_el = stoniths; ston_el; ston_el = ston_el->next) {
		if (ston_el->nodeid == node) {
			n = ston_el->stonith->s_ops->reset_req(
				ston_el->stonith, ST_POWERON, ston_el->port);
			if (n != S_OK) {
				clulog(LOG_ERR, "STONITH: Device at %s FAILED"
				       " to unfence %s: %d\n", 
				       ston_el->ipaddr, name, 
				       stonith_errlist[n]);
				rv++;
			}
			nd++;
		}
	}

	if (rv)
		return rv;

	if (nd)
		clulog(LOG_NOTICE, "STONITH: %s is no longer fenced off.\n",
		       name);

	return 0;
}


/**
 * Check the status of all STONITH provisions around a given node.
 *
 * @param node		The ID of the node to check on
 * @return		0 on success, or the number of failed devices on
 *			failure.
 */
int
clu_stonith_status(int node)
{
	stonith_el *ston_el;
	int x = 0, rv = 0, nd = 0;
	char *nodeName;

	getNodeName(node, &nodeName);
       
	for (ston_el = stoniths; ston_el; ston_el = ston_el->next) {
		if (ston_el->nodeid == node) {
			x = ston_el->stonith->s_ops->status(ston_el->stonith);
			if (x != S_OK) {
				clulog(LOG_ERR, "STONITH: Device at %s "
				       "controlling %s FAILED status "
				       "check: %s\n", 
				       ston_el->ipaddr, nodeName,
				       stonith_errlist[x]);
				rv++;
			}
			nd++;
		}
	}

	/* Only print a message if there are 1 or more devices */
	if (!rv && nd)
		clulog(LOG_DEBUG, "STONITH: All devices controlling %s "
		       "in good state\n", nodeName);

	return rv;
}


/**
 * Calculate how many STONITH devices are configured for a node.
 *
 * @param node		The ID of the node to check on
 * @return		The number of configured STONITH devices for the node
 */
int
clu_stonith_count(int node)
{
	stonith_el *ston_el;
	int rv = 0;

	for (ston_el = stoniths; ston_el; ston_el = ston_el->next)
		if (ston_el->nodeid == node)
			rv++;

	return rv;
}


/**
 * Calls the stonith driver module associated with the specified node
 * to shoot it.
 */
static int 
clu_stonith_op(int node, int optype)
{
	int retval = 0;

	if (clu_node_id < 0) {
		clu_node_id = cluGetLocalNodeId();
	}

	if (optype == POWER_OP_SHOOT) {

		if (node == clu_node_id) {
			clulog(LOG_ALERT, "STONITH: Shooting self!\n");
			REBOOT(RB_AUTOBOOT);
		}

		/*
		 * Turn everything off...
		 */
		if (clu_stonith_fence(node)) {
			return -1;
		}

		/*
		 * Then turn it all back on...
		 */
		if (clu_stonith_unfence(node)) {
			return -1;
		}
	}
	else if (optype == POWER_OP_STATUS) {
	   	clulog(LOG_DEBUG, "clu_stonith_op: status query.\n");
		if (clu_stonith_status(node))
			return -1;
	}
	else {
		clulog(LOG_CRIT, "clu_stonith_op: bad op %d.\n",
				optype);
		retval = -1;
	}

	return (retval);
}


/**
 * Calls *ALL* the stonith driver modules to query their statuses.
 * It returns a status of good, only if all the the stonith power switches
 * are successful.  Note - doesn't really help you distinghish which specific
 * one is in error; use clu_stonith_check_one...
 *
 * @return		0 on success, -1 on error.
 */
int 
clu_stonith_check(void)
{
	int rc;
	stonith_el *ston_el;

	if (stoniths == NULL) {
		clulog(LOG_CRIT, "clu_stonith_check: no stonith devices.\n");
		return(-1);
	}
	for (ston_el = stoniths; ston_el != NULL; 
			ston_el = ston_el->next) {
		rc = ston_el->stonith->s_ops->status(ston_el->stonith);
		if (rc != S_OK) {
			clulog(LOG_ERR, "clu_stonith_check: stonith device "
					"with IPaddr %s has bad status\n",
			       ston_el->ipaddr);
			return(-1);
		}
	}
	return(0);
}


/**
 * Extract the information for a given node power switch/stonith ID
 * and add a new stonith driver to our list of drivers.
 *
 * XXX Use #defines when building token strings!!!!
 */
static stonith_el *
find_add_stonith(int member_count, int *num, char *type, int nodeid,
		 int pswitchid)
{
	char tok[MAX_TOKEN_LEN];
	char *ip_or_dev = NULL;
	char *login = NULL;
	char *passwd = NULL;
	char *portname = NULL;
	char *nodename = NULL;
	stonith_el *rv, *ston_el;

	snprintf(tok, sizeof(tok),"members%%member%d%%name", nodeid);
	if (CFG_Get(tok, NULL, &nodename) != CFG_OK) {
		clulog(LOG_ERR, "Member #%d nonexistent?!\n");
		return NULL;
	}

	/* Get the ip/device name */
	snprintf(tok, sizeof(tok),
		 "members%%member%d%%powercontroller%d%%ipaddress",
		 nodeid, pswitchid);

	if (strcmp(type, "gulm-bridge") &&
	    CFG_Get(tok, NULL, &ip_or_dev) != CFG_OK) {
		snprintf(tok, sizeof(tok),
			 "members%%member%d%%powercontroller%d%%device",
			 nodeid, pswitchid);
		if (CFG_Get(tok, NULL, &ip_or_dev) != CFG_OK) {
			clulog(LOG_ERR, "Member #%d Switch %d: No device or "
			       "IP associated!\n", nodeid, pswitchid);
			return NULL;
		}

		if ((member_count > 2) && (!strcmp(type, "rps10"))) {
			clulog(LOG_WARNING, "STONITH: Serial power "
			       "controller(s) cannot be used in clusters "
			       "with more than two members!\n");
			return NULL;
		}
	}

	/* Got the device/ip.  Get the port, if possible */
	snprintf(tok, sizeof(tok),
		 "members%%member%d%%powercontroller%d%%port", nodeid,
		 pswitchid);
	CFG_Get(tok, NULL, &portname);
	if (!portname)
		portname = nodename;

	/* Get the username, if applicable */
	snprintf(tok, sizeof(tok),
		 "members%%member%d%%powercontroller%d%%user", nodeid,
		 pswitchid);
	CFG_Get(tok, NULL, &login);

	/* Get the password, if applicable */
	snprintf(tok, sizeof(tok),
		 "members%%member%d%%powercontroller%d%%password", nodeid,
		 pswitchid);
	CFG_Get(tok, NULL, &passwd);

	/*
	 * No duplicate STONITHs; at least, not on a per-host basis
	 */
	for (ston_el = stoniths; ston_el != NULL; 
	     ston_el = ston_el->next) {

		/*
		 * BZ 116380 - allow RPS10 with duplicate configuration
		 * on different members. XXX Special-case ;(
		 */
		if (!strcasecmp(ston_el->devtype, "rps10") &&
		    nodeid != ston_el->nodeid) 
			continue;

		/* Don't even bother with gulm-bridge driver */
		if (!strcasecmp(ston_el->devtype, "gulm-bridge"))
			continue;

		if (!strcmp(ston_el->ipaddr, ip_or_dev) &&
		    (ston_el->port[0] && !strcmp(ston_el->port, portname))) {
			clulog(LOG_WARNING, "%s at %s with port %s already"
			       " controls member #%d; ignoring\n", type,
			       ip_or_dev, portname ? portname : "[default]",
			       ston_el->nodeid);
			return NULL;
		}
	}

	++(*num);
	rv = add_new_stonith(*num, type, ip_or_dev, portname, nodeid,
      			     login, passwd);

	if (rv) {
		if (ip_or_dev && portname) {
			clulog(LOG_INFO,
			       "STONITH: %s at %s, port %s controls %s\n",
		       	       type, ip_or_dev, portname, nodename);
		} else if (ip_or_dev) {
			clulog(LOG_INFO,
			       "STONITH: %s at %s controls %s\n",
		       	       type, ip_or_dev, nodename);
		} else if (portname) {
			clulog(LOG_INFO,
			       "STONITH: %s port %s controls %s\n",
		       	       type, portname, nodename);
		} else {
			clulog(LOG_INFO,
			       "STONITH: %s controls %s\n",
		       	       type, nodename);
		}
	}

	return rv;
}


/**
 * Find the total number of nodes configured in our cluster database.
 * XXX This appears in cluquorumd.c as well.
 *
 * @return		Number of nodes.
 * @see			CFG_Get, total_nodes
 */
int
count_nodes(void)
{
	char stuff[80];
	int count = 0;
	int x;
	char *val;

	count = 0;
	for (x=0; x < MAX_NODES; x++) {
		snprintf(stuff,sizeof(stuff),"members%%member%d%%name", x);
		if (CFG_Get(stuff, NULL, &val) == CFG_OK)
			count++;
	}

	return count;
}


/**
 * Looks at the cluster configuration file and estabilshes a mapping between
 * each cluster member and a corresponding stonith driver.  Along the way
 * it performs some configuration validation checks and also a status 
 * check on the power switch itself.
 *
 * @return	0 on success, 1 on non-fatal error (retries may succeed), -1
 *		on fatal error (don't bother retrying)
 */
int 
clu_stonith_init(void)
{
	char tok[MAX_TOKEN_LEN];
	char *val, *nodename;
	int x,y, num=0, member_count, stonith_count;

	if (stonith_modules_check()) {
		clulog(LOG_CRIT, "clu_stonith_init: no stonith modules.\n");
		return -1;
	}
	
	if (stonith_config_init(STONITH_CONFIG_DIRECTORY, STONITH_CONFIG_FILE)){
		clulog(LOG_CRIT, "clu_stonith_init: unable to initialize "
				"config files.");
		return -1;
	}

	member_count = count_nodes();

	/*
	 * Find all the power controllers for all the hosts ;)
	 */
	for (x=0; x<MAX_NODES; x++) {
		snprintf(tok, sizeof(tok), "members%%member%d%%name", x);
		if (CFG_Get(tok, NULL, &nodename) != CFG_OK)
			continue;

		stonith_count = 0;

		for (y=0; y<MAX_POWER_CONTROLLERS_PER_NODE; y++) {
			snprintf(tok, sizeof(tok),
				 "members%%member%d%%powercontroller%d%%type",
				 x,y);
			if (CFG_Get(tok, NULL, &val) != CFG_OK)
				continue;

			if (find_add_stonith(member_count, &num, val, x, y)) {
				stonith_count++;
			}
		}

		/*
		 * No fencing thingies for a given node == NASTY message
		 */
		if (!stonith_count) {
			clulog(LOG_WARNING, "STONITH: No drivers configured "
			       "for host '%s'!\n", nodename);
			clulog(LOG_WARNING, "STONITH: Data integrity may be "
			       "compromised!\n");
		} else
			clulog(LOG_DEBUG,
			       "%d/%d device maps assigned to host %s\n",
			       stonith_count, num, nodename);
	}

	return 0;
}


/**
 * Allocate and initialize a new stonith module and add it into the 
 * linked list of drivers.
 *
 * @param num		Just a count of how many we have.
 * @param type		Stonith module to load.
 * @param IPaddr	IP address or device name of power switch.
 * @param portname	Port name which we need to power cycle.
 * @param nodeid	Node ID associated with all of this.
 * @param login		User name for switch.
 * @param passwd	Password for switch.
 * @return		NULL on failure or a pointer to the new stonith_el
 *			structure on success.
 */
static stonith_el *
add_new_stonith(int num, char *type, char *IPaddr, char *portname,
		uint32_t nodeid, char *login, char *passwd)
{
	int		rc;
	const char *	optfile = NULL;
	char *		nodename = NULL;
	char	stonith_config_file[MAXPATHLEN];
	stonith_el *ston_el;

	ston_el = malloc(sizeof(stonith_el));
	if (ston_el == NULL) {
		clulog(LOG_ERR, "add_new_stonith: malloc failed.");
		return(NULL);
	}

	memset(ston_el, 0, sizeof(stonith_el));
	/*
	 * Create a separate config file for each stonith driver.  This is
	 * necessary because the stonith modules aren't smart enough to
	 * weed out:
	 * - Entries of different controller types.
	 * - Second instances of the same type of power controller.
	 */
	snprintf(stonith_config_file, sizeof(stonith_config_file),
		 "%s_%d", STONITH_CONFIG_FILE, num);
	optfile = stonith_config_file;
	clulog(LOG_DEBUG, "add_new_stonith: Using config file: %s\n", optfile);

	ston_el->stonith = stonith_new(type);
	if (ston_el->stonith == NULL) {
		clulog(LOG_CRIT,
		       "add_new_stonith: Unable to load stonith: %s\n", type);
		free(ston_el);
		return NULL;
	}
	if (stonith_config_load((char *)optfile, type, IPaddr, login, passwd,
				portname)) {
		clulog(LOG_CRIT,
		       "add_new_stonith: bad ret from clu_load_opt_file\n");
		free(ston_el->stonith);
		free(ston_el);
		return NULL;
	}

	rc = ston_el->stonith->s_ops->set_config_file(ston_el->stonith,
       						      optfile);
	if (rc != S_OK) {
		clulog(LOG_CRIT,
		       "add_new_stonith: Invalid config file for %s device.\n",
		       type);
		ston_el->stonith->s_ops->destroy(ston_el->stonith);
		free(ston_el->stonith);
		free(ston_el);
		return NULL;
	}
	unlink(optfile);

	if (type)
		strncpy(ston_el->devtype, type, sizeof(ston_el->devtype));
	if (IPaddr) 
		strncpy(ston_el->ipaddr, IPaddr, IPADDR_LEN);
	if (portname) {
		strncpy(ston_el->port, portname, sizeof(ston_el->port));
	} else if (getNodeName(nodeid, &nodename) == SUCCESS) {
		/* No port?  Use the nodename then */
		strncpy(ston_el->port, nodename, sizeof(ston_el->port));
	}

	ston_el->nodeid = nodeid;
	ston_el->num = num;
	ston_el->next = stoniths;
	stoniths = ston_el;
	return ston_el;
}


/**
 * Verify that there are some stonith drivers in the directory
 * where their shared library files reside.
 *
 * @return		0 on success, -1 on failure.
 */
static int 
stonith_modules_check(void)
{
#ifdef notyet
	char **stonith_types_list = NULL;

	// XXX - there's a malloc/free bug burried here resulting in segfaults
    	stonith_types_list = stonith_types();
    	if ((stonith_types_list == NULL) || 
	    (*stonith_types_list == NULL)) {
		clulog(LOG_CRIT, "stonith_modules_check: no stonith drivers.");
		return -1;
    	}
#endif //notyet
	return 0;
}


/**
 * See if a given node is controlled by GuLM.
 *
 * @param	nodeid
 * @return	0 if controlled by GuLM, 1 if not
 */
int
clu_stonith_gulm(int nodeid)
{
	stonith_el *ston_el;

	for (ston_el = stoniths; ston_el != NULL; 
	     ston_el = ston_el->next) {

		if (ston_el->nodeid != nodeid)
			continue;

		if (!strcasecmp(ston_el->devtype, "gulm-bridge"))
			return 0;
	}

	return 1;
}

