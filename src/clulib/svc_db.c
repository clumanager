/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/*
 * Author: Gregory P. Myrdal <Myrdal@MissionCriticalLinux.Com>
 *         Xudong Tang       <Tang@MissionCriticalLinux.Com>
 */
/** @file
 * Functions to manipulate the service section of the cluster configuration.
 *
 * XXX Needs Doxygenification.
 * This file contains the functions to manipulate the service
 * section of the cluster configuration database.  Functions
 * here provide support to get, set and check values for service
 * entries.
 */

/*
 * Version string that is filled in by CVS
 * $Revision: 1.11 $
 */

/*
 * This module contains functions that deal with writing and reading
 * from the service section of the cluster database.
 *
 */

/*
 * System includes
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <errno.h> 
#include <netdb.h>  
#include <pwd.h>
#include <grp.h>
#include <regex.h>
#include <sys/stat.h>         
#include <sys/ioctl.h> 
#include <sys/socket.h>  
#include <sys/param.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <net/if.h>    
#include <ctype.h>


/*
 * Cluster includes
 */
#include <clusterdefs.h>
#include <xmlwrap.h>
#include <svcmgr.h>
#include <clulog.h>

/*****************************************************************************
 *
 * The following functions are general utility functions for the service 
 * section of the cluster configuration database.
 *
 ****************************************************************************/

/*
 * getDatabaseToken
 *
 * Given a token string, get its associated value in the database.
 *
 * Return values:
 *	SUCCESS		- found value, value is in 'value'
 *	NOT_FOUND	- did not find a value
 *	FAIL		- failure
 */
int
getDatabaseToken(char *token, char **value)
{
	int retVal;

	switch (retVal=CFG_Get(token, (char *)NULL, value))
	  {
	  case CFG_DEFAULT:
	  case CFG_FALSE:		// token not defined
	    return(NOT_FOUND);
	  case CFG_OK:			// found it
	    return(SUCCESS);
	  default:			// Uh oh, we failed in CFG_Get()
	    clulog(LOG_ERR, 
"Cannot get %s from database; CFG_Get() failed, err=%d\n", token, retVal);
	    return(FAIL);
	  }     
}

/*****************************************************************************
 *
 * The following functions get the token string for a service in the
 * cluster database.  These token strings are defined by metaconfig.
 *
 ****************************************************************************/
/*
 * getSvcNameTokenStr
 *
 * Given a service ID return the metaconfig token string for its name
 * entry in tokenStr.
 *
 * Return values:
 *	SUCCESS		- tokenStr was filled with token string 
 *	FAIL		- on failure
 */
int
getSvcNameTokenStr(int svcID, char *tokenStr, int tokenlen)
{
	if (tokenStr == NULL)
	  {
	    clulog(LOG_ERR,
"Cannot get service name token string, token string is NULL\n");
	    return(FAIL);
	  }

	// services%service0%name
	snprintf(tokenStr, tokenlen, "%s%c%s%d%c%s", 
		 SVC_SERVICES_LIST_STR, CLU_CONFIG_SEPARATOR,
		 SVC_SERVICE_STR, svcID, CLU_CONFIG_SEPARATOR, SVC_NAME_STR);
	return(SUCCESS);
}
/*
 * getSvcCheckIntervalTokenStr
 *
 * Given a service ID return the metaconfig token string for service check
 * interval entry in tokenStr.
 *
 * Return values:
 *	SUCCESS		- tokenStr was filled with token string 
 *	FAIL		- on failure
 */
int
getSvcCheckIntervalTokenStr(int svcID, char *tokenStr, int tokenlen)
{
	if (tokenStr == NULL)
	  {
	    clulog(LOG_ERR,
"Cannot get service check interval token string, token string is NULL\n");
	    return(FAIL);
	  }

	// services%service0%checkInterval
	snprintf(tokenStr, tokenlen, "%s%c%s%d%c%s", 
		 SVC_SERVICES_LIST_STR, CLU_CONFIG_SEPARATOR,
		 SVC_SERVICE_STR, svcID, CLU_CONFIG_SEPARATOR,
		 SVC_CHECK_INTERVAL_STR);
	return(SUCCESS);
}


/****************************************************************************
 *
 * The following functions get the different service entries in the database.
 *
 ***************************************************************************/
/*
 * getSvcID
 *
 * Given a service name return the service ID.  
 *
 * NOTE: there is no setSvcID() as service ID's are persistent and assigned
 * at service create time.  A service ID is created by finding the next 
 * available ID and creating the services%service1%name entry.
 *
 * Return values:
 *	SUCCESS		- service ID found, value stored in svcID
 *	NOT_FOUND	- service ID not found
 *	FAIL		- failure
 */
int
getSvcID(char *svcName, int *svcID)
{
	
	int serviceID;
	char *value=(char *)NULL;

	for (serviceID=0; serviceID < MAX_SERVICES; serviceID++)
	  {
	    if (serviceExists(serviceID) != YES)
	        continue;

	    if (getSvcName(serviceID, &value) != SUCCESS)
	      continue;

	    /*
	     * See if this is the service we are looking for
	     */
	    if (strcmp(value, svcName) == 0)
	      {
	        *svcID = serviceID;
	        return(SUCCESS);
	      }
	  }

	clulog(LOG_DEBUG, "Cannot get service ID for service %s from "
	       "database: not found\n", svcName);

	return(NOT_FOUND);
}


/*
 * getSvcName
 *
 * Given a service ID return the service name in svcName.
 *
 * Return values:
 *	SUCCESS		- a service name was found and stored in svcName
 *	FAIL		- failure
 */
int
getSvcName(int svcID, char **svcName)
{
	char token[MAX_TOKEN_LEN];
	int retVal;
	static char nameBuf[MAX_SERVICE_NAMELEN];

	getSvcNameTokenStr(svcID, token, sizeof(token));
	/*
	 * If we do not find the service name, return the reason why
	 * but send back a string of the service ID so we can get
	 * useful printf's to the log.
	 */
	if ((retVal=getDatabaseToken(token, svcName)) != SUCCESS)
	  {
	    clulog(LOG_DEBUG, "Cannot get service name for service #%d\n",
		   svcID);
	    snprintf(nameBuf, sizeof(nameBuf), "%d", svcID);
	    *svcName=(char *)&nameBuf;
	    return(retVal);
	  }

	return(SUCCESS);
}


/*
 * getSvcMaxFalseStarts
 *
 * Given a service ID return the service name in svcName.
 *
 * Return values:
 *	SUCCESS		- a service name was found and stored in svcName
 *	FAIL		- failure
 */
int
getSvcMaxFalseStarts(int svcID, char **ret)
{
	char token[MAX_TOKEN_LEN];
	int retVal;
	static char nameBuf[MAX_SERVICE_NAMELEN];

	snprintf(token, sizeof(token),
		 "services%%service%d%%maxfalsestarts", svcID);
	/*
	 * If we do not find the service name, return the reason why
	 * but send back a string of the service ID so we can get
	 * useful printf's to the log.
	 */
	if ((retVal=getDatabaseToken(token, ret)) != SUCCESS)
	  {
	    clulog(LOG_DEBUG, "Cannot get max false starts for service #%d\n",
		   svcID);
	    snprintf(nameBuf, sizeof(nameBuf), "%d", svcID);
	    *ret=(char *)&nameBuf;
	    return(retVal);
	  }

	return(SUCCESS);
}


/*
 * getSvcMaxFalseStarts
 *
 * Given a service ID return the service name in svcName.
 *
 * Return values:
 *	SUCCESS		- a service name was found and stored in svcName
 *	FAIL		- failure
 */
int
getSvcMaxRestarts(int svcID, char **ret)
{
	char token[MAX_TOKEN_LEN];
	int retVal;
	static char nameBuf[MAX_SERVICE_NAMELEN];

	snprintf(token, sizeof(token),
		 "services%%service%d%%maxrestarts", svcID);
	/*
	 * If we do not find the service name, return the reason why
	 * but send back a string of the service ID so we can get
	 * useful printf's to the log.
	 */
	if ((retVal=getDatabaseToken(token, ret)) != SUCCESS)
	  {
	    clulog(LOG_DEBUG, "Cannot get Max restarts for service #%d\n",
		   svcID);
	    snprintf(nameBuf, sizeof(nameBuf), "%d", svcID);
	    *ret=(char *)&nameBuf;
	    return(retVal);
	  }

	return(SUCCESS);
}


/*
 * getSvcCheckInterval
 *
 * Given a service ID find the check interval.
 *
 * Return values
 *	SUCCESS		- interval found, value returned in interval
 *	NOT_FOUND	- device not found
 *	FAIL		- failure
 */
int
getSvcCheckInterval(int svcID, char **intervalStr)
{
	char token[MAX_TOKEN_LEN];

	getSvcCheckIntervalTokenStr(svcID, token, sizeof(token));
	return (getDatabaseToken(token, intervalStr));
}


/*****************************************************************************
 *
 * Following are functions that query the database and return YES NO values
 *
 *****************************************************************************/
/*
 * serviceExists
 *
 * Given a service ID determine if the service exists by checking to
 * see if its ID is in the database.  An easy way to do this is to check
 * for the service name since this is the only required attribute of a
 * service.  If the service exists, YES is returned.  If it does not
 * exist, NO is returned.
 */
int
serviceExists(int svcID)
{
	char *svcName;
	char token[MAX_TOKEN_LEN];

	getSvcNameTokenStr(svcID, token, sizeof(token));

	if (getDatabaseToken(token, &svcName) != SUCCESS)
	    return(NO);
	else
	    return(YES);
}


/*
 * getSvcMgrLogLevel
 *
 * Get the log level for the Service Manager from the database.
 */
int
getSvcMgrLogLevel(int *logLevel)
{
	char token[MAX_TOKEN_LEN];
	char *value;
	int retVal;

	snprintf(token, sizeof(token), "%s%c%s",
	       	 SVCMGR_STR, CLU_CONFIG_SEPARATOR, SVCMGR_LOGLEVEL_STR);

	switch (retVal=CFG_Get(token, NULL, &value))
	  {
	  case CFG_DEFAULT:
	  case CFG_FALSE:		// no log level defined, do not error
	    *logLevel=LOG_DEFAULT;
	    break;
	  case CFG_OK:			// found it
	    *logLevel=atoi(value);
	    break;
	  default:			// Uh oh, we failed in CFG_Get()
	    clulog(LOG_ERR, "Cannot get log level for Service Manager from "
		   "database; CFG_Get() failed, err=%d\n", retVal);
	    return(FAIL);
	  }     

	return(SUCCESS);
}


/*
 * getNodeName
 *
 * Given a node ID return its node name in nodeName.  If one is not found
 * return (char *)NULL.
 */
int
getNodeName(int nodeID, char **nodeName)
{
	char token[MAX_TOKEN_LEN];
	char *value;
	int retVal;
	static char nameBuf[MAXHOSTNAMELEN];

	snprintf(token, sizeof(token), "%s%c%s%d%c%s", 
		 NODE_LIST_STR, CLU_CONFIG_SEPARATOR,
		 NODE_STR, nodeID, CLU_CONFIG_SEPARATOR,
		 NODE_NAME_STR);

	/*
	 * If we do not find the service name, return the reason why
	 * but send back a string of the service ID so we can get
	 * useful printf's to the log.
	 */
	if ((retVal=getDatabaseToken(token, &value)) != SUCCESS)
	  {
	    /*
	    clulog(LOG_DEBUG, 
	           "Cannot get node name for member #%d, err=%d\n", 
	           nodeID, retVal);
	     */
	    snprintf(nameBuf, sizeof(nameBuf), "%d", nodeID);
	    *nodeName=(char *)&nameBuf;
	  }

	switch (retVal)
	  {
	  case SUCCESS:			// found it
	    strncpy(nameBuf, value, sizeof(nameBuf));
	    *nodeName = (char*)&nameBuf;
	    break;
	  case NOT_FOUND:		// no node name defined
	  case FAIL:			// error
	    *nodeName=(char *)NULL;
	    return(retVal);
	  }     

	return(SUCCESS);
}


/*
 * getNodeID
 *
 * Given a nodeName, return the corresponding node ID from the cluster
 * database.
 */
int
getNodeID(char *nodeName)
{
	char token[MAX_TOKEN_LEN];
	int nodeID;
	int retVal;
	char *value;

	for (nodeID=0; nodeID < MAX_NODES; nodeID++)
	  {
	    snprintf(token, sizeof(token), "%s%c%s%d%c%s", 
		     NODE_LIST_STR, CLU_CONFIG_SEPARATOR,  
		     NODE_STR, nodeID, CLU_CONFIG_SEPARATOR,
		     NODE_NAME_STR);
	    switch (retVal=CFG_Get(token, NULL, &value))
	      {
	        case CFG_OK:		// Found it
	          break;

		case CFG_DEFAULT:
	        case CFG_FALSE:		// Not found, try next
	          continue;

	        default:
	          clulog(LOG_ERR,
"Cannot get member ID for %s from database; CFG_Get() failed, err=%d.\n", 
	              nodeName, retVal);
	          return(FAIL);
	      }

	    if (strcmp(value,nodeName) == 0)
	      {
	        return(nodeID);
	      }
	  }

	clulog(LOG_ERR,
"Cannot get member ID for %s from database; not found\n", nodeName);
	return(FAIL);
}
