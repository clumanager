/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * View-Formation Library.
 *                    
 * XXX Not thread safe?
 */
#include <msgsvc.h>
#include <clusterdefs.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/time.h>
#include <membership.h>
#include <pthread.h>
#include <vf.h>
#include <stdio.h>
#include <fcntl.h>

static key_node_t *key_list = NULL;	/** List of key nodes. */
static uint32_t __node_id = -1;		/** Our node ID, set with vf_init. */
static uint32_t __daemon_id = -1;	/** Our daemon ID, set with vf_init. */

/*
 * TODO: We could make it thread safe, but this might be unnecessary work
 * Solution: Super-coarse-grained-bad-code-locking!
 */
static pthread_mutex_t id_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t key_list_mutex = PTHREAD_MUTEX_INITIALIZER;


/*
 * External Functions
 */
extern int __select_retry(int, fd_set *, fd_set *, fd_set *, struct timeval *);

/*
 * Internal Functions
 */
static int send_to_all(int *peer_fds, int32_t command, int arg1, int arg2,
		       int log_errors);
static int vf_send_abort(int *fds);
static int vf_send_commit(int *fds);
static void close_all(int *fds);
static key_node_t * kn_find_key(uint32_t keyid);
static key_node_t * kn_find_fd(uint32_t fd);
static int vf_handle_join_view_msg(int fd, vf_msg_t * hdrp);
static int vf_resolve_views(key_node_t *key_node);
static int vf_unanimous(int *peer_fds, int remain, int timeout);
static view_node_t * vn_new(int fd, uint32_t nodeid, uint64_t viewno,
			    void *data, uint32_t datalen);

/* Join-view buffer list functions */
static int vn_cmp(view_node_t *left, view_node_t *right);
static int vn_insert_sorted(view_node_t **head, view_node_t *node);
static view_node_t * vn_remove(view_node_t **head, int fd);
static int vf_buffer_join_msg(int fd, vf_msg_t *hdr,
			      struct timeval *timeout);

/* Commits buffer list functions */
static int vc_cmp(commit_node_t *left, commit_node_t *right);
static int vc_insert_sorted(commit_node_t **head, commit_node_t *node);
static commit_node_t * vc_remove(commit_node_t **head, int fd);
static int vf_buffer_commit(int fd);

/* Simple functions which client calls to vote/abort */
static int vf_vote_yes(int fd);
static int vf_vote_no(int fd);
static int vf_abort(int fd);
static int tv_cmp(struct timeval *left, struct timeval *right);

/* Resolution */
static int vf_try_commit(key_node_t *key_node);

int vf_init(uint32_t my_node_id, uint32_t my_daemon_id);
int vf_key_init(uint32_t keyid, int timeout, vf_vote_cb_t vote_cb,
		vf_commit_cb_t commit_cb);
int vf_start(memb_mask_t curr_memb_mask, uint32_t flags,
	     uint32_t keyid, void *data, uint32_t datalen);
int vf_process_msg(int handle, generic_msg_hdr *msgp, int nbytes);
int vf_end(uint32_t keyid);
int vf_current(uint32_t keyid, uint64_t *view, void **data,
	       uint32_t *datalen);
	       
/* Reply to request for current data */
static int vf_send_current(int fd, uint32_t keyid);


static int
send_to_all(int *peer_fds, int32_t command, int arg1, int arg2, int log_errors)
{
	generic_msg_hdr hdr;
	int x, rv = 0;

	hdr.gh_magic = GENERIC_HDR_MAGIC;
	hdr.gh_length = sizeof(hdr);
	hdr.gh_command = command;
	hdr.gh_arg1 = arg1;
	hdr.gh_arg2 = arg2;

	swab_generic_msg_hdr(&hdr);

	for (x=0; x<MAX_NODES; x++)
		if (peer_fds[x] != -1) {
			if (msg_send(peer_fds[x], &hdr, sizeof(hdr)) == sizeof(hdr))
				continue;

			if (log_errors) {
				clulog(LOG_ERR, "Failed to send %d "
				       "bytes to %d!\n", sizeof(hdr),
				       x);
			}
			rv = -1;
		}

	return rv;
}


static int 
vf_send_abort(int *fds)
{
	clulog(LOG_DEBUG,"VF: Broadcasting ABORT\n");
	return send_to_all(fds, VF_MESSAGE, VF_ABORT, 0, 0);
}


static int
vf_send_commit(int *fds)
{
	clulog(LOG_DEBUG,"VF: Broadcasting FORMED\n");
	return send_to_all(fds, VF_MESSAGE, VF_VIEW_FORMED, 0, 1);
}


static void
close_all(int *fds)
{
	int x;
	for (x = 0; x < MAX_NODES; x++)
		if (fds[x] >= 0) {
			msg_close(fds[x]);
			fds[x] = -1;
		}
}


static key_node_t *
kn_find_key(uint32_t keyid)
{
	key_node_t *cur;

	for (cur = key_list; cur; cur = cur->kn_next)
		if (cur->kn_keyid == keyid)
			return cur;

	return NULL;
}


static key_node_t *
kn_find_fd(uint32_t fd)
{
	key_node_t *cur;
	view_node_t *curvn;

	for (cur = key_list; cur; cur = cur->kn_next)
		for (curvn = cur->kn_jvlist; curvn; curvn = curvn->vn_next)
			if (curvn->vn_fd == fd)
				return cur;

	return NULL;
}


static int
vf_handle_join_view_msg(int fd, vf_msg_t * hdrp)
{
	struct timeval timeout;
	key_node_t *key_node;

	clulog(LOG_DEBUG, "VF_JOIN_VIEW from member #%d! Key: 0x%08x #%d\n",
	       hdrp->vm_msg.vf_coordinator, hdrp->vm_msg.vf_keyid,
	       (int) hdrp->vm_msg.vf_view);

	pthread_mutex_lock(&key_list_mutex);
	key_node = kn_find_key(hdrp->vm_msg.vf_keyid);
	pthread_mutex_unlock(&key_list_mutex);

	/*
	 * Call the voting callback function to see if we should continue.
	 */
	if (key_node && key_node->kn_vote_cb) {
		if ((key_node->kn_vote_cb)(hdrp->vm_msg.vf_keyid,
					   hdrp->vm_msg.vf_view,
					   hdrp->vm_msg.vf_data,
					   hdrp->vm_msg.vf_datalen) == 0) {
			clulog(LOG_DEBUG, "VF: Voting NO (via callback)\n");
			vf_vote_no(fd);
			return VFR_OK;
		}
	}
	
	/*
	 * Buffer the join-view message.
	 */
	timeout.tv_sec = key_node->kn_tsec;
	timeout.tv_usec = 0;

	if (vf_buffer_join_msg(fd, (vf_msg_t *) hdrp, &timeout)) {
		clulog(LOG_DEBUG, "VF: Voting YES\n");
		vf_vote_yes(fd);
		return VFR_OK;
	}

	clulog(LOG_DEBUG, "VF: Voting NO\n");
	vf_vote_no(fd);
	return VFR_NO;
}


/*
 * Try to resolve (JOIN_VIEW, FORMED_VIEW) messages in the proper order.
 * Returns the number of commits.
 */
static int
vf_resolve_views(key_node_t *key_node)
{
	int commitfd, commits = 0;

	while ((commitfd = vf_try_commit(key_node)) != -1) {

		/* XXX in general, this shouldn't kill the fd... */
		commits++;
		msg_close(commitfd);
	}

	if (key_node->kn_commit_cb) {
		(key_node->kn_commit_cb)(key_node->kn_keyid,
					 key_node->kn_viewno,
					 key_node->kn_data,
					 key_node->kn_datalen);
	}

	return commits;
}


static int
vf_unanimous(int *peer_fds, int remain, int timeout)
{
	generic_msg_hdr response;
	struct timeval tv;
	fd_set rfds;
	int nready, x, ret;

	/* Set up for the select */
	tv.tv_sec = timeout;
	tv.tv_usec = 0;

	/*
	 * Wait for activity
	 */
	
	/*
	 * Flag hosts which we received messages from so we don't
	 * read a second message.
	 */
	while (remain) {
		FD_ZERO(&rfds);
		for (x = 0; x < MAX_NODES; x++)
			if (peer_fds[x] != -1)
				FD_SET(peer_fds[x], &rfds);

		nready = __select_retry(MAX_FDS, &rfds, NULL, NULL, &tv);
		if (nready <= 0) {
			if (nready == 0)
				clulog(LOG_INFO,"VF Abort: Timed out!\n");
			else
				clulog(LOG_ERR,"VF Abort: %s\n",
				       strerror(errno));
			return 0;
		}

		for (x = 0; (x < MAX_NODES) && nready; x++) {
			if ((peer_fds[x] == -1) ||
			    (!FD_ISSET(peer_fds[x], &rfds)))
				continue;

			remain--;
			nready--;
			/*
			 * Get reply from node x.
			 */
			memset(&response, 0, sizeof(response));

			ret = msg_receive_timeout(peer_fds[x], &response,
						  sizeof(response), 2);
			if (ret < 0) {
				clulog(LOG_INFO,"VF: Abort: Timed out during "
				       "receive from member #%d\n", x);
				return 0;
			}
			
			/*
			 * Decode & validate message
			 */
			swab_generic_msg_hdr(&response);
			if ((response.gh_magic != GENERIC_HDR_MAGIC) ||
			    (response.gh_command != VF_MESSAGE) ||
			    (response.gh_arg1 != VF_VOTE)) {
				clulog(LOG_ERR, "VF: Abort: Invalid header in"
				       " reply from member #%d\n", x);
				clulog(LOG_ERR, "VF: Abort: %d bytes {%08x %08x %08x %08x}\n",
				       ret, (int)response.gh_magic, (int)response.gh_command,
				       (int)response.gh_arg1, (int)response.gh_arg2);
				return 0;
			}
			
			/*
			 * If we get a 'NO', we are done.
			 */
			if (response.gh_arg2 != 1) {
				/*
				 * XXX ok, it might be a mangled message;
				 * treat it as no anyway!
				 */
				clulog(LOG_DEBUG,
				       "VF: Abort: Member #%d voted NO\n", x);
				return 0;
			}

			clulog(LOG_DEBUG, "VF: Member #%d voted YES\n", x);
		}
	}

	/*
	 * Whohoooooooo!
	 */
	return 1;
}


/*
 * ...
 */
static view_node_t *
vn_new(int fd, uint32_t nodeid, uint64_t viewno, void *data, uint32_t datalen)
{
	view_node_t *new;
	size_t totallen;

	totallen = sizeof(*new) + datalen;
	new = malloc(totallen);
	if (!new)
		return NULL;

	memset(new,0,totallen);

	new->vn_fd = fd;
	new->vn_nodeid = nodeid;
	new->vn_viewno = viewno;
	new->vn_datalen = datalen;
	memcpy(new->vn_data, data, datalen);

	return new;
}


static int
vn_cmp(view_node_t *left, view_node_t *right)
{
	if ((left->vn_viewno < right->vn_viewno) || 
	    ((left->vn_viewno == right->vn_viewno) &&
	     (left->vn_nodeid < right->vn_nodeid)))
		return -1;

	/* Equal? ERROR!!! */
	if ((left->vn_viewno == right->vn_viewno) &&
	    (left->vn_nodeid == right->vn_nodeid))
		return 0;

	return 1;
}


static int
vn_insert_sorted(view_node_t **head, view_node_t *node)
{
	view_node_t *cur = *head, *back = NULL;

	/* only entry */
	if (!cur) {
		*head = node;
		return 1;
	}

	while (cur) {
		switch (vn_cmp(node, cur)) {
		case 0:
			/* duplicate */
			return 0;
		case -1:
			if (back) {
				/* middle */
				node->vn_next = cur;
				back->vn_next = node;
				return 1;
			}

			node->vn_next = *head;
			*head = node;
			return 1;
		}

		back = cur;
		cur = cur->vn_next;
	}

	/* end */
	back->vn_next = node;
	node->vn_next = NULL;

	return 1;
}


static view_node_t *
vn_remove(view_node_t **head, int fd)
{
	view_node_t *cur = *head, *back = NULL;

	if (!cur)
		return NULL;

	do {
		if (cur->vn_fd == fd) {
			if (back) {
				back->vn_next = cur->vn_next;
				cur->vn_next = NULL;
				return cur;
			}

			*head = cur->vn_next;
			cur->vn_next = NULL;
			return cur;
		}

		back = cur;
		cur = cur->vn_next;
	} while (cur);

	return NULL;
}


/*
 * Buffer a join-view message.  We attempt to resolve the buffered join-view
 * messages whenever:
 * (a) we receive a commit message
 * (b) we don't receive any messages.
 */
static int
vf_buffer_join_msg(int fd, vf_msg_t *hdr, struct timeval *timeout)
{
	key_node_t *key_node;
	view_node_t *newp;
	int rv = 0;

	key_node = kn_find_key(hdr->vm_msg.vf_keyid);
	if (!key_node) {
		clulog(LOG_DEBUG, "Key 0x%08x not initialized\n",
		       hdr->vm_msg.vf_keyid);
		return 0;
	}

	/*
	 * Store if the view < viewno.
	 */
	if (hdr->vm_msg.vf_view < key_node->kn_viewno) {
		return 0;
	}

	newp = vn_new(fd, hdr->vm_msg.vf_coordinator, hdr->vm_msg.vf_view, 
		      hdr->vm_msg.vf_data, hdr->vm_msg.vf_datalen);

	if (timeout && (timeout->tv_sec || timeout->tv_usec)) {
		if (getuptime(&newp->vn_timeout) == -1) {
			/* XXX What do we do here? */
			free(newp);
		return 0;
		}
	
		newp->vn_timeout.tv_sec += timeout->tv_sec;
		newp->vn_timeout.tv_usec += timeout->tv_usec;
	}

	rv = vn_insert_sorted(&key_node->kn_jvlist, newp);
	if (!rv)
		free(newp);

	return rv;
}


/*
 * XXX...
 */
static int
vc_cmp(commit_node_t *left, commit_node_t *right)
{
	if (left->vc_fd < right->vc_fd)
		return -1;

	if (left->vc_fd == right->vc_fd)
		return 0;

	return 1;
}


static int
vc_insert_sorted(commit_node_t **head, commit_node_t *node)
{
	commit_node_t *cur = *head, *back = NULL;

	/* only entry */
	if (!cur) {
		*head = node;
		return 1;
	}

	while (cur) {
		switch (vc_cmp(node, cur)) {
		case 0:
			/* duplicate */
			return 0;
		case -1:
			if (back) {
				/* middle */
				node->vc_next = cur;
				back->vc_next = node;
				return 1;
			}

			node->vc_next = *head;
			*head = node;
			return 1;
		}

		back = cur;
		cur = cur->vc_next;
	}

	/* end */
	back->vc_next = node;
	node->vc_next = NULL;

	return 1;
}


static commit_node_t *
vc_remove(commit_node_t **head, int fd)
{
	commit_node_t *cur = *head, *back = NULL;

	if (!cur)
		return NULL;

	do {
		if (cur->vc_fd == fd) {
			if (back) {
				back->vc_next = cur->vc_next;
				cur->vc_next = NULL;
				return cur;
			}

			*head = cur->vc_next;
			cur->vc_next = NULL;
			return cur;
		}

		back = cur;
		cur = cur->vc_next;
	} while (cur);

	return NULL;
}


/*
 * Buffer a commit message received on a file descriptor.  We don't need
 * to know the node id; since the file descriptor will still be open from
 * the last 'join-view' message.
 */
static int
vf_buffer_commit(int fd)
{
	key_node_t *key_node;
	commit_node_t *newp;
	int rv;

	key_node = kn_find_fd(fd);
	if (!key_node)
		return 0;

	newp = malloc(sizeof(*newp));
	if (!newp)
		return 0;

	newp->vc_next = NULL;
	newp->vc_fd = fd;

	rv = vc_insert_sorted(&key_node->kn_clist, newp);
	if (!rv)
		free(newp);

	return rv;
}


static int
vf_vote_yes(int fd)
{
	return msg_send_simple(fd, VF_MESSAGE, VF_VOTE, 1);

}


static int
vf_vote_no(int fd)
{
	return msg_send_simple(fd, VF_MESSAGE, VF_VOTE, 0);
}


static int
vf_abort(int fd)
{
	key_node_t *key_node;
	view_node_t *cur;

	key_node = kn_find_fd(fd);
	if (!key_node)
		return -1;

	cur = vn_remove(&key_node->kn_jvlist, fd);
	if (!cur)
		return -1;

	free(cur);
	return 0;
}


static int
tv_cmp(struct timeval *left, struct timeval *right)
{
	if (left->tv_sec > right->tv_sec)
		return 1;

	if (left->tv_sec < right->tv_sec)
		return -1;

	if (left->tv_usec > right->tv_usec)
		return 1;

	if (left->tv_usec < right->tv_usec)
		return -1;

	return 0;
}


/**
 * Grab the uptime from /proc/uptime.
 * 
 * @param tv		Timeval struct to store time in.  The sec
 * 			field contains seconds, the usec field 
 * 			contains the hundredths-of-seconds (converted
 * 			to micro-seconds)
 * @return		-1 on failure, 0 on success.
 */
int
getuptime(struct timeval *tv)
{
	FILE *fp;
	struct timeval junk;
	int rv;
	
	fp = fopen("/proc/uptime","r");
	
	if (!fp)
		return -1;

	rv = fscanf(fp,"%ld.%ld %ld.%ld\n", &tv->tv_sec, &tv->tv_usec,
		    &junk.tv_sec, &junk.tv_usec);
	fclose(fp);
	
	if (rv != 4) {
		return -1;
	}
	
	tv->tv_usec *= 10000;
	
	return 0;
}


/**
 * Try to commit views in a given key_node.
 */
static int
vf_try_commit(key_node_t *key_node)
{
	view_node_t *vnp;
	commit_node_t *cmp;
	int fd = -1;

	if (!key_node)
		return -1;

	if (!key_node->kn_jvlist)
		return -1;

	fd = key_node->kn_jvlist->vn_fd;
		
	cmp = vc_remove(&key_node->kn_clist, fd);
	if (!cmp) {
		/*clulog(LOG_DEBUG,
		       "VF: Commit for fd%d not received yet!", fd);*/
		return -1;
	}

	free(cmp); /* no need for it any longer */
		
	vnp = vn_remove(&key_node->kn_jvlist, fd);
	if (!vnp) {
		/*
		 * But, we know it was the first element on the list?!!
		 */
		clulog(LOG_CRIT, "VF: QUAAAAAAAAAAAAAACKKKK!");
		raise(SIGSTOP);
	}
	
	clulog(LOG_DEBUG, "VF: Commit Key 0x%08x #%d from member #%d\n",
	       key_node->kn_keyid, (int)vnp->vn_viewno, vnp->vn_nodeid);

	/*
	 * Store the current view of everything in our key node
	 */
	key_node->kn_viewno = vnp->vn_viewno;
	if (key_node->kn_data)
		free(key_node->kn_data);
	key_node->kn_datalen = vnp->vn_datalen;
	key_node->kn_data = malloc(vnp->vn_datalen);
	memcpy(key_node->kn_data, vnp->vn_data, vnp->vn_datalen);

	free(vnp);
	return fd;
}


int
vf_running(uint32_t keyid)
{
	key_node_t *key_node;
	int rv;
	
	pthread_mutex_lock(&key_list_mutex);
	key_node = kn_find_key(keyid);

	rv = !!key_node->kn_pid;
	
	pthread_mutex_unlock(&key_list_mutex);
	return rv;
}


/**
 * Initialize VF.  Initializes the View Formation sub system.
 * @param my_node_id	The node ID of the caller.
 * @param my_daemon_id	The daemon ID of the caller.
 * @return		0 on success, -1 on failure.
 */
int
vf_init(uint32_t my_node_id, uint32_t my_daemon_id)
{
	if ((my_node_id < 0) || (my_node_id >= MAX_NODES))
		return -1;

	if (my_daemon_id < 0)
		return -1;

	pthread_mutex_lock(&id_mutex);
	__node_id = my_node_id;
	__daemon_id = my_daemon_id;
	pthread_mutex_unlock(&id_mutex);

	return 0;
}


/**
 * Adds a key to key node list and sets up callback functions.
 *
 * @param keyid		The ID of the key to add.
 * @param timeout	Amount of time to wait before purging a JOIN_VIEW
 *			message from our buffers.
 * @param vote_cb	Function to call on a given data set/view number
 *			to help decide whether to vote yes or no.  This is
 *			optional, and DOES NOT obviate the need for VF's
 *			decision-making (version/node ID based).  Also,
 *			the data from the view-node is passed UNCOPIED to
 *			the callback function!
 * @param commit_cb	Function to call when a key has had one or more
 *			commits.  Same info applies: the data passed to the
 *			callback function is UNCOPIED.
 * @return 0 (always)
 */
int
vf_key_init(uint32_t keyid, int timeout, vf_vote_cb_t vote_cb,
	    vf_commit_cb_t commit_cb)
{
	key_node_t *newnode = NULL;

	pthread_mutex_lock(&key_list_mutex);

	newnode = kn_find_key(keyid);
	if (newnode) {
		clulog(LOG_ERR, "Key 0x%08x already initialized\n",
		       keyid);
		pthread_mutex_unlock(&key_list_mutex);
		return -1;
	}

	newnode = malloc(sizeof(*newnode));
	newnode->kn_data = NULL;
	memset(newnode,0,sizeof(*newnode));
	newnode->kn_keyid = keyid;
	newnode->kn_vote_cb = vote_cb;
	newnode->kn_commit_cb = commit_cb;
	newnode->kn_tsec = timeout;

	newnode->kn_next = key_list;
	key_list = newnode;

	pthread_mutex_unlock(&key_list_mutex);

	return 0;
}


static int
block_signal(int sig)
{
       	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, sig);
	
	return(sigprocmask(SIG_BLOCK, &set, NULL));
}


vf_msg_t *
build_vf_data_message(int cmd, uint32_t keyid, void *data, uint32_t datalen,
		      uint64_t viewno, uint32_t *retlen)
{
	uint32_t totallen;
	vf_msg_t *msg;
	/*
	 * build the message
	 */
	totallen = sizeof(vf_msg_t) + datalen;
	msg = malloc(totallen);
	*retlen = 0;
	if (!msg)
		return NULL;
	memset(msg, 0, totallen);
	
	/* header */
	msg->vm_hdr.gh_magic = GENERIC_HDR_MAGIC;
	msg->vm_hdr.gh_length = totallen;
	msg->vm_hdr.gh_command = VF_MESSAGE;
	msg->vm_hdr.gh_arg1 = cmd;

	/* Data */
	msg->vm_msg.vf_keyid = keyid;
	msg->vm_msg.vf_datalen = datalen;
	msg->vm_msg.vf_coordinator = __node_id;
	msg->vm_msg.vf_view = viewno;
	memcpy(msg->vm_msg.vf_data, data, datalen);

	*retlen = totallen;
	return msg;
}


/**
 * Begin VF.  Begins View-Formation for agiven set of data.
 *
 * @param curr_memb_mask Current membership mask.
 * @param flags		Operational flags.
 * @param keyid		Key ID of the data to distribute.
 * @param data		The actual data to distribute.
 * @param datalen	The length of the data.
 * @param viewno	The current view number of the data.
 * @return		-1 on failure, or 0 on success.  The parent will
 * 			either get a SIGCHLD or can randomly call vf_end()
 * 			on keyid to cause the VF child to be cleaned up.
 * @see vf_end
 */
int
vf_start(memb_mask_t curr_memb_mask, uint32_t flags, uint32_t keyid,
	 void *data, uint32_t datalen)
{
	int peer_fds[MAX_NODES];
	key_node_t *key_node;
	vf_msg_t *join_view;
	int remain = 0, x, rv = 1;
	uint32_t totallen;
	struct timeval start, end, dif, timeout;
	int pid;

	if (!data || !datalen)
		return -1;

	pthread_mutex_lock(&key_list_mutex);
	key_node = kn_find_key(keyid);
	if (!key_node) {
		pthread_mutex_unlock(&key_list_mutex);
		return -1;
	}

	if (key_node->kn_pid) {
		pthread_mutex_unlock(&key_list_mutex);
		clulog(LOG_DEBUG, "VF: PID%d running\n",key_node->kn_pid);
		errno = EAGAIN;
		return -1;
	}

	/* Parent process is done with all data needing protection */
	pthread_mutex_unlock(&key_list_mutex);

	/* set to -1 */
	memset((void *)(peer_fds), 0xff, sizeof(peer_fds));
	getuptime(&start);

	/* Fork */
	switch (pid = fork()) {
	case -1:
		clulog(LOG_CRIT, "VF: Fork failure: %s\n", strerror(errno));
		return -1;

	case 0:		/* child */
		block_signal(SIGTERM);
		break;

	default:	/* Parent */
		close_all(peer_fds);

		/* Store the pid */
		key_node->kn_pid = pid;
		return 0;
	}

	for (x = 0; x < FD_SETSIZE; x++) {
		if (fcntl(x, F_GETFD, 0) < 0)
			continue;
		close(x);
	}

	x = open("/dev/null", O_RDWR);
	if (x >= 0) {
		if (x != 0) dup2(x, 0);
		if (x != 1) dup2(x, 1);
		if (x != 2) dup2(x, 2);
	}

	for (x=0; x<MAX_NODES; x++)
		peer_fds[x] = -1;
retry_top:
	/*
	 * Connect to everyone, except ourself.  We separate this from the
	 * initial send cycle because the connect cycle can cause timeouts
	 * within the code - ie, if a node is down, it is likely the connect
	 * will take longer than the client is expecting to wait for the
	 * commit/abort messages!
	 *
	 * We assume we're up.  Since challenge-response needs both
	 * processes to be operational...
	 */
	for (x = 0; x < MAX_NODES; x++) {
		if (!memb_online(curr_memb_mask, x)) {
			peer_fds[x] = -1;
			continue;
		}

		if (peer_fds[x] != -1)
			continue;

		clulog(LOG_DEBUG, "VF: Connecting to member #%d\n",x);
		/* 5 seconds is a little long for us. */
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;

		pthread_mutex_lock(&id_mutex);
		peer_fds[x] = msg_open_timeout(__daemon_id, x,
					       &timeout);
		pthread_mutex_unlock(&id_mutex);

		if (peer_fds[x] == -1) {
			clulog(LOG_DEBUG, "VF: Connect to %d failed: %s\n",
			       x, strerror(errno));
			if (flags & VFF_RETRY)
				goto retry_top;
			if (flags & VFF_IGNORE_ERRORS)
				continue;
			exit(1);
		}
	}

	join_view = build_vf_data_message(VF_JOIN_VIEW, keyid, data, datalen,
					  key_node->kn_viewno+1, &totallen);

	if (!join_view)
		exit(1);

	clulog(LOG_DEBUG, "VF: Push %d.%d #%d\n", __node_id,
	       getpid(), (int)join_view->vm_msg.vf_view);
	/* 
	 * Encode the package.
	 */
	swab_vf_msg_t(join_view);

	/*
	 * Send our message to everyone
	 */
	for (x = 0; x < MAX_NODES; x++) {
		if (peer_fds[x] == -1)
			continue;

		clulog(LOG_DEBUG, "VF: Sending to member #%d\n",x);
		if (msg_send(peer_fds[x], join_view, totallen) != totallen) {
			vf_send_abort(peer_fds);
			clulog(LOG_ERR,
			       "VF Abort: Failed to send to member #%d\n", x);
			close_all(peer_fds);

			free(join_view);
			exit(1);
		} 

		remain++;
	}

	clulog(LOG_DEBUG, "VF: Checking for consensus...\n");
	/*
	 * See if we have a consensus =)
	 */
	if ((rv = (vf_unanimous(peer_fds, remain, VF_COORD_TIMEOUT)))) {
		vf_send_commit(peer_fds);

		getuptime(&end);

		dif.tv_usec = end.tv_usec - start.tv_usec;
		dif.tv_sec = end.tv_sec - start.tv_sec;
		
		if (dif.tv_usec < 0) {
		    dif.tv_usec += 1000000;
		    dif.tv_sec--;
		}

		clulog(LOG_DEBUG, "VF: Converge Time: %d.%06d\n", dif.tv_sec,
		       dif.tv_usec);
	} else {
		vf_send_abort(peer_fds);
		clulog(LOG_DEBUG, "VF: Aborted!\n");
	}

	/*
	 * Clean up
	 */
	close_all(peer_fds);

	/*
	 * unanimous returns 1 for true; 0 for false, so negate it and
	 * return our value...
	 */
	free(join_view);
	exit(!rv);
}


/**
 * Cleans up a given instance of a child running View-Formation.
 *
 * @param keyid		Key ID to clean up.
 * @return		-1 if no children, -2 if child has not exited,
 *			or the exit status of the child.
 * @see vf_start
 */
int
vf_end(uint32_t keyid)
{
	key_node_t *key_node = NULL;
	pid_t rv = -1;
	int status;

	pthread_mutex_lock(&key_list_mutex);
	key_node = kn_find_key(keyid);

	if (key_node->kn_pid) 
		rv = wait4(key_node->kn_pid, &status, WNOHANG, NULL);

	switch (rv) {
	case 0:
		clulog(LOG_DEBUG, "VF: Key 0x%08x Still running\n", keyid);
		pthread_mutex_unlock(&key_list_mutex);
		return -2;
	case -1:
		/*
	    	 * no children - okay to continue
    		 */
		pthread_mutex_unlock(&key_list_mutex);
		return -1;
	}

	/*
	 * child exited
	 */
	key_node->kn_pid = 0;
	pthread_mutex_unlock(&key_list_mutex);

	clulog(LOG_DEBUG, "VF: pid %d exited, status %d\n", rv,
	       WEXITSTATUS(status));
	return WEXITSTATUS(status);
}


/**
 * Purge an unresolved JOIN-VIEW message if it has expired.  This only
 * purges a single message; if used, it should be called in a while()
 * loop.  The function returns a file descriptor which can be closed and
 * cleaned up by the caller if a request has indeed timed out.  Also,
 * if a request has timed out, the function calls vf_resolve_views to try
 * to resolve any outstanding views which were opened up by the timed-out
 * request.
 *
 * @param keyid		Key ID on which to purge timeouts.
 * @param fd		Pointer which, upon return, will either contain -1
 *			whenever VFR_NO is the return value, or the file
 *			descriptor which was resolved.
 * @return		VFR_ERROR on error.  VFR_NO if there are no timed-out
 *			requests, or if there are no requests at all, or if
 *			keyid isn't valid.  VFR_OK if there are timed-out
 *			requests and the virtue of removing the timed-out
 *			requests did not cause commit-resolution, or
 *			VFR_COMMIT if new views	were committed.  
 */
int
vf_purge(uint32_t keyid, int *fd)
{
	key_node_t *key_node = NULL;
	view_node_t *cur, *dead = NULL;
	struct timeval tv;

	*fd = -1;
	
	pthread_mutex_lock(&key_list_mutex);
	key_node = kn_find_key(keyid);
	if (!key_node) {
		pthread_mutex_unlock(&key_list_mutex);
		return VFR_NO;
	}
	pthread_mutex_unlock(&key_list_mutex);

	cur = key_node->kn_jvlist;

	if (!cur)
		return VFR_NO;

	if (getuptime(&tv) == -1) {
		clulog(LOG_ERR, "VF: getuptime(): %s\n", strerror(errno));
		return VFR_ERROR;
	}

	for (; cur; cur = cur->vn_next) {
		if (tv_cmp(&tv, &cur->vn_timeout) < 0)
			continue;

		*fd = cur->vn_fd;
		dead = vn_remove(&key_node->kn_jvlist, *fd);
		free(dead);

		/*
		 * returns the removed associated file descriptor
		 * so that we can close it and get on with life
		 */
		break;
	}

	if (*fd == -1)
		return VFR_NO;
		
	return (vf_resolve_views(key_node) ? VFR_COMMIT : VFR_OK);
}


/**
 * Process a VF message.
 *
 * @param handle	File descriptor on which msgp was received.
 * @param msgp		Pointer to already-received message.
 * @param nbytes	Length of msgp.
 * @return		-1 on failure, 0 on success.
 */
int
vf_process_msg(int handle, generic_msg_hdr *msgp, int nbytes)
{
	vf_msg_t *hdrp;

	if ((nbytes <= 0) || (nbytes < sizeof(generic_msg_hdr)) ||
	    (msgp->gh_command != VF_MESSAGE))
		return VFR_ERROR;

	switch(msgp->gh_arg1) {
	case VF_CURRENT:
		return vf_send_current(handle, msgp->gh_arg2);
	
	case VF_JOIN_VIEW:
		/* Validate size... */
		if (nbytes < sizeof(*hdrp)) {
			clulog(LOG_WARNING,
			       "VF_JOIN_VIEW message too short!\n");
			return VFR_ERROR;
		}

		/* Unswap so we can swab the whole message */
		hdrp = (vf_msg_t *)msgp;
		swab_vf_msg_info_t(&hdrp->vm_msg);

		if ((hdrp->vm_msg.vf_datalen + sizeof(*hdrp)) != nbytes) {
			clulog(LOG_ERR, "VF_JOIN_VIEW: Invalid size %d/%d\n",
			       nbytes,
			       hdrp->vm_msg.vf_datalen + sizeof(*hdrp));

			free(msgp);
			return VFR_ERROR;
		}
		return vf_handle_join_view_msg(handle, hdrp);
		
	case VF_ABORT:
		clulog(LOG_DEBUG, "VF: Received VF_ABORT, fd%d\n", handle);
		vf_abort(handle);
		return VFR_ABORT;
		
	case VF_VIEW_FORMED:
		clulog(LOG_DEBUG, "VF: Received VF_VIEW_FORMED, fd%d\n",
		       handle);
		vf_buffer_commit(handle);
		return (vf_resolve_views(kn_find_fd(handle)) ?
			VFR_COMMIT : VFR_OK);
			
	default:
		clulog(LOG_DEBUG, "VF: Unknown msg type 0x%08x\n",
		       msgp->gh_arg1);
	}

	return VFR_OK;
}


/**
 * Retrieves the current dataset for a given key ID.
 *
 * @param keyid		Key ID of data set to retrieve.
 * @param view		Pointer which will be filled with the current data
 *			set's view number.
 * @param data		Pointer-to-pointer which will be allocated and
 *			filled with the current data set.  Caller must free.
 * @param datalen	Pointer which will be filled with the current data
 *			set's size.
 * @return		-1 on failure, 0 on success.
 */
int
vf_current(uint32_t keyid, uint64_t *view, void **data, uint32_t *datalen)
{
	key_node_t *key_node;

	pthread_mutex_lock(&key_list_mutex);

	key_node = kn_find_key(keyid);
	if (!key_node) {
		pthread_mutex_unlock(&key_list_mutex);
		return VFR_ERROR;
	}

	*data = malloc(key_node->kn_datalen);
	if (! *data) {
		pthread_mutex_unlock(&key_list_mutex);
		return VFR_ERROR;
	}

	memcpy(*data, key_node->kn_data, key_node->kn_datalen);
	*datalen = key_node->kn_datalen;
	*view = key_node->kn_viewno;

	pthread_mutex_unlock(&key_list_mutex);

	return VFR_OK;
}


static int
vf_send_current(int fd, uint32_t keyid)
{
	key_node_t *key_node;
	vf_msg_t *msg;
	uint32_t totallen;

	if (fd == -1)
		return VFR_ERROR;

	pthread_mutex_lock(&key_list_mutex);

	key_node = kn_find_key(keyid);
	if (!key_node) {
		pthread_mutex_unlock(&key_list_mutex);
		return (msg_send_simple(fd, VF_NACK, 0, 0) != -1)?
			VFR_OK : VFR_ERROR;
	}

	/*
	 * XXX check for presence of nodes on the commit lists; send
	 * VF_AGAIN if there is any.
	 */

	msg = build_vf_data_message(VF_ACK, keyid, key_node->kn_data,
				    key_node->kn_datalen,
				    key_node->kn_viewno,
				    &totallen);

	if (!msg)
		return (msg_send_simple(fd, VFR_ERROR, 0, 0) != -1)?
			VFR_OK : VFR_ERROR;

	return (msg_send(fd, msg, totallen) != -1)?VFR_OK:VFR_ERROR;
}


static int
vf_set_current(uint32_t keyid, uint64_t view, void *data, uint32_t datalen)
{
	key_node_t *key_node;
	void *datatmp;

	pthread_mutex_lock(&key_list_mutex);

	key_node = kn_find_key(keyid);
	if (!key_node) {
		pthread_mutex_unlock(&key_list_mutex);
		return VFR_ERROR;
	}

	datatmp = malloc(datalen);
	if (! datatmp) {
		pthread_mutex_unlock(&key_list_mutex);
		return VFR_ERROR;
	}
	
	if (key_node->kn_data)
		free(key_node->kn_data);

	key_node->kn_data = datatmp;
	memcpy(key_node->kn_data, data, datalen);
	key_node->kn_datalen = datalen;
	key_node->kn_viewno = view;

	pthread_mutex_unlock(&key_list_mutex);

	return VFR_OK;
}


/**
 * Request the current state of a keyid from the membership.
 * XXX This doesn't wait for outstanding transactions to complete.
 * Perhaps it should.
 *
 * @param membership	Membership mask.
 * @param keyid		VF key id (application-defined).
 * @param viewno	Return view number.  Passed in pre-allocated.
 * @param data		Return data pointer.  Allocated within.
 * @param datalen	Size of data returned.
 */
int
vf_request_current(memb_mask_t membership, uint32_t keyid, uint64_t *viewno,
		   void **data, uint32_t *datalen)
{
	int fd, x, n, rv = VFR_OK;
	vf_msg_t *msg = NULL;

	if (__daemon_id == -1)
		return -1;

	for (x = 0; x < MAX_NODES; x++) {
		if (!memb_online(membership, x))
			continue;

		rv = VFR_ERROR;
		fd = msg_open(__daemon_id, x);
		if (fd == -1)
			continue;

		if (msg_send_simple(fd, VF_MESSAGE, VF_CURRENT, keyid) == -1)
			continue;

		if ((n = msg_receive_simple(fd, (generic_msg_hdr **)&msg, 10))
		    == -1) {
			msg_close(fd);
			continue;
		}
		msg_close(fd);
		
		swab_generic_msg_hdr(&(msg->vm_hdr));
		if (msg->vm_hdr.gh_command == VF_NACK) {
			free(msg);
			return VFR_ERROR;
		}

		if (msg->vm_hdr.gh_length < sizeof(vf_msg_t)) {
			clulog(LOG_ERR, "VF: Short reply from %d, x\n");
			free(msg);
			return VFR_ERROR;
		}

		if (msg->vm_hdr.gh_length > n) {
			clulog(LOG_ERR, "VF: Size mismatch during decode\n");
			free(msg);
			return VFR_ERROR;
		}

		swab_vf_msg_info_t(&(msg->vm_msg));

		if (msg->vm_msg.vf_datalen != (n - sizeof(generic_msg_hdr))){
			clulog(LOG_ERR, "VF: Size mismatch during decode\n");
			free(msg);
			return VFR_ERROR;
		}

		/*
		 * Ok, we have real data.  Let's store it.
		 */
		if (vf_set_current(keyid, msg->vm_msg.vf_view,
				   msg->vm_msg.vf_data,
				   msg->vm_msg.vf_datalen) == VFR_ERROR) {
			free(msg);
			return VFR_ERROR;
		}

		/* Cleanup */
		free(msg);
		return vf_current(keyid, viewno, data, datalen);
	}
	return rv;
}


