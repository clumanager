/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Global Cluster Lock Daemon.
 *
 * This daemon handles assigning and freeing locks, as well as
 * recovering in the event of the current lock server failing.
 * Failover is seamless since the lock states are stored on the
 * shared private location (usually on shared FC storage).
 */
#include <clusterdefs.h>
#include <msgsvc.h>
#include <sharedstate.h>
#include <clushared.h>
#include <nodeid.h>
#include <namespace.h>
#include <clulog.h>
#include <clu_lock.h>
#include <xmlwrap.h>
#include <libgen.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cm_api.h>

#define MSG_TIMEOUT 10
#define DAEMON_STR "clulockd"
#define LOGLEVEL_STR "loglevel"

SharedLockBlock locks[MAX_LOCKS];

int consider_shutdown(char *);

#define lock_available(lockid) (locks[lockid].lb_state == 0)

static int exiting = 0;
static char daemon_name[MAXPATHLEN];
static int my_node_id = -1;
static int keeper = -1;

/*
 * Local copy of current membership mask as well as quorum status
 */
static memb_mask_t membership, mask_panic;
static uint32_t quorum = EV_NO_QUORUM;
static uint64_t incarnation = 0;
static sigset_t pending_signals;


void daemon_init(char *);

/**
 * Constructs the lock string we'll pass down to the I/O abstration layer.
 *
 * @param lockid	ID of the lock
 * @param lockname	Preallocated return buffer
 * @param locknamesize	Size of preallocated return buffer.
 */
static inline int
lockName(uint32_t lockid, char *lockname, size_t locknamesize)
{
	return snprintf(lockname, locknamesize, NS_D_LOCK "/%d" NS_F_STATUS,
			lockid);
}


/**
 * Write a lockblock to shared state.
 *
 * @param lockid	ID of lock to write.
 * @param lock		Shared lock block.
 */
static int
lockWrite(uint32_t lockid, SharedLockBlock *lock)
{
	char lockstr[256];
	SharedLockBlock real_lock;

	lockName(lockid, lockstr, sizeof(lockstr));
	memcpy(&real_lock, lock, sizeof(SharedLockBlock));
	real_lock.lb_holder.h_master = my_node_id;
	swab_SharedLockBlock(&real_lock);
	return sh_write_atomic(lockstr, (char *)&real_lock,
			       sizeof(SharedLockBlock));
}


/**
 * Read a lock block in to the preallocated space.
 *
 * @param lockid	ID of lock to read.
 * @param lock		Preallocated storage space in which to read the lock
 *			data.
 */
static int
lockRead(uint32_t lockid, SharedLockBlock *lock)
{
	char lockstr[256];
	int rv;

	lockName(lockid, lockstr, sizeof(lockstr));
	rv = sh_read_atomic(lockstr, (char *)lock, sizeof(*lock));
	if (rv == -1)
		return -1;

	swab_SharedLockBlock(lock);
	return rv;
}


/**
 * lock_free(int lockid)
 */
static int
lock_free(int lockid)
{
	locks[lockid].lb_holder.h_node = -1;
	locks[lockid].lb_holder.h_pid = 0;
	locks[lockid].lb_holder.h_pc = 0;
	locks[lockid].lb_state = 0;
	return lockWrite(lockid, &locks[lockid]);
}


/**
 * Attempts to reclaim a lock.
 *
 * @param lockid	ID of the lock to read/fix
 * @param allow_remote	Set to '0' during fail-over.
 */
static int
lock_reclaim(int lockid, int allow_remote, lock_request_t *req)
{
	generic_msg_hdr msg;
	int fd;

	if (lock_available(lockid))
		return 1;
		
	/* Recursive locks not supported... */
	if (req &&
	    (locks[lockid].lb_holder.h_node == req->lr_lock.lb_holder.h_node &&
	     locks[lockid].lb_holder.h_pid == req->lr_lock.lb_holder.h_pid &&
	     locks[lockid].lb_holder.h_crc == req->lr_lock.lb_holder.h_crc)) {
	    	clulog(LOG_WARNING, "Potential recursive lock #%d grant to"
	    	       " member #%d, PID%d\n", lockid,
		       req->lr_lock.lb_holder.h_node,
		       req->lr_lock.lb_holder.h_pid);
		return 1;
	}

	/* Check local PID */
	if ((locks[lockid].lb_holder.h_node == my_node_id) &&
	    ((locks[lockid].lb_holder.h_pid == 0) ||
	     (kill(locks[lockid].lb_holder.h_pid, 0) != 0))) {

		clulog(LOG_DEBUG, "Reclaiming lock %d held locally by "
	       "nonexistent PID %d (%s @ 0x%08x%08x)\n", lockid,
		       locks[lockid].lb_holder.h_pid,
			locks[lockid].lb_holder.h_exe,
			(uint32_t)(locks[lockid].lb_holder.h_pc >> 32 & 0xffffffff),
			(uint32_t)(locks[lockid].lb_holder.h_pc & 0xffffffff));
		if (lock_free(lockid) == -1)
			clulog(LOG_EMERG,"lock_free(%d) line %d failed: %s\n",
			       lockid, __LINE__, strerror(errno));
		return 1;
	}

	/* Check for downed node */
	if ((locks[lockid].lb_holder.h_node != my_node_id) &&
	    (!memb_online(membership, locks[lockid].lb_holder.h_node))) {

		clulog(LOG_DEBUG,
		       "Reclaiming lock %d held by down member #%d\n",
		       lockid, locks[lockid].lb_holder.h_node);
		if (lock_free(lockid) == -1)
			clulog(LOG_EMERG,"lock_free(%d) line %d failed: %s\n",
			       lockid, __LINE__, strerror(errno));
		return 1;
	}

	/*
	 * When taking over locking duties, we don't allow remote connections
	 * during fail-over
	 */
	if (!allow_remote)
		return 0;

	/* Check remote PID */
	if (locks[lockid].lb_holder.h_node == my_node_id)
		return 0;

	fd = msg_open(PROCID_CLULOCKD, locks[lockid].lb_holder.h_node);
	if (fd == -1) {
		return 0;
	}

	if (msg_send_simple(fd, LOCK_PID_QUERY,
			    locks[lockid].lb_holder.h_pid, 0) <= 0) {
		msg_close(fd);
		clulog(LOG_ERR, "Failed to send LOCK_PID_QUERY\n");
		return 0;
	}

	if (msg_receive(fd, &msg, sizeof(msg)) != sizeof(msg)) {
		msg_close(fd);
		clulog(LOG_ERR, "Failed to receive response to "
		       "LOCK_PID_QUERY\n");
		return 0;
	}

	swab_generic_msg_hdr(&msg);
	msg_close(fd);

	/* PID query == NACK when pid nonexistent: free lock. */
	if (msg.gh_command == LOCK_NACK) {
		clulog(LOG_DEBUG, "Reclaiming lock %d held by member #%d by "
		       "nonexistent PID %d\n", lockid,
		       locks[lockid].lb_holder.h_node,
		       locks[lockid].lb_holder.h_pid);

		if (lock_free(lockid) == -1)
			clulog(LOG_EMERG,"lock_free(%d) line %d failed: %s\n",
			       lockid, __LINE__, strerror(errno));
		return 1;
	}

	return 0;
}


/*
 * Call this when we get a node event to determine the (possibly new) lock
 * keeper.
 */
int
recalculate_keeper(void)
{
	int x;
	int old_keeper = keeper, unsure_keeper;

	keeper = memb_low_id(membership);
	unsure_keeper = memb_low_id(mask_panic);
	
        /*
         * Avoid data corruption: Assume that a member in UNCERTAIN state
         * *knows* it is the lock keeper, even if we can't reach it
         * via the network.
         *
	 * This could cause services to become unavailable locally,
	 * but avoids data corruption on the shared state partitions.
         */
	if ((unsure_keeper != -1) && (unsure_keeper < keeper))
		keeper = unsure_keeper;

	if ((keeper != my_node_id) && (old_keeper == my_node_id)) {
		/* I was the keeper.  I no longer am. */
		clulog(LOG_DEBUG, "New lock keeper = member #%d\n",
		       keeper);
	} else if ((keeper == my_node_id) && (old_keeper != my_node_id)) {
		/* I am the new lock keeper! */
		clulog(LOG_DEBUG, "I am the new lock keeper\n");

		if (quorum == EV_NO_QUORUM || quorum == EV_QUORUM_LOST)
			return keeper;

		/* Read all the locks off of shared state */
		for (x = 0; x < MAX_LOCKS; x++) {
			if (lockRead(x, &locks[x]) == -1) {
				clulog(LOG_EMERG, "Couldn't read lock %d!\n",
				       x);
				consider_shutdown("Cluster Instability: "
						  "Could not read "
				                  "lock block!\n");
			}
				     
			lock_reclaim(x, 0, NULL);
		}
	} else {
		/*
		 * I wasn't the keeper and still am not.
		 * Who cares?
		 *
		for (x=0; x<MAX_LOCKS; x++) {
			lock_reclaim(x, 0);
		}
		 */
	}

	return keeper;
}


/*
 * release a lock.
 */
int
lockd_unlock(int fd, lock_request_t *req)
{
	int lockid, nodeid;

	lockid = req->lr_lock.lb_lockid;
	nodeid = req->lr_lock.lb_holder.h_node;

	clulog(LOG_DEBUG, "lockd_unlock: member #%d lock %d\n", nodeid,
	       lockid);
	
	if ((lockid < 0) || (lockid >= MAX_LOCKS)) {
		clulog(LOG_DEBUG, "unlock: invalid lock #%d\n", lockid);
		return msg_send_simple(fd, LOCK_INVALID, 0, 0);
	}

	if (!memb_online(membership, nodeid)) {
		clulog(LOG_DEBUG, "unlock: request from offline node #%d\n", nodeid);
		return msg_send_simple(fd, LOCK_INVALID, 0, 0);
	}

	if ((nodeid < 0) || (nodeid >= MAX_NODES)) {
		clulog(LOG_DEBUG, "unlock: invalid member #%d\n", nodeid);
		return msg_send_simple(fd, LOCK_INVALID, 0, 0);
	}

	/* If the lock isn't held, grant an unlock request */
	if (locks[lockid].lb_state == 0) {
		clulog(LOG_DEBUG, "lockd_unlock: unlock of lock %d "
		       "while not held by anyone\n", lockid);
		return msg_send_simple(fd, LOCK_ACK, 0, 0);
	}

	if (locks[lockid].lb_holder.h_node != nodeid) {
		/* Not holder */
		/* XXX What if we haven't gotten a nodestate yet? */
		clulog(LOG_DEBUG, "NACK: member #%d doesn't hold lock\n",
		       nodeid);
		return msg_send_simple(fd, LOCK_NACK, 0, 0);
	}

	/* Lock memory-resident copy */
	memcpy(&locks[lockid], &req->lr_lock, sizeof(SharedLockBlock));
	locks[lockid].lb_holder.h_node = -1;
	locks[lockid].lb_state = 0;

	/* Save to shared storage */
	if (lockWrite(lockid, &locks[lockid]) == -1) {
		clulog(LOG_EMERG, "lockWrite(%d,...) @ line %d failed: %s\n",
		       lockid, __LINE__, strerror(errno));
		consider_shutdown("Cluster Instability: Could not write "
		                  "lock block!\n");
	}

	clulog(LOG_DEBUG, "ACK: lock unlocked\n");
	return msg_send_simple(fd, LOCK_ACK, 0, 0);
}


/*
 * test for lock
 */
int
lockd_trylock(int fd, lock_request_t *req)
{
	int lockid, nodeid;
	uint64_t remote_incarnation;

	lockid = req->lr_lock.lb_lockid;
	nodeid = req->lr_lock.lb_holder.h_node;
	remote_incarnation = (uint64_t) req->lr_lock.lb_incarnation;

	clulog(LOG_DEBUG, "lockd_trylock: member #%d lock %d\n", nodeid,
	       lockid);

	if ((lockid < 0) || (lockid >= MAX_LOCKS)) {
		clulog(LOG_DEBUG, "lock: invalid lock #%d\n", lockid);
		return msg_send_simple(fd, LOCK_INVALID, 0, 0);
	}

	if (!memb_online(membership, nodeid)) {
		clulog(LOG_DEBUG, "lock: request from offline node #%d\n", nodeid);
		return msg_send_simple(fd, LOCK_INVALID, 0, 0);
	}

	if ((nodeid < 0) || (nodeid >= MAX_NODES)) {
		clulog(LOG_DEBUG, "lock: invalid member #%d\n", nodeid);
		return msg_send_simple(fd, LOCK_INVALID, 0, 0);
	}

	if (!lock_available(lockid) && (lock_reclaim(lockid, 1, req) == 0)) {
		clulog(LOG_DEBUG, "lock: held\n");
		return msg_send_simple(fd, LOCK_NACK, 0, 0);
	}

	if (remote_incarnation > incarnation) {
		clulog(LOG_DEBUG, "lock: Member #%d has view %u, mine is %u\n",
		       nodeid, (unsigned int)remote_incarnation,
		       (unsigned int)incarnation);
		/* Sending this will cause the caller to retry */
		return msg_send_simple(fd, LOCK_NO_QUORUM, 0, 0);
	}

	/* Copy lock data into memory-resident copy */
	memcpy(&locks[lockid], &req->lr_lock, sizeof(SharedLockBlock));

	/* Ensure someone doesn't send us a bogus lock state */
	if (locks[lockid].lb_state == 0)
		locks[lockid].lb_state = 10;

	/* Save to shared storage */
	if (lockWrite(lockid, &locks[lockid]) == -1) {
		clulog(LOG_EMERG, "lockWrite(%d,...) @ line %d failed: %s\n",
		       lockid, __LINE__, strerror(errno));
		consider_shutdown("Cluster Instability: Could not write "
		                  "lock block!\n");
	}

	clulog(LOG_DEBUG, "Replying ACK\n");
	return msg_send_simple(fd, LOCK_ACK, 0, 0);
}


/*
 * QUIT, TERM
 *
 * In this case, we go down ASAP.  But first, we sync.  These will, like the
 * above, short-out msg_accept_timeout() and drop down for that one last
 * sync.
 */
static void
sh_mark(int sig)
{
	sigaddset(&pending_signals, sig);
}


/*
 * IO, SEGV, BUS, ILL
 *
 * *Try* to restart myself in these cases.  Hopefully, daemon_name wasn't
 * mangled.  This helps prevent loss of synchronization with the other node
 * in the case rmtabd dies.
 */
static void
sh_restart(int sig)
{
	if (daemon_name[0] != '/') {
		exit(1);
	}

#if 0
	clulog(LOG_ERR, "Signal %d received; restarting\n", sig);
	if (execl(daemon_name, daemon_name, NULL) == -1) {
		clulog(LOG_CRIT, "Failed to execl: %s! => NFS BROKEN\n",
		       strerror(errno));
		exit(1);
	}
#else
	raise(SIGSTOP);
	while (1)
		sleep(10);
#endif

}


static void
set_logfacility(void)
{
	char *val;

	if (CFG_Get("cluster%logfacility", NULL, &val) == CFG_OK) {
		if (val)
			clu_set_facility(val);
	}
}


static void
set_loglevel(void)
{
	char *val;
	int level;

	if (CFG_Get("clulockd%loglevel", NULL, &val) == CFG_OK) {
		level = atoi(val);
		clu_set_loglevel(level);
	}
}


static inline void
register_sighandlers(void)
{
	sigset_t set;
	struct sigaction act;

	sigemptyset(&set);

	sigaddset(&set, SIGHUP);
	sigaddset(&set, SIGINT);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGQUIT);

	sigaddset(&set, SIGILL);
	sigaddset(&set, SIGIO);
	sigaddset(&set, SIGSEGV);
	sigaddset(&set, SIGBUS);

	sigprocmask(SIG_UNBLOCK, &set, NULL);

	memset(&act, 0, sizeof (act));
	sigemptyset(&act.sa_mask);


	/* Drat.  Restart conditions */
	memset(&act, 0, sizeof (act));
	sigemptyset(&act.sa_mask);
	act.sa_handler = sh_restart;
	sigaction(SIGILL, &act, NULL);
	sigaction(SIGIO, &act, NULL);
	sigaction(SIGSEGV, &act, NULL);
	sigaction(SIGBUS, &act, NULL);

	memset(&act, 0, sizeof (act));
	sigemptyset(&act.sa_mask);
	/* async signals */
	act.sa_handler = sh_mark;
	sigaction(SIGHUP, &act, NULL);
	sigaction(SIGINT, &act, NULL);
	sigaction(SIGTERM, &act, NULL);
	sigaction(SIGQUIT, &act, NULL);
}


int
update_quorum(cm_event_t *quorum_event)
{
	uint32_t event = cm_ev_event(quorum_event);

	/* Record the quorum message */
	quorum = event;

	incarnation = cm_quorum_view(quorum_event);

	switch (event) {
	case EV_QUORUM:
	case EV_QUORUM_GAINED:
		clulog(LOG_DEBUG, "Quorum Event: Quorum view #%d, %s\n",
		       (int)incarnation,
		       memb_mask_str(cm_quorum_mask(quorum_event)));
		shared_storage_init(); /* Hope this works! */
		break;
	case EV_SHUTDOWN:
		clulog(LOG_INFO, "Exiting\n");
		shared_storage_deinit();
		exit(0);
	case EV_QUORUM_LOST:
		clulog(LOG_CRIT, "Quorum Event: LOST QUORUM\n");
		shared_storage_deinit();
		break;
	default:
		clulog(LOG_DEBUG, "Quorum Event: NO QUORUM\n");
		shared_storage_deinit();
		break;
	}

	/* What is my incarnation? */
	memcpy(membership, cm_quorum_mask(quorum_event), sizeof(memb_mask_t));
	memcpy(mask_panic, cm_quorum_mask_panic(quorum_event),
	       sizeof(memb_mask_t));
	recalculate_keeper();

	if (keeper != my_node_id)
		clulog(LOG_DEBUG, "Lock Keeper = Member #%d\n", keeper);

	return 0;
}


/*
 * handle_message
 *
 * Called in the case that we know there is a message waiting on the socket
 * indicated by fd, this function decides what to do with that message.
 * Nine times out of ten, it's a diff message from the remote node, so we
 * return the diff in **diff.  This list is allocated herein and hence must
 * be freed by the user (generally with rmtab_kill...).
 *
 * Returns 0 on success; -1 on failure.
 */
static int
handle_message(int fd)
{
	generic_msg_hdr *buf = NULL;
	SharedLockBlock *lockp;
	size_t buf_size;
#ifdef DEBUG
	int ret = 0;
#endif

	clulog(LOG_DEBUG,"Processing message on %d\n", fd);

	if ((buf_size = msg_receive_simple(fd, &buf, 5)) <
	    sizeof(generic_msg_hdr)) {
		if (buf)
			free(buf);
		return -1;
	}

	clulog(LOG_DEBUG, "Received %d bytes from peer\n", buf_size);

	if (!buf)
		return -1;

	swab_generic_msg_hdr(buf);

	/* Actually handle the message */
	switch (buf->gh_command) {
	case LOCK_MASTER_QUERY:
		clulog(LOG_DEBUG, "LOCK_MASTER_QUERY\n");

		if (quorum == EV_NO_QUORUM) {
			clulog(LOG_DEBUG, "replying NO QUORUM\n");
			msg_send_simple(fd, LOCK_NO_QUORUM, 0, 0);
			break;
		}

		if (quorum == EV_QUORUM_LOST) {
			clulog(LOG_DEBUG, "replying LOST QUORUM\n");
			msg_send_simple(fd, LOCK_LOST_QUORUM, 0, 0);
			break;
		}

		clulog(LOG_DEBUG, "replying LOCK_ACK %d\n", keeper);
		msg_send_simple(fd, LOCK_ACK, keeper, 0);
		break;

	case LOCK_PID_QUERY:
		clulog(LOG_DEBUG, "LOCK_PID_QUERY\n");
		if (!quorum)
			break;

		if (kill(buf->gh_arg1, 0) != 0) {
			clulog(LOG_DEBUG, "PID %d Nonexistent\n", buf->gh_arg1);
			msg_send_simple(fd, LOCK_NACK, 0, 0);
			break;
		}

		clulog(LOG_DEBUG, "PID %d Exists\n", buf->gh_arg1);
		msg_send_simple(fd, LOCK_ACK, 0, 0);
		break;

	case LOCK_UNLOCK:
		clulog(LOG_DEBUG, "LOCK_UNLOCK\n");
		if (buf_size != sizeof(lock_request_t)) {
			clulog(LOG_ERR, "LOCK: Invalid request size.\n");
			break;
		}

		if (quorum == EV_QUORUM_LOST) {
			msg_send_simple(fd, LOCK_LOST_QUORUM, 0, 0);
			break;
		}

		if (quorum == EV_NO_QUORUM) {
			msg_send_simple(fd, LOCK_NO_QUORUM, 0, 0);
			break;
		}

		if (keeper != my_node_id) {
			msg_send_simple(fd, LOCK_NOT_MASTER, 0, 0);
			break;
		}

		lockp = &((lock_request_t *)buf)->lr_lock;

		swab_SharedLockBlock(lockp);
		lockd_unlock(fd, (lock_request_t *)buf);
		break;

	case LOCK_LOCK:
	case LOCK_TRYLOCK:
		clulog(LOG_DEBUG, "LOCK_LOCK | LOCK_TRYLOCK\n");
		if (buf_size != sizeof(lock_request_t)) {
			clulog(LOG_ERR, "LOCK: Invalid request size.\n");
			break;
		}

		if (!quorum) {
			clulog(LOG_DEBUG, "Reply: No quorum\n");
			msg_send_simple(fd, LOCK_NO_QUORUM, 0, 0);
			break;
		}

		if (keeper != my_node_id) {
			clulog(LOG_DEBUG, "Reply: Not master\n");
			msg_send_simple(fd, LOCK_NOT_MASTER, 0, 0);
			break;
		}

		lockp = &((lock_request_t *)buf)->lr_lock;
		swab_SharedLockBlock(lockp);

		lockd_trylock(fd, (lock_request_t *)buf);

		/*
		 * Check lock.  If it's taken, reply immediately with a
		 * "failed" message.
		 */
		break;

	default:
		clulog(LOG_WARNING, "Unhandled message: %d\n",
		       buf->gh_command);
		break;
	}

	free(buf);
	return 0;
}


static void
clulockd_init(int argc, char **argv)
{
	int opt;
	char buf[256], debug = 0, foreground = 0;

	while ((opt = getopt(argc, argv, "fd")) != EOF) {
		switch (opt) {
		case 'd':
			debug = 1;
			break;
		case 'f':
			foreground = 1;
		default:
			break;
		}
	}

	set_logfacility();
	if (!debug)
		(void) clu_set_loglevel(LOG_INFO);
	else
		(void) clu_set_loglevel(LOG_DEBUG);

	if (CFG_ReadFile(CLU_CONFIG_FILE) != CFG_OK) {
		clulog(LOG_EMERG, "Could not read configuration file!\n");
		exit(1);
	}
	set_logfacility();

	if (!foreground)
		daemon_init(argv[0]);
	else
		clu_log_console(1);

	/*
	 * Restart info - daemon_init must've been called prior to this if
	 * we plan on forking, or we will have a different pid!
	 */
	snprintf(buf, sizeof (buf), "/proc/%d/exe", getpid());
	memset(daemon_name, 0, sizeof (daemon_name));
	if (readlink(buf, daemon_name, sizeof (daemon_name)) == -1)
		clulog(LOG_ERR, "Couldn't stat %s: %s; restart disabled\n",
		       buf, strerror(errno));


	/* ... */
	if ((my_node_id = getLocalNodeIDCached()) == -1) {
		clulog(LOG_EMERG, "Could not determine local member ID!\n");
		exit(1);
	}

	register_sighandlers();
	if (!debug)
		set_loglevel();
}


void
process_signals(void)
{
	sigset_t processing;

	memcpy(&processing, &pending_signals, sizeof(pending_signals));
	sigemptyset(&pending_signals);

	if (sigismember(&processing, SIGHUP)) {
		clulog(LOG_INFO, "Reloading configuration\n");
		CFG_Destroy();
		CFG_Read();
		msg_svc_init(1);
		set_loglevel();
	}

	if (sigismember(&processing, SIGINT) ||
	    sigismember(&processing, SIGQUIT) ||
	    sigismember(&processing, SIGTERM)) {
		clulog(LOG_NOTICE, "Exit signal received\n");
		exiting = 1;
	}
}


static inline void
dispatch(int *fd)
{
	if (*fd < 0)
		return;
	handle_message(*fd);
	msg_close(*fd);
	*fd = -1;
}


/*
 * main
 *
 * Main.  Main.  Main.  Main.  Main.  Main.  Main.  Main.  Main.  Main.  
 */
int
main(int argc, char **argv)
{
	int rv;
	msg_handle_t fd;
	msg_handle_t listen_fd, local_fd;
	cm_event_t *qevent;
	int quorumfd;
	fd_set rfds;

	clulockd_init(argc, argv);
	sigemptyset(&pending_signals);

	clulog(LOG_DEBUG, "%s starting\n", argv[0]);

	/*
	 * Set up the message service
	 */
	if ((listen_fd = msg_listen(PROCID_CLULOCKD)) < 0) {
		clulog(LOG_ERR, "Error setting up message listener: %s\n",
		       strerror(errno));
		clulog(LOG_ERR, "%s process may already be running.\n",
		       argv[0]);
		exit(1);
	}

	/*
	   Set up local comms listener
	 */
	if ((local_fd = msg_listen_local(PROCID_CLULOCKD)) < 0) {
		clulog(LOG_ERR, "Error setting up local message listener: %s\n",
		       strerror(errno));
	}

	if (local_fd >= 0) 
		clulog(LOG_DEBUG, "Local socket = %d\n", local_fd);


	/*
	 * Ok, we're ready to go.  Sort of.
	 */
	if ((quorumfd = cm_ev_register(EC_QUORUM)) == -1) {
		clulog(LOG_CRIT, "Couldn't subscribe to quorum events: %s\n",
		       strerror(errno));
		exit(1);
	}

	while (!exiting) {

		fd = -1;
		FD_ZERO(&rfds);
		FD_SET(quorumfd, &rfds);
		FD_SET(listen_fd, &rfds);
		if (local_fd >= 0)
			FD_SET(local_fd, &rfds);

		process_signals();

		rv = select(1024, &rfds, NULL, NULL, NULL);
		if (rv <= 0) {
			if (errno == EBADF) {
				/* 
				 * If the quorum daemon is alive, it will
				 * respawn the lock daemon.
				 */
				clulog(LOG_CRIT, "Primary file descriptor(s) "
				       "broke - Restarting\n");
				execvp(argv[0],argv);
			}
			continue;
		}

		if (FD_ISSET(quorumfd, &rfds)) {
			if ((qevent =
			     (cm_event_t *)cm_ev_read(quorumfd))) {
				update_quorum(qevent);
				cm_ev_free((void *)qevent);
			} else {
				/* 
				 * If the quorum daemon is alive, it will
				 * respawn the lock daemon.
				 */
				clulog(LOG_CRIT, "Lost connection to "
				       "cluquorumd - Restarting\n");
				execvp(argv[0],argv);
			}
		}

	       	if (local_fd >= 0 && FD_ISSET(local_fd, &rfds))  {
			/* Process local connections first */
			if ((fd = msg_accept_local(local_fd)) < 0) {
				if ((errno != ETIMEDOUT) &&
				    (errno != EINTR)) {
					clulog(LOG_ERR, "select error: %n",
					       strerror(errno));
				}
			}
			dispatch(&fd);
		} 
		
		if (FD_ISSET(listen_fd, &rfds)) {
			if ((fd = msg_accept_timeout(listen_fd, 5)) < 0) {
				if ((errno != ETIMEDOUT) &&
				    (errno != EINTR)) {
					clulog(LOG_ERR, "select error: %s\n",
					       strerror(errno));
				}
			}
			dispatch(&fd);
		}
	}

	return 0;
}
