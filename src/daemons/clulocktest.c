#include <clu_lock.h>
#include <stdio.h>
#include <stdlib.h>


int
main(int argc, char **argv)
{
	int x, lockid;
	char buf[80];

	if (argc < 1) {
		printf("Usage: %s lockid\n", argv[0]);
		return 1;
	}

	lockid = atoi(argv[1]);
	if (lockid < 0)
		return -1;

	x = clu_lock(lockid);
	if (x < 0) {
		perror("clu_lock");
		return 0;
	}

	printf("Lock %d held; press <ENTER> to release\n", lockid);
	fgets(buf, sizeof(buf)-1, stdin);
	clu_unlock(lockid);
	
	return 0;
}
