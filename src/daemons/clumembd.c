/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Consensus/VF-based membership daemon for Red Hat Cluster Manager.
 */
/*
 * $Id: clumembd.c,v 1.60 2008/08/14 21:19:25 lhh Exp $
 *
 * Author: Lon Hohberger <lhh at redhat.com>
 *	   Brian Stevens <bstevens at redhat.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/reboot.h>
#include <sys/wait.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/param.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdarg.h>
#include <termios.h>
#include <sys/syslog.h>
#include <membership.h>
#include <sharedstate.h>
#include <sched.h>
#include <pthread.h>

/*
 * Local include files
 */
#include <xmlwrap.h>
#include <msgsvc.h>
#include <clusterdefs.h>
#include <cm_api.h>
#include <clulog.h>
#include <nodeid.h>
#include <clist.h>
#include <crc32.h>
#include "../clulib/if_lookup.h"
#include <vf.h>

#define KEY_MEMBERSHIP 0x27456381

#ifdef TESTING
#ifdef reboot
#undef reboot
#endif
#define reboot(arg) {\
	clulog(LOG_EMERG, "reboot(%s) @ %s:%d\n", #arg, __FILE__, __LINE__); \
	raise(SIGSTOP); \
}
#endif

/*
 * Callback functions.
 */
int32_t
memb_strict_cb(uint32_t __attribute__ ((unused)) keyid,
	       uint64_t __attribute__ ((unused)) viewno,
	       void *data, uint32_t datalen);
int32_t
memb_commit_cb(uint32_t __attribute__ ((unused)) keyid,
	       uint64_t viewno, void *data, uint32_t datalen);

memb_mask_t curr_internal_view;
uint64_t curr_internal_viewno;

/*
 * Local, internal structures.
 */

/**
 * A IPV4 heartbeat channel.  Includes broadcast file descriptor
 */
typedef struct hb_if {
	int hb_bcast;		/**< Broadcast file descriptor */
	int hb_mcast_send;	/**< Multicast send file descriptor */
	int hb_mcast_recv;	/**< Multicast recv file descriptor */
	struct sockaddr_in hb_bcast_addr;	/**< Multicast address */
	struct sockaddr_in hb_mcast_addr;	/**< Broadcast address */
	struct sockaddr_in hb_ip_addr;		/**< IP address */
	char hostname[MAXHOSTNAMELEN];		/**< Hostname */
	struct hb_if *next;			/**< Next pointer */
} hb_if_t;

/**
 * A structure containing what we think another node's health is.  This is 
 * used and updated regularly.
 */
typedef struct node_state {
	uint32_t ns_nodeid;	/**< Node id */
	char *ns_name;		/**< Node name (from cfg file) */
	struct in_addr ns_addr; /**< Node IP address -> gethostbyname() */
	uint32_t ns_state;	/**< Node state */
	int32_t ns_cskip;	/**< Number of consecutive skipped packets */
	int32_t ns_cseen;	/**< Number of consecutive seen packets */
	uint8_t ns_bump;	/**< bump this node? semi-private */
	uint64_t ns_token;	/**< Generated when a node starts */
	uint32_t ns_grace;	/**< Adaptive grace period. */
	/*
	 * When ((ns_cskip >= memb_tko_count) && (ns_cseen < memb_tko_count)),
	 * ns_nodeid is declared out!
	 *
	 * When the token of any node changes (without a state change),
	 * we immediately initiate a new VF instance.
	 */
} node_state_t;

/**
 * A heartbeat message.
 */
typedef struct __attribute__ ((packed)) _node_ping_msg {
	uint32_t np_crc;	/* CRC of message. */
	uint32_t np_nodeid;	/* sender's node ID. */
	uint64_t np_token;	/* Sender's generated token. */
} node_ping_msg_t;

/*
 * Prototypes - Imported functions.
 */
extern void daemon_init(char *prog);	/* from daemon_init.c */

/*
 * Prototypes - Forward Declarations
 */
static void sighup_handler(int signo);
static void sig_exit_handler(int __attribute__ ((unused)) sig);
static void setup_sig_handlers(void);
static void setup_sig_handlers(void);
static void memb_read_params(void);
static int send_node_id(int fd);
struct timeval *__scale_tv(struct timeval *tv, int scale);
int heartbeat_init(int port);
void start_watchdog(void);
int heartbeat_send(int);
void local_init(void);
int update_seen(node_ping_msg_t * msg, int size, struct sockaddr_in *sinp);
int update_skips(void);
void clear_seen(void);
int coordinator(uint32_t * current_view);
int build_memb_view(memb_mask_t mask);
int commit_memb_view(memb_mask_t mask);
int commit_views(memb_mask_t current_view);
int handle_join_view_msg(int fd, memb_msg_t * hdrp);
int send_memb_update(int fd, memb_mask_t current_view);
int start_notify_timer(struct timeval *notify_expire);
int check_notify_timer(struct timeval *notify_expire);
int memb_process_msg(int fd, memb_mask_t current_view,
		    struct timeval *notify_expire);
void *pulsar(void __attribute__ ((unused)) * unused_parameter);
int create_hb_trans_thread(pthread_t * thread);
int main(int argc, char **argv);

/*
 * Our local C variables defining our behavior
 */
static int memb_adaptive = DFLT_ADAPTIVE;
static int memb_bcast = DFLT_BCAST;
static int memb_primary_only = DFLT_PRIMARY_ONLY;
struct timeval memb_interval = DFLT_INTERVAL;
static int memb_mcast = DFLT_MCAST;
static char *memb_mcaddr = DFLT_MCADDR;
static int memb_port = DFLT_PORT;
static int memb_rtp = DFLT_RTP;
static int memb_smooth = DFLT_SMOOTH;
static int memb_strict = DFLT_STRICT;
static int memb_thread = DFLT_THREAD;
static int memb_tko_count = DFLT_TKO_COUNT;
#define memb_up_count ((memb_tko_count/3)+1)


/*
 * Our other globals...
 */
static int member_count = 0;
static int force_update = 0;
static int debug = 0;
static int my_node_id;
static uint64_t my_token = 0;
static char proceed = 1;
static int need_reconfig;
static pthread_mutex_t interval_mutex = PTHREAD_MUTEX_INITIALIZER;


/**
 * Our list of IPv4 interfaces.  Currently doesn't do ipv6.
 */
static hb_if_t *hb_if_list = NULL;


/**
 * Our idea of the health of all nodes in the cluster.
 */
static node_state_t nodestates[MAX_NODES];


static void
sighup_handler(int signo)
{
	/*
	 * Re-read our part of the configuration file. First, we need to
	 * explicitly close the CFG library, so that we don't just reread
	 * parameters cached by the library.
	 */

	if (signo != SIGHUP)
		return;

	need_reconfig=1;
}


static void
reconfigure(void)
{
	if (!need_reconfig)
		return;
	need_reconfig = 0;

#ifdef DEBUG
	clulog(LOG_INFO, "Reloading configuration\n");
#else
	clulog(LOG_DEBUG, "Reloading configuration\n");
#endif
	CFG_Destroy();
	CFG_ReadFile(CLU_CONFIG_FILE);
	msg_svc_init(1);
	memb_read_params();
	
	/* Restart the watchdog timer */
	clumembd_sw_watchdog_stop();
	start_watchdog();
}


static void
sig_exit_handler(int __attribute__ ((unused)) sig)
{
	proceed = 0;
}


static void
setup_sig_handlers(void)
{
	sigset_t set;
	struct sigaction act;

	sigemptyset(&set);
	sigaddset(&set, SIGHUP);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGINT);
	/*
	 * Make sure the HUP signal is not blocked.
	 */
	sigprocmask(SIG_UNBLOCK, &set, NULL);
	memset(&act,0,sizeof(act));
	act.sa_handler = sighup_handler;
	sigemptyset(&act.sa_mask);
	act.sa_flags |= SA_RESTART;
	sigaction(SIGHUP, &act, NULL);

	/*
	 * Handle exits
	 */
	memset(&act,0,sizeof(act));
	act.sa_handler = sig_exit_handler;
	sigemptyset(&act.sa_mask);
	act.sa_flags |= SA_RESTART;
	sigaction(SIGINT, &act, NULL);
	sigaction(SIGTERM, &act, NULL);
}


static void
block_signal(int sig)
{
	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, sig);
	sigprocmask(SIG_BLOCK, &set, NULL);
}


static void
set_facility(void)
{
	char *p;
	if (CFG_Get((char *) "cluster%logfacility", NULL, &p) == CFG_OK) {
		if (p)
			clu_set_facility(p);
	}
}


static void
memb_read_params(void)
{
	char *p;
	uint32_t interval_mus;
	struct sched_param param;
	int x, cfg_memb_count = 0;
	char buf[80];
	struct hostent *hp;

	/*
	 * Read in any override to the log level, heartbeat port, ping interval
	 * or tko count, etc.
	 */
	if (!debug && CFG_Get((char *) CFG_MEMB_LOGLEVEL, NULL, &p) == CFG_OK) {
		if (p)
			clulog(LOG_DEBUG, "Changing loglevel from %d to %d\n",
			       clu_set_loglevel(atoi(p)), atoi(p));
	}

	clulog(LOG_DEBUG, "Setting configuration parameters.\n");

	for (x=0; x < MAX_NODES; x++) {
		snprintf(buf, sizeof(buf), "members%%member%d%%name", x);
		if (CFG_Get(buf, NULL, &nodestates[x].ns_name) != CFG_OK)
			continue;

		cfg_memb_count++;

		hp = gethostbyname(nodestates[x].ns_name);
		if (hp == NULL) {
			clulog(LOG_ERR, "No IP address for member %s!\n",
			       nodestates[x].ns_name);
			nodestates[x].ns_name = NULL;
		} else
			memcpy(&nodestates[x].ns_addr,hp->h_addr,
			       hp->h_length);
	}
	
	if (cfg_memb_count != member_count) {
		if (member_count)
			force_update = 1;
		member_count = cfg_memb_count;
	}

	if (CFG_Get((char *) CFG_MEMB_ADAPTIVE, NULL, &p) == CFG_OK) {
		if (p) {
			if (p[0] == 'y' || p[0] == 'Y')
				memb_adaptive = 1;
			else
				memb_adaptive = 0;
			clulog(LOG_DEBUG, "Adaptive membership set to %s\n",
			       memb_adaptive ? "ON" : "OFF");
		}
	}

	if (CFG_Get((char *) CFG_MEMB_INTERVAL, NULL, &p) == CFG_OK) {
		if (p) {
			interval_mus = atoi(p);
			clulog(LOG_DEBUG, "Overriding interval to be %d\n",
			       interval_mus);

			pthread_mutex_lock(&interval_mutex);
			memb_interval.tv_sec = interval_mus / 1000000;
			memb_interval.tv_usec = interval_mus % 1000000;
			pthread_mutex_unlock(&interval_mutex);
		}
	}

	if (CFG_Get((char *) CFG_MEMB_PORT, NULL, &p) == CFG_OK) {
		if (p) {
			memb_port = atoi(p);
			clulog(LOG_DEBUG,
			       "Overriding heartbeat port to be %d\n",
			       memb_port);
		}
	}

	if (CFG_Get((char *) CFG_MEMB_PRIMARY_ONLY, NULL, &p) == CFG_OK) {
		if (p) {
			if (p[0] == 'y' || p[0] == 'Y')
				memb_primary_only = 1;
			else
				memb_primary_only = 0;
			clulog(LOG_DEBUG,
			       "Using only primary NIC + lo for broadcast: %s\n",
			       memb_primary_only?"YES":"NO");
		}
	}

	if (CFG_Get((char *) CFG_MEMB_RTP, NULL, &p) == CFG_OK) {
		if (p) {
			memset(&param,0,sizeof(param));
			param.sched_priority = atoi(p);
			if (param.sched_priority < 0
			    || param.sched_priority > 100)
				param.sched_priority = 0;

			memb_rtp = param.sched_priority;

			if (sched_setscheduler(0, SCHED_FIFO,
					       (void *)&param) != 0)
				clulog(LOG_WARNING, "Set FIFO Priority"
				       " failed: %s\n", strerror(errno));
			else 
				clulog(LOG_DEBUG,"Using RT/FIFO Priority %d\n",
				       param.sched_priority);
		}
	}

	if (CFG_Get((char *) CFG_MEMB_STRICT, NULL, &p) == CFG_OK) {
		if (p) {
			if (p[0] == 'y' || p[0] == 'Y')
				memb_strict = 1;
			else
				memb_strict = 0;
			clulog(LOG_DEBUG, "Strict Voting set to %s\n",
			       memb_strict ? "ON" : "OFF");
		}
	}

	if (CFG_Get((char *) CFG_MEMB_THREAD, NULL, &p) == CFG_OK) {
		if (p) {
			if (p[0] == 'y' || p[0] == 'Y')
				memb_thread = 1;
			else
				memb_thread = 0;
			clulog(LOG_DEBUG, "Transmit thread set to %s\n",
			       memb_thread ? "ON" : "OFF");
		}
	}

	if (CFG_Get((char *) CFG_MEMB_TKO_COUNT, NULL, &p) == CFG_OK) {
		if (p) {
			memb_tko_count = atoi(p);
			clulog(LOG_DEBUG, "Overriding TKO count to be %d\n",
			       memb_tko_count);
		}
	}

	if (CFG_Get((char *) CFG_MEMB_SMOOTH, NULL, &p) == CFG_OK) {
		if (p) {
			memb_smooth = atoi(p);
			clulog(LOG_DEBUG, "Overriding smooth factor to %d\n",
			       memb_smooth);
		}

		if (memb_smooth > (memb_tko_count - 1)) {
			clulog(LOG_DEBUG, "Smooth factor %d too high; set to %d\n",
	      		       memb_smooth, (memb_tko_count - 1));
			memb_smooth = memb_tko_count - 1;
		}
	}

	if (CFG_Get((char *) CFG_MEMB_BCAST, NULL, &p) == CFG_OK) {
		if (p) {
			if (p[0] == 'y' || p[0] == 'Y' || (atoi(p) != 0))
				memb_bcast = 1;
			else
				memb_bcast = 0;
			clulog(LOG_DEBUG, "Broadcast hearbeating set to %s\n",
	      		       memb_bcast ? "ON" : "OFF");
		}
	}

	if (CFG_Get((char *) CFG_MEMB_MCAST, NULL, &p) == CFG_OK) {
		if (p) {
			if (p[0] == 'n' || p[0] == 'N' || !strcmp(p, "0")) {
				memb_mcast = 0;
				clulog(LOG_DEBUG,
				       "Multicast heartbeat OFF\n");
			} else {
				memb_mcast = 1;
				clulog(LOG_DEBUG,
				       "Multicast hearbeat ON\n");
			}
		}
	}

	if (memb_mcast) {
		if ((CFG_Get((char *) CFG_MEMB_MCADDR, NULL, &p) == CFG_OK) &&
		    p) {
			memb_mcaddr = p;
			clulog(LOG_DEBUG, "Multicast address is %s\n",
			       memb_mcaddr);
		}
	}
}

           
/**
 * Scale a (struct timeval).
 *
 * @param tv		The timeval to scale.
 * @param scale		Positive multiplier.
 * @return		tv
 */
struct timeval *
__scale_tv(struct timeval *tv, int scale)
{
	tv->tv_sec *= scale;
	tv->tv_usec *= scale;

	if (tv->tv_usec > 1000000) {
		tv->tv_sec += (tv->tv_usec / 1000000);
		tv->tv_usec = (tv->tv_usec % 1000000);
	}

	return tv;
}


/**
 * Adds a broadcast file descriptor to a heartbeat interface.
 *
 * @param flags		Interface flags.
 * @param hb_if		Heartbeat interface structure.
 * @param port		Port to listen on.
 * @return		-1 on failure, 0 on success/recoverable error,
 *			1 if the address is in use.
 * @see			add_multicast, add_interface
 */
int
add_broadcast(int flags, hb_if_t * hb_if, int port)
{
	int val;

	if (!memb_bcast)
		return 0;

	clulog(LOG_DEBUG, "Adding broadcast heartbeat to %s\n",
	       inet_ntoa(hb_if->hb_bcast_addr.sin_addr));

	/*
	 * Setup the socket associated with this interface.
	 */
	hb_if->hb_bcast_addr.sin_port = htons(port);
	hb_if->hb_bcast = socket(AF_INET, SOCK_DGRAM, 0);
	if (hb_if->hb_bcast < 0) {
		clulog(LOG_ERR, "socket: %s\n", strerror(errno));
		return (1);
	}

	val = 1;
	if (setsockopt(hb_if->hb_bcast, SOL_SOCKET, SO_BROADCAST, &val,
		       sizeof(val))) {
		clulog(LOG_ERR, "turning on broadcast failed %s\n",
				 strerror(errno));
		close(hb_if->hb_bcast);
		return 0;
	}

	if (bind(hb_if->hb_bcast, (struct sockaddr *) &hb_if->hb_bcast_addr,
		 sizeof(struct sockaddr)) < 0) {
		close(hb_if->hb_bcast);

		/*
		 * XXX this is here because we don't check hardware addresses.
		 * We need to ensure we're not binding to IP aliases!
		 */
		if (errno == EADDRINUSE)
			return 1;

		clulog(LOG_ERR, "bind failed: %s\n", strerror(errno));
		return -1;
	}

	if (fcntl(hb_if->hb_bcast, F_SETFL, O_NONBLOCK))
		clulog(LOG_WARNING,
				 "While setting O_NONBLOCK: fcntl: %s\n",
				 strerror(errno));
	return 0;
}


/**
 * Add multicast information to a given heartbeat interface.
 *
 * @param flags		Interface flags
 * @param hb_if		The heartbeat interface.
 * @param mc_addr	The multicast address (in ASCII dotted-quad form)
 * @param port		The port to bind to.
 * @return		-1 on failure, 0 on success, 1 if we need to free
 *			the heartbeat interface.
 * @see			add_broadcast, add_interface
 */
int
add_multicast(int flags, hb_if_t * hb_if, char *mc_addr, int port)
{
	int val;
	struct ip_mreq mreq;
	struct sockaddr_in any_where;

	if (!memb_mcast)
		return 0;

	/*
	 * Set up the structures we need.
	 */
	clulog(LOG_DEBUG, "Setting up multicast %s on %s\n",
	       mc_addr, inet_ntoa(hb_if->hb_ip_addr.sin_addr));

	if (!(flags & IFF_MULTICAST)) {
		clulog(LOG_WARNING,
		       "Interface %s does have multicast flag set\n",
		       inet_ntoa(hb_if->hb_ip_addr.sin_addr));
	}

	hb_if->hb_mcast_addr.sin_family = AF_INET;
	hb_if->hb_mcast_addr.sin_port = htons(port + 1);
	hb_if->hb_ip_addr.sin_port = htons(port + 1);

	if (inet_aton(mc_addr, &hb_if->hb_mcast_addr.sin_addr) < 0) {
		clulog(LOG_ERR, "invalid multicast address\n", strerror(errno));
		return 1;
	}

	/*************************
	 * SET UP MULTICAST SEND *
	 *************************/
	hb_if->hb_mcast_send = socket(AF_INET, SOCK_DGRAM, 0);
	if (hb_if->hb_mcast_send < 0) {
		clulog(LOG_ERR, "socket: %s\n", strerror(errno));
		return 1;
	}

	/*
	 * Join Multicast group.
	 */
	mreq.imr_multiaddr.s_addr = hb_if->hb_mcast_addr.sin_addr.s_addr;
	mreq.imr_interface.s_addr = hb_if->hb_ip_addr.sin_addr.s_addr;
	if (setsockopt
	    (hb_if->hb_mcast_send, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq,
	     sizeof(mreq)) == -1) {
		clulog(LOG_ERR, "Failed to add multicast membership to transmit "
		       "socket %s: %s\n", mc_addr, strerror(errno));
		clulog(LOG_ERR, "Check network configuration or use "
		       "broadcast heartbeat.\n");
		close(hb_if->hb_mcast_send);
		return 1;
	}

	/*
	 * Join Multicast group.
	 */
	mreq.imr_multiaddr.s_addr = hb_if->hb_mcast_addr.sin_addr.s_addr;
	mreq.imr_interface.s_addr = hb_if->hb_ip_addr.sin_addr.s_addr;
	if (setsockopt
	    (hb_if->hb_mcast_send, IPPROTO_IP, IP_MULTICAST_IF,
	     &hb_if->hb_ip_addr.sin_addr,
	     sizeof(hb_if->hb_ip_addr.sin_addr)) == -1) {
		clulog(LOG_ERR, "Failed to bind multicast transmit socket to "
		       "%s: %s\n", mc_addr, strerror(errno));
		clulog(LOG_ERR, "Check network configuration or use "
		       "broadcast heartbeat.\n");
		close(hb_if->hb_mcast_send);
		return 1;
	}

	/*
	 * set time to live to unlimited
	 */
	val = 255;
	if (setsockopt(hb_if->hb_mcast_send, SOL_IP, IP_MULTICAST_TTL, &val,
		       sizeof(val)))
		clulog(LOG_WARNING, "setting TTL failed %s\n", strerror(errno));

	/********************************
	 * SET UP MULTICAST RECV SOCKET *
	 ********************************/
	hb_if->hb_mcast_recv = socket(AF_INET, SOCK_DGRAM, 0);
	if (hb_if->hb_mcast_recv < 0) {
		clulog(LOG_ERR, "socket: %s\n", strerror(errno));
		close(hb_if->hb_mcast_send);
		hb_if->hb_mcast_send = -1;
		return 1;
	}

	/*
	 * When using Multicast, bind to the LOCAL address, not the MULTICAST
	 * address.
	 */
	any_where.sin_family = AF_INET;
	any_where.sin_port = htons(port + 1);
	any_where.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(hb_if->hb_mcast_recv, (struct sockaddr *) &any_where,
		 sizeof(struct sockaddr)) < 0) {
		/*
		 * don't close the send socket here... we still need it.
		 */
		close(hb_if->hb_mcast_recv);
		hb_if->hb_mcast_recv = -1;

		if (errno == EADDRINUSE) {
			clulog(LOG_DEBUG,
			       "Already have a receive multicast socket.\n");
			return 0;
		}

		clulog(LOG_ERR, "bind failed: %s\n", strerror(errno));
		return -1;
	}

	/*
	 * Join multicast group
	 */
	mreq.imr_multiaddr.s_addr = hb_if->hb_mcast_addr.sin_addr.s_addr;
	mreq.imr_interface.s_addr =	//hb_if->hb_ip_addr.sin_addr.s_addr;
	    htonl(INADDR_ANY);
	if (setsockopt
	    (hb_if->hb_mcast_recv, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq,
	     sizeof(mreq)) == -1) {
		clulog(LOG_ERR, "Failed to bind multicast receive socket to "
		       "%s: %s\n", mc_addr, strerror(errno));
		clulog(LOG_ERR, "Check network configuration or use "
		       "broadcast heartbeat.\n");
		close(hb_if->hb_mcast_recv);
		return 1;
	}

	/*
	 * nonblocking recv... sorta
	 */
	if (fcntl(hb_if->hb_mcast_recv, F_SETFL, O_NONBLOCK))
		clulog(LOG_WARNING, "While setting O_NONBLOCK: fcntl: %s\n",
		       strerror(errno));

	return 0;
}


/**
 * Determine if an IP address matches our hostname.
 *
 * @param interface_ip	The IP address to check.
 * @return		1 if match, 0 if not.
 */
int
is_main_interface(struct sockaddr_in *interface_ip)
{
	struct ifreq tmp;
	char buf[80];
	char *val;

	snprintf(buf, sizeof(buf), "members%%member%d%%name", my_node_id);
	if (CFG_Get(buf, NULL, &val) == CFG_OK) {
		if (if_lookup(val, &tmp) != 0)
			return 0;

		/*
		 * If the IP addresses match, then we have our main interface
		 */
		if (((struct sockaddr_in *)(&tmp.ifr_addr))->sin_addr.s_addr ==
		    interface_ip->sin_addr.s_addr)
			return 1;
	}

	return 0;
}


/**
 * Add an interface to our heartbeat list.
 *
 * @param fd		File descriptor we're doing ioctls on
 * @param ifr		Interface structure (from kernel)
 * @param hb_if		Heartbeat interface structure
 * @param port		Port we expect to use.
 * @return		-1 on failure (no heartbeat channels), 0 
 *			on success.
 * @see			add_broadcast, add_multicast
 */
static int
add_interface(int fd, struct ifreq *ifr, hb_if_t *hb_if, int port)
{
	struct ifreq addr;
	int success = 0;
	int flags = 0;
	int is_main = 0;

	/*
	 * Don't allow channel-bond slaves to be primary interface!
	 */
	memset(&addr, 0, sizeof(addr));
	strcpy(addr.ifr_name, ifr->ifr_name);
	if (ioctl(fd, SIOCGIFFLAGS, &addr)) {
		clulog(LOG_ERR, "ioctl: getting flags %s\n",
		       strerror(errno));
		return -1;
	}

	flags = addr.ifr_flags;
	if (flags & IFF_SLAVE) {
		clulog(LOG_DEBUG, "%s is a SLAVE; skipping!\n",
		       addr.ifr_name);
		return 1;
	}

	if (!(flags & IFF_UP)) {
		clulog(LOG_DEBUG, "%s is not UP; skipping!\n",
		       addr.ifr_name);
		return 1;
	}

	memset(&addr, 0, sizeof(addr));
	strcpy(addr.ifr_name, ifr->ifr_name);
	if (ioctl(fd, SIOCGIFADDR, &addr)) {
		clulog(LOG_ERR, "ioctl: getting IP %s\n",
		       strerror(errno));
		return -1;
	}

	memcpy(&hb_if->hb_ip_addr, &addr.ifr_addr,
	       sizeof(struct sockaddr_in));
	hb_if->hb_ip_addr.sin_family = AF_INET;

	clulog(LOG_DEBUG, "Interface IP is %s\n",
	       inet_ntoa(hb_if->hb_ip_addr.sin_addr));
	strcpy(addr.ifr_name, ifr->ifr_name);

	if (ioctl(fd, SIOCGIFBRDADDR, &addr)) {
		clulog(LOG_ERR, "ioctl: getting broadcast %s\n",
	      	       strerror(errno));
		return -1;
	}

	memcpy(&hb_if->hb_bcast_addr, &addr.ifr_addr,
	       sizeof(struct sockaddr_in));
	hb_if->hb_bcast_addr.sin_family = AF_INET;

	is_main = is_main_interface(&hb_if->hb_ip_addr);

	if (memb_bcast) {
		/*
		   any interface if (!primary_only) or
		   Primary interface (primary only)
		   loopback interface, or
		 */
		if ((!memb_primary_only ||
		    is_main ||
		    !strcmp(addr.ifr_name, "lo")) &&
	    	    (add_broadcast(flags, hb_if, port) == 0))
			success++;
	}

	/*
	 * Only add multicast to the interface corresponding to our
	 * hostname for now...
	 */
	if (!memb_mcast)
		goto out;

	if (!is_main)
		goto out;

	if (add_multicast(flags, hb_if, memb_mcaddr, port) == 0)
		success++;

out:
	if (!success)
		return -1;

	return 0;
}


/**
 * Initialize heartbeating.
 *
 * @param port		Broadcast heartbeat port.  Multicast will be port+1
 * return		0 on success, nonzero on failure.
 */
int
heartbeat_init(int port)
{
	hb_if_t *hb_if = NULL;
	struct ifreq *ifr;
	struct ifconf ifc;
	int len, lastlen;
	char *b, *buf;
	int fd;

	/*
	 * Set up the interfaces we use for discovery.
	 */
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0) {
		clulog(LOG_ERR, "socket: %s\n", strerror(errno));
		return (1);
	}

	lastlen = 0;
	len = 100 * sizeof(struct ifreq);

	for (;;) {

		buf = (char *) malloc(len);
		ifc.ifc_len = len;
		ifc.ifc_buf = buf;
		if (ioctl(fd, SIOCGIFCONF, &ifc) < 0) {
			if (errno != EINVAL || lastlen != 0) {
				free(buf);
				close(fd);
			}
			clulog(LOG_ERR, "ioctl: getting net config %s\n",
			       strerror(errno));

			free(buf);
			return (1);
		} else {
			if (ifc.ifc_len == lastlen)
				break;	/* success, len has not changed */
			lastlen = ifc.ifc_len;
		}
		len += 10 * sizeof(struct ifreq);
		free(buf);
	}

	/*
	 * Get the IP and broadcast addresses for each interface
	 */
	for (b = buf; b < buf + ifc.ifc_len; b += sizeof(struct ifreq)) {

		ifr = (struct ifreq *) b;

		if (!hb_if)
			hb_if = (hb_if_t *) malloc(sizeof(hb_if_t));

		memset(hb_if, 0, sizeof(*hb_if));
		hb_if->hb_mcast_send = -1;
		hb_if->hb_mcast_recv = -1;
		hb_if->hb_bcast = -1;

		if (add_interface(fd, ifr, hb_if, port) != 0)
			continue;

		hb_if->next = hb_if_list;
		hb_if_list = hb_if;
		hb_if = NULL;
	}

	if (hb_if)
		free(hb_if);

	if (!hb_if_list) {
		clulog(LOG_CRIT, "No heartbeat channels available!\n");
		close(fd);
		return 1;
	}

	close(fd);
	return 0;
}


/**
 * Send our heartbeat packets to the world!  Writes to all broadcast and
 * multicast addresses.
 */
int
heartbeat_send(int running)
{
	hb_if_t *hb_if;
	int ret, tries = 0, errors = 0;
	node_ping_msg_t ping_msg;

	ping_msg.np_nodeid = my_node_id;

	if (running)
		ping_msg.np_token = my_token;
	else
		ping_msg.np_token = (uint64_t)0; /* Last breath */
	ping_msg.np_crc = 0;
	ping_msg.np_crc = clu_crc32((char *)&ping_msg, sizeof(ping_msg));

	swab32(ping_msg.np_nodeid);
	swab64(ping_msg.np_token);
	swab32(ping_msg.np_crc);

	/*
	 * Ping the watchdog, if we've got it open
	 */
	clumembd_sw_watchdog_reset();	

	for (hb_if = hb_if_list; hb_if; hb_if = hb_if->next) {
		if (hb_if->hb_bcast != -1) {
			tries++;
			ret = sendto(hb_if->hb_bcast, (char *) &ping_msg,
			  	     sizeof(ping_msg), 0,
			  	     (struct sockaddr *) &hb_if->hb_bcast_addr,
			  	     sizeof(struct sockaddr));
			if (ret <= 0) {
				errors++;
				clulog(LOG_WARNING,
				       "sending broadcast message failed %s\n",
				       strerror(errno));
			}
		}

		if (hb_if->hb_mcast_send != -1) {
			tries++;
			ret = sendto(hb_if->hb_mcast_send, (char *) &ping_msg,
				     sizeof(ping_msg), 0,
				     (struct sockaddr *) &hb_if->hb_mcast_addr,
				     sizeof(struct sockaddr));
			if (ret <= 0) {
				errors++;
				clulog(LOG_WARNING,
				       "sending multicast message failed %s\n",
				       strerror(errno));
			}
		}
	}

	/* Failed to send any heartbeat discovery packets */
	if (errors >= tries)
		return -1;

	return 0;
}


/**
 * Initialize the local state variable.
 */
void
local_init(void)
{
	int i;
	struct timeval ut;
	struct timeval tv;

	memb_read_params();

	/*
	 * Find our node ID 
	 */
	my_node_id = getLocalNodeIDCached();

	/*
	 * Generate our session token.
	 */
	while(my_token == 0) {
		getuptime(&ut);
		gettimeofday(&tv, NULL);

		my_token = ((uint64_t) (ut.tv_sec) << 32) |
	    		(uint64_t) (tv.tv_sec & 0x00000000ffffffff);
	}

	clulog(LOG_DEBUG, "I am member #%d\n", my_node_id);

	for (i = 0; i < MAX_NODES; i++) {
		nodestates[i].ns_nodeid = i;
		nodestates[i].ns_state = NODE_DOWN;

		/*
		 * We must see ourself through TWO cycles.  This gives us
		 * more time to see other nodes online.  Other nodes can
		 * convince us that we're up, though.
		 */
		nodestates[i].ns_cseen =
		    (i == my_node_id) ? (-memb_up_count) : 0;
		nodestates[i].ns_cskip = 0;
		nodestates[i].ns_bump = 1;
		nodestates[i].ns_token = (uint64_t) 0;
		nodestates[i].ns_grace = 0;
	}

	clist_init();
	vf_init(my_node_id, PROCID_CLUMEMBD);
	vf_key_init(KEY_MEMBERSHIP, 10, memb_strict_cb, memb_commit_cb);

	/*
	 * Store our own token.  Remember that we monitor ourself as well!
	 */
	nodestates[my_node_id].ns_token = my_token;
}


/**
 * Handles updating an entry in our node_states table when a message
 * is received.
 *
 * @param msg		Heartbeat message to process.
 * @param size		Size received.
 * @param sinp		Pointer to inet "from" address received with packet.
 * @return		0 if no update performed, 1 if update performed.
 * @see			update_skips
 */
int
update_seen(node_ping_msg_t * msg, int size, struct sockaddr_in *sinp)
{
	node_state_t *nsp;
	uint32_t crc;

	if ((msg->np_nodeid < 0) || (msg->np_nodeid >= MAX_NODES))
		return 0;

	/*
	 * Fixed size.  If we're under the minimum size, then we drop the
	 * message.
	 */
	if (size < sizeof(node_ping_msg_t))
		return 0;

	/* 
	 * Check CRC
	 */
	crc = msg->np_crc;
	msg->np_crc = 0;
	if (crc != clu_crc32((char *)msg, sizeof(*msg))) {
		clulog(LOG_WARNING, "Invalid message: CRC32 Mismatch\n");
		return 0;
	}

	nsp = &nodestates[msg->np_nodeid];

	/* No node name? */
	if (!nsp->ns_name)
		return 0;

	/*
	 * Validate nodeid/address matches one of our cluster members.
	 * If we're using broadcast mode, we need to allow other IPs from the
	 * same claimed node.  In this case, use the token to validate.
	 */
	if (memcmp(&sinp->sin_addr, &nsp->ns_addr, sizeof(struct in_addr)) &&
	    (!memb_bcast ||
	     memcmp(&nsp->ns_token, &msg->np_token, sizeof(uint64_t)))) {
		/*
		 * Two clusters on the same subnet... No big deal...
		clulog(LOG_DEBUG, "IP/NodeID mismatch: Probably another "
		       "cluster on our subnet...\n");
		 */
		return 0;
	}

	/*
	 * We have already seen this node this round.  This will only happen on
	 * machines which have multiple interfaces - eth0/eth1, etc.
	 */
	if (msg->np_token && nsp->ns_bump == 0)
		return 0;

	/*
	 * Update the number of consecutive messages seen by the peer.
	 * In the case of an "adaptive" membership algorithm, save ns_cskip in
	 * ns_grace - that way we can give wider grace periods as necessary.
	 */
	nsp->ns_cseen++;

	if (memb_adaptive && nsp->ns_cskip)
		nsp->ns_grace = nsp->ns_cskip;

	nsp->ns_cskip = 0;
	nsp->ns_bump = 0;

	/*
	 * Check to see if a node just came online.
	 */
	if ((nsp->ns_cseen >= memb_up_count) && (nsp->ns_state == NODE_DOWN)) {
		clulog(LOG_NOTICE, "Member %s UP\n", nsp->ns_name);
		nsp->ns_state = NODE_UP;
		nsp->ns_token = msg->np_token;
		return 1;
	}

	/*
	 * See if the token changed.  A changed token is generally a sign that
	 * a node went down and came back up faster than our
	 * interval*memb_tko_count.  This generally won't happen in a
	 * real-world situation, but it comes up a lot in testing.
	 */
	if ((nsp->ns_state == NODE_UP) && (nsp->ns_token != msg->np_token)) {
		/*
		 * Consider this a *fast* transition...
		 *
		 * Ensure that people don't slam the cluster by carefully
		 * crafted UDP packets...
		 */
		clulog(LOG_NOTICE, "Member %s DOWN\n", nsp->ns_name);
		clulog(LOG_DEBUG, "Member #%d token change: "
		       "old=0x%08x%08x new=0x%08x%08x\n", nsp->ns_nodeid,
		       (uint32_t) ((nsp->ns_token >> 32) & 0xffffffff),
		       (uint32_t) ((nsp->ns_token) & 0xffffffff),
		       (uint32_t) ((msg->np_token >> 32) & 0xffffffff),
		       (uint32_t) ((msg->np_token) & 0xffffffff));

		nsp->ns_cseen = 1;
		nsp->ns_cskip = 0;
		nsp->ns_bump = 0;
		nsp->ns_state = NODE_DOWN;
		nsp->ns_token = msg->np_token;
		nsp->ns_grace = 0;
		return 1;
	}

	/*
	 * Ok, so we just got a message.  No transition event; update our copy
	 * of this node's token.
	 */
	nsp->ns_token = msg->np_token;

	return 0;
}


/**
 * This updates our table with respect to nodes whose heartbeats we've missed.
 *
 * @return		0 if no update performed, 1 if update performed.
 * @see			update_seen
 */
int
update_skips(void)
{
	int x, rv = 0;

	for (x = 0; x < MAX_NODES; x++) {

		if (!nodestates[x].ns_bump)
			/* We saw node x this round. */
			continue;

		nodestates[x].ns_cskip++;
		if (nodestates[x].ns_cskip > memb_tko_count)
			nodestates[x].ns_cskip = memb_tko_count;

		/*
		 * Decrement the grace counter.  Once it reaches 0,
		 * the member with a delayed grace period will have
		 * (memb_tko_count * interval) to send another packet.
		 */
		if (memb_adaptive && nodestates[x].ns_grace) {
			nodestates[x].ns_grace--;
			continue;
		}

		if (nodestates[x].ns_cseen > 0) 
			nodestates[x].ns_cseen = 0;

		/*
		 * Determine if it went down
		 */
		if ((nodestates[x].ns_cskip >= memb_tko_count) &&
		    (nodestates[x].ns_state == NODE_UP)) {
			clulog(LOG_DEBUG, "Member #%d (%s) DOWN\n",
			       nodestates[x].ns_nodeid,
			       nodestates[x].ns_name);
			nodestates[x].ns_state = NODE_DOWN;
			rv = 1;
		}
	}

	return rv;
}


/**
 * Everybody is assumed to have not been seen each round.  This clears out
 * our node_states array.
 */
void
clear_seen(void)
{
	int x;

	for (x = 0; x < MAX_NODES; x++)
		nodestates[x].ns_bump = 1;
}


/**
 * Determine if we are supposed to coordinate membership changes.  If we are
 * the lowest-numbered online node *we see*, then we coordinate.  Since the
 * modified VF protocol we use for membership is okay with multiple
 * coordinators, it is okay if this duplicates across any number of cluster
 * members.  Nodes which just came online can't be the coordinator, unless
 * it's the only node it sees.
 *
 * @param current_view	Current local membership view.
 * @return		1 if we are the coordinator, 0 if not.
 */
int
coordinator(memb_mask_t current_view)
{
	int x;

	if ((memb_count(current_view) == 0) &&
	    (nodestates[my_node_id].ns_state == NODE_UP)) {
		/*
		 * It's just me!
		 */
		return 1;
	}

	for (x = 0; x < MAX_NODES; x++) {
		if ((nodestates[x].ns_state == NODE_UP) &&
		    (memb_online(current_view, x))) {
			/*
			 * Coordinator is (still) present.
			 */
			return (x == my_node_id);
#if 0
		} else if ((nodestates[x].ns_state == NODE_UP) &&
			   (!memb_online(current_view, x))) {
			/*
			 * New, lower-ID node came online
			 */
		} else if ((nodestates[x].ns_state == NODE_DOWN) &&
			   (memb_online(current_view, x))) {
			/*
			 * Previous coordinator went down...
			 */
#endif
		}
	}

	return 0;
}


/**
 * Build membership bitmask from our internal global state.
 *
 * @param mask		Membership mask to fill.
 * @return		0
 */
int
build_memb_view(memb_mask_t mask)
{
	int x;

	memset(mask, 0, sizeof(memb_mask_t));

	for (x = 0; x < MAX_NODES; x++)
		if (nodestates[x].ns_state == NODE_UP)
			memb_mark_up(mask, x);
		else
			memb_mark_down(mask, x);

	return 0;
}


/**
 * Commit a membership mask to our internal global node_states array.
 *
 * @param mask		Membership mask to commit.
 * @return		0
 */
int
commit_memb_view(memb_mask_t mask)
{
	int x;

	for (x = 0; x < MAX_NODES; x++)
		if (memb_online(mask, x)) {
			nodestates[x].ns_state = NODE_UP;
			nodestates[x].ns_cseen = memb_up_count;
			nodestates[x].ns_cskip = 0;
			nodestates[x].ns_bump = 0;
		} else {
			nodestates[x].ns_state = NODE_DOWN;
			nodestates[x].ns_cseen = 0;
			nodestates[x].ns_cskip = memb_tko_count;
			nodestates[x].ns_bump = 1;
		}

	return 0;
}


int32_t
memb_strict_cb(uint32_t __attribute__ ((unused)) keyid,
	       uint64_t __attribute__ ((unused)) viewno,
	       void *data, uint32_t datalen)
{
	memb_mask_t local_view;
	char *lview = NULL;
	
	if (datalen < sizeof(memb_mask_t))
		return 0;
		
	if (viewno < curr_internal_viewno)
		force_update = 1;
		
	if (memb_strict) {

		build_memb_view(local_view);
		/*
		 * If we got a message, someone else things we're up...
		 */
		memb_mark_up(local_view, my_node_id);
		if (memcmp(data, local_view, sizeof(memb_mask_t))) {

			lview = strdup(memb_mask_str(local_view));

			clulog(LOG_DEBUG,
			       "MB: Strict req: L%s != R%s; voting NO\n",
			       lview,
			       memb_mask_str(data));

			if (lview)
				free(lview);
			return 0;
		}
	}

	return 1;
}

int32_t
memb_commit_cb(uint32_t __attribute__ ((unused)) keyid,
	       uint64_t viewno, void *data, uint32_t datalen)
{
	if (datalen < sizeof(memb_mask_t))
		return 0;

	curr_internal_viewno = viewno;
	memcpy(curr_internal_view, data, sizeof(memb_mask_t));

	/*
	 * Update our tables.
	 */
	commit_memb_view(curr_internal_view);

	clulog(LOG_INFO, "Membership View #%d:%s\n",
	       (int)viewno, memb_mask_str(curr_internal_view));

	return 1;	
}


/**
 * Send a membership update to a file descriptor based on the current
 * membership view.
 *
 * @param fd		File descriptor to send to.
 * @param current_view	Current membership view to send to fd.
 * @see			memb_query
 * @return		-1 on failure, 0 on success.
 */
int
send_memb_update(int fd, memb_mask_t current_view)
{
	cm_event_t event;
	int counter;

	event.em_header.eh_magic = GENERIC_HDR_MAGIC;
	event.em_header.eh_length = sizeof(cm_event_t);
	event.em_header.eh_type = EC_MEMBERSHIP;
	event.em_header.eh_event = EV_MEMB_UPDATE;
	event.em_header.eh_memberid = my_node_id;
	event.u.ev_memb.mm_view = curr_internal_viewno;
	memcpy(event.u.ev_memb.mm_mask, current_view, sizeof(memb_mask_t));

	swab_cm_event_hdr_t(&event.em_header);
	swab_cm_memb_event_t(&event.u.ev_memb, counter);

	if (fd == ALL)
		return clist_broadcast((void *)&event, sizeof(event));

	return msg_send(fd, (void *) &event, sizeof(event));
}


/**
 * Not exported to users.
 */
static int
send_node_id(int fd)
{
	/* Note: This might not always match up! */
	return msg_send_simple(fd, EV_ACK, 0, my_node_id);
}


/**
 * If smoothing is used, we delay a period of time to allow node transitions
 * to settle down.  This function starts that timer.  Or rather, it sets
 * when the timer will give up.
 *
 * @param notify_expire	Time at which point the notify timer will expire,
 *			causing the commit messages to go to clients.
 * @return		0
 */
int
start_notify_timer(struct timeval *notify_expire)
{
	struct timeval notify_inc;

	notify_expire->tv_sec = -1;
	notify_expire->tv_usec = 0;

	if (memb_smooth == 0)
		return 0;

	if (getuptime(notify_expire) == -1) {
		clulog(LOG_ERR, "getuptime() failed! Notifying NOW\n");
		notify_expire->tv_sec = -1;
		return 0;
	}

	pthread_mutex_lock(&interval_mutex);
	notify_inc = memb_interval;
	pthread_mutex_unlock(&interval_mutex);

	__scale_tv(&notify_inc, memb_smooth);
	notify_expire->tv_sec += notify_inc.tv_sec;
	notify_expire->tv_usec += notify_inc.tv_usec;

	return 0;
}


/**
 * If smoothing is used, we delay a period of time to allow node transitions
 * to settle down.  This function checks to see if we've passed the end of
 * notify_expire.
 *
 * @param notify_expire Time at which point the notify timer will expire.
 * @return		1 if expired, 0 if not.
 */
int
check_notify_timer(struct timeval *notify_expire)
{
	struct timeval now;

	if (notify_expire->tv_sec == -1)
		return 1;
	if ((notify_expire->tv_sec == 0) && (notify_expire->tv_usec == 0))
		return 0;

	if (getuptime(&now) == -1) {
		clulog(LOG_ERR, "getuptime() failed! Notifying NOW\n");
		return 1;
	}

	if (notify_expire->tv_sec < now.tv_sec)
		return 1;
	if (notify_expire->tv_usec <= now.tv_usec)
		return 1;
	return 0;
}


/**
 * Handle an incoming message on an arbitrary file descriptor.
 * @param fd		File descriptor on which the message was received.
 * @param current_view	Current membership view.
 * @param notify_expire	Notify timer expiration (might be modified)
 * @return		-1 on failure, 0 on success.
 */
int
memb_process_msg(int fd, memb_mask_t current_view,
		struct timeval *notify_expire)
{
	generic_msg_hdr *hdrp;
	int nbytes;

	/* got it - wait for one second */
	/* msg_receive_simple allocates the second buffer!! */
	if ((nbytes = msg_receive_simple(fd, &hdrp, 1)) < 0) {
		clist_delete(fd);
		msg_close(fd);
		return -1;
	}

	/*clulog(LOG_DEBUG, "MB: Received %d bytes\n", nbytes); */

	if (nbytes < sizeof(generic_msg_hdr)) {
		clist_delete(fd);
		msg_close(fd);
		return -1;
	}

	swab_generic_msg_hdr(hdrp);

	switch (hdrp->gh_command) {

	case VF_MESSAGE:
		clulog(LOG_DEBUG, "MB: Received VF_MESSAGE, fd%d\n", fd);

		switch(vf_process_msg(fd, hdrp, nbytes)) {
		case VFR_COMMIT:
			start_notify_timer(notify_expire);
			
			clist_delete(fd);
			/*
			 * FIXME if vf_process_msg runs down into
			 * vf_commit_views, then this file descriptor MIGHT
			 * have already been closed...  Though it's possible
			 * that others were closed instead.
			 */
			msg_close(fd);
			/* SEND STUFF */
			break;
		case VFR_NO:
			clist_delete(fd);
			msg_close(fd);
			break;
		}
		break;

	case EV_REGISTER:
		clulog(LOG_DEBUG, "MB: Received EV_REGISTER, fd%d\n", fd);
		msg_send_simple(fd, EV_ACK, 0, 0);
		//clist_set_flags(fd, EF_MEMBERSHIP);
		send_memb_update(fd, current_view);
		break;

	case EV_UNREGISTER:
		clulog(LOG_DEBUG, "MB: Received EV_UNREGISTER, fd%d\n", fd);
		clist_set_flags(fd, 0);
		break;

	case MEMB_QUERY:
		clulog(LOG_DEBUG, "MB: Received MEMB_QUERY, fd%d\n", fd);
		send_memb_update(fd, current_view);
		clist_delete(fd);
		msg_close(fd);
		break;

	case MEMB_QUERY_NODEID:
		send_node_id(fd);
		clist_delete(fd);
		msg_close(fd);
		break;

	default:
		clulog(LOG_DEBUG, "MB: Mangled message, %d bytes\n", nbytes);
		break;
	}

	if (hdrp)
		free(hdrp);

	return 0;
}


static inline void
__diff_tv(struct timeval *dest, struct timeval *start, struct timeval *end)
{
	dest->tv_sec = end->tv_sec - start->tv_sec;
	dest->tv_usec = end->tv_usec - start->tv_usec;

	if (dest->tv_usec < 0) {
		dest->tv_usec += 1000000;
		dest->tv_sec--;
	}
}


static inline int
__cmp_tv(struct timeval *left, struct timeval *right)
{
	if (left->tv_sec > right->tv_sec)
		return -1;

	if (left->tv_sec < right->tv_sec)
		return 1;

	if (left->tv_usec > right->tv_usec)
		return -1;
	
	if (left->tv_usec < right->tv_usec)
		return 1;

	return 0;
}


#define log_timeval(tv) clulog(LOG_DEBUG, "%s = %d.%06d\n", #tv, tv.tv_sec,\
			       tv.tv_usec)

/**
 * Heartbeat thread, if clumembd%thread == 1.  Our only piece of shared
 * data is memb_interval.  Be aware of this:  Lots of the code might not be
 * thread safe.
 */
void *
pulsar(void __attribute__ ((unused)) * unused_parameter)
{
	struct timeval maxtime, oldtime, newtime, diff, local_memb_interval,
		sleeptime;
	int rv;

	block_signal(SIGTERM);
	block_signal(SIGHUP);

	clulog(LOG_DEBUG, "Transmit thread: %s\n", __FUNCTION__);

	pthread_mutex_lock(&interval_mutex);
	memcpy(&local_memb_interval, &memb_interval, sizeof(struct timeval));
	pthread_mutex_unlock(&interval_mutex);

	memcpy(&sleeptime, &local_memb_interval, sizeof(struct timeval));
	memcpy(&maxtime, &local_memb_interval, sizeof(struct timeval));
	__scale_tv(&maxtime, memb_tko_count);

	getuptime(&oldtime);

	while (proceed && memb_thread) {

		select(0, NULL, NULL, NULL, &sleeptime);
		rv = heartbeat_send(1);

		getuptime(&newtime);
		__diff_tv(&diff, &oldtime, &newtime);

		/*
		 * We sent one; let's ensure we don't remove ourself from
		 * the cluster.
		 */
		if (rv == 0)
			memcpy(&oldtime, &newtime, sizeof(oldtime));

		/*
		 * Reboot if we didn't send a heartbeat in interval*TKO_COUNT
		 */
		if (!debug && __cmp_tv(&maxtime, &diff) == 1) {
			clulog(LOG_EMERG, "Failed to send a heartbeat within "
			       "failover time - REBOOTING\n");
			REBOOT(RB_AUTOBOOT);
		}

		/*
		 * If the amount we took to complete a loop is greater or less
		 * than our interval, we adjust by the difference each round.
		 *
		 * It's not really "realtime", but it helps!
		 */
		if (__cmp_tv(&diff, &local_memb_interval) == 1)
			__diff_tv(&newtime, &diff, &local_memb_interval);
		else
			__diff_tv(&newtime, &local_memb_interval, &diff);
		__diff_tv(&sleeptime, &newtime, &local_memb_interval);
	}

	return NULL;
}


/**
 * Create the heartbeat transmit thread if clumembd%thread == 1.
 * @param thread	Thread structure which will be filled by the call
 *			to pthread_create.
 * @return		-1 on failure, 0 on success.
 */
int
create_hb_trans_thread(pthread_t * thread)
{
	pthread_attr_t attrs;

	pthread_attr_init(&attrs);
	pthread_attr_setinheritsched(&attrs, PTHREAD_INHERIT_SCHED);
	pthread_atfork(NULL, NULL, NULL);

	return pthread_create(thread, &attrs, pulsar, NULL);
}


/**
 * Receive heartbeat packets on our heartbeat channels.
 *
 * @param readfds	Set of all active file descriptors.
 * @return		0 if no update is necessary, >0 if a membership push
 *			is required.
 */
int
heartbeat_recv(fd_set *readfds)
{
	hb_if_t *hb_if;
	int nbytes, need_update = 0;
	socklen_t addrlen;
	struct sockaddr_in from;
	node_ping_msg_t ping_msg;

	/*
	 * Receive heartbeat packets from any active file
	 * descriptors which are set
	 */
	for (hb_if = hb_if_list; hb_if; hb_if = hb_if->next) {
		
		if ((hb_if->hb_bcast != -1) &&
		    FD_ISSET(hb_if->hb_bcast, readfds)) {
			
			addrlen = sizeof(from);
			while ((nbytes = recvfrom(hb_if->hb_bcast, &ping_msg,
						  sizeof(ping_msg), 0,
						  (struct sockaddr *) &from,
						  &addrlen)) > 0) {
				
				addrlen = sizeof(from);
				swab32(ping_msg.np_nodeid);
				swab64(ping_msg.np_token);
				swab32(ping_msg.np_crc);
				
				need_update += update_seen(&ping_msg, nbytes,
							   &from);
			}
		}
		
		if ((hb_if->hb_mcast_recv != -1) &&
		    FD_ISSET(hb_if->hb_mcast_recv, readfds)) {
			
			addrlen = sizeof(from);
			while ((nbytes = recvfrom(hb_if->hb_mcast_recv,
						  &ping_msg, sizeof(ping_msg),
						  0,
						  (struct sockaddr *) &from,
						  &addrlen)) > 0) {
				
				addrlen = sizeof(from);
				swab32(ping_msg.np_nodeid);
				swab64(ping_msg.np_token);
				swab32(ping_msg.np_crc);
				
				need_update += update_seen(&ping_msg, nbytes,
							   &from);
			}
		}
	} /* for (hb_if... */

	return need_update;
}


/**
 * Start the watchdog if applicable
 */
void
start_watchdog(void)
{
	struct timeval fo_time;

	memcpy(&fo_time, &memb_interval, sizeof(struct timeval));
	__scale_tv(&fo_time, memb_tko_count);
	clumembd_sw_watchdog_start(&fo_time, my_node_id);
}


/**
 * Driver function. XXX This function is WAY too long.  Too complicated.
 */
int
main(int argc, char **argv)
{
	int foreground = 0;
	int rv, tmpfd;
	struct timeval tv, notify_expire = { 0, 0 };
	msg_handle_t msg_listen_fd = 0;
	fd_set readfds;
	int nfds, need_update, failed_sends = 0;
	hb_if_t *hb_if;
	memb_mask_t new_view;
	extern char *optarg;
	char *conffile = CLU_CONFIG_FILE;
	pthread_t hb_trans;

	while ((rv = getopt(argc, argv, "fdc:")) != EOF) {
		switch (rv) {
		case 'c':
			conffile = optarg;
			break;
		case 'd':
			debug = 1;
			break;
		case 'f':
			foreground = 1;
		default:
			break;
		}
	}

	set_facility();
	if (!debug)
		(void) clu_set_loglevel(LOG_INFO);
	else
		(void) clu_set_loglevel(LOG_DEBUG);

	if (!foreground && (geteuid() == 0)) 
		daemon_init(argv[0]);

	if (foreground)
		clu_log_console(1);

	clulog(LOG_DEBUG, "Starting up\n");

	/*
	 * We want to catch sighup
	 */
	setup_sig_handlers();

	/*
	 * See if networking is configured
	 */
	if (access(PROC_NET, R_OK)) {
		if (errno == ENOENT)
			clulog(LOG_CRIT, "networking not configured\n");
		else
			clulog(LOG_CRIT, "access: %s\n", strerror(errno));
		return 1;
	}

	/*
	 * Create comm device
	 */
	if (CFG_ReadFile(conffile) != CFG_OK) {
		clulog(LOG_CRIT, "Couldn't read %s: %s\n", conffile,
		       strerror(errno));
		return 1;
	}
	set_facility();
	local_init();

	if (heartbeat_init(memb_port) != 0) {
		clulog(LOG_CRIT, "Heartbeat failed to initialize!\n");
		return 1;
	}

	if (!memb_bcast && !memb_mcast) {
		clulog(LOG_CRIT, "No heartbeat methods available.\n");
		return 1;
	}

	/*
	 * Find our node id, etc.
	 */
	memset(curr_internal_view, 0, sizeof(curr_internal_view));
	memset(new_view, 0, sizeof(new_view));

	msg_listen_fd = msg_listen(PROCID_CLUMEMBD);
	if (msg_listen_fd == -1) {
		clulog(LOG_CRIT, "Unable to establish listen channel!\n");
		return 1;
	}

	need_update = 0;
	start_watchdog();
	clulog(LOG_DEBUG, "Waiting for requests.\n");

	/*
	 * Don't assume the view will change.
	 */
	if (memb_thread && create_hb_trans_thread(&hb_trans)) {
		clulog(LOG_WARNING, "Threading disabled: %s\n",
				 strerror(errno));
		memb_thread = 0;
	}

	while (proceed) {

		/*
		 * Delay for the interval time. We need to be able to be
		 * interrupted in order to respond to messages while we are
		 * waiting.
		 */
		pthread_mutex_lock(&interval_mutex);
		memcpy(&tv, &memb_interval, sizeof(tv));
		pthread_mutex_unlock(&interval_mutex);

		/*
		 * Nobody has sent us anything yet.
		 */
		clear_seen();

		/*
		 * Send our ping packet(s)
		 */
		if (!memb_thread) {
			if (heartbeat_send(1) == -1) {
				failed_sends++;
				if (!debug &&
				    (failed_sends >= memb_tko_count)) {
					clulog(LOG_EMERG, "Failed to send "
					       "a heartbeat within failover "
					       "time - REBOOTING\n");
					sync();
					REBOOT(RB_AUTOBOOT); /* :( */
				}
			} else {
				failed_sends = 0;
			}
		}

		while ((tv.tv_sec || tv.tv_usec) && !force_update) {

			FD_ZERO(&readfds);
			FD_SET(msg_listen_fd, &readfds);

			clist_fill_fdset(&readfds);

			/* interface sockets */
			for (hb_if = hb_if_list; hb_if; hb_if = hb_if->next) {
				if (hb_if->hb_bcast != -1)
					FD_SET(hb_if->hb_bcast, &readfds);
				if (hb_if->hb_mcast_recv != -1)
					FD_SET(hb_if->hb_mcast_recv, &readfds);
			}

			nfds = select(1024, &readfds, NULL, NULL, &tv);
			if (nfds == -1)
				/*
				 * error... try again?
				 */
				continue;

			if (nfds == 0)
				/*
				 * No messages received; everybody gets bumped.
				 */
				break;

			/*
			 * Accept new connections.
			 */
			if (FD_ISSET(msg_listen_fd, &readfds)) {

				if ((tmpfd = msg_accept(msg_listen_fd)) < 0)
					tmpfd = -1;
				else {
					clulog(LOG_DEBUG,
					       "MB: New connect: fd%d\n",
					       tmpfd);
					clist_insert(tmpfd, 0);
				}

				FD_CLR(msg_listen_fd, &readfds);
			}

			/*
			 * Handle any new requests on connected sockets.
			 */
			while ((tmpfd = clist_next_set(&readfds)) != -1)
				memb_process_msg(tmpfd, curr_internal_view,
						&notify_expire);

			need_update += heartbeat_recv(&readfds);
		} /* while (tv.tv_sec || tv.tv_usec) */

		/*
		 * Remove timed out sockets.  We actually need to transition
		 * ourself if this happens - we need to declare ourself out.
		 */
		while ((rv = vf_purge(KEY_MEMBERSHIP, &tmpfd)) != VFR_NO) {

			clulog(LOG_WARNING,
			       "MB: Request on fd%d timed out\n",
			       tmpfd);
			clist_delete(tmpfd);
			msg_close(tmpfd);

			/*
			 * Since we're already doing it wrong, might as well
			 * go all the way!
			 */
			if (rv == VFR_COMMIT) {
				clulog(LOG_WARNING,"MB: Timed-out "
				       "fd%d caused commits!\n", tmpfd);
				start_notify_timer(&notify_expire);
			}
		}

		/*
		 * Check the view timer
		 */
		if (check_notify_timer(&notify_expire)) {
			send_memb_update(ALL, curr_internal_view);
			notify_expire.tv_sec = 0;
			notify_expire.tv_usec = 0;
		}

		/*
		 * Look for member state transitions.
		 */
		update_skips();

		if (vf_end(KEY_MEMBERSHIP) == -2)
			continue; /* Child still running */

		build_memb_view(new_view);
		need_update = !!memcmp(new_view, curr_internal_view,
					sizeof(curr_internal_view));
		/*
		 * Do we need to push our view out?  If we changed something,
		 * we do.  Only if we're the coordinator though.
		 */
		if ((force_update || need_update) &&
		    coordinator(curr_internal_view)) {

			clulog(LOG_DEBUG, "MB: Initiating vote on: %s\n",
				memb_mask_str(new_view));
			if ((vf_start(new_view, VFF_IGNORE_ERRORS,
				      KEY_MEMBERSHIP, (void *)new_view,
				      sizeof(memb_mask_t)) == -1) &&
			    (errno != EAGAIN))
				clulog(LOG_CRIT, "VF Failure - "
				       "Membership services down!\n");
		}

		if (force_update)
			force_update = 0;

		if (need_reconfig)
			reconfigure();
	}

	/* Make sure the thread sending heartbeats terminated. */
	if (memb_thread) {
		/* TODO: Should check the returned code. */
		pthread_join(hb_trans, NULL);
	}
	clumembd_sw_watchdog_stop();
	heartbeat_send(0); /* Last breath */
	clulog(LOG_INFO, "Exiting\n");
	exit(0);
}
