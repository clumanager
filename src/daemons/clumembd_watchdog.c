/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
 */
/** @file
 * Extension to membership daemon to handle watchdog timers.
 *
 *  $Id: clumembd_watchdog.c,v 1.7 2003/09/09 03:30:09 lhh Exp $
 *
 *  author: Tim Burke  <tburke at redhat.com>
 *
 *  This is done as an intermediate level of data integrity.  The strongest
 *  levels of data integrity exist when a power switch is employed in the 
 *  cluster.  Short of that, a watchdog timer is better than using a
 *  power switch type of none.
 *
 *  This module first checks to see if the watchdog capability has been
 *  enabled for this member, if so the watchdog capability is enabled.
 *  This gets disabled upon cluster stop.
 *
 *  Although this functionality may be more closely aligned with the
 *  functions performed by powerd, it is implemented out of clumembd in order
 *  to make it an integral portion of the membership algorithm.  You wouldn't
 *  want the watchdog to continue operating when the rest of the cluster 
 *  membership daemons have died off.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/syslog.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <assert.h>
#include <signal.h>
#include <sys/time.h>

#include <msgsvc.h>
#include <clusterdefs.h>
#include <clulog.h>
#include <xmlwrap.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/wait.h>

#ifndef _LINUX_WATCHDOG_H
/*
 * XXX - Should be including <linux/watchdog.h>, but these new ioctls may
 * not be present on the build engine, so on a temporary basis, these
 * defines are being spelled out here.  (An accident waiting to happen
 * should these defines diverge from the header file.
 */
#define WATCHDOG_IOCTL_BASE     'W'
#define WATCHDOG_EXIT_STRING 	"V" /* GRRRRR */
#define WDIOC_SETTIMEOUT        _IOWR(WATCHDOG_IOCTL_BASE, 6, int)
#define WDIOC_GETTIMEOUT        _IOR(WATCHDOG_IOCTL_BASE, 7, int)
//#define WDIOC_SETPERSIST        _IOR(WATCHDOG_IOCTL_BASE, 8, int)
//#define WDIOC_SETNONPERSIST     _IOR(WATCHDOG_IOCTL_BASE, 9, int)
#endif				/* ifndef _LINUX_WATCHDOG_H */

static const char *version __attribute__ ((unused)) = "$Revision: 1.7 $";
static int clumembd_start_watchdog(int duration);

/** file descriptor of watchdog timer device */
static int clumembd_watchdog_fd = -1;	

#if defined(WDIOC_SETPERSIST) && defined(WDIOC_SETNONPERSIST)
int clumembd_watchdog_setpersistent = 0;	// new ioctls present
#endif

/**
 * Called to commence operation of the software watchdog timer.
 *
 * @param failover_time	The the timeval from membership (interval*tko_count)
 *			specifying the expected failover time.
 * @param mynodeid	The local node ID.
 * @return		0 on success, 1 when the local node is not
 *			using the watchdog, -1 on failure
 */
int
clumembd_sw_watchdog_start(struct timeval *failover_time, int mynodeid)
{
	int duration;
	int retval;
	char *val;
	char tok[256];

	if (!failover_time->tv_sec && !failover_time->tv_usec)
		return -1;

	snprintf(tok, sizeof(tok), "members%%member%d%%watchdog", mynodeid);
	if (CFG_Get(tok, NULL, &val) != CFG_OK)
		return 1;

	if ((val[0] != 'y') && (val[0] != 'Y') && (atoi(val) == 0))
		return 1;

	duration = failover_time->tv_sec;
	if (!duration)
		duration++;

	/*
	 * Ensure our watchdog fires *before* failover time!
	 */
	if (!failover_time->tv_usec && 
	    (failover_time->tv_sec == duration))
		duration--;

	/*
	 * Parameter validation.  Consider an hour to be perposetrous.
	 */
	if ((duration <= 2) || (duration > (60 * 60))) {
		clulog(LOG_ERR, "%s: Failover time too fast/slow for watchdog "
		       " use.\n", __FUNCTION__);
		return (-1);
	}
	

	retval = clumembd_start_watchdog(duration);
	if (retval != 0) {
		clulog(LOG_ERR, "%s: failed to initiate watchdog.",
		       __FUNCTION__);
		return (-1);
	}

	if (failover_time->tv_usec) {
		clulog(LOG_INFO,
		       "WATCHDOG: Expected failure detection time: %d.%06ds!\n",
		       failover_time->tv_sec, failover_time->tv_usec);
		clulog(LOG_INFO,
		       "WATCHDOG: Watchdog update time: %ds!\n", duration);
	}

	return (0);
}


/**
 * Called to commence operation of the software watchdog timer.
 * Does nothing if the local node has watchdog disabled.
 * @return		0 on success, -1 on failure
 */
int
clumembd_sw_watchdog_stop(void)
{
	int retval;
#ifdef WDIOC_SETNONPERSIST
	int unused_param = 0;
#endif

	if (clumembd_watchdog_fd == -1) {
		clulog(LOG_DEBUG, "%s: watchdog is not running.\n",
		       __FUNCTION__);
		return (0);
	}
	/*
	 * Disarm the watchdog (refer to the comments in
	 * clumembd_sw_watchdog_start for details).  Don't bother with this
	 * call if the set persistent didn't succeed.
	 */

#ifdef WATCHDOG_EXIT_STRING
	if (write(clumembd_watchdog_fd, WATCHDOG_EXIT_STRING,
		  strlen(WATCHDOG_EXIT_STRING)) < 0) {
		clulog(LOG_WARNING,
		       "%s: Unable to write closure!\n", __FUNCTION__);
		sync();
	}
#endif 

#ifdef WDIOC_SETNONPERSIST
	if (clumembd_watchdog_setpersistent) {
		if (ioctl
		    (clumembd_watchdog_fd, WDIOC_SETNONPERSIST, &unused_param)
		    == -1) {
			clulog(LOG_WARNING,
			       "%s: unable to clear persistence.\n",
			       __FUNCTION__);
		} else {
			clulog(LOG_DEBUG, "%s: SETNONPERSIST success\n",
			       __FUNCTION__);
		}
	}
#endif				// WDIOC_SETNONPERSIST

	/*
	 * Disable the watchdog timer by closing the file descriptor.
	 */
	retval = close(clumembd_watchdog_fd);
	if (retval != 0) {
		clulog(LOG_ERR, "%s: failed stop watchdog.\n", __FUNCTION__);
		// We're probably dead meat soon!
		return (-1);
	}
	clumembd_watchdog_fd = -1;
	clulog(LOG_DEBUG, "%s: successfully stopped watchdog.\n", __FUNCTION__);
	return (0);
}


/**
 * Called to tickle the watchdog timer. This will reset its clock.
 * @return		0 on success, -1 on failure
 */
int
clumembd_sw_watchdog_reset(void)
{

	int retval;

	if (clumembd_watchdog_fd == -1) {
		return (0);	// Nothing to do.
	}
	retval = write(clumembd_watchdog_fd, "\0", 1);
	if (retval != 1) {
		clulog(LOG_DEBUG, "%s: write failed!\n", __FUNCTION__);
		return (-1);
	}
	return (0);
}


/**
 * Prepare for software watchdog processing. This consist of opening the 
 * device special file to establish the file descriptor.
 *
 * @param duration	The time it will take, in seconds, for the
 *			watchdog to fire if not written to.
 * @return		0 on success, -1 on failure.
 */
static int
clumembd_start_watchdog(int duration)
{
#ifdef WDIOC_SETPERSIST
	int unused_param = 0;
#endif
	int check_duration = 0;

	clumembd_watchdog_fd = open("/dev/watchdog", O_WRONLY);
	if (clumembd_watchdog_fd < 0) {
		clulog(LOG_ERR, "%s: unable to open /dev/watchdog\n",
		       __FUNCTION__);
		return (-1);
	}
	/*
	 * Configure the watchdog 
	 * expiration interval, based on the cluster configuration params
	 * for quorum intervals.  Problem is that not all watchdog drivers 
	 * not uniformly implement this ioctl.  Consequently, we are
	 * dependent on module loading parameters to pass the corruct
	 * value for some watchdog types.
	 *
	 * Due to this non-uniformity, failure of this ioctl is not
	 * considered a fatal enough error to return a failure status
	 * from this routine.
	 */
#ifdef WDIOC_SETTIMEOUT
	if (ioctl(clumembd_watchdog_fd, WDIOC_SETTIMEOUT, &duration) == -1) {
		clulog(LOG_WARNING, "%s: unable to set duration to %d.\n",
		       __FUNCTION__, duration);
	} else {
		clulog(LOG_DEBUG, "%s: set duration to %d.\n", __FUNCTION__,
		       duration);
		// Paranoia check.
		if (ioctl(clumembd_watchdog_fd, WDIOC_GETTIMEOUT,
			  &check_duration) != -1) {
			if (duration != check_duration) {
				clulog(LOG_ERR,
				       "%s: duration mismatch %d, %d.\n",
				       __FUNCTION__, duration, check_duration);
			}
		}
	}
#endif				// WDIOC_SETTIMEOUT
	/*
	 * By default, if a process exits, the kernel cleanup code will
	 * call the close routine for all open fd's.  This presents a problem
	 * for watchdog usage because if clumembd abnormally terminates,
	 * the close gets called which disarms the watchdog.  To work around
	 * that behavior, there is a WDIOC_SETPERSIST ioctl which arms
	 * the watchdog until such time as a WDIOC_SETNONPERSIST call is 
	 * made.
	 */
#ifdef WDIOC_SETPERSIST
	if (ioctl(clumembd_watchdog_fd, WDIOC_SETPERSIST, &unused_param) == -1) {
		clulog(LOG_WARNING, "%s: unable to set persistence.\n",
		       __FUNCTION__);
	} else {
		clumembd_watchdog_setpersistent = 1;
		clulog(LOG_DEBUG, "%s: SETPERSIST success\n", __FUNCTION__);
	}
#endif				// WDIOC_SETPERSIST
	return (0);
}
