/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Quorum Daemon of Red Hat Cluster Manager.
 */
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/reboot.h>

#include <clusterdefs.h>
#include <msgsvc.h>
#include <clulog.h>
#include <xmlwrap.h>
#include <nodeid.h>
#include <clist.h>
#include <quorum.h>
#include <quorumparams.h>
#include <sharedstate.h>
#include <clushared.h>
#include <vf.h>
#include <ping.h>
#include <stonithapis.h>
#include <svcmgr.h>
#include <sched.h>
#include <findproc.h>
#include <cm_api.h>

#include <net_tie.h>
#include <disk_tie.h>

#ifdef TESTING
#ifdef reboot
#undef reboot
#endif
#define reboot(arg) {\
	clulog(LOG_EMERG, "reboot(%s) @ %s:%d\n", #arg, __FILE__, __LINE__); \
	raise(SIGSTOP); \
}
#endif

int start_vf(memb_mask_t curr_mask, quorum_view_t *qv);
int send_quorum_update(int fd, int msg, uint64_t qview, memb_mask_t qmask,
		       memb_mask_t pmask);
int gulm_alive(void);

/**
 * If set to 1, we run without spawning the lock daemon or service manager.
 * This is in case someone only needs fencing, membership, and quorum services.
 *
 * This does NOT change the behavior of the simple majority algorithm!!!
 */
static int simple_operation = 0;
static int proceed = 1, sigterm_received = 0, sighup_received = 0;
static int my_node_id = -1;
static int membfd = -1, listenfd = -1;
static int debug = 0;
static char *tb_ip = NULL; /* Tie breaker IP-address */
static time_t pswitch_check = 0;
static int allow_soft_quorum = 0;
static int allow_disk_quorum = 1;
static int ignore_gulm_absence = 0;

/*
 * Local copies of the VF stuff
 */
static quorum_view_t quorum_status;
static memb_mask_t nodes_exiting; /* LHH */
static uint64_t quorum_view = 0;
static char clean_cluster_transition = 0;

/* Power switch status */
#define PSWITCH_GOOD    2
#define PSWITCH_BAD	1
#define PSWITCH_UNKNOWN	0

int total_nodes(int force);

/** TODO: make this accessible from outside */
struct {
	uint32_t ss_checker;
	uint32_t ss_status;
	uint64_t ss_timestamp;
	uint32_t ss_errors;
} pswitch_states[MAX_NODES];


static void
set_facility(void)
{
	char *p;
	if (CFG_Get((char *) "cluster%logfacility", NULL, &p) == CFG_OK) {
		if (p)
			clu_set_facility(p);
	}
}


/**
 * Read quorum-daemon-specific configuration parameters from the cluster
 * configuration.
 *
 * @see CFG_Get
 * @return 0
 */
static int
quorumd_read_params(void)
{
	int nodes;
	char *p;
	struct sched_param param;

	nodes = total_nodes(1);

	if (CFG_Get((char *) CFG_QUORUM_LOGLEVEL, NULL, &p) == CFG_OK) {
		if (p) {
			clulog(LOG_DEBUG, "changing loglevel from %d to %d\n",
			       clu_set_loglevel(atoi(p)), atoi(p));
		}
	}

	if (CFG_Get((char *) CFG_QUORUM_IGNORE_GULM, NULL, &p) == CFG_OK) {
		if (p && (p[0] == 'y' || p[0] == '1' || p[0] == 'Y')) {
			clulog(LOG_WARNING,
			       "Ignoring absence of lock_gulmd\n");
			ignore_gulm_absence = 1;
		}
	}

	if (CFG_Get((char *) "cluquorumd%tiebreaker_ip", NULL, &tb_ip) ==
		    CFG_OK) {

		if (tb_ip) {
			if (nodes == 2 || nodes == 4) {
				clulog(LOG_INFO, "IPv4-TB: %s\n",
				       tb_ip);
			} else {
				clulog(LOG_DEBUG, "Specified IP Tie-Breaker ",
				       "disabled.");
				clulog(LOG_DEBUG, "Member count must be 2 or 4"
				       " to use IP Tie-Breaker\n");
				tb_ip = NULL;
			}
			net_tiebreaker_init(tb_ip);
		}
	}

	if (CFG_Get((char *) CFG_QUORUM_ALLOW_SOFT, NULL, &p) == CFG_OK) {
		if (p && (p[0] == 'y' || p[0] == '1' || p[0] == 'Y')) {
			if (nodes == 2 || nodes == 4)
				clulog(LOG_WARNING, "Allowing soft quorum.\n");
			allow_soft_quorum = 1;
		}
	}

	if (CFG_Get((char *) CFG_QUORUM_DISK_QUORUM, NULL, &p) == CFG_OK) {
		if (p && (p[0] == 'y' || p[0] == '1' || p[0] == 'Y') &&
		    (nodes == 2)) {
			allow_disk_quorum = 1;
		} else {
			clulog(LOG_NOTICE, "Not allowing disk quorum.\n");
			allow_disk_quorum = 0;
		}
	}


	if (CFG_Get((char *) CFG_QUORUM_RTP, NULL, &p) == CFG_OK) {
		if (p) {
			memset(&param,0,sizeof(param));
			param.sched_priority = atoi(p);
			if (param.sched_priority < 0
			    || param.sched_priority >= 100)
				param.sched_priority = 0;

			if (sched_setscheduler(0, SCHED_FIFO,
					       (void *)&param) != 0)
				clulog(LOG_WARNING, "Set FIFO Priority"
				       " failed: %s\n", strerror(errno));
			else 
				clulog(LOG_INFO, "Using RT/FIFO Priority %d\n",
				       param.sched_priority);
		}
	}

	return 0;
}


/**
 * Determine the next online member of the cluster.  This is used to determine
 * which member we intend to collect switch information on.
 *
 * @return	Next online member, or ourself if no one else is online.
 */
uint32_t
next_member(void)
{
	int x;
	
	x = my_node_id + 1;
	if (x >= MAX_NODES)
		x = 0;

	do {
		if (memb_online(quorum_status.qv_mask, x))
			return x;

		x++;
		if (x >= MAX_NODES)
			x = 0;
	} while (x != my_node_id);

	return my_node_id;
}


/**
 * Distribute the state of a power switch to all running members
 * of the cluster.  They will update their internal view of the
 * state of the power switch, which is, as of now, unused, but
 * could be used to request power switch status from the quorum
 * daemon.
 *
 * @param member_controlled	This is the member controlled by a group of
 *				switches.
 * @param msg			QUORUM_PSWITCH_GOOD or QUORUM_PSWITCH_BAD
 */
void
distribute_switch_state(int member_controlled, int msg)
{
	int x, fd;
	struct timeval tv;


	for (x=0; x<MAX_NODES; x++) {
		if (!memb_online(quorum_status.qv_mask, x))
			continue;
		tv.tv_sec = 3;
		tv.tv_usec = 0;
		fd = msg_open_timeout(PROCID_CLUQUORUMD, x, &tv);
		if (fd == -1)
			continue;
		msg_send_simple(fd, msg, my_node_id, member_controlled);
		msg_close(fd);
	}
}


void
gulm_check_states(void)
{
	int x;
	for (x=0; x < MAX_NODES; x++) {
		if (clu_stonith_gulm(x) == 0)
			clu_stonith_status(x);
	}
}


/**
 * Check the power switch status of the next-higher running member of the
 * cluster.
 */
void
check_power_switch(pid_t *child_pid)
{
	int pid, x;
	char *nodename = NULL;

	*child_pid = 0;
	x = next_member();
	if (x == my_node_id)
		return;

	/* Do this so we keep an active record of gulm's view of
	   node states */
	gulm_check_states();
	
	pid = fork();
	if (pid == -1) {
		clulog(LOG_ERR, "fork: %s\n", strerror(errno));
	}

	if (pid) {
		*child_pid = (pid_t)pid;
		return;
	}

	/* CHILD */
	getNodeName(x, &nodename);

	clulog(LOG_DEBUG, "PID%d handling PSWITCH_CHECK for member %s\n",
	       getpid(), nodename);
	if (clu_stonith_status(x)) {
		distribute_switch_state(x, QUORUM_PSWITCH_BAD);
		exit(0);
	}

	distribute_switch_state(x, QUORUM_PSWITCH_GOOD);
	exit(0);
}


/**
 * Reconfiguration signal handler.
 */
static void
sighup_handler(int signo)
{
	if (signo == SIGHUP)
		sighup_received = 1;
}


static void
quorumd_reconfig(void)
{
	/*
	 * Re-read our part of the configuration file. First, we need to
	 * explicitly close the CFG library, so that we don't just reread
	 * parameters cached by the library.
	 */
	sighup_received = 0;

	clulog(LOG_INFO, "Reloading configuration\n");

	CFG_Destroy();
	CFG_ReadFile(CLU_CONFIG_FILE);
	set_facility(); /* Only on startup */
	quorumd_read_params();
	msg_svc_init(1); /* Reconfigure msg subsys */
	clu_stonith_deinit(); /* Reconfigure power switches */
	clu_stonith_init();
}


/**
 * Notify all the other quorum daemons that we are going to exit *cleanly*
 * so that we do not get STONITHed.
 */
void
notify_exiting(void)
{
	int x, fd, errors = 0;
	struct timeval tv;

	for (x=0; x < MAX_NODES; x++) {
		if (!memb_online(quorum_status.qv_mask, x))
			continue;

		if (x == my_node_id)
			continue;

		tv.tv_sec = 10;
		tv.tv_usec = 0;
		fd = msg_open_timeout(PROCID_CLUQUORUMD, x, &tv);
		if (fd == -1) {
			errors++;
			continue;
		}

		/* XXX check return value?? */
		msg_send_simple(fd, QUORUM_CLEAN_EXIT, my_node_id, 0);
		msg_close(fd);
	}
	
	if (errors)
		clulog(LOG_WARNING, 
		       "Couldn't notify all members.  We might get "
		       "fenced!\n", x);
}


/**
 * Exit the quorum daemon: Bye bye.
 *
 * @param retval	Value to return to parent process.
 * @return		I hope not.
 */
void
quorum_exit(int retval)
{
	send_quorum_update(ALL, EV_SHUTDOWN, 0, quorum_status.qv_mask,
			   quorum_status.qv_panic_mask);

        /*
	 * Kill the disk/net threads if in use.
	 */
	net_cancel_quorum_thread();
	disk_cancel_quorum_thread();

	/*
	 * Kill membership
	 */
	killall("clumembd", SIGTERM);
	sleep(1);
	killall("clulockd", SIGTERM);

	if (retval != 0) {
		clulog(LOG_CRIT, "Unclean exit: Status %d\n", retval);
	} else {
		clulog(LOG_INFO, "Exiting\n");
	}

	exit(retval);
}


/**
 * Send TERM signal to the service manager.
 *
 * @return		0 if no service manager processes exist,
 *			otherwise 1.
 * @see findproc
 */
int
notify_svcmgr(void)
{
	return killall("clusvcmgrd", SIGTERM);
}


/**
 * Spawn a cluster daemon, given the full path name.
 *
 * @param daemon_name	Path to the executable of the daemon to spawn.
 * @return		-1 on fork failure or the exit status of the
 *			of the child process we spawned.
 */
static int
spawn_daemon(const char *daemon_name)
{
	pid_t dmn_pid = -1;
#ifndef DEBUG
	int status;
#endif

	if (debug)
		return 0;

	clulog(LOG_DEBUG, "spawn_daemon: starting %s.\n", daemon_name);
	if ((dmn_pid = fork()) == 0) {
		/*
		 * Child.  Now Exec.
		 */
		execl(daemon_name, daemon_name, (char *)NULL);
		/*
		 * Should not reach here.
		 */
		quorum_exit(1);
	} else if (dmn_pid < 0) {
		/*
		 * What the fork?
		 */
		return -1;
	}

	/*
	 * The first thing this daemon should do is disassociate itself from
	 * its controlling terminal and fork into the background.
	 */
#ifndef DEBUG
	waitpid(dmn_pid, &status, 0);
	if (WIFEXITED(status))
		return (WEXITSTATUS(status));
	else
		return -1;
#else
	return 0;
#endif
}


/**
 * Send a quorum update to a file descriptor.
 *
 * @param fd		File descriptor.
 * @param msg		Message type to send as quorum event.
 * @param qview		Quorum view number
 * @param qmask		Quorum node mask (online nodes)
 * @param pmask		Quorum node mask (panicked nodes)
 * @return 0 for now
 */
int
send_quorum_update(int fd, int msg, uint64_t qview,
		   memb_mask_t qmask, memb_mask_t pmask)
{
	cm_event_t event;
	int counter;

	/*
	 * send the quorum update
	 */
	event.em_header.eh_magic = GENERIC_HDR_MAGIC;
	event.em_header.eh_length = sizeof(cm_event_t);
	event.em_header.eh_type = EC_QUORUM;
	event.em_header.eh_event = msg;
	event.em_header.eh_memberid = my_node_id;

	event.u.ev_quorum.qm_view = qview;
	memcpy(event.u.ev_quorum.qm_mask, qmask, sizeof(memb_mask_t));
	memcpy(event.u.ev_quorum.qm_mask_panic, pmask, sizeof(memb_mask_t));

	swab_cm_event_hdr_t(&event.em_header);
	swab_cm_quorum_event_t(&event.u.ev_quorum, counter);

	if (fd == ALL) {
		clist_broadcast((void *)&event, sizeof(event));
	} else {
		msg_send(fd, (void *) &event, sizeof(event));
	}

	return 0;
}


/**
 * STONITH operation: Shoot all nodes which are in shoot_mask.
 *
 * @param shoot_mask	The cluster member mask to shoot.
 * @param panic_mask	Empty mask which is filled with the bits
 * 			corresponding to all cluster members we
 *			tried to shoot but failed. EG: power switch
 *			failure, etc.
 */
void
shoot_nodes(memb_mask_t shoot_mask, memb_mask_t panic_mask)
{
	/*
	 * TODO: Benefit-of-the-doubt case: Serial power controllers:
	 * If you lose connection to serial power controllers controlling a
	 * node as well as the connection to the node, chances are good that
	 * someone pulled the plug on the power controllers!
	 */
	int x;

	for (x=0; x<MAX_NODES; x++) {

		/* If it's marked as panicked, don't shoot it! */
		if (memb_online(panic_mask, x)) {
			memb_mark_down(shoot_mask, x);
			continue;
		}
			
		if (memb_online(shoot_mask, x)) {
			if (clu_stonith_fence_cycle(x) != 0) {
				clulog(LOG_EMERG,
				       "Failed to shoot member #%d!\n",x);
				memb_mark_up(panic_mask, x);
			}
			memb_mark_down(shoot_mask, x);
		}
	}
}


/**
 * Handle formation of a quorum.
 */
void
quorum_formed(void)
{
	if (simple_operation) {
		clulog(LOG_NOTICE, "Quorum Formed\n");
	} else {
		clulog(LOG_NOTICE, "Quorum Formed; Starting Service "
		       "Manager\n");

		spawn_daemon("/usr/sbin/clusvcmgrd");
	}

	send_quorum_update(ALL, EV_QUORUM_GAINED, quorum_view,
			   quorum_status.qv_mask,
			   quorum_status.qv_panic_mask);
}


/**
 * Handle loss of quorum.  Not gracefully, either.  This is a serious
 * condition from a member's perspective.
 */
void
quorum_lost(void)
{
	/*
	 * Members without a STONITH device which were not shutting
	 * down must reboot _immediately_.  We need to do this
	 * because other members can not fence us off, and we don't
	 * know WHY we lost quorum - therefore, it's not safe to
	 * continue operation.  A network cable-pull on a member
	 * w/o a power switch configured will cause this event.
	 *
	 * We make a concession in the narrow case that someone 
	 * was already stopping the cluster when quorum was lost.
	 * This, however, is provided for convenience - and may
	 * adversely affect data integrity in some cases.
	 *
	 * The short: USE POWER SWITCHES.
	 */
	
	if (clu_stonith_count(my_node_id) == 0 && !sigterm_received) {

		if (!clean_cluster_transition) {
			clulog(LOG_EMERG, "Quorum lost! "
			       "No STONITH devices - REBOOTING NOW");
			REBOOT(RB_AUTOBOOT);
			exit(1);
		}

		clean_cluster_transition = 0;
	}

	if (sigterm_received)
		clulog(LOG_EMERG, "Quorum lost during shutdown!");
	else
		clulog(LOG_EMERG, "Quorum lost!\n");

	send_quorum_update(ALL, EV_QUORUM_LOST, quorum_view,
			   quorum_status.qv_mask,
			   quorum_status.qv_panic_mask);
}



/**
 * Update the quorum status by retrieving the current state from VF subsys.
 * If we are the highest-online member ID in the cluster, then we are also
 * responsible for commencing STONITH operations to ensure data integrity.
 * See code-inline comments for more information.
 *
 * @see		vf_current, shoot_nodes
 */
void
update_quorum_status(void)
{
	char *data = NULL;
	uint32_t datalen = 0;
	uint64_t view;
	quorum_view_t old_quorum_status, new_status;

	memcpy(&old_quorum_status, &quorum_status, sizeof(quorum_view_t));

	vf_current(KEY_QUORUM, &view, (void **)&data, &datalen);

	if (datalen < sizeof(quorum_view_t)) {
		clulog(LOG_CRIT, "Wrong size! %d != %d\n", datalen,
		       sizeof(quorum_view_t));
		return;
	}

	/* Store a local copy */
	quorum_view = view;

	/* Note: Datalen used as COUNTER here... */
	/* Un-swab (we stores it in big-endian form in vf for platform
	   independence always */
	swab_quorum_view_t((quorum_view_t *)data, datalen);
	memcpy(&quorum_status, data, sizeof(quorum_status));
	free(data);

	if (old_quorum_status.qv_status && quorum_status.qv_status) {

		if (!memcmp(quorum_status.qv_mask, old_quorum_status.qv_mask,
			    sizeof(memb_mask_t)) &&
		    !memb_count(old_quorum_status.qv_stonith_mask) &&
		    !memb_count(old_quorum_status.qv_panic_mask)) {
			/*
			 * Don't send duplicate quorum events if the only
			 * thing that's changed is the view number.
			 * 
			 * We need to obviously send if we've shot the nodes
			 * we were waiting to shoot; aside from that,
			 * we're all good.
			 */
			clulog(LOG_DEBUG, "Quorum maintained; no change in "
			       "membership.  Not sending notification.\n");
		} else
			clulog(LOG_DEBUG, "Quorum Maintained\n");

		/*
		 * Power cycle down nodes here.
		 */
		if ((memb_high_id(quorum_status.qv_mask) == my_node_id) &&
		    memb_count(quorum_status.qv_stonith_mask) ) {
			clulog(LOG_WARNING, "--> Commencing STONITH <--\n");
			/*
			 * TODO Parallelize this - switches can be REALLY
			 * slow sometimes
			 */

			/*
			 * Use a local value so we're forced to wait for a
			 * full quorum commit before we proceed with the
			 * value we're sending.
			 */
			memcpy(&new_status, &quorum_status, sizeof(new_status));

			shoot_nodes(new_status.qv_stonith_mask,
				    new_status.qv_panic_mask);

			/*
			 * Great.  We hopefully power-cycled all of the nodes
			 * in our STONITH mask.  Time to send out the message.
			 * Nodes which did not power-cycle correctly are
			 * stored in the qv_panic_mask, and generally should
			 * be considered "down" - Membership thinks it's down,
			 * but it's unclean to take services over.
			 */
			clulog(LOG_DEBUG, "%s: Starting VF\n", __FUNCTION__);
			start_vf(new_status.qv_mask, &new_status);
		}

		/*
		 * Only send a node update message if no nodes need to be
		 * power-cycled.
		 */
		if (!memb_count(quorum_status.qv_stonith_mask)) {
			send_quorum_update(ALL, EV_QUORUM, view,
					   quorum_status.qv_mask,
					   quorum_status.qv_panic_mask);
		}
	} else if (!old_quorum_status.qv_status && !quorum_status.qv_status) {
		clulog(LOG_DEBUG, "Lack of Quorum Maintained\n");
		send_quorum_update(ALL, EV_NO_QUORUM, view,
				   quorum_status.qv_mask,
				   quorum_status.qv_panic_mask);
	} else if (!old_quorum_status.qv_status && quorum_status.qv_status) {

		/* Yay! */
		quorum_formed();
		
	} else if (old_quorum_status.qv_status && !quorum_status.qv_status) {

		/*
		 * We've lost our place in the cluster quorum, or lost 
		 * communication with the quorum.
		 */
		quorum_lost();
	}
}


/**
 * Handle a SIGTERM.
 */
static void
sig_exit_handler(int __attribute__ ((unused)) sig)
{
	/*clulog(LOG_DEBUG, "Processing exit signal\n");*/
	proceed = 0;
	sigterm_received = 1;
}


/**
 * Set up signal handling.
 */
static void
setup_sig_handlers(void)
{
	sigset_t set;
	struct sigaction act;

	sigemptyset(&set);
	sigaddset(&set, SIGHUP);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGINT);
	/*
	 * Make sure the HUP signal is not blocked.
	 */
	sigprocmask(SIG_UNBLOCK, &set, NULL);

	memset(&act, 0, sizeof(act));
	act.sa_handler = sighup_handler;
	sigemptyset(&act.sa_mask);
	act.sa_flags = SA_RESTART;
	sigaction(SIGHUP, &act, NULL);

	/*
	 * Handle exits
	 */
	memset(&act, 0, sizeof(act));
	act.sa_handler = sig_exit_handler;
	sigemptyset(&act.sa_mask);
	act.sa_flags = SA_RESTART;
	sigaction(SIGINT, &act, NULL);
	sigaction(SIGTERM, &act, NULL);
}


/**
 * If we're using shared raw devices, ensure access during startup.
 *
 * @return 0 on failure, 1 on success or !2-node-cluster or simple_operation
 */
void
validate_shared_state(void)
{
	char *ret;
	SharedStateHeader ssh;

	if (CFG_Get(SIMPLE_OPERATION, NULL, &ret) == CFG_OK) {
		if ((ret[0] == 'y') || (ret[0] == 'Y') || (atoi(ret) != 0)) {
			simple_operation = 1;
			/* Simple cluster.  No shared state. */
			return;
		}
	}

	if (CFG_Get(SHAREDSTATE_DRIVER, NULL, &ret) != CFG_OK) {
		clulog(LOG_EMERG, "Not Starting Cluster Manager: "
		       "No shared state configured!");
		exit(1);
	}

	/*
	 * init shared storage
	 */
	if (shared_storage_init() != 0) {
		clulog(LOG_EMERG, "Not Starting Cluster Manager: "
		       "Shared State Error: %s\n", strerror(errno));
		exit(1);
	}
	
	/*
	 * Get the driver version
	 */
	ret = sh_version();
	clulog(LOG_DEBUG, "%s: %s in use\n", __FUNCTION__, ret);
	
	/*
	 * Try a read
	 */
	if (sh_read_atomic("/cluster/header", (void *)&ssh,
			   sizeof(ssh)) == -1) {
		clulog(LOG_EMERG, "Not Starting Cluster Manager: "
		       "Shared State Header Unreadable: %s (run: shutil -i)",
		       strerror(errno));
		exit(1);
	}

	clulog(LOG_INFO, "Shared State Valid: %s\n", ret);
		
	/*
	 * Success!
	 * Clean up and return.
	 */
	shared_storage_deinit();
}


/**
 * Initialize the local things the quorum daemon needs.
 *
 * @see		clist_init
 * @see		vf_init
 * @see		vf_key_init
 * @return	0
 */
int
quorumd_init(void)
{
	set_facility();

	memset(&quorum_status,0,sizeof(quorum_status));
	/* Paramount importance: Ensure shared state works */
	validate_shared_state();

	quorumd_read_params();
	my_node_id = getLocalNodeIDCached();
	if (my_node_id == -1) {
		clulog(LOG_CRIT, "Not starting Cluster Manager: Local node ID "
		       "unavailable\n");
		clulog(LOG_CRIT, "Ensure DNS and /etc/hosts match a "
		       "configured IPv4 address\n");
		exit(1);
	}

	clist_init();

	vf_init(my_node_id, PROCID_CLUQUORUMD);
	vf_key_init(KEY_QUORUM, 10, NULL, NULL);

	clu_stonith_init();

	if (!gulm_alive()) {
		clulog(LOG_ERR, "Not starting Cluster Manager: lock_gulmd is"
		       " missing.\n");
		exit(1);
	}

	return 0;
}


/**
 * Find the total number of nodes configured in our cluster database.
 *
 * @return		Number of nodes.
 * @see			CFG_Get
 */
int
total_nodes(int force)
{
	char stuff[80];
	static int count = 0;
	int x;
	char *val;

	if (!force && count)
		return count;

	count = 0;
	for (x=0; x < MAX_NODES; x++) {
		snprintf(stuff,sizeof(stuff),"members%%member%d%%name", x);
		if (CFG_Get(stuff, NULL, &val) == CFG_OK)
			count++;
	}

	return count;
}


/**
 * Break the tie...
 */
int
tie_breaker(int nodes_online)
{
	/* ... */
	if ((nodes_online == 1) && (!tb_ip))
		return disk_tiebreaker();

	if (!allow_soft_quorum && !quorum_status.qv_status)
		return 0;

	return net_tiebreaker();
}


/**
 * Determine a simple majority (n/2 + 1).  In the case of two or four nodes,
 * we allow for a backup tie-breaker.  Currently, this is limited to shared
 * storage reading/writing.
 *
 * @param mask		Membership mask.
 * @param force		Force recount of members based on configuration
 * @return		1 if majority or 0 if not.
 */
char
quorum_simple_majority(memb_mask_t mask, int force)
{
	int nodes_in_cluster, nodes_for_quorum, nodes_online;
	char have_quorum = 0;

	/* Simple majority */
	nodes_in_cluster = total_nodes(force);
	nodes_for_quorum = (nodes_in_cluster / 2) + 1;
	nodes_online = memb_count(mask);

	if ((nodes_in_cluster == 4 && nodes_online == 2) ||
	    (nodes_in_cluster == 2 && nodes_online == 1)) {
		nodes_online += tie_breaker(nodes_online);
	}

	if (nodes_online < nodes_for_quorum) {

		clulog(LOG_DEBUG, "Need %d more members for quorum!\n",
      		       nodes_for_quorum - nodes_online);

	} else {
		have_quorum = 1;
	}

	return have_quorum;
}


/**
 * Actually start VF
 */
int
start_vf(memb_mask_t curr_mask, quorum_view_t *qv)
{
	quorum_view_t qv_cpy;
	int retries;

	/* Note: retries is used as a counter here */
	memcpy(&qv_cpy, qv, sizeof(qv_cpy));
	swab_quorum_view_t(&qv_cpy, retries);

	retries = 0;

	while(vf_start(curr_mask, VFF_RETRY, KEY_QUORUM, &qv_cpy,
			sizeof(qv_cpy)) == -1) {
		if (errno == EAGAIN) {
			/* See if it's exited */
			vf_end(KEY_QUORUM);
			retries++;
			usleep(200000);
		}

		if (retries < 50)
			continue;

		clulog(LOG_ERR, "vf_start: %s\n", strerror(errno));
		return -1;
	}

	return 0;
}


/**
 * Handle a membership event.  Initiates the View-Formation algorithm.
 *
 * @param event		Membership event.
 * @return		0
 */
int
process_memb_update(cm_event_t *event)
{
	quorum_view_t qv;
	int x;
	int unclean_node_deaths = 0;
	//char *nodename = NULL;
	memb_mask_t gained;

	/*
	 * We calculate STONITH + nodes_exiting mask on all nodes...
	 * This can be optimized.  We really do need the STONITH mask and the
	 * nodes_exiting mask updated though -- all nodes should have a 
	 * consistent view of nodes_exiting.
	 */

	/*
	 * Determine the mask of nodes we lost.  We need this to converge
	 * on whether or not we need to SHOOT anyone ;)
	 */
	memb_mask_lost(qv.qv_stonith_mask, quorum_status.qv_mask, 
			cm_memb_mask(event));
			
	/*
	 * Determine the mask of nodes we gained.  We need this so we
	 * can clear the "exiting" bit from a member.
	 */
	memb_mask_gained(gained, quorum_status.qv_mask, cm_memb_mask(event));

	/*
	 * Figure out how many nodes we expect to shoot.  Any which we were
	 * expecting to exit shouldn't constitute a problem; so we should
	 * allow them to leave.
	 */
	unclean_node_deaths = memb_count(qv.qv_stonith_mask);
	clean_cluster_transition = 0;
	
	/*
	 * Those nodes we are expecting to exit should NOT be shot.
	 */
	for (x=0; x<MAX_NODES; x++) {
		if (memb_online(nodes_exiting, x) &&
		    memb_online(qv.qv_stonith_mask, x)) {
			memb_mark_down(qv.qv_stonith_mask, x);
			unclean_node_deaths--;
		}
		
		if (memb_online(gained, x))
			memb_mark_down(nodes_exiting, x);
	}

	if (unclean_node_deaths == 0)
		clean_cluster_transition = 1;

	/*
	 * Coordinator:  Current high node OR previous high node.  No need
	 * for MAX_NODES attempting at quorum convergence.
	 */
	if ((memb_high_id(cm_memb_mask(event)) != my_node_id) &&
 	    (memb_high_id(quorum_status.qv_mask) != my_node_id))
		return 0;

	qv.qv_status = quorum_simple_majority(cm_memb_mask(event), 1);

	/*
	 * What happens if a panicked node comes back to life
	 * after we fail to STONITH it and subsequently declare it
	 * as 'panicked'?
	 *
	 * Here are the cases:
	 *
	 * (1) The node was just hung, and, if no STONITH drivers
	 * were configured, it reboots itself.
	 * (2) The node was just hung, and it had STONITH drivers,
	 * but we failed to reboot it.  It becomes un-hung and 
	 * services have stayed running.
	 * (3) The power switches are unplugged or unavailable,
	 * and the node was rebooted.  The node stops all services
	 * cleanly, sees that it owned services before the transition
	 * and restarts them.
	 *
	 * In clumanager-1.0.16+, there was a provision for a 
	 * power switch timeout - that is, we continually send
	 * power cycle messages to the power switch controlling a
	 * node every 10 seconds.  If the timer reaches a certain
	 * timeout, the node is ASSUMED down.  This was originally
	 * a feature request from a customer in regards to a type of
	 * power switch (which is prone to odd behavior).
	 *
	 * Hence, we make no special consideration for power
	 * switch failures, and any node which was marked online 
	 * by membership is, at this point, no longer in the
	 * 'panicked' state.
	 */
	/*
	 * Copy in our current view of the 'panic' mask..
	 */
	memcpy(qv.qv_panic_mask, quorum_status.qv_panic_mask,
	       sizeof(memb_mask_t));

	/*
	 * Clear any bits in our panic mask which are now, according to 
	 * membership, online again
	 */
	for (x=0; x < MAX_NODES; x++) {
		if (memb_online(cm_memb_mask(event), x) &&
		    memb_online(quorum_status.qv_panic_mask, x))
			memb_mark_down(qv.qv_panic_mask, x);
	}

	/* 
	 * Copy in our membership mask
	 */
	memcpy(qv.qv_mask, cm_memb_mask(event), sizeof(memb_mask_t));
	
	/*
	 * Check disk information: a node down according to membership
	 * but up according to the disk thread is considered 'panicked'.
	 */
	if (disk_in_use() && disk_other_status() && qv.qv_status)
		if (!memb_online(qv.qv_mask, disk_other_node())) {
			if (allow_disk_quorum) {
				memb_mark_down(qv.qv_stonith_mask,
					       disk_other_node());
			} 

			clulog(LOG_WARNING, "Membership reports #%d as"
			       " down, but disk reports as up: "
			       "State uncertain!\n",
			       disk_other_node());

			memb_mark_up(qv.qv_panic_mask, disk_other_node());
		}

	clulog(LOG_DEBUG, "%s: Starting VF\n", __FUNCTION__);
	return start_vf(cm_memb_mask(event), &qv);
}


/**
 * Force a quorum update after changing to allow_soft_quorum
 */
int
force_update(void)
{
	cm_event_t event;

	memset(&event, 0, sizeof(event));
	memcpy(cm_memb_mask(&event), quorum_status.qv_mask, sizeof(memb_mask_t));
	
	return process_memb_update(&event);
}


/**
 * Send tiebreaker information to clustat
 */
void
send_tb_info(int fd)
{
	int event = 0;

        if (!tb_ip && disk_in_use()) {
		event |= EV_TBF_DISK;

		if (disk_tiebreaker())
			event |= EV_TBF_ONLINE;

		if (disk_other_node())
			event |= EV_TBF_DISK_OTHER;

	} else if (tb_ip) {
		event |= EV_TBF_NET;

		if (net_tiebreaker())
			event |= EV_TBF_ONLINE;

		if (allow_soft_quorum)
			event |= EV_TBF_NET_SOFT;
	} else {
		event |= EV_TBF_NONE;
	}

	msg_send_simple(fd, EC_QUORUM_TB, event, my_node_id);
}


/**
 * Handle a message from the power switch child program.  We log a message
 * based on the reported status.
 *
 * @param msg		Message to handle
 */
void
handle_pswitch_msg(generic_msg_hdr *msg)
{
	char token[80];
	char *master = NULL, *controlled = NULL;

	if (msg->gh_arg1 >= MAX_NODES)
		return;
	if (msg->gh_arg2 >= MAX_NODES)
		return;

	if (msg->gh_arg1 == msg->gh_arg2) {
		clulog(LOG_DEBUG, "Oddness...\n");
		return;
	}

	/*
	 * arg1 -> member who checked the switch(es)
	 */
	snprintf(token, sizeof(token), "%s%c%s%d%c%s",
		 NODE_LIST_STR, CLU_CONFIG_SEPARATOR,
		 NODE_STR, msg->gh_arg1, CLU_CONFIG_SEPARATOR,
		 NODE_NAME_STR);
	CFG_Get(token, NULL, &master);

	/*
	 * arg2 -> member controlled by the switch(es)
	 */
	snprintf(token, sizeof(token), "%s%c%s%d%c%s",
		 NODE_LIST_STR, CLU_CONFIG_SEPARATOR,
		 NODE_STR, msg->gh_arg2, CLU_CONFIG_SEPARATOR,
		 NODE_NAME_STR);
	CFG_Get(token, NULL, &controlled);

	pswitch_states[msg->gh_arg2].ss_checker = msg->gh_arg1;
	pswitch_states[msg->gh_arg2].ss_timestamp = (uint64_t)time(NULL);
	
	/*
	 * Retry in two minutes
	 */
	if (msg->gh_arg1 == my_node_id)
		pswitch_check = time(NULL) + 120;

	if (msg->gh_command == QUORUM_PSWITCH_BAD) {
		pswitch_states[msg->gh_arg2].ss_errors++;
			
		if (msg->gh_arg1 != my_node_id)
			clulog(LOG_CRIT, "Error returned from STONITH "
			       "device(s) controlling %s. See system "
			       "logs on %s for more information.\n",
			       controlled, master);
		else {
			clulog(LOG_CRIT, "Error returned from STONITH "
			       "device(s) controlling %s.\n",
			       controlled);
		}
		pswitch_states[msg->gh_arg2].ss_status = PSWITCH_BAD;
	} else {
		clulog(LOG_DEBUG, "Member %s reports all STONITH device(s)"
		       " controlling member %s are in a good state!\n",
		       master, controlled);
		pswitch_states[msg->gh_arg2].ss_status = PSWITCH_GOOD;
		pswitch_states[msg->gh_arg2].ss_errors = 0;
	}
}


/**
 * Attempt to process a request on an active file descriptor.  For requests
 * the quorum daemon handles internally, the file descriptor is closed.
 *
 * @param fd		The file descriptor which is active.
 * @return		-1 on failure, 0 on success.
 */
int
process_quorum_req(int fd)
{
	generic_msg_hdr *hdrp;
	cm_event_t *evp;
	int nbytes, c;

	/* got it - wait for one second */
	/* msg_receive_simple allocates the second buffer!! */
	if ((nbytes = msg_receive_simple(fd, &hdrp, 1)) < 0) {
		if (fd == membfd) {
#ifdef DEBUG
			clulog(LOG_ALERT, "Lost connection to membership: "
			       "EXITING\n");
			exit(1);
#else
			clulog(LOG_EMERG, "Lost connection to membership: "
			       "REBOOTING NOW\n");
			sync(); sync(); sync();
			REBOOT(RB_AUTOBOOT);
#endif
		}
		clist_delete(fd);
		msg_close(fd);
		return -1;
	}

	if (nbytes < sizeof(generic_msg_hdr)) {
		free(hdrp);
		clist_delete(fd);
		msg_close(fd);
		return -1;
	}

	/*
	 * Special case.  Membership events only allowed on membfd.
	 */
	if (fd == membfd) {
		evp = (cm_event_t *)hdrp;

		swab_cm_event_hdr_t(&evp->em_header);
		swab_cm_memb_event_t(&((cm_event_t *)hdrp)->u.ev_memb, c);

		if (evp->em_header.eh_magic != GENERIC_HDR_MAGIC) {
			clulog(LOG_ERR, "Invalid magic!\n");
			goto out;
		}

		if (evp->em_header.eh_length != sizeof(cm_event_t)) {
			clulog(LOG_ERR,
			       "Invalid size: %d != %d\n", 
			       evp->em_header.eh_length, sizeof(cm_event_t));
			goto out;
		}

		clulog(LOG_DEBUG, "Q: Received EV_MEMB_UPDATE, fd%d\n", fd);
		process_memb_update(evp);

		goto out;
	}

	swab_generic_msg_hdr(hdrp);
	switch (hdrp->gh_command) {
	case VF_MESSAGE:
		clulog(LOG_DEBUG, "Q: Received VF_MESSAGE, fd%d\n", fd);
		
		switch(vf_process_msg(fd, hdrp, nbytes)) {
		case VFR_COMMIT:
			/* ... can use callback function here */
			clist_delete(fd);
			/*
			 * FIXME if vf_process_msg runs down into
			 * vf_commit_views, then this file descriptor MIGHT
			 * have already been closed...  Though it's possible
			 * that others were closed instead.
			 */
			msg_close(fd);
			update_quorum_status();
			break;
		case VFR_NO:
			clist_delete(fd);
			msg_close(fd);
			break;
		}
		break;

	case QUORUM_EXIT: /* From service manager */
		clulog(LOG_DEBUG, "Q: Received QUORUM_EXIT, fd%d\n", fd);
		clist_delete(fd);

		/* Loss of quorum or unclean exit from svcmgr. */
		if (hdrp->gh_arg1 != 0) {
			clulog(LOG_WARNING, "Service Manager has exited "
			       "uncleanly - Not shutting down\n");
			msg_close(fd);
			break;
		}
		msg_close(fd);

		/*
 		 * Tell everyone we're exiting cleanly
		 * EXIT
		 */
		notify_exiting();
		quorum_exit(0);
		break;

	case QUORUM_CLEAN_EXIT: /* From quorum daemon */
		clulog(LOG_DEBUG, "Q: Received QUORUM_CLEAN_EXIT, fd%d\n",fd);
		if ((hdrp->gh_arg1 < 0) || (hdrp->gh_arg1 > (MAX_NODES-1))) {
			clulog(LOG_ERR, "Q: CLEAN_EXIT argument invalid\n");
			msg_close(fd);
			break;
		}
		memb_mark_up(nodes_exiting, hdrp->gh_arg1);
		msg_close(fd);
		break;

	case QUORUM_PSWITCH_BAD:
	case QUORUM_PSWITCH_GOOD:
		clulog(LOG_DEBUG, "Q: Received QUORUM_PSWITCH_X, fd%d\n", fd);
		handle_pswitch_msg(hdrp);
		msg_close(fd);
		break;

	case QUORUM_QUERY:
		clulog(LOG_DEBUG, "Q: Received QUORUM_QUERY, fd%d\n", fd);
		send_quorum_update(fd, quorum_status.qv_status ? 
				   EV_QUORUM : EV_NO_QUORUM,
				   quorum_view, quorum_status.qv_mask,
				   quorum_status.qv_panic_mask);
		msg_close(fd);
		break;

	case QUORUM_QUERY_TB:
		clulog(LOG_DEBUG, "Q: Received QUORUM_QUERY_TB, fd%d\n", fd);
		send_tb_info(fd);
		msg_close(fd);
		break;

	case QUORUM_FORCE:
		clulog(LOG_WARNING, "Allowing soft quorum.\n");
		allow_soft_quorum = 1;
		if (!quorum_status.qv_status)
			force_update();
		break;

	case EV_REGISTER:
		clulog(LOG_DEBUG, "Q: Received EV_REGISTER, fd%d\n", fd);
		msg_send_simple(fd, EV_ACK, 0, 0);
		send_quorum_update(fd, quorum_status.qv_status ? 
				   EV_QUORUM : EV_NO_QUORUM,
				   quorum_view, quorum_status.qv_mask,
				   quorum_status.qv_panic_mask);

		break;

	default:
		clulog(LOG_ERR, "Q: Unknown message: %x\n", hdrp->gh_command);
		msg_close(fd);
	}

out:
	free(hdrp);
	return 0;
}


/**
 * Check for GuLM viability
 */
int
gulm_alive(void)
{
	/* See if we're supposed to ignore GuLM... */
	if (ignore_gulm_absence)
		return 1;

	/* See if GuLM controls us */
	if (clu_stonith_gulm(my_node_id) != 0)
		return 1;

	/* See if GuLM is running... */
	if (findproc("lock_gulmd", NULL, 0) != 0)
		return 1;

	/* uh oh */
	return 0;
}


/**
 * Do a cycle of that quorum daemon stuff.
 *
 * @return 		0
 */
int
quorumd_body(void)
{
	struct timeval tv;
	int nready, newfd, fd;
	fd_set rfds;

	FD_ZERO(&rfds);
	FD_SET(membfd, &rfds);
	FD_SET(listenfd, &rfds);

	clist_fill_fdset(&rfds);

	tv.tv_sec = 5;
	tv.tv_usec = 0;

	/* Clean up VF child */
	vf_end(KEY_QUORUM);

	/*
	 * Re-check the IP tie-breaker every few seconds.
	 * This is to handle the case where the IP tiebreaker goes down.
	 *
	 * This is a double-fault; it only can happen when the cluster is
	 * teetering on a majority and the switch fails.  After this, we
	 * need administrator intervention - the cluster will not recover
	 * until it can reach a majority of *real* members.
	 */
	if (tb_ip) {
		if (quorum_status.qv_status &&
		    !quorum_simple_majority(quorum_status.qv_mask, 0)) {
			/* Not good... */
			if (!vf_running(KEY_QUORUM))
				force_update();
		}

		/* Except when allow_soft_quorum, which is bad. */
		if (allow_soft_quorum && !quorum_status.qv_status &&
		    quorum_simple_majority(quorum_status.qv_mask, 0)) {
			if (!vf_running(KEY_QUORUM))
				force_update();
		}
	}
	
	if (!gulm_alive()) {
		clulog(LOG_NOTICE, "lock_gulmd not present; shutting down\n");
		raise(SIGTERM);
	}

	/* Check for service manager viability */
	if ((quorum_status.qv_status == 1) && 
	    !debug &&
	    !simple_operation &&
	    !sigterm_received &&
	    (findproc("clusvcmgrd", NULL, 0) == 0)) {
	    	/* Service manager died. */
	    	clulog(LOG_CRIT, "Service manager missing; restarting!\n");
	    	if (spawn_daemon("/usr/sbin/clusvcmgrd") != 0) {
	    		clulog(LOG_ALERT,
	    		       "Service manager restart failed!\n");
		}
	}

    	
	/* Check for lock manager viability */
	if (!sigterm_received && !simple_operation && !debug &&
	    (findproc("clulockd", NULL, 0) == 0)) {
	    	/* Lock manager died. */
	    	clulog(LOG_CRIT, "Lock manager missing; restarting!\n");
	    	if (spawn_daemon("/usr/sbin/clulockd") != 0) {
	    		clulog(LOG_ALERT,
	    		       "Lock manager restart failed!\n");
	    		if (quorum_status.qv_status)
		    		sync();
	    	}
	}

	/*
	 * Process network events.
	 */
	nready = select(1024, &rfds, NULL, NULL, &tv);
	if (nready <= 0) {
		if (!vf_running(KEY_QUORUM))
			disk_check_states(&quorum_status, nodes_exiting);
		return 0;
	}

	if (FD_ISSET(listenfd, &rfds)) {
		FD_CLR(listenfd, &rfds);

		newfd = msg_accept(listenfd);
		nready--;
		if (newfd < 0)
			newfd = -1;
		else
                        clist_insert(newfd, 0);
	}

	if (nready && FD_ISSET(membfd, &rfds)) {
		nready--;
		FD_CLR(membfd, &rfds);
		process_quorum_req(membfd);
	}

	while (nready && ((fd = clist_next_set(&rfds)) != -1)) {
		nready--;
		FD_CLR(fd, &rfds);
		process_quorum_req(fd);
	}

	/*
	 * Check and handle disk state changes
	 */
	if (!vf_running(KEY_QUORUM))
		disk_check_states(&quorum_status, nodes_exiting);
	
	return 0;
}


/**
 * Main loop of quorum daemon.
 *
 * @return		Well, nothing - it doesn't return.
 */
int
quorumd_loop(void)
{
	pid_t pswitch_child = 0;
	
	pswitch_check = time(NULL) + 10;

	while (1) {
                if (pswitch_child &&
		    (waitpid(pswitch_child, NULL, WNOHANG) ==
		     pswitch_child))
			pswitch_child = 0;
		
		if (!proceed) {

			/*
			 * If nobody is viewed as online, exit immediately
			 */
			if (!memb_count(quorum_status.qv_mask)) {
				notify_svcmgr();
				quorum_exit(0);
			}

			/* Note: only do this once per signal */
			proceed = 1;

			/*
			 * If we're not in simple operation and no service
			 * managers are alive, exit the quick way.
			 */
			if (simple_operation || (notify_svcmgr() == 0)) {
				notify_exiting();
				quorum_exit(0);
			}
		}

		if (sighup_received)
			quorumd_reconfig();

		if (!pswitch_child && pswitch_check < time(NULL))
			check_power_switch(&pswitch_child);
		
		quorumd_body();
	}

	return 0;
}


/**
 * Main setup/etc. of the quorum daemon.
 *
 * @see		CFG_Read
 * @see		CFG_ReadFile
 * @see		cm_ev_register
 *
 * return	Something.
 */
int
main(int argc, char **argv)
{
	extern char *optarg;
	int retries = 0, opt, foreground = 0;
	pthread_t dthread;

	/*
	 * First of all, parse the command line.
	 */
	while ((opt = getopt(argc, argv, "fds")) != EOF) {
		switch (opt) {
		case 'd':
			debug = 1;
			break;
		case 'f':
			foreground = 1;
			break;
		case 's':
			simple_operation = 1;
			break;
		default:
			break;
		}
	}

	if (foreground)
		clu_log_console(1);

	/* Load local copy of configuration */
	CFG_ReadFile(CLU_CONFIG_FILE);
	quorumd_init();

	if (!foreground)
		daemon_init(argv[0]);

	setup_sig_handlers();

	if (debug)
		(void) clu_set_loglevel(LOG_DEBUG);

	/*
	 * Spawn the membership daemon now.  We want the membership daemon
	 * to converge *before* we determine disk states.  This way, we
	 * prevent a condition where both members can become lock masters for
	 * a short period of time.
	 */
	if (spawn_daemon("/usr/sbin/clumembd") == -1) {
		clulog_and_print(LOG_CRIT,
				 "Fatal: Couldn't spawn clumembd!\n");
		return -1;
	}

	/*
	 * If we're not in simple mode and we're a two member cluster
	 * with no tie-breaker IP address specified, then we use
	 * disk-based tie-breakers.  Here, we start the thread which
	 * is used to determine disk states transparently of our
	 * main program.  This is designed to be modular so that it
	 * maybe removed esaily.  We need to do this prior to anything
	 * major so that we don't sit around while pending VF_MESSAGEs
	 * expire, which could cause a split-brain scenario.
	 *
	 * You can't use disk-tie-breaker with simple mode because simple
	 * mode implies *no* shared storage requirements.
	 */
	if (!simple_operation) {
		if (tb_ip) {
			clulog(LOG_DEBUG, "IP tie-breaker in use, not "
			       "starting disk thread.");
			net_create_quorum_thread(NULL);
		} else {
			switch(disk_tiebreaker_init(my_node_id)) {
			case -1:
				clulog(LOG_ERR, "Disk tie-breaker "
				       "unavailable: Invalid membership "
				       "timings / ping interval\n");
				break;
			case 0:
				clulog(LOG_DEBUG,
				       "Starting disk status thread\n");
				if (disk_create_quorum_thread(&dthread) ==
				    -1) {
					clulog(LOG_ERR, "pthread_create: %s\n",
					       strerror(errno));
				}
				break;
			case 1:
				clulog(LOG_DEBUG,
				       "Disk tie breaker unavailable.");
				break;
			} /* switch */
		} /* net-tie-breaker if...*/
	} /* simple_operation */

	/*
	 * Open our socket.
	 */
	listenfd = msg_listen(PROCID_CLUQUORUMD);
	if (listenfd == -1) {
		clulog_and_print(LOG_CRIT,
				 "Unable to establish listen channel!\n");
		return 1;
	}

	/*
	 * Subscribe to membership events.
	 */
	while ((membfd = cm_ev_register(EC_MEMBERSHIP)) == -1) {
		sleep(1);
		retries++;
		if (retries == MAX_MEMB_RETRIES) {
			clulog_and_print(LOG_CRIT, "Connection to membership "
					 "daemon failed!\n");
			quorum_exit(-1);
		}
	}

	/*
	 * If we're in Simple Operation mode, we're ready to roll.  Otherwise,
	 * we need to make sure that the lock daemon comes online prior to
	 * spawning the service manager - the way to to this is to ensure
	 * that we start it here instead of when we receive a membership or
	 * quorum event.
	 */
	if (simple_operation)
		clulog_and_print(LOG_NOTICE, "Operating in simple mode\n");
	else {
		/* Membership is up.  Spawn lock daemon if not in simple mode. */
		if (spawn_daemon("/usr/sbin/clulockd") == -1) {
			clulog_and_print(LOG_CRIT, "Could not start lock "
					 "manager - Exiting.\n");
			quorum_exit(-1);
		}
		
	}

	return quorumd_loop();
}
