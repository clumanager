/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Quorum Daemon Disk-Tiebreaker Thread + Functions.  Easy to remove
 * from the quorum daemon.
 */
 
#include <sharedstate.h>
#include <clushared.h>
#include <pthread.h>
#include <xmlwrap.h>
#include <clulog.h>
#include <sys/reboot.h>
#include <sharedstate.h>
#include <stonith.h>
#include <stonithapis.h>
#include <disk_tie.h>
#include <cm_api.h>

#define SHAREDSTATE_DRIVER	"sharedstate%driver"

#ifdef TESTING
#ifdef reboot
#undef reboot
#endif
#define reboot(arg) {\
	clulog(LOG_EMERG, "reboot(%s) @ %s:%d\n", #arg, __FILE__, __LINE__); \
	raise(SIGSTOP); \
}
#endif

void disk_error_action(void);

static int status_block_id, interval = -1, tko_count = -1;
static int my_status = -1, other_status = -1, need_shoot = 0,
           other_gained = 0, other_lost = 0;
static int other_node_id = -1, my_node_id = -1, _disk_in_use = 0,
	   disk_thread_running = 0;
static pthread_mutex_t status_mutex = PTHREAD_MUTEX_INITIALIZER;

int write_status_block(int, int);
int read_status_block(int, PartStatusBlock *);
int disk_status_thingy(uint64_t *last_read_time, int *skips);

/**
 * Get the membership daemon's interval and tko_count as well as the quorum
 * daemon's ping interval.  From those pieces of information, we calculate
 * the disk ping interval and the disk tko count.
 *
 * @return		-1 on invalid configuration, 0 on success.
 */
int
get_interval_tko(void)
{
	int _interval, _tko, _pingint;
	int fo_time;
	char *val;
	
	if (CFG_Get("clumembd%interval", NULL, &val) == CFG_OK)
		_interval = atoi(val);
	else
		return -1;

	if (CFG_Get("clumembd%tko_count", NULL, &val) == CFG_OK)
		_tko = atoi(val);
	else
		return -1;

	if (CFG_Get("cluquorumd%pingInterval", NULL, &val) == CFG_OK)
		_pingint = atoi(val);
	else
		_pingint = 2;

	/* Calculate failover time, in seconds */
	fo_time = (_interval * _tko) / 1000000;

	if (fo_time < 2) {
		clulog(LOG_ERR, "Disk-TB: Failover time too fast for "
		       "disk-based tiebreaker.\n");
		return -1;
	}

	tko_count = fo_time / _pingint;
	interval = _pingint;

	/* Ensure we don't exceed membership f/o speed */
	if ((tko_count * _pingint) >= fo_time)
		tko_count--;

	if (tko_count < 3) {
		clulog(LOG_ERR, "Disk-TB: Disk Interval too large for "
		       "specified failover time.\n");
		return -1;
	}

	clulog(LOG_INFO, "Disk-TB: Interval %d, TKO %d (%d sec)\n",
	       interval, tko_count, interval * tko_count);

	return 0;
}


/**
 * Initialize the disk tiebreaker/quorum subsystem.
 * This must be called at least once prior to disk_create_quorum_thread().
 *
 * @param my_id		Local member ID
 * @return		-1 on configuration problem, 1 on 'no shared disk', 0
 *			on success.
 */
int
disk_tiebreaker_init(int my_id)
{
//	uint64_t partner_read_time = 0;
//	int skips = 0;
	int x, nnodes = 0, other_id = -1;
	char buf[256], *driver;
	PartStatusBlock status_block;

	pthread_mutex_lock(&status_mutex);

	_disk_in_use = 0;
	my_node_id = my_id;
	
	if (CFG_Get(SHAREDSTATE_DRIVER, NULL, &driver) != CFG_OK) {
		pthread_mutex_unlock(&status_mutex);
		return -1;
	}

	if (!strstr(driver, "sharedraw")) {
		pthread_mutex_unlock(&status_mutex);
		return 1;
	}

	if (shared_storage_init() == -1) {
		pthread_mutex_unlock(&status_mutex);
		return -1;
	}
			
	if (get_interval_tko() == -1) {
		pthread_mutex_unlock(&status_mutex);
		return -1;
	}

	status_block_id = -1;

	for (x=0; x<MAX_NODES; x++) {
		snprintf(buf, sizeof(buf), "members%%member%d%%name", x);
		if (CFG_Get(buf, NULL, NULL) == CFG_OK) {
			nnodes++;
			if (x != my_node_id)
				other_id = x;
		}
	}

	if ((other_id == -1) || (nnodes > 2)) {
		pthread_mutex_unlock(&status_mutex);
		return 1;
	}

	status_block_id = (my_node_id > other_id);

	other_node_id = other_id;
	_disk_in_use = 1;

	if (write_status_block(status_block_id, NODE_UP) == -1) {
		clulog(LOG_CRIT, "Couldn't write status block!\n");
		pthread_mutex_unlock(&status_mutex);
		return -1;
	}
	
 	if (read_status_block(!status_block_id, &status_block) == -1) {
		clulog(LOG_CRIT, "Couldn't read other node's status block!\n");
		pthread_mutex_unlock(&status_mutex);
		return -1;
	}

	if (my_status != -1 && other_status != -1) {
		pthread_mutex_unlock(&status_mutex);
		return 0;
	}

	pthread_mutex_unlock(&status_mutex);

	/*
	 * Determine initial members' disk state.
	 */
#if 0
	clulog(LOG_INFO, "Disk-TB: Determining states...\n");
	x = 0;
	while (++x < tko_count) {
		disk_status_thingy(&partner_read_time, &skips);
		sleep(interval);
	}
#endif

	/*
	 * Return OUR disk status.
	 */
	pthread_mutex_lock(&status_mutex);
	clulog(LOG_INFO, "Disk-TB: My status: %s  Partner status: %s\n",
	       my_status    ? "UP" : "DOWN",
	       other_status ? "UP" : "DOWN");
	x = !my_status; /* 1 = my state = down */
	pthread_mutex_unlock(&status_mutex);
 
	return x;
}


/**
 * Returns the disk state of the other member of the cluster.
 *
 * @return		State of other member, or -1 if unavailable.
 * @see disk_my_status
 */
int
disk_other_status(void)
{
	int rv;

	pthread_mutex_lock(&status_mutex);
	if (_disk_in_use || disk_thread_running)
		rv = other_status;
	else
		rv = -1;
	pthread_mutex_unlock(&status_mutex);
	return rv;
}


/**
 * Retrieves the other node ID from the disk thread.
 *
 * @return		Other node's ID.
 */
int
disk_other_node(void)
{
	int rv;

	pthread_mutex_lock(&status_mutex);
	rv = other_node_id;
	pthread_mutex_unlock(&status_mutex);
	return rv;
}


/**
 * @return		1 if disk is in use, 0 if not.
 */
int
disk_in_use(void)
{
	int rv;

	pthread_mutex_lock(&status_mutex);
	rv = (_disk_in_use || disk_thread_running);
	pthread_mutex_unlock(&status_mutex);
	return rv;
}


/**
 * Returns the disk state of the other member of the cluster.
 *
 * @return		State of other member, or -1 if unavailable.
 * @see disk_other_status
 */
int
disk_my_status(void)
{
	int rv;

	pthread_mutex_lock(&status_mutex);
	if (_disk_in_use || disk_thread_running)
		rv = my_status;
	else
		rv = -1;
	pthread_mutex_unlock(&status_mutex);
	return rv;
}


/*
typedef struct __attribute__ ((packed)) {
	uint32_t	ps_magic;
	char		ps_nodename[MAXHOSTNAMELEN];
	uint64_t	ps_timestamp;		// time of last update
	uint32_t	ps_updateNodeNum;	// ID of node doing last update
	uint64_t	ps_incarnationNumber;	// cluster startup time
	uint32_t	ps_state;		// running or stopped
	uint32_t	ps_substate;		// more details on state,
						// eg panic-ing
	uint64_t	ps_configTimestamp;	// date on config database
} PartStatusBlock;
*/

/**
 * Writes a status block to shared storage.
 *
 * @param blockid	Status block id (0 or 1)
 * @param state		State to record.
 * @return		Same as sh_write_atomic().
 * @see sh_write_atomic
 */
int
write_status_block(int blockid, int state)
{
	char buf[256];
	PartStatusBlock status;

	memset(&status, 0, sizeof(status));
	status.ps_state = state;
	status.ps_magic = STATUS_BLOCK_MAGIC_NUMBER;
	status.ps_timestamp = (uint64_t)time(NULL);
	status.ps_updateNodeNum = my_node_id;

	swab_PartStatusBlock(&status);

	snprintf(buf, sizeof(buf), "/partition/%d/status", blockid);
	return sh_write_atomic(buf, &status, sizeof(status));
}


/**
 * Reads a status block from shared storage.
 *
 * @param blockid	Status block id (0 or 1)
 * @param status	PartStatusBlock structure to read into.
 * @return		Same as sh_read_atomic().
 * @see sh_read_atomic
 */
int
read_status_block(int blockid, PartStatusBlock *status)
{
	char buf[256];
	int rv;

	snprintf(buf, sizeof(buf), "/partition/%d/status", blockid);
	rv = sh_read_atomic(buf, status, sizeof(*status));
	if (rv == -1)
		return -1;

	swab_PartStatusBlock(status);
	return rv;
}


/**
 * The meat of the disk status thread.  Reads our status block, checks for
 * local hang, reads the other node's status block, checks for remote hangs,
 * writes out our current status block and handles shooting the other node.
 *
 * @param last_read_time	Last timestamp from other member.
 * @param skips			Pointer to the number of skipped I/Os from
 *				partner member.
 * @return			0 if no notification is needed; nonzero if notification is
 *				necessary.
 */
int
disk_status_thingy(uint64_t *last_read_time, int *skips)
{
	PartStatusBlock status_block;
	int _other_status = 0, _my_status = 0, rv = 0;

	/* XXX not thread safe ... ;( */
	shared_storage_init();
	
	/* XXX Add ref count? */
	pthread_mutex_lock(&status_mutex);
	_other_status = other_status;
	_my_status = my_status;
	pthread_mutex_unlock(&status_mutex);

	/*
	 * Read OUR status block to make sure we didn't hang!
	 */
	if (read_status_block(status_block_id, &status_block) != -1) {
		if ((status_block.ps_updateNodeNum != my_node_id) &&
		    (status_block.ps_state == NODE_DOWN)) {
#ifdef OLD_BEHAVIOR
			clulog(LOG_EMERG, "Disk-TB: Detected I/O Hang! Rebooting NOW!\n");
			REBOOT(RB_AUTOBOOT);
#else
			clulog(LOG_WARNING, "Disk-TB: Detected I/O Hang!\n");
#endif
		}
	}


	/*
	 * Read the other node's status block and determine if it is up
	 * or down.
	 */
	if (read_status_block(!status_block_id, &status_block) != -1) {

		/* broken magic -> down */
		if (status_block.ps_magic != STATUS_BLOCK_MAGIC_NUMBER) {
			_other_status = 0;
		} else if (status_block.ps_state == NODE_DOWN) {
			/* Node is down?  No need to powercycle */
			_other_status = 0;
		} else if (status_block.ps_timestamp == *last_read_time) {
			/* Same timestamp?! */
			(*skips)++;
			
			clulog(LOG_DEBUG,
			       "Disk-TB: Missed update from partner: %d/%d\n",
			       *skips, tko_count);
			
			if (*skips >= tko_count) {
				if (write_status_block(!status_block_id,
						       NODE_DOWN) == -1) {
					clulog(LOG_CRIT, "Disk-TB: Could not mark"
					       " partner DOWN\n");
				}

				_other_status = 0;
				rv = 1;

				*skips = 0;
			}
		} else if (!*last_read_time) {
			/* Not yet really read in a timestamp */
			*last_read_time = status_block.ps_timestamp;
		} else {
			/* Different timestamp. */
			*last_read_time = status_block.ps_timestamp;
			_other_status = 1;
			*skips = 0;
		}
	} else {
		/* I/O error... */
		_my_status = 0;
		_other_status = 0;
	}

	/* Write our status block */
	if (write_status_block(status_block_id, NODE_UP) != -1) {
		_my_status = 1;
	} else {
		/* I/O error... */
		_my_status = 0;
		_other_status = 0;
	}

	/* Update the accessible ones. */
	pthread_mutex_lock(&status_mutex);
	if ((other_status==1) && !_other_status && !other_lost) {
		other_gained = 0;
		other_lost = 1;
		need_shoot = rv;
		clulog(LOG_INFO, "Disk-TB: Partner is DOWN (%s)\n",
		       rv ? "Dead/Hung" : "Clean Shutdown");
	} else if (!other_status && _other_status && !other_gained) {
		other_lost = 0;
		other_gained = 1;
		need_shoot = 0;
		clulog(LOG_INFO, "Disk-TB: State Change: Partner UP\n");
	}
		
	other_status = _other_status;
	my_status = _my_status;
	pthread_mutex_unlock(&status_mutex);

	if (!_my_status) {
		/* Disk error */
		disk_error_action();
	}	


	return rv;
}


/**
 * Clear our status fields.
 */
void
disk_reset(void)
{
	pthread_mutex_lock(&status_mutex);
	need_shoot = 0;
	other_gained = 0;
	other_lost = 0;
	pthread_mutex_unlock(&status_mutex);
}


/**
 * Poll our 'other-lost' status.
 *
 * @return		1 if our partner has gone down according to the
 *			disk, 0 if not.
 */
int
disk_other_lost(void)
{
	int rv;
	
	pthread_mutex_lock(&status_mutex);
	rv = (other_lost && disk_thread_running && _disk_in_use);
	pthread_mutex_unlock(&status_mutex);

	return rv;
}


/**
 * Poll our 'need-shoot' status.
 *
 * @return		1 if our partner needs to be STONITHed, 0 if not.
 */
int
disk_need_shoot(void)
{
	int rv;

	pthread_mutex_lock(&status_mutex);
	rv = (need_shoot && disk_thread_running && _disk_in_use);
	pthread_mutex_unlock(&status_mutex);

	return rv;
}


/**
 * Poll our 'other-came-online' status.
 *
 * @return		1 if our partner came online according to the
 *			disk, 0 if not.
 */
int
disk_other_gained(void)
{
	int rv;

	pthread_mutex_lock(&status_mutex);
	rv = (other_gained && disk_thread_running && _disk_in_use);
	pthread_mutex_unlock(&status_mutex);

	return rv;
}


/**
 * Breaks a tie in the two node case using the shared storage.
 *
 * @return		1 if successful or 0 if not.
 */
int
disk_tiebreaker(void)
{
	return (disk_in_use() && (disk_my_status()==1));
}


/**
 * Our disk quorum thread.
 */
void *
disk_quorum_thread(void __attribute__ ((unused)) * unused_parameter)
{
	uint64_t partner_read_time = 0;
	int skips = tko_count / 2;

	pthread_mutex_lock(&status_mutex);
	disk_thread_running = 1;
	pthread_mutex_unlock(&status_mutex);
	
	while (1) {
		disk_status_thingy(&partner_read_time, &skips);
		
		sleep(interval);

		pthread_mutex_lock(&status_mutex);
		if (!_disk_in_use || !disk_thread_running) {
			if (write_status_block(status_block_id, NODE_DOWN)
			    == -1) {
				clulog(LOG_WARNING,
				       "Couldn't mark our disk state as "
				       "DOWN - we might get shot!\n");
			}

			disk_thread_running = 0;
			pthread_mutex_unlock(&status_mutex);
			return NULL;
		}
		pthread_mutex_unlock(&status_mutex);
	}
}
 

/**
 * Create the disk quorum thread.
 *
 * @param thread	Pointer which will be filled with pthread_t structure
 *			pointing to the created thread.
 * @return		Same as pthread_create.
 * @see			pthread_create
 */
int
disk_create_quorum_thread(pthread_t * thread)
{
	pthread_attr_t attrs;

	pthread_attr_init(&attrs);
	pthread_attr_setinheritsched(&attrs, PTHREAD_INHERIT_SCHED);
	pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED);
	pthread_atfork(NULL, NULL, NULL);

	pthread_mutex_lock(&status_mutex);
	disk_thread_running = 1;
	pthread_mutex_unlock(&status_mutex);

	return pthread_create(thread, &attrs, disk_quorum_thread, NULL);
}


/**
 * Cancel the disk quorum thread.
 *
 * @return		0
 */
int
disk_cancel_quorum_thread(void)
{
	pthread_mutex_lock(&status_mutex);
	if (disk_thread_running && _disk_in_use)
		clulog(LOG_DEBUG, "Telling disk quorum thread to exit\n");
	_disk_in_use = 0;
	pthread_mutex_unlock(&status_mutex);

	/* Wait for it... */
	while (1) {
		pthread_mutex_lock(&status_mutex);
		if (!disk_thread_running) {
			pthread_mutex_unlock(&status_mutex);
			return 0;
		}
		pthread_mutex_unlock(&status_mutex);
		usleep(100000);
	}

	return 0;
}


/**
 * Check and respond to disk status.
 *
 * @param quorum_status	Current quorum status.
 * @param exit_mask	Mask of members exiting.  May be modified.
 */
void
disk_check_states(quorum_view_t *quorum_status, memb_mask_t exit_mask)
{
	/*
	 * Check the disk states - only in a two-member cluster.
	 */
	if (!disk_in_use() || (memb_count(quorum_status->qv_mask) > 1))
		return;

	if (quorum_status->qv_status == 0) {
		if (disk_tiebreaker())
			/*
			 * Local disk state online.
			 */
			disk_memb_up(quorum_status);
		return;
	}

	/* We have quorum */
	if (disk_my_status() == 0) {
		/*
		 * We are the only member and we've lost shared storage.
		 * We've lost quorum.  Have a nice day.
		 */
		disk_local_memb_down(quorum_status);
		return;
	}

	/* Disk info: Local member is online */
	if (disk_other_lost()) {
		/*
		 * We're the only member according to membership and the
		 * other member has stopped updating its timestamp.
		 * Cause quorum event.
		 */
		disk_other_memb_down(quorum_status, exit_mask,
		                     disk_need_shoot());
		disk_reset();
	}

	/* Disk info: We didn't lose the other member. */
	if (disk_other_gained()) {
		disk_memb_up(quorum_status);
		disk_reset();
	}
}



/**
 * Generate a membership event based on loss of disk membership.  Called ONLY
 * in a two member cluster when both DISK and NET heartbeating go away,fing
 *
 * @param quorum_status	Current Quorum Status.
 * @param exit_mask	Mask of members expected to exit.  Might me modified.
 * @param shoot		Should we shoot the other member?
 * @return 		0, or error returned from start_vf
 * @see start_vf
 */
int
disk_other_memb_down(quorum_view_t *quorum_status, memb_mask_t exit_mask,
                     int shoot)
{
	quorum_view_t qv;
	int other_node;
	memb_mask_t notify_mask;

	/*
	 * Ensure the disk view doesn't override the
	 * membership view.
	 */
	if (memb_count(quorum_status->qv_mask) > 1)
		return 0;

	other_node = disk_other_node();
	memset(notify_mask, 0, sizeof(notify_mask));

	/*
	 * Only we are online.
	 */
	memb_mark_up(notify_mask, my_node_id);

	/*
	 * Copy in member.
	 */
	memcpy(&qv, quorum_status, sizeof(qv));

        /*
         * Mark the other node down in the membership mask.  This
         * should have already been done if we're getting here...
         */
        memb_mark_down(qv.qv_mask, other_node);

	/*
	 * Mark the other node down in the panic_mask.
	 */
	memb_mark_down(qv.qv_panic_mask, other_node);

	/*
	 * Mark the other node UP in the stonith_mask
	 */
	if (shoot)
		memb_mark_up(qv.qv_stonith_mask, other_node);
	else
		memb_mark_up(exit_mask, other_node);

	clulog(LOG_DEBUG, "%s: Starting VF\n", __FUNCTION__);
	return start_vf(notify_mask, &qv);
}


/**
 * Generate a quorum event.  We've lost shared storage **AND** we can't
 * contact the other member!  Two member cluster only.
 *
 * @param quorum_status	Current Quorum Status.
 * @return 		0, or error returned from start_vf
 * @see start_vf
 */
int
disk_local_memb_down(quorum_view_t *quorum_status)
{
	quorum_view_t qv;
	memb_mask_t notify_mask;

	/*
	 * Ensure we're the only one we've seen!
	 */
	if (memb_count(quorum_status->qv_mask) != 1)
		return 0;

	memset(notify_mask, 0, sizeof(notify_mask));

	/*
	 * Only we are online.
	 */
	memb_mark_up(notify_mask, my_node_id);

	/*
	 * Copy in info..
	 */
	memcpy(&qv, quorum_status, sizeof(qv));

	/*
	 * Loss of disk quorum.
	 */
	qv.qv_status = 0;

	clulog(LOG_ALERT, "Disk-TB: Lost quorum!\n");

	clulog(LOG_DEBUG, "%s: Starting VF\n", __FUNCTION__);
	return start_vf(notify_mask, &qv);
}


/**
 * Generate a quorum event.  We've gained quorum.
 * Two member cluster only.
 *
 * @param quorum_status	Current Quorum Status.
 * @return 		0, or error returned from start_vf
 * @see start_vf
 */
int
disk_memb_up(quorum_view_t *quorum_status)
{
	quorum_view_t qv;
	memb_mask_t notify_mask;

	/*
	 * Ensure we're the only one we see.  We do nothing
	 * with disk events unless network membership is down.
	 */
	if (memb_count(quorum_status->qv_mask) != 1)
		return 0;

	memset(notify_mask, 0, sizeof(notify_mask));

	/*
	 * Only we are online.
	 */
	memb_mark_up(notify_mask, my_node_id);

	/*
	 * Copy in info..
	 */
	memcpy(&qv, quorum_status, sizeof(qv));

	/*
	 * Gain of disk quorum?
	 */
	if (!qv.qv_status)
		qv.qv_status = 1;

	/*
	 * Mark the panic mask of the other member if the
	 * disk says it's online.
	 */
	if (disk_other_status())
		memb_mark_up(qv.qv_panic_mask, disk_other_node());

       	clulog(LOG_DEBUG, "%s: Starting VF\n", __FUNCTION__);
	return start_vf(notify_mask, &qv);
}
