/*
  Copyright Red Hat, Inc. 2002-2003
  Portions (C) Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Cluscand - a daemon which scans the shared storage forconsistency.
 *
 * $Id: cluscand.c,v 1.4 2003/08/26 21:39:54 lhh Exp $
 *
 * Author: Lon H. Hohberger <lhh at redhat.com>
 */
#include <signal.h>
#include <errno.h>
#include <clulog.h>
#include <xmlwrap.h>
#include <svcmgr.h>
#include <clushared.h>
#include <sharedstate.h>
#include <stdlib.h>
#include <string.h>
#include <clu_lock.h>
#include <quorumparams.h>

/* 
 * use the quorum daemon's log level, not that this thing logs anything 
 * anyway...
 */

#define LOGLEVEL_STR "cluquorumd%logLevel"

static int exiting = 0;
static char daemon_name[MAXPATHLEN];

/*
 * This parameter specifies the number of seconds between scans of
 * a category of state information on the shared state partition.  This
 * controls the read frequency which indirectly results in a repair operation
 * should the partition become corrupted due to user error.
 * TIMXXX - perhaps this should scale with the number of cluster members.
 */
static int scan_delay = DEFAULT_CATEGORY_SCAN_DELAY;

static void scand_reconfigure(void);
int main(int argc, char **argv);

/* ... */
void daemon_init(char *prog);

static void
sh_exit(int sig)
{
	clulog(LOG_INFO, "Signal %d received; exiting\n", sig);
	exiting = 1;
}

/*
 * HUP
 *
 * Traditional behavior.  Reconfigure on SIGHUP.
 */
static void
sh_reconfigure(int sig)
{
	clulog(LOG_INFO, "Re-reading the cluster database\n");
	CFG_Destroy();
	clu_lock_hup();

	scand_reconfigure();
}

/*
 * IO, SEGV, BUS, ILL
 *
 * *Try* to restart myself in these cases.  Hopefully, daemon_name wasn't
 * mangled.  This helps prevent loss of synchronization with the other node
 * in the case rmtabd dies.
 */
static void
sh_restart(int sig)
{
	if (daemon_name[0] != '/') {
		clulog(LOG_CRIT, "Signal %d received; going down\n", sig);
		exit(1);
	}

	clulog(LOG_CRIT, "Signal %d received; restarting\n", sig);
	if (execl(daemon_name, daemon_name, NULL) == -1) {
		clulog(LOG_EMERG, "Failed to execl: %s! => NFS BROKEN\n",
		       strerror(errno));
		exit(1);
	}
}

static inline void
register_sighandlers(void)
{
	sigset_t set;
	struct sigaction act;

	sigemptyset(&set);
	sigaddset(&set, SIGHUP);

	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGQUIT);

	sigaddset(&set, SIGILL);
	sigaddset(&set, SIGIO);
	sigaddset(&set, SIGSEGV);
	sigaddset(&set, SIGBUS);

	sigprocmask(SIG_UNBLOCK, &set, NULL);

	memset(&act, 0, sizeof (act));
	sigemptyset(&act.sa_mask);

	/* Ok, reconfigure here */
	act.sa_handler = sh_reconfigure;
	sigaction(SIGHUP, &act, NULL);

	/* Exit signals */
	act.sa_handler = sh_exit;
	sigaction(SIGTERM, &act, NULL);
	sigaction(SIGQUIT, &act, NULL);

	/* Drat.  Restart conditions */
	act.sa_handler = sh_restart;
	sigaction(SIGILL, &act, NULL);
	sigaction(SIGIO, &act, NULL);
	sigaction(SIGSEGV, &act, NULL);
	sigaction(SIGBUS, &act, NULL);
}

/* ******************************* *
 * Configuration Utility Functions
 * ******************************* */
static inline int
__get_int_param(char *str, int *val, int dflt)
{
	char *value;
	int ret;

	ret = CFG_Get(str, NULL, &value);

	switch (ret) {
	case CFG_DEFAULT:
		*val = dflt;
		break;
	case CFG_OK:
		*val = atoi(value);
		break;
	default:
		clulog(LOG_ERR, "Cannot get \"%s\" from database; "
		       "CFG_Get() failed, err=%d\n", ret);
		return 0;
	}

	return 0;
}

/*
 * get_scand_loglevel
 */
static int
get_scand_loglevel(int *level)
{
	return __get_int_param(LOGLEVEL_STR, level, LOG_DEFAULT);
}

static int
get_scand_scandelay(int *s_delay)
{
	return __get_int_param(CFG_QUORUM_SCAN_DELAY, s_delay,
			       DEFAULT_CATEGORY_SCAN_DELAY);
}

/*
 * scand_reconfigure
 *
 * This is called at init and by sh_reconfigure and sets up daemon-specific
 * configuration params.
 */
static void
scand_reconfigure(void)
{
	int level, old_level, old_delay;

	/* loglevel */
	old_level = clu_get_loglevel();
	get_scand_loglevel(&level);

	if (old_level != level) {
		if (clu_set_loglevel(level) == -1) {
			clulog(LOG_ERR, "Failed set log level\n");
		}
		clulog(LOG_DEBUG, "Log level is now %d\n", level);
	}

	/* scan delay interval (tw33k4bl3) */
	old_delay = scan_delay;
	get_scand_scandelay(&scan_delay);

	/* bounds-check */
	if (scan_delay < 1)
		scan_delay = 1;
	else if (scan_delay > 60)
		scan_delay = 60;

	if (old_delay != scan_delay) {
		clulog(LOG_DEBUG, "Scan interval is now %d seconds\n",
		       scan_delay);
	}

}

/*
 * scand_init
 *
 * Set up local parameters; call daemon_init, etc.
 */
static void
scand_init(int argc, char **argv)
{
	char buf[MAXPATHLEN];

	daemon_init(argv[0]);
	scand_reconfigure();

	register_sighandlers();
	/*
	 * Restart info - daemon_init must've been called prior to this if
	 * we plan on forking, or we will have a different pid!
	 */
	snprintf(buf, sizeof (buf), "/proc/%d/exe", getpid());
	memset(daemon_name, 0, sizeof (daemon_name));
	if (readlink(buf, daemon_name, sizeof (daemon_name)) == -1) {
		clulog(LOG_ERR, "Couldn't stat %s: %s; restart disabled\n",
		       buf, strerror(errno));
	}
}

/*
 * main
 *
 * Main.  Main.  Main.  Main.  Main.  Main.  Main.  Main.  Main.  Main.  
 */
int
main(int argc, char **argv)
{
	scand_init(argc, argv);
	clulog(LOG_DEBUG, "%s starting\n", argv[0]);

	while (!exiting) {
		scanNextCategory(scan_delay);
		sleep(scan_delay);
	}

	return 0;
}
