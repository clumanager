/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Configuration change coordination/updating.
 */
#include <stdint.h>
#include <xmlwrap.h>
#include <sys/stat.h>
#include <msgsvc.h>
#include <cm_api.h>
#include <clulog.h>
#include <clu_lock.h>
#include <errno.h>
#include <svcmgr.h>

#define CLU_CONFIG_VIEW "cluster%config_viewnumber"

/* in clusvcmgrd.c */
int svc_add(int);
int svc_remove(int);

/* in shared_services.c */
void disk_error_action(void);


/**
 * Find removed services + added services.
 *
 * @param reconfig	Set to 1 if this is a reconfiguration, 0 if not.
 */
void
update_services(int reconfig)
{
	static char configured_services[MAX_SERVICES];
	char buf[128];
	int x;
	char *val;

	for (x = 0; x < MAX_SERVICES; x++) {
		snprintf(buf, sizeof(buf), "services%%service%d%%name", x);
		if (CFG_Get(buf, NULL, &val) == CFG_OK) {
			if (reconfig && !configured_services[x]) {
				clulog(LOG_INFO, "Initializing service %s\n",
				       val);
				svc_add(x);
			}
			configured_services[x] = 1;
		} else {
			if (reconfig && configured_services[x])
				svc_remove(x);
			configured_services[x] = 0;
		}
	}
}


/**
 * Rebuild the configuration file from shared storage.  This should be
 * lockless - this is because weez hates it, my precious...
 *
 * @return		-1 on failure, 0 on success.
 */
int
rebuild_config_lockless(void)
{
	clulog(LOG_DEBUG, "%s: rereading from shared state\n", __FUNCTION__);
	if (CFG_Read() != CFG_OK) {
		clulog(LOG_CRIT,
		       "Could not read configuration from shared state!\n");
		return -1;
	}

	clulog(LOG_DEBUG, "%s: writing to %s\n", __FUNCTION__, CLU_CONFIG_FILE);
	if (CFG_WriteFile(CLU_CONFIG_FILE) != CFG_OK) {
		clulog(LOG_CRIT, "Could not write configuration to %s!\n",
		       CLU_CONFIG_FILE);
		return -1;
	}
	return 0;
}


/**
 * Rebuild the configurationd data.
 *
 * @return		-1 on failure, 0 on success.
 */
int
rebuild_config(void)
{
	int rv;

	if (clu_config_lock() == -1) {
		clulog(LOG_ERR, "Unable to obtain configuration lock: %s\n",
		       strerror(errno));
		return -1;
	}

	rv = rebuild_config_lockless();

	clu_config_unlock();

	return rv;
}


/**
 * Handle a configuration update signal (SIGHUP)
 *
 * @param mask		Current membership mask, according to the service
 *			manager.
 * @param my_node_id	My member number/node ID.
 * @return		-1 on failure, 0 on success.
 */
int
handle_config_update(memb_mask_t mask, int my_node_id)
{
	char *val;
	uint32_t new_config_vn = (uint32_t)(~0),
		 old_config_vn = 0;
	uint32_t node, errors = 0;
	int fd;

	clulog(LOG_DEBUG, "Processing config update");
	if (CFG_Get(CLU_CONFIG_VIEW, NULL, &val) != CFG_OK) {
		clulog(LOG_WARNING,
		       "No View Number in current config; assuming 0");
	} else {
		old_config_vn = atoi(val);
	}

	/* clu_lock(some_thing) */

	CFG_Destroy();
	CFG_ReadFile(CLU_CONFIG_FILE);

	/* clu_unlock() */

	if (CFG_Get(CLU_CONFIG_VIEW, NULL, &val) != CFG_OK) {
		clulog(LOG_WARNING,
		       "No View Number in new config; assuming %u!\n",
		       (unsigned)~0);
	} else {
		new_config_vn = atoi(val);
	}

	if (new_config_vn == old_config_vn) {
		return 0;
	}

	if (new_config_vn <= old_config_vn) {
		clulog(LOG_ERR, "New configuration has obsolete view number.\n");
		clulog(LOG_ERR, "Reverting to current version.\n");
		return rebuild_config_lockless();
	}

	/*
	 * Grab config lock so that we can ensure we're the only one 
	 * writing to the config.
	 */
	if (clu_config_lock() == -1) {
		clulog(LOG_ERR, "Unable to obtain configuration lock: %s\n",
		       strerror(errno));
		return -1;
	}

	if (CFG_Write() != CFG_OK) {
		clulog(LOG_EMERG,
		       "Couldn't write configuration to shared storage: %s!\n",
		       strerror(errno));
		clu_config_unlock();
		disk_error_action();
		return -1;
	}

	clu_config_unlock();

	/* Recalculate msg service stuff... in case node count has changed */
	clulog(LOG_DEBUG, "Notifying other members of config change\n");
	msg_svc_init(1);
	update_services(1);
		       
	for (node = 0; node < MAX_NODES; node++) {
		if (node == my_node_id)
			continue;

		if (memb_online(mask, node)) {
			clulog(LOG_DEBUG, "Sending update msg to member #%d\n",
			       node);

			fd = msg_open(PROCID_CLUSVCMGRD, node);
			if (fd == -1) {
				clulog(LOG_ERR, "Couldn't connect for config "
				       "update to member #%d: %s\n", node,
				       strerror(errno));
				errors++;
				continue;
			}

			if (msg_send_simple(fd, SVC_CONFIG_UPDATE, 0, 0) == -1){
				clulog(LOG_CRIT, "Couldn't send update msg to "
				       "member #%d: %s\n", node,
				       strerror(errno));
				errors++;
			}

			msg_close(fd);
		}
	}

	return (errors ? -1 : 0);
}


/**
 * Check for changes to shared storage config.  We need to do this
 * periodically in case config-change messages are lost.
 *
 * @return		-1 on failure, 0 on no change, 1 on changed.
 */
int
check_config_data(void)
{
	static SharedHeader my_view;
	SharedHeader hdr;

	if (sh_stat("/cluster/config.xml", &hdr) == -1) {
		disk_error_action();
		return -1;
	}

	if (my_view.h_hcrc == 0) {
		memcpy(&my_view, &hdr, sizeof(my_view));
		return 0;
	}

	if (!memcmp(&hdr, &my_view, sizeof(hdr)))
		return 0;

	memcpy(&my_view, &hdr, sizeof(my_view));
	return 1;
}


/**
 * Check for the existence of /etc/cluster.xml.  If it doesn't exist, we
 * recreate it from our cache.
 *
 * @return		-1 on failure, 0 on success.
 */
int
check_config_file(void)
{
	struct stat s;
	int err;

	if (stat(CLU_CONFIG_FILE, &s) == 0)
		return 0;

	err = errno;
	switch(err) {
	default: /* ELOOP ENOMEM, etc... */
		clulog(LOG_EMERG, "Couldn't stat %s: %s\n", CLU_CONFIG_FILE,
	       	       strerror(err));
		errno = err;
		return -1;
#if 0
	/* XXX ... grrr */
	case ENOTDIR:
		clulog(LOG_EMERG, "/etc missing, recreating!\n");
		if (mkdir("/etc", 0755) == -1) {
			err = errno;
			clulog(LOG_EMERG, "Couldn't recreate /etc: %s\n",
			       strerror(errno));
			break;
		}
#endif
	case ENOENT:
		clulog(LOG_WARNING,
		       "Couldn't stat %s: %s. Recreating from cache!\n",
		       CLU_CONFIG_FILE, strerror(err));

		if (CFG_WriteFile(CLU_CONFIG_FILE) == CFG_OK)
			return 0;

		/* Save errno */
		err = errno;
		clulog(LOG_ALERT, "Could not recreate %s: %s\n",
		       CLU_CONFIG_FILE, strerror(errno));
	}

	errno = err;
	return -1;
}


/**
 * We need to pull the information off of shared storage and compare it with
 * what's on the local storage so we can update it.
 *
 * @return		0 if the configurations match, 1 if not.  -1 indicates
 *			a failure.
 */
int
boot_config_init(void)
{
	uint32_t shared_version = 0x1fffffff;
	uint32_t local_version = 0;
	char *val;
	CFG_Struct *local_cfg = NULL;

	CFG_Destroy();
	if (CFG_ReadFile(CLU_CONFIG_FILE) != CFG_OK) {
		clulog(LOG_CRIT,
		       "Couldn't read local configuration: %s\n",
		       strerror(errno));
		return -1;
	}

	if (CFG_Get(CLU_CONFIG_VIEW, NULL, &val) == CFG_OK) {
		local_version = atoi(val);
	} else {
		clulog(LOG_WARNING, "No View Number in %s: Assuming %u\n",
		       CLU_CONFIG_FILE, local_version);
	}

	local_cfg = CFG_MemBackup();

	if (CFG_Read() != CFG_OK) {
		clulog(LOG_CRIT,
		       "Couldn't read configuration from shared state: %s\n",
		       strerror(errno));
		CFG_MemBackupKill(local_cfg);
		disk_error_action();
		return -1;
	}

	if (CFG_Get(CLU_CONFIG_VIEW, NULL, &val) == CFG_OK) {
		shared_version = atoi(val);
	} else {
		clulog(LOG_WARNING, "No View Number in shared config: "
		       "Assuming %u\n", CLU_CONFIG_FILE, shared_version);
	}

	if (shared_version == local_version) {
		update_services(0);
		CFG_MemBackupKill(local_cfg);
		if (CFG_WriteFile(CLU_CONFIG_FILE) != CFG_OK) {
			clulog(LOG_ERR, "Couldn't rewrite %s: %s\n",
			       CLU_CONFIG_FILE, strerror(errno));
		}	
		return 0;
	}

	if (shared_version < local_version) {
		/*
		 * We allow for config view number wrapping.
		 * 2^31 changes should be PLENTY.
		 */
		if ( (local_version - shared_version) > 0x80000000 ) {
			/* Free up the local config memory */
			CFG_MemBackupKill(local_cfg);

			update_services(0);
			if (CFG_WriteFile(CLU_CONFIG_FILE) != CFG_OK) {
				clulog(LOG_CRIT, "Couldn't write %s: %s\n",
				       CLU_CONFIG_FILE, strerror(errno));
				return -1;
			}

			return 1;
		} 

		clulog(LOG_WARNING,
		       "Overwriting shared configuration during boot.");

		CFG_MemRestore(local_cfg);

		update_services(0);
		if (clu_config_lock() == -1) {
			clulog(LOG_CRIT, "clu_config_lock: %s\n",
			       strerror(errno));
			return -1;
		}

		if (CFG_Write() != CFG_OK) {
			clulog(LOG_CRIT, "CFG_Write: %s\n", strerror(errno));
			clu_config_unlock();
			disk_error_action();
			return -1;
		}
		clu_config_unlock();
		return 1;
	} 

	/* Free up the local config memory */
	CFG_MemBackupKill(local_cfg);

	update_services(0);
	if (CFG_WriteFile(CLU_CONFIG_FILE) != CFG_OK) {
		clulog(LOG_CRIT, "Couldn't write %s: %s\n", CLU_CONFIG_FILE,
		       strerror(errno));
		return -1;
	}

	return 1;
}
