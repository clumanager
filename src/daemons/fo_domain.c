/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Fail-over Domain & Preferred Node Ordering Driver.
 */
#include <xmlwrap.h>
#include <svcmgr.h>

/*
#ifndef DEBUG
#define DEBUG
#endif
*/

#ifdef DEBUG
#define ENTER() clulog(LOG_DEBUG, "ENTER: %s\n", __FUNCTION__)
#define RETURN(val) {\
	clulog(LOG_DEBUG, "RETURN: %s line=%d value=%d\n", __FUNCTION__, \
	       __LINE__, (val));\
	return(val);\
}
#else
#define ENTER()
#define RETURN(val) return(val)
#endif

/**
 * Check to see if a given node is the current preferred node within a domain
 * on which we should start the service...
 * @param nodename		Node/member name.
 * @param domainid		Existing domain.
 * @param membership		Current membership mask.
 * @return			0 for No, All domain members offline.
 *				1 for No, 1+ Domain member(s) online.
 *				2 for Yes, Not lowest-ordered, online member.
 *				3 for Yes, Lowest-ordered, online member.
 */
int
node_in_domain(char *nodename, uint32_t domainid, memb_mask_t membership)
{
	int x, id, lowest = 1, online = 0;
	char tok[MAX_TOKEN_LEN];
	char *ret;

	for (x=0; x<MAX_NODES; x++) {
		snprintf(tok, sizeof(tok), "failoverdomains%%failoverdomain"
			 "%d%%failoverdomainnode%d%%name", domainid, x);

		if (CFG_Get(tok, NULL, &ret) != CFG_OK)
			continue;

		/*
		 * Get the node ID of the current failover domain member
		 */
		id = getNodeID(ret);

		/*
		 * We have to check the membership mask here so that
		 * we can decide whether or not 'nodename' is the lowest
		 * ordered node.
		 */
		if (!memb_online(membership, id))
			continue;

		if (!strcmp(nodename, ret)) {
			/*
			 * If we get here, we know:
			 * (1) We are a member of the domain.
			 * (2) Whether or not we are the lowest-ordered, online
			 * member of the domain.
			 */
			return (2 + lowest);
		}

		/*
		 * If we get here, we know:
		 * (1) We're not the lowest-ordered node in the domain.
		 * (2) A member of the domain is online.
		 */
		online = 1;
		lowest = 0;
	}

	/*
	 * If we get here, we know:
	 * (1) We are NOT a member of the domain.
	 * (2) We know whether or not a domain member is online. (From above)
	 */
	return online;
}


/**
 * Given a service id, tell whether or not the service has a failover domain.
 * @param svcid		The id of the service.
 * @return		1 for YES, 0 for NO.
 */
int
svc_has_domain(int svcid)
{
	char tok[MAX_TOKEN_LEN];
	char *ret;

	snprintf(tok, sizeof(tok), "services%%service%d%%failoverdomain",
		 svcid);

	return (CFG_Get(tok, NULL, &ret) == CFG_OK);
}


/**
 * Given a domain name, return its id
 * @param domainname	The name of the domain to find.
 * @return		-1 on 'non existent'; otherwise the domain ID.
 */
uint32_t
domain_name_to_id(char *domainname)
{
	int x;
	char tok[MAX_TOKEN_LEN];
	char *ret;

	/*
	 * Only one domain per service, max...
	 */
	for (x=0; x<MAX_SERVICES; x++) {
		snprintf(tok, sizeof(tok),
			 "failoverdomains%%failoverdomain%d%%name",
			 x);

		if (CFG_Get(tok, NULL, &ret) != CFG_OK)
			continue;

		if (!strcmp(domainname, ret))
			return x;
	}

	return -1;
}


/**
 * See if a given nodeid should start a specified service svcid.
 *
 * @param nodeid	The node ID in question.
 * @param membership	Current membership mask.
 * @param svcid		The service ID in question.
 * @return		0 on NO, 1 for YES
 */
int
node_should_start(uint32_t nodeid, memb_mask_t membership, uint32_t svcid)
{
	char tok[MAX_TOKEN_LEN];
	char *ret, *nodename = NULL;
	uint32_t domainid = -1;
	int ordered = 0;
	int restricted = 0;

	ENTER();

	/*
	 * Um, if the node isn't online...
	 */
	if (!memb_online(membership, nodeid)) {
#ifdef DEBUG
		clulog(LOG_DEBUG,"Member #%d is not online -> NO\n", nodeid);
#endif
		RETURN(FOD_ILLEGAL);
	}

	/*
	 * Get the node name
	 */
	getNodeName(nodeid, &nodename);
	if (!nodename)
		RETURN(FOD_ILLEGAL);

	snprintf(tok, sizeof(tok), "services%%service%d%%failoverdomain",
		 svcid);
	if (CFG_Get(tok, NULL, &ret) != CFG_OK) {
		/*
		 * If no domain is present, then the node in question should
		 * try to start the service.
		 */
#ifdef DEBUG
		clulog(LOG_DEBUG,
		       "Fail-over Domain for service %d nonexistent\n");
#endif
		RETURN(FOD_BEST);
	}

	/*
	 * Ok, we've got a failover domain associated with the service.
	 * Let's see if the domain actually exists...
	 */
	domainid = domain_name_to_id(ret);
	if (domainid == -1) {
		/*
		 * Domain doesn't exist!  Weird...
		 */
		clulog(LOG_WARNING,
		       "Domain '%s' specified for service %d nonexistent!\n",
		       ret, svcid);
		RETURN(FOD_BEST);
	}

	/*
	 * Determine whether this domain is restricted or not...
	 */
	snprintf(tok, sizeof(tok),
		 "failoverdomains%%failoverdomain%d%%restricted",
		 domainid);
	if ((CFG_Get(tok, NULL, &ret) == CFG_OK) &&
	    ((ret[0] == 'y') || (ret[0] == 'Y')))
		restricted = 1;

	/*
	 * Determine whether this domain is ordered or not...
	 */
	snprintf(tok, sizeof(tok),
		 "failoverdomains%%failoverdomain%d%%ordered",
		 domainid);
	if ((CFG_Get(tok, NULL, &ret) == CFG_OK) &&
	    ((ret[0] == 'y') || (ret[0] == 'Y')))
		ordered = 1;

	switch (node_in_domain(nodename, domainid, membership)) {
	case 0:
		/*
		 * Node is not a member of the domain and no members of the
		 * domain are online.
		 */
#ifdef DEBUG
		clulog(LOG_DEBUG, "Member #%d is not a member and no "
		       "members are online\n", nodeid);
#endif
		if (!restricted) {
#ifdef DEBUG
			clulog(LOG_DEBUG,"Restricted mode off -> BEST\n");
#endif
			RETURN(FOD_BEST);
		}
#ifdef DEBUG
		clulog(LOG_DEBUG,"Restricted mode -> ILLEGAL\n");
#endif
		RETURN(FOD_ILLEGAL);
	case 1:
		/* 
		 * Node is not a member of the domain and at least one member
		 * of the domain is online.
		 */
		/* In this case, we can ignore 'restricted' */
#ifdef DEBUG
		clulog(LOG_DEBUG, "Member #%d is not a member of domain %d "
		       "and a member is online\n", nodeid, domainid);
#endif
		if (!restricted) {
#ifdef DEBUG
			clulog(LOG_DEBUG,"Restricted mode off -> GOOD\n");
#endif
			RETURN(FOD_GOOD);
		}
#ifdef DEBUG
		clulog(LOG_DEBUG,"Restricted mode -> ILLEGAL\n");
#endif
		RETURN(FOD_ILLEGAL);
	case 2:
		/*
		 * Node is a member of the domain, but is not the
		 * lowest-ordered, online member.
		 */
#ifdef DEBUG
		clulog(LOG_DEBUG, "Member #%d is a member, but is not the "
		       "lowest-ordered\n", nodeid);
#endif
		if (ordered) {
#ifdef DEBUG
			clulog(LOG_DEBUG,"Ordered mode -> BETTER\n");
#endif
			RETURN(FOD_BETTER);
		}

#ifdef DEBUG
		clulog(LOG_DEBUG,"Not using ordered mode -> BEST\n");
#endif
		RETURN(FOD_BEST);
	case 3:
		/*
		 * Node is a member of the domain and is the lowest-ordered,
		 * online member.
		 */
		/* In this case, we can ignore 'ordered' */
#ifdef DEBUG
		clulog(LOG_DEBUG, "Member #%d is the lowest-ordered member "
		       "of the domain -> BEST\n", nodeid);
#endif
		RETURN(FOD_BEST);
	default:
		/* Do what? */
		clulog(LOG_ERR, "Code path error: "
		       "Invalid return from node_in_domain()\n");
		RETURN(FOD_ILLEGAL);
	}

	/* not reached */
	RETURN(FOD_ILLEGAL);
}


#if defined(STANDALONE) && defined(DEBUG)
#include <string.h>
memb_mask_t curr_members;

void
usage(void)
{
	printf("Debug stuff\n");
	printf("up <nodeid>\n");
	printf("down <nodeid>\n");
	printf("check <nodeid> <svcid>\n");
	printf("membership\n");
}


void
do_check(char *cmdline)
{
	char *nnp, *svcp;
	int nodeid;
	int svcid;

	nnp = strchr(cmdline,' ');
	if (!nnp) {
		usage();
		return;
	}
	nnp++;

	svcp = strchr(nnp, ' ');
	if (!svcp) {
		usage();
		return;
	}

	nodeid = atoi(nnp);
	svcid = atoi(svcp);

	node_should_start(nodeid, curr_members, svcid);
}


void
do_up(char *cmdline)
{
	char *nnp;
	int nodeid;

	nnp = strchr(cmdline,' ');
	if (!nnp) {
		usage();
		return;
	}
	nnp++;

	nodeid = atoi(nnp);

	memb_mark_up(curr_members, nodeid);
	printf("Marked member #%d online\n", nodeid);
}

void
do_down(char *cmdline)
{
	char *nnp;
	int nodeid;

	nnp = strchr(cmdline,' ');
	if (!nnp) {
		usage();
		return;
	}
	nnp++;

	nodeid = atoi(nnp);

	memb_mark_down(curr_members, nodeid);
	printf("Marked member #%d OFFLINE\n", nodeid);
}


int
main(int argc, char **argv)
{
	char cmd[80];

	if (argc < 2) {
		printf("usage: %s <cluster.xml>\n", argv[0]);
		return 1;
	}

	if (CFG_ReadFile(argv[1]) != CFG_OK) {
		printf("usage: %s <cluster.xml>\n", argv[0]);
		return 1;
	}

	memset(curr_members,0,sizeof(curr_members));

	while (1) {
		printf("debug> ");
		fgets(cmd, sizeof(cmd), stdin);
		switch (cmd[0]) {
		case 'u':
			do_up(cmd);
			break;
		case 'd':
			do_down(cmd);
			break;
		case 'c':
			do_check(cmd);
			break;
		case 'm':
			printf("Membership mask = %s\n",
			       memb_mask_str(curr_members));
			break;
		case 'q':
			exit(0);
			break;
		default:
			usage();
			break;
		}
	}

	return 0;
}

#endif
