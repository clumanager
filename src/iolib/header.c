/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Functions which manipulate shared state headers.
 */
#include <clusterdefs.h>
#include <crc32.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sharedstate.h>
#include <platform.h>
#include <time.h>

/**
 * Swap the bytes of a shared header so that it's always in big-endian form
 * when stored on disk.
 *
 * @param hdr		Header to encode.
 */
void
header_encode(SharedHeader *hdr)
{
	/* sanity check - LE machine -> already encoded. */
	if (hdr->h_magic == be_swap32(SHARED_HEADER_MAGIC))
		return;

	swab32(hdr->h_magic);
	swab32(hdr->h_hcrc);
	swab32(hdr->h_dcrc);
	swab32(hdr->h_length);
	swab64(hdr->h_view);
	swab64(hdr->h_timestamp);
}


/**
 * Swap the bytes of a shared header so that it's always in host-byte order
 * after we read it.  This should be a macro calling header_encode.
 *
 * @param hdr		Header to decode.
 */
void
header_decode(SharedHeader *hdr)
{
	/* sanity check - LE machine -> already decoded. */
	if (hdr->h_magic == SHARED_HEADER_MAGIC)
		return;

	swab32(hdr->h_magic);
	swab32(hdr->h_hcrc);
	swab32(hdr->h_dcrc);
	swab32(hdr->h_length);
	swab64(hdr->h_view);
	swab64(hdr->h_timestamp);
}


/**
 * Generate a shared header suitable for storing data.  This includes:
 * header magic, header crc, data crc, header length, timestamp.
 * The header CRC is generated *after* the data CRC; so the header,
 * in effect, ensures that the data CRC is valid before we even look
 * at the data.  Thus, if the header CRC decodes properly, then we
 * assume that there's a very very high chance that the data CRC is valid.
 * If the data CRC doesn't match the data, it's indicative of a problem.
 *
 * @param hdr		Preallocated pointer to SharedHeader structure.
 * @param data		Data to be stored with hdr.
 * @param count		Size of data.
 * @return		-1 if CRC32 generation fails, or 0 on success.
 */
int
header_generate(SharedHeader *hdr, const char *data, size_t count)
{
	memset(hdr,0,sizeof(*hdr));

	hdr->h_magic = SHARED_HEADER_MAGIC;

	if (data && count) {
		hdr->h_dcrc = clu_crc32(data, count);
		hdr->h_length = (uint32_t)count;

		if (hdr->h_dcrc == 0) {
			fprintf(stderr, "Invalid CRC32 generated on data!\n");
			return -1;
		}
	}

	hdr->h_timestamp = (uint64_t)time(NULL);

	hdr->h_hcrc = clu_crc32((char *)hdr, sizeof(*hdr));
	if (hdr->h_hcrc == 0) {
		fprintf(stderr, "Invalid CRC32 generated on header!\n");
		return -1;
	}

	return 0;
}


/**
 * Verify the integrity of a shared header.  Basically, check the CRC32
 * information against the data and header.  A better name for this would
 * be "shared_block_verify".
 *
 * @param hdr		Preallocated pointer to SharedHeader structure.
 * @param data		Data to be stored with hdr.
 * @param count		Size of data.
 * @return		-1 if CRC32 generation fails, or 0 on success.
 */
int
header_verify(SharedHeader *hdr, const char *data, size_t count)
{
	uint32_t crc;
	uint32_t bkupcrc;

	/*
	 * verify the header's CRC32.  Ok, we know it's overkill taking
	 * the CRC32 of a friggin' 16-byte (12 bytes, really) structure,
	 * but why not?
	 */
	bkupcrc = hdr->h_hcrc;
	hdr->h_hcrc = 0;
	crc = clu_crc32((char *)hdr, sizeof(*hdr));
	hdr->h_hcrc = bkupcrc;
	if (bkupcrc != crc) {
		fprintf(stderr, "Header CRC32 mismatch; Exp: 0x%08x "
			"Got: 0x%08x\n", bkupcrc, crc);
		return -1;
	}

	/*
	 * Verify the magic number.
	 */
	if (hdr->h_magic != SHARED_HEADER_MAGIC) {
		fprintf(stderr, "Magic mismatch; Exp: 0x%08x "
			"Got: 0x%08x\n", SHARED_HEADER_MAGIC, hdr->h_magic);
		return -1;
	}

	/* 
	 * If there's no data or no count, or perhaps the length fed in is less
	 * then the expected length, bail.
	 */
	if (!data || !count || (count < hdr->h_length))
		return 0;

	crc = clu_crc32(data, (count > hdr->h_length) ?
			hdr->h_length : count);

	if (hdr->h_dcrc != crc) {
		fprintf(stderr, "Data CRC32 mismatch; Exp: 0x%08x "
			"Got: 0x%08x\n", hdr->h_dcrc, crc);
		return -1;
	}

	return 0;
}
