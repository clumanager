/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Basic Shared I/O Driver loading routines & No-op functions.
 */
#include <clushared.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>


/*
 * Our actual function pointers.
 */
int (*sh_null)(void) = unimplemented_sh_null;
int (*sh_stat)(const char *pathname, SharedHeader *hdr) = unimplemented_sh_stat;
int (*sh_open)(const char *pathname, int flags) = unimplemented_sh_open;
int (*sh_read)(int fd, void *buf, size_t count) = unimplemented_sh_read;
int (*sh_write)(int fd, const void *buf, size_t count) = unimplemented_sh_write;
int (*sh_read_atomic)(const char *pathname, void *buf, size_t count) =
						unimplemented_sh_read_atomic;
int (*sh_write_atomic)(const char *pathname, const void *buf,
		       size_t count) = unimplemented_sh_write_atomic;
int (*sh_close)(int fd) = unimplemented_sh_close;
char *(*sh_version)(void) = unimplemented_sh_version;

char *sh_desc = NULL;
char *sh_author = NULL;


/*
 * Define our unimplemented-versions of our shared storage 
 */
int
unimplemented_sh_null (void)
{
	printf("Unimplemented NULL function called\n");
	return 0;
}


int
unimplemented_sh_stat (const char * __attribute__ ((unused)) pathname,
		       SharedHeader * __attribute__ ((unused)) hdr)
{
	errno = ENOSYS;
	return -1;
}


int
unimplemented_sh_open (const char * __attribute__ ((unused)) pathname, 
	 	       int __attribute__ ((unused)) flags)
{
	errno = ENOSYS;
	return -1;
}


int
unimplemented_sh_read (int __attribute__ ((unused)) fd,
		       void * __attribute__ ((unused)) buf,
		       size_t __attribute__ ((unused)) count)
{
	errno = ENOSYS;
	return -1;
}


int
unimplemented_sh_write (int __attribute__ ((unused)) fd,
			const void * __attribute__ ((unused)) buf,
			size_t __attribute__ ((unused)) count)
{
	errno = ENOSYS;
	return -1;
}


int
unimplemented_sh_read_atomic (const char * __attribute__ ((unused)) pathname,
			      void * __attribute__ ((unused)) buf,
			      size_t __attribute__ ((unused)) count)
{
	errno = ENOSYS;
	return -1;
}


int
unimplemented_sh_write_atomic (const char * __attribute__ ((unused)) pathname,
			       const void * __attribute__ ((unused)) buf,
			       size_t __attribute__ ((unused)) count)
{
	errno = ENOSYS;
	return -1;
}


int
unimplemented_sh_close (int __attribute__ ((unused)) fd)
{
	errno = ENOSYS;
	return -1;
}


char *
unimplemented_sh_version (void)
{
	return sh_desc;
}


/**
 * Load a cluster storage driver .so file and map all the functions
 * provided to entries in a ClusterStorageDriver structure.
 *
 * @param libpath	Path to file.
 * @return		NULL on failure, or a newly allocated
 *			ClusterStorageDriver on success.
 */
ClusterStorageDriver *
csd_load(const char *libpath)
{
	void *handle;
	ClusterStorageDriver *csdp;
	double (*modversion)(void);

	handle = dlopen(libpath, RTLD_NOW);
	if (!handle) {
		errno = EINVAL;
		return NULL;
	}

	modversion = dlsym(handle, CLU_STORAGE_VERSION_SYM);
	if (!modversion) {
		fprintf(stderr, "API version missing in %s\n", libpath);
		dlclose(handle);
		errno = ENOSYS;
		return NULL;
	}

	if (modversion() != SHARED_API_VERSION) {
		fprintf(stderr, "API version mismatch in %s. %f expected; %f"
			" received.\n", libpath, SHARED_API_VERSION,
			modversion());
		dlclose(handle);
		errno = EINVAL;
		return NULL;
	}

	csdp = malloc(sizeof(*csdp));
	if (!csdp) {
		errno = ENOMEM;
		return NULL;
	}

	memset(csdp, 0, sizeof(*csdp));

	/* Initially, set everything to unimplemented */
	csdp->cs_fops.s_stat = unimplemented_sh_stat;
	csdp->cs_fops.s_open = unimplemented_sh_open;
	csdp->cs_fops.s_read = unimplemented_sh_read;
	csdp->cs_fops.s_write = unimplemented_sh_write;
	csdp->cs_fops.s_read_atomic = unimplemented_sh_read_atomic;
	csdp->cs_fops.s_write_atomic = unimplemented_sh_write_atomic;
	csdp->cs_fops.s_close = unimplemented_sh_close;
	csdp->cs_fops.s_version = unimplemented_sh_version;

	/* Store the handle */
	csdp->cs_private.p_dlhandle = handle;

	/* Grab the init and deinit functions */
	csdp->cs_private.p_load_func = dlsym(handle, CLU_STORAGE_LOAD_SYM);
	csdp->cs_private.p_init_func = dlsym(handle, CLU_STORAGE_INIT_SYM);
	csdp->cs_private.p_deinit_func = dlsym(handle, CLU_STORAGE_UNLOAD_SYM);

	/*
	 * Modules *MUST* have a load function, and it can not fail.
	 */
	if (!csdp->cs_private.p_load_func) {
		fprintf(stderr, "Module load function not found in %s\n",
			libpath);
		free(csdp);
		dlclose(handle);
		return NULL;
	}

	/*
	 * Modules *MUST* have an init function.
	 */
	if (!csdp->cs_private.p_init_func) {
		fprintf(stderr, "Module init function not found in %s\n",
			libpath);
		free(csdp);
		dlclose(handle);
		return NULL;
	}

	if (csdp->cs_private.p_load_func(csdp) < 0) {
		free(csdp);
		return NULL;
	}

	return csdp;
}


/**
 * Initialize a cluster storage driver structure.  This calls the
 * initialization function we loaded in csd_load.
 *
 * @param csdp		Pointer to cluster storage driver to initialize.
 * @param priv		Optional driver-specific private data to copy in
 *			to csdp.
 * @param privlen	Size of data in priv.
 * @return		-1 on failure; 0 on success.
 */
int
csd_init(ClusterStorageDriver *csdp, const void *priv, size_t privlen)
{
	/*
	 * Modules *MUST* have an initialization function, and it can not
	 * fail.
	 */
	if (!csdp->cs_private.p_init_func) {
		errno = ENOSYS;
		return -1;
	}

	if ((csdp->cs_private.p_init_func)(csdp, priv, privlen) == -1) {
		return -1;
	}

	return 0;
}


int
csd_unload(ClusterStorageDriver *csdp)
{
	void *handle;

	if (!csdp)
		return 0;

	/*
	 * Call the deinitialization function, if it exists.
	 */
	if (csdp->cs_private.p_deinit_func &&
	    (csdp->cs_private.p_deinit_func(csdp) == -1)) {
		return -1;
	}

	/* 
	 * Unset any that were default.
	 */
	if (csdp->cs_fops.s_null == sh_null)
		SET_UNIMP(sh_null);
	if (csdp->cs_fops.s_open == sh_open)
		SET_UNIMP(sh_open);
	if (csdp->cs_fops.s_stat == sh_stat)
		SET_UNIMP(sh_stat);
	if (csdp->cs_fops.s_read == sh_read)
		SET_UNIMP(sh_read);
	if (csdp->cs_fops.s_write == sh_write)
		SET_UNIMP(sh_write);
	if (csdp->cs_fops.s_read_atomic == sh_read_atomic)
		SET_UNIMP(sh_read_atomic);
	if (csdp->cs_fops.s_write_atomic == sh_write_atomic)
		SET_UNIMP(sh_write_atomic);
	if (csdp->cs_fops.s_close == sh_close)
		SET_UNIMP(sh_close);
	if (csdp->cs_fops.s_version == sh_version)
		SET_UNIMP(sh_version);

	/*
	 * Unset our pointers
	 */
	if (csdp->cs_desc == sh_desc)
		sh_desc = NULL;
	if (csdp->cs_author == sh_author)
		sh_author = NULL;


	handle = csdp->cs_private.p_dlhandle;
	free(csdp);
	dlclose(handle);

	return 0;
}


/**
 * Use a specific cluster storage driver as the default for global "sh"
 * function calls.  This assigns the global function pointers to the
 * functions contained within the ClusterStorageDriver passed in.
 *
 * @param driver	Storage driver structure to make default.
 */
void
csd_set_default(ClusterStorageDriver *driver)
{
	sh_null = driver->cs_fops.s_null;
	sh_stat = driver->cs_fops.s_stat;
	sh_read = driver->cs_fops.s_read;
	sh_write = driver->cs_fops.s_write;
	sh_open = driver->cs_fops.s_open;
	sh_close = driver->cs_fops.s_close;
	sh_read_atomic = driver->cs_fops.s_read_atomic;
	sh_write_atomic = driver->cs_fops.s_write_atomic;
	sh_version = driver->cs_fops.s_version;

	if (strlen(driver->cs_desc))
		sh_desc = driver->cs_desc;
	if (strlen(driver->cs_author))
		sh_author = driver->cs_author;
}


/**
 * Clear out the default functions
 */
void
csd_reset(void)
{
	SET_UNIMP(sh_null);
	SET_UNIMP(sh_open);
	SET_UNIMP(sh_read);
	SET_UNIMP(sh_write);
	SET_UNIMP(sh_stat);
	SET_UNIMP(sh_read_atomic);
	SET_UNIMP(sh_write_atomic);
	SET_UNIMP(sh_close);
	SET_UNIMP(sh_version);

	sh_desc = NULL;
	sh_author = NULL;
}
