/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Global Cluster Shared Storage/State Initialization/cleanup functions
 */
#include <stdio.h>
#include <clushared.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <xmlwrap.h>

/*
 * make thread safe
 */

static ClusterStorageDriver *__csdp = NULL;

/**
 * Loads cluster storage driver specified in /etc/cluster.xml,
 * initializes it, and makes it the default for cluster operations.
 *
 * @return		0 on success; -1 on failure.
 */
int
shared_storage_init(void)
{
	char need_destroy = 0;
	char *value;
	char qstr[80];
	char filename[1024];

	if (__csdp)
		return 0;

	if (!CFG_Loaded()) {
		need_destroy = 1;
		if (CFG_ReadFile(CLU_CONFIG_FILE) != CFG_OK) {
			fprintf(stderr, "Couldn't read %s\n",
				CLU_CONFIG_FILE);
			return -1;
		}
	}

	snprintf(qstr, sizeof(qstr), "sharedstate%%driver");

	if (CFG_Get(qstr, NULL, &value) == -1) {
		fprintf(stderr, "Error retrieving %s ", qstr);
		if (need_destroy)
			CFG_Destroy();
		return -1;
	}

	/* Try default dir */
	snprintf(filename, sizeof(filename), "%s/%s", IOLIBDIR,
		 value);
	if ((__csdp = csd_load(filename)) != NULL) {
		if (csd_init(__csdp, NULL, 0) != 0) {
			csd_unload(__csdp);
			__csdp = NULL;
			return -1;
		}
		csd_set_default(__csdp);
		if (need_destroy)
			CFG_Destroy();
		return 0;
	}

	/* Try current dir (for testing) */
	snprintf(filename, sizeof(filename), "./%s", value);
	if ((__csdp = csd_load(filename)) != NULL) {
		if (csd_init(__csdp, NULL, 0) != 0) {
			csd_unload(__csdp);
			__csdp = NULL;
			return -1;
		}

		csd_set_default(__csdp);
		if (need_destroy)
			CFG_Destroy();
		return 0;
	}

	if (need_destroy)
		CFG_Destroy();
	return -1;
}


/**
 * Unloads and cleans up shared storage driver.
 *
 * @return		As csd_unload.
 * @see csd_unload
 */
int
shared_storage_deinit(void)
{
	int rv;
       
	rv = csd_unload(__csdp);
	if (rv == 0)
		__csdp = NULL;
	return rv;
}
