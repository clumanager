/*
  Copyright Red Hat, Inc. 2002-2003
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Aligned buffer handling routines.
 *
 * These functions build and free memory areas in an efficient
 * manner with the memory set to 0 upon return (so memset() is not
 * required.
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/mman.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>
#include <sharedstate.h>

static int zfd = -1;		/** File descriptor of /dev/zero */
static int refcnt = 0;		/** Number of times we opened zfd.. */

#define clulog(a, args...) fprintf(stderr, ##args)

extern int sharedPartitionFDinited;
extern int *sharedPartitionFD;

/**
 * This initialization routine must be called prior to any of the
 * others to set things up.
 */
int
initAlignedBufSubsys(void)
{
	int retval;

	if (zfd != -1) {
		return (0);
	}

	zfd = open("/dev/zero", O_RDWR);
	if (zfd == -1) {
		clulog(LOG_ERR,
		       "initAlignedBufStuff: unable to open /dev/zero.\n");
		return (-1);
	}

        retval = fcntl(zfd, F_GETFD, 0);
        if (retval < 0) {
                close(zfd);
                return -1;
        }
        retval |= FD_CLOEXEC;
        if (fcntl(zfd, F_SETFD, retval) < 0) {
                clulog(LOG_ERR, "initAlignedBufSubsys: Unable to set the "
		       "FD_CLOEXEC flag\n");
                close(zfd);
                return -1;
        }

	return (0);
}


/**
 * Called at the time we decide to stop using aligned buffers.  Closes
 * up our file descriptors.
 *
 * @return 		See close(2)
 */
int
deinitAlignedBufSubsys(void)
{
	int ret;

	if (refcnt != 0) 
		clulog(LOG_WARNING, "%d buffers still out...\n",
		       refcnt);

	refcnt = 0;
	ret = close(zfd);
	zfd = -1;
	return (ret);
}


/**
 * mmap()s an aligned buffer into which we can copy data.  An aligned
 * buffer, in our case, is simply a zero-ized buffer which is the
 * size of a system page.
 *
 * @param count		Size to mmap().  Must be a multiple of
 *			the system page size.
 * @return		See mmap(2)
 */
char *
allocAlignedBuf(size_t count)
{
	char *alignedBuffer;

	if (zfd == -1)
		return MAP_FAILED;

	if (count % 512)
		return MAP_FAILED;

	alignedBuffer = mmap(0, count, PROT_READ | PROT_WRITE, MAP_PRIVATE,
			     zfd, 0);

	refcnt++;

	if (alignedBuffer == MAP_FAILED) {
		clulog(LOG_ERR, "allocAlignedBuf: mmap failed.\n");
	}
	return (alignedBuffer);
}


/**
 * Frees an aligned buffer, given the size.
 *
 * @param buf		Aligned buffer to free.
 * @param count		Size of aligned buffer.
 * @return		See munmap(2).
 */
int
freeAlignedBuf(char *buf, size_t count)
{
	int ret;

	refcnt--;
	ret = munmap(buf, count);
	if (ret < 0) {
		clulog(LOG_ERR, "freeAlignedBuf: munmap failed.\n");
	}
	return (ret);
}


