/*
 * Copyright 2003 Red Hat, Inc.
 *
 * License: GPL
 *
 * Uses internals of the cluster raw device driver to exercise shared
 * RAID arrays.  This tool is primarily used for hardware certification.
 *
 * Red Hat Enterprise Linux 3 only.
 */
#include <clushared.h>
#include <cm_api.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sharedstate.h>
#include <namespace.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>
#include <platform.h>
#include <xmlwrap.h>
#include <time.h>

/*
 * Globals ;)
 */
ClusterStorageDriver shared;
extern int *sharedPartitionFD;
time_t start_time = 0, end_time = 0;


/*
 * Private-ish functions we're importing.
 */
int cluster_storage_module_load(ClusterStorageDriver *csdp);
int cluster_storage_module_init(ClusterStorageDriver *csdp);
int name2offset(char *, uint32_t *);
int diskRawWrite(int, char *, int);

typedef struct {
	ulong fullPasses;
	ulong slowPasses;

	ulong writePasses;
	ulong goodWrites;
	ulong errorWrites;

	ulong readPasses;
	ulong goodReads;
	ulong errorReads;

	ulong goodValidates;
	ulong errorValidates;

	ulong trashPasses;
} SharedStats;

SharedStats sharedstats = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

void
printSharedStats(void)
{
	int problems = 0;
	time_t days, hours, mins, secs, elapsed;

	elapsed = time(NULL) - start_time;

	days = elapsed / 86400;
	elapsed -= (days * 86400);
	hours = elapsed / 3600;
	elapsed -= (3600 * hours);
	mins = elapsed / 60;
	secs = elapsed % 60;

	printf("\n%ld iterations run in %d days, %02d:%02d:%02d\n\n",
	       sharedstats.fullPasses, (int)days, (int)hours, (int)mins,
	       (int)secs);

	if (sharedstats.slowPasses) {
		printf("WARNING: This shared storage does not meet the minimum"
		       " performance\nguidelines.\n\n");
		problems++;
	}

	if (sharedstats.writePasses) {
		printf("Write Exercise Passes: %ld\n",
		       sharedstats.writePasses);
		printf("\t* %ld successful writes\n",
		       sharedstats.goodWrites);
		if (sharedstats.errorWrites) {
			printf("\t* %ld failed writes\n",
			       sharedstats.errorWrites);
			problems++;
		}
		printf("\n");
	}

	if (sharedstats.readPasses) {
		printf("Read Exercise Passes: %ld\n", sharedstats.readPasses);
		printf("\t* %ld successful reads\n", sharedstats.goodReads);
		if (sharedstats.errorReads) {
			printf("\t* %ld failed reads\n",
			       sharedstats.errorReads);
			problems++;
		}
		printf("\t* %ld successful data integrity checks\n",
	       	sharedstats.goodValidates);
		if (sharedstats.errorValidates) {
			printf("\t* %ld failed data integrity checks\n",
			       sharedstats.errorValidates);
			problems++;
		}
		printf("\n");
	}

	printf("Overall result: %s\n",!problems?"Pass":"FAIL");
}


/*
 * Set up w/o loading a module.
 */
int
init_shared_storage_local(void)
{
	char *val;

	if (CFG_ReadFile("/etc/cluster.xml") != CFG_OK) {
		perror("CFG_ReadFile");
		return -1;
	}

	if (CFG_Get("sharedstate%driver", NULL, &val) != CFG_OK) {
		fprintf(stderr, "Couldn't determine shared state driver\n");
		return -1;
	}

	if (strcmp(val, "libsharedraw.so")) {
		fprintf(stderr,
			"This must only be used with shared raw devices.\n");
		return -1;
	}

	if (cluster_storage_module_load(&shared) != 0) {
		perror("cluster_storage_module_load");
		return -1;
	}

	if (cluster_storage_module_init(&shared) != 0) {
		perror("cluster_storage_module_init");
		return -1;
	}

	csd_set_default(&shared);

	/* Ignore disk errors for this test */
	CFG_Set("cluster%disk_error_action","ignore");

	return 0;
}


/**
 * Blatant code-rip from shutil.c
 * Calls into the shared state driver to initialize shared storage.
 * @return 0
 */
int
initialize_shared_storage(void)
{
	char name[256];
	char buf[16384];
	int x;

	/* Service status blocks */
	for (x=0; x < MAX_SERVICES; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_SERVICE, x,
			 NS_F_STATUS);

		/* Build the block */
		memset(buf,0,sizeof(buf));
		((SharedServiceBlock *)buf)->sb_magic =
			SERVICE_BLOCK_MAGIC_NUMBER;
		((SharedServiceBlock*)buf)->sb_svcblk.sb_id = x;
		((SharedServiceBlock*)buf)->sb_svcblk.sb_owner = NODE_ID_NONE;
		((SharedServiceBlock*)buf)->sb_svcblk.sb_state =
			SVC_UNINITIALIZED;

		/* Ensure we swap the bytes around! */
		swab_SharedServiceBlock(((SharedServiceBlock *)buf));
		if (sh_write_atomic(name, buf,
				    sizeof(SharedServiceBlock)) == -1)
			return -1;
	}

	/* Shared lock blocks */
	for (x=0; x< MAX_LOCKS; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_LOCK, x,
			 NS_F_STATUS);

		/* Build the block */
		memset(buf,0,sizeof(buf));
		((SharedLockBlock *)buf)->lb_magic = LOCK_BLOCK_MAGIC_NUMBER;
		((SharedLockBlock *)buf)->lb_holder.h_node = -1;

		/* Ensure we swap the bytes around! */
		swab_SharedLockBlock(((SharedLockBlock *)buf));
		if (sh_write_atomic(name, buf, sizeof(SharedLockBlock)) == -1)
			return -1;
	}

	/* Configuration file */
	snprintf(name, sizeof(name), "%s/%s", NS_D_CLUSTER, NS_F_CONFIG);
	if (sh_write_atomic(name, NULL, 0) == -1)
		return -1;

	/* Shared state header - not really used anymore */
	snprintf(name, sizeof(name), "%s/%s", NS_D_CLUSTER, NS_F_CLUHDR);

	memset(buf,0,sizeof(buf));
	((SharedStateHeader *)buf)->ss_magic = SHARED_STATE_MAGIC_NUMBER;
	((SharedStateHeader *)buf)->ss_timestamp = (uint64_t)time(NULL);
	gethostname(((SharedStateHeader *)buf)->ss_updateHost,
		    sizeof(((SharedStateHeader *)buf)->ss_updateHost));

	swab_SharedStateHeader(((SharedStateHeader *)buf));
	if (sh_write_atomic(name, buf, sizeof(SharedStateHeader)) == -1)
		return -1;
	
	/* Partition status blocks */
	for (x=0; x < MAX_PARTS_DISK; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_PARTITION, x,
			 NS_F_STATUS);

		memset(buf,0,sizeof(buf));
		/* Build the block */
		((PartStatusBlock *)buf)->ps_magic = STATUS_BLOCK_MAGIC_NUMBER;

		/* Write the block out */
		swab_PartStatusBlock(((PartStatusBlock *)buf));
		if (sh_write_atomic(name, buf, sizeof(PartStatusBlock)) == -1)
			return -1;
	}

	return 0;
}
/* End rip from shutil.c */


/**
 * Trash *one* of the mirrored copies of a piece of data.
 */
int
trash_block(char *ns_name)
{
	static char garbage[128];
	static int rfd = -1;
	__off64_t offset;
	uint32_t size;
	static int partnum = 0; /* This being static here depends on an
				   odd number of pieces of data in the 
				   shared partitions... */
	int fd = 0;

	while (rfd == -1) {
		rfd = open("/dev/urandom", O_RDONLY);
		if (read(rfd, garbage, sizeof(garbage)) != sizeof(garbage)) {
			close(rfd);
			rfd = -1;
			printf("Retrying read of /dev/urandom...\n");
			sleep(1);
			continue;
		}
		close(rfd);
	}

	offset = name2offset(ns_name, &size);
	fd = sharedPartitionFD[partnum];
	partnum ^= 1;

	if (lseek(fd, offset, SEEK_SET) != offset) {
		fprintf(stderr,"trash_block:lseek(%d,%d,SEEK_SET): %s\n",
			fd, (int)offset, strerror(errno));
		return -1;
	}

	if (diskRawWrite(fd, garbage, sizeof(garbage)) != sizeof(garbage)) {
		fprintf(stderr,"trash_block:write(%d,%p,%d): %s\n",
			(int)fd, garbage, (int)sizeof(garbage),
		        strerror(errno));
		return -1;
	}

	return 0;
}


int
write_pass(void)
{
	char name[256];
	char buf[16384];
	int x;

	sharedstats.writePasses++;

	/* Service status blocks */
	for (x=0; x < MAX_SERVICES; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_SERVICE, x,
			 NS_F_STATUS);

		/* Build the block */
		memset(buf,0,sizeof(buf));
		((SharedServiceBlock *)buf)->sb_magic =
			SERVICE_BLOCK_MAGIC_NUMBER;
		((SharedServiceBlock*)buf)->sb_svcblk.sb_id = x;
		((SharedServiceBlock*)buf)->sb_svcblk.sb_owner = NODE_ID_NONE;
		((SharedServiceBlock*)buf)->sb_svcblk.sb_state =
			SVC_UNINITIALIZED;

		/* Ensure we swap the bytes around! */
		swab_SharedServiceBlock(((SharedServiceBlock *)buf));
		if (sh_write_atomic(name, buf,
				   sizeof(SharedServiceBlock)) == -1)
			sharedstats.errorWrites++;
		else
			sharedstats.goodWrites++;
	}

	/* Shared lock blocks */
	for (x=0; x< MAX_LOCKS; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_LOCK, x,
			 NS_F_STATUS);

		/* Build the block */
		memset(buf,0,sizeof(buf));
		((SharedLockBlock *)buf)->lb_magic = LOCK_BLOCK_MAGIC_NUMBER;
		((SharedLockBlock *)buf)->lb_holder.h_node = -1;

		/* Ensure we swap the bytes around! */
		swab_SharedLockBlock(((SharedLockBlock *)buf));
		if (sh_write_atomic(name, buf, sizeof(SharedLockBlock)) == -1)
			sharedstats.errorWrites++;
		else
			sharedstats.goodWrites++;
	}

	/* Configuration file */
	snprintf(name, sizeof(name), "%s/%s", NS_D_CLUSTER, NS_F_CONFIG);
	if (sh_write_atomic(name, NULL, 0) == -1)
		sharedstats.errorWrites++;
	else
		sharedstats.goodWrites++;

	/* Shared state header - not really used anymore */
	snprintf(name, sizeof(name), "%s/%s", NS_D_CLUSTER, NS_F_CLUHDR);

	memset(buf,0,sizeof(buf));
	((SharedStateHeader *)buf)->ss_magic = SHARED_STATE_MAGIC_NUMBER;
	((SharedStateHeader *)buf)->ss_timestamp = (uint64_t)time(NULL);
	gethostname(((SharedStateHeader *)buf)->ss_updateHost,
		    sizeof(((SharedStateHeader *)buf)->ss_updateHost));

	swab_SharedStateHeader(((SharedStateHeader *)buf));
	if (sh_write_atomic(name, buf, sizeof(SharedStateHeader)) == -1)
		sharedstats.errorWrites++;
	else
		sharedstats.goodWrites++;
	
	/* Partition status blocks */
	for (x=0; x < MAX_PARTS_DISK; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_PARTITION, x,
			 NS_F_STATUS);

		memset(buf,0,sizeof(buf));
		/* Build the block */
		((PartStatusBlock *)buf)->ps_magic = STATUS_BLOCK_MAGIC_NUMBER;

		/* Write the block out */
		swab_PartStatusBlock(((PartStatusBlock *)buf));
		if (sh_write_atomic(name, buf, sizeof(PartStatusBlock)) == -1)
			sharedstats.errorWrites++;
		else
			sharedstats.goodWrites++;
	}

	return 0;
}


int
read_pass(void)
{
	char name[256];
	char buf[16384];
	int x;

	sharedstats.readPasses++;

	/* Service status blocks */
	for (x=0; x < MAX_SERVICES; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_SERVICE, x,
			 NS_F_STATUS);

		if (sh_read_atomic(name, buf,
			           sizeof(SharedServiceBlock)) == -1)
			sharedstats.errorReads++;
		else
			sharedstats.goodReads++;

		swab_SharedServiceBlock(((SharedServiceBlock *)buf));
		if (
		    ((SharedServiceBlock *)buf)->sb_magic ==
			SERVICE_BLOCK_MAGIC_NUMBER &&
		    ((SharedServiceBlock*)buf)->sb_svcblk.sb_id == x &&
		    ((SharedServiceBlock*)buf)->sb_svcblk.sb_owner ==
				NODE_ID_NONE &&
		    ((SharedServiceBlock*)buf)->sb_svcblk.sb_state ==
			SVC_UNINITIALIZED)
			sharedstats.goodValidates++;
		else
			sharedstats.errorValidates++;
	}

	/* Shared lock blocks */
	for (x=0; x< MAX_LOCKS; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_LOCK, x,
			 NS_F_STATUS);

		if (sh_read_atomic(name, buf, sizeof(SharedLockBlock)) == -1)
			sharedstats.errorReads++;
		else
			sharedstats.goodReads++;

		/* Ensure we swap the bytes around! */
		swab_SharedLockBlock(((SharedLockBlock *)buf));
		if (
		  ((SharedLockBlock *)buf)->lb_magic==LOCK_BLOCK_MAGIC_NUMBER &&
		  ((SharedLockBlock *)buf)->lb_holder.h_node == -1)
			sharedstats.goodValidates++;
		else
			sharedstats.errorValidates++;
	}

	/* NO validation for config or header */
	/* Configuration file */
	snprintf(name, sizeof(name), "%s/%s", NS_D_CLUSTER, NS_F_CONFIG);
	if (sh_read_atomic(name, buf, sizeof(buf)) == -1)
		sharedstats.errorReads++;
	else
		sharedstats.goodReads++;

	/* Shared state header - not really used anymore */
	snprintf(name, sizeof(name), "%s/%s", NS_D_CLUSTER, NS_F_CLUHDR);

	if (sh_read_atomic(name, buf, sizeof(SharedStateHeader)) == -1)
		sharedstats.errorReads++;
	else
		sharedstats.goodReads++;
	
	/* Partition status blocks */
	for (x=0; x < MAX_PARTS_DISK; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_PARTITION, x,
			 NS_F_STATUS);

		/* Write the block out */
		if (sh_read_atomic(name, buf, sizeof(PartStatusBlock)) == -1)
			sharedstats.errorReads++;
		else
			sharedstats.goodReads++;

		swab_PartStatusBlock(((PartStatusBlock *)buf));
		if ( ((PartStatusBlock *)buf)->ps_magic == STATUS_BLOCK_MAGIC_NUMBER)
			sharedstats.goodValidates++;
		else
			sharedstats.errorValidates++;
	}

	return 0;
}


int
trash_pass(void)
{
	char name[256];
	int x;

	sharedstats.trashPasses++;

	/* Service status blocks */
	for (x=0; x < MAX_SERVICES; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_SERVICE, x,
			 NS_F_STATUS);
		trash_block(name);
	}

	/* Shared lock blocks */
	for (x=0; x< MAX_LOCKS; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_LOCK, x,
			 NS_F_STATUS);
		trash_block(name);
	}

	/* Configuration file */
	snprintf(name, sizeof(name), "%s/%s", NS_D_CLUSTER, NS_F_CONFIG);
	trash_block(name);

	/* Shared state header - not really used anymore */
	snprintf(name, sizeof(name), "%s/%s", NS_D_CLUSTER, NS_F_CLUHDR);
	trash_block(name);
	
	/* Partition status blocks */
	for (x=0; x < MAX_PARTS_DISK; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_PARTITION, x,
			 NS_F_STATUS);
		trash_block(name);
	}

	return 0;
}

int
test_pass(int readonly)
{
	/*
	 * The 18 and 87 second max time for read-only and read/write passes
	 * are derived from testing high I/O loads on a very slow cluster
	 * with extremely slow disk I/O speeds (about 4MB/sec max...)
	 */
	time_t start;
	time_t maxtime = 18; /* assume readonly */

	start = time(NULL);
	if (!readonly) {
		maxtime = 87; 
		write_pass();
		read_pass();
		trash_pass();
		read_pass(); /* Because of the strange way we trash the disks
				and the way diskRawReadShadow() works,
				it is necessary to do two read-passes to
				ensure that data is fully restored after
				a trash pass. */
	}
	read_pass();

	if ((time(NULL) - start) > maxtime)
		/* BAD. */
		sharedstats.slowPasses++;

	return 0;
}


void
int_handler(int sig)
{
	printSharedStats();
	exit(0);
}


void
usage(char *name)
{
	printf("usage: %s [-m minutes] [-i iterations] [-r] [-v]\n", name);
}


int
main(int argc, char **argv)
{
	int opt;
	int readonly = 0, iterations = 0, verbose = 0;
	extern char *optarg;

	printf("\nThis program is an unsupported test tool used to verify\n");
	printf("correct access to the cluster's shared raw partitions.\n");
	printf("It exercises the same routines used by the cluster, but at\n");
	printf("a much higher frequency.\n\n");
	printf("WARNING: Do not run this when any member of the cluster is\n");
	printf("         running the cluster daemons.\n\n");

	while ((opt = getopt(argc, argv, "m:i:r")) != EOF) {
		switch(opt) {
		case 'm':
			printf("Running for %d minutes\n", atoi(optarg));
			end_time = time(NULL) + (60 * atoi(optarg));
			break;
		case 'i':
			printf("Running %d iterations\n", atoi(optarg));
			iterations = atoi(optarg);
			break;
		case 'r':
			printf("Operating in read-only mode.\n");
			readonly = 1;
			break;
		case 'v':
			printf("Verbose mode enabled.\n");
			verbose = 1;
			break;
		case 'h':
		case 'H':
		case '?':
			usage(argv[0]);
			return 0;
		default:
			usage(argv[0]);
			return 1;
		}
	}

	if (init_shared_storage_local())
		exit(1);

	if (!readonly)
		printf("WARNING: Write-mode enabled.  All data on shared raw "
		       "partitions\n         will be LOST!\n\n");

	printf("Do you wish to (a)bort or (p)roceed ([a]/p)? ");
	if (getc(stdin) != 'p') {
		printf("\nAborted.\n\n");
		exit(0);
	}

	if (!readonly) {
		printf("Initializing shared storage...");
		fflush(stdout);
		if (initialize_shared_storage() != 0) {
			printf("FAILED\n");
			exit(1);
		}
		printf("OK\n");
	}

	if (!iterations && !end_time)
		printf("Note: Running forever; press Ctrl-C to stop.\n");

	signal(SIGINT, int_handler);

	printf("Starting exercises...\n");
	start_time = time(NULL);

	if (!verbose)
		/* Shut the cluster messages up */
		close(STDERR_FILENO);

	while ((!iterations ||
                (iterations && (sharedstats.fullPasses < iterations))) && 
	       (!end_time ||
	        (end_time && (time(NULL) < end_time)))) {
		sharedstats.fullPasses++;
		test_pass(readonly);
	}

	printSharedStats();

	return 0;
}
	

