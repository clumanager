/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 *  Generic utility routines to access shared partition.
 *
 * XXX Needs Doxygenification.
 */
 
/*
 *  $Id: diskutils.c,v 1.7 2003/08/26 21:39:54 lhh Exp $
 *
 *  author: Tim Burke <tburke at redhat.com>
 */
#include <xmlconfig.h>
#include <offsets.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <net/if.h>
#include <if_lookup.h>

int *sharedPartitionFD = NULL;
int sharedPartitionFDinited = 0;
int getPartitionName(int which, char *name, int namelen);

#define clulog(level, args...) fprintf(stderr, ##args)

/*
 * Forward routine declarations.
 */

/*
 * openPartition
 * Called to open the shared state partition with appropriate mode.
 * Returns - (the file descriptor), a value >= 0 on success.
 */
int
openPartition(char *name) {
	int fd;
	int retval;

	/*
	 * Open for synchronous writes to insure all writes go directly
	 * to disk.
	 */
	fd = open(name, O_RDWR | O_SYNC);
	if (fd < 0) {
	    return(fd);
	}
	// Check to verify that the partition is large enough.
	retval = lseek(fd, END_OF_DISK, SEEK_SET);
	if (retval != END_OF_DISK) {
		clulog(LOG_CRIT,
		       "openPartition: cant seek to offset %d, retval=%d.\n",
		       END_OF_DISK, retval);
		clulog(LOG_CRIT,
		       "openPartition: partition may be too small.\n");
#ifdef DEBUG
		perror("Seek failure");
#endif
		return(-1);
	}

        retval = fcntl(fd, F_GETFD, 0);
        if (retval < 0) {
                close(fd);
                return -1;
        }
        retval |= FD_CLOEXEC;
        if (fcntl(fd, F_SETFD, retval) < 0) {
                clulog(LOG_ERR, "openPartition: Unable to set the "
		       "FD_CLOEXEC flag\n");
                close(fd);
                return -1;
        }

	return(fd);
}

/*
 * closePartition
 * Closes the shared state disk partition.
 * Returns - value from close syscall.
 */
int
closePartition(int *fd) {
	int retval;

	if (*fd < 0) {
	    clulog(LOG_ERR, "ERROR: closePartition called when partition is not open.\n");
	    return(-1);
	}
	retval = close(*fd);
	*fd = -1;
	if (retval < 0) {
	    clulog(LOG_ERR, "ERROR: closePartition failed.\n");
	}
	return(retval);
}

/*
 * validatePartitionName
 * Called to verify that the specified device special file representing
 * the partition appears to be a valid device.
 * Returns: 0 - success, 1 - failure
 */
int
validatePartitionName(char *name)
{
	struct stat stat_st, *stat_ptr;
	int fd;
	stat_ptr = &stat_st;

	if (stat(name, stat_ptr) < 0) {
		clulog(LOG_ERR, "Unable to stat %s.\n", name);
#ifdef DEBUG
		perror("stat");
#endif
		return(1);
	}
	/*
	 * Verify that its a block or character special file.
	 */
	if (S_ISCHR(stat_st.st_mode) == 0) {
		//clulog(LOG_WARNING, "%s is not a character special file.\n", name);
	}

	/*
	 * Verify read/write permission.
	 */
	fd = openPartition(name);
	if (fd < 0) {
		clulog(LOG_ERR, "Unable to open %s read/write.\n", name);
		return(1);
	}
	closePartition(&fd);
	return(0);
}


/*
 * Called to retrieve the shared state partition name out of the cluster
 * config file.
 * Parameter: name - a pointer to where the name will be copied into.
 * Returns: 0 -  success, -1 - failure.
 */
int
initSharedFD(int *fd_array)
{
	char partitionName[2][MAXPATHLEN];
	int i;
	
	/* Short circuit */
	if(sharedPartitionFDinited)
		return 0;

	sharedPartitionFD = fd_array;

	for(i = 0; i < 2; i++) {
		if (getPartitionName(i, partitionName[i], MAXPATHLEN) != 0) {
			fprintf(stderr,
			       "initSharedFD: unable to get partition name "
			       "from config file.\n");
			return(-1);
		}

		/*
		 * Perform some validaton check on the specified
		 * partition to weed out configuration errors.
		 */
		if (validatePartitionName(partitionName[i]) != 0) {
			fprintf(stderr, "initSharedFD: unable to validate "
			       "partition %s. Configuration error?\n",
			       partitionName[i]);
			return(-1);
		}

		sharedPartitionFD[i] = openPartition(partitionName[i]);
		if (sharedPartitionFD[i] < 0) {
			fprintf(stderr, "initSharedFD: unable to open "
			       "partition %s.\n", partitionName[i]);
			return(-1);
		}
	}

	sharedPartitionFDinited = 1;
	return 0;
}
	    


/*
 * Retrieve a string representing the name of the shared state disk
 * partition.  There are 2 partitions (for "mirroring" purposes.
 * The "which" parameter designates which one is returned in the "name"
 * parameter.  Returns 0 on success, 1 on failure.
 */
#if 0
static int
get_nodeid_uncached(xmlDocPtr xtree)
{
	struct ifreq ifr;
	int x;
	char qstr[256];
	char *value;

	if (!xtree)
		return -1;

	for (x=0; x<MAX_NODES; x++) {

		snprintf(qstr, sizeof(qstr), "members%%member%d%%name",
			 x);

		if (xtree_get(xtree, qstr, NULL, &value) != 0) {
			continue;
		}

		if (if_lookup(value, &ifr) == 0) {
			return x;
		}
	}

	return -1;
}
#endif


int
getPartitionName(int which, char *name, int namelen)
{
#if 0
	int nodeid = -1;
#endif
	xmlDocPtr xtree = NULL;
	char *value;
	char qstr[80];

	if (xtree_readfile("/etc/cluster.xml", &xtree) == -1) {
		fprintf(stderr, "Couldn't read /etc/cluster.xml!\n");
		return -1;
	}

#if 0
	if ((nodeid = get_nodeid_uncached(xtree)) == -1) {
		fprintf(stderr, "Couldn't determine local member id!\n");
		xmlFreeDoc(xtree);
		return -1;
	}
#endif

	if (which == 0) {
		snprintf(qstr, sizeof(qstr),
			 "sharedstate%%rawprimary");
	} else {
		snprintf(qstr, sizeof(qstr),
			 "sharedstate%%rawshadow");
	}

	if (xtree_get(xtree, qstr, NULL, &value) == -1) {
		fprintf(stderr, "Error retrieving %s ", qstr);
		xmlFreeDoc(xtree);
		return -1;
	}

	snprintf(name, namelen-1, "%s", value);
	xmlFreeDoc(xtree);
	return 0;
}
