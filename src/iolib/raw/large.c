/*
  Copyright Red Hat, Inc. 2002-2003
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Multi-Block I/O to Shared Raw Devices.
 *
 * XXX Needs Doxygenification.
 */
/*
 *  $Id: large.c,v 1.9 2004/09/02 15:16:36 lhh Exp $
 *
 *  author: Tim Burke <tburke at redhat.com>
 *  description: Configuration interface.
 *
 * large.c
 *
 * This file implements the routines used to store the cluster configuration
 * file/database on the shared disk.  Previously we had /etc/cluster.cfg, but
 * it would be probelmatic to keep this file in sync on all cluster members.
 * Rather we store the cluster configuration information on the shared state
 * disk partition.
 *
 * Here the configuration "database" is considered to be binary "blob". So
 * the services offered are to save and retrieve the database as a whole.
 * There are no facilities to change "records" within the "database".
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/mman.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <sharedstate.h>
#include <sharedraw.h>
#include <offsets.h>

static const char *version __attribute__ ((unused)) = "$Revision: 1.9 $";

#define clulog(a, args...) fprintf(stderr, ##args)

extern int sharedPartitionFDinited;
extern int *sharedPartitionFD;
extern int preferredReadPartition;

/* import */
int diskRawRead(int fd, char *buf, int len);
int diskRawWrite(int fd, char *buf, int len);



size_t diskLseekRawWriteLarge(int partition, __off64_t offset, char *data,
			      size_t count);
int diskLseekRawReadLarge(int partition, __off64_t readOffset, char *data,
			  size_t count, uint32_t *length, uint32_t *crc);

/*
 * Returns the length of the current "data" in the configuration database.
 * Return values:
 *	-1 - Unable to read a database header describing the length.  This
 *	     occurs if the shared state disk partition has never been
 *	     initialized or an IO error occurs.
 *	0  - The database is currently initialized, but is empty.
 *      greater than 0 - the length of actual database contents.
 */
size_t
getLargeLength(void)
{
	return 0;
}


/*
 * Returns the checksum stored in the configuration database.
 * This provides the "management layer" a quick way of determining if
 * the database has been modified from an original snapshot which was taken
 * out when the management session began.
 * Return values:
 *	0 - Unable to read a database header to get the checksum.  This
 *	     occurs if the shared state disk partition has never been
 *	     initialized or an IO error occurs.
 *	Otherwise the checksum value is returned.
 */
ulong
getLargeChecksum(void)
{
	return 0;
}


/*
 * Write the service block out to disk.
 * This routine also provides the ability to delete (clear out) the
 * contents of the database by passing in a length parameter of 0.
 * Returns: -1 on IO error, -2 on parameter error, the number of 
 *	    bytes written on success.
 */
size_t
diskRawWriteLarge(__off64_t offset, char *data, size_t count)
{
	int ret;

	if (!sharedPartitionFDinited)
		return -1;

	ret = diskLseekRawWriteLarge(0, offset, data, count);
	if (ret)
		return -1;
	ret = diskLseekRawWriteLarge(1, offset, data, count);
	if (ret)
		return -1;
	else
		return count;
}


size_t
diskLseekRawWriteLarge(int partition, __off64_t offset, char *data,
		       size_t count)
{
	off_t retval_seek;
	size_t write_amount;
	size_t retval_write;
	size_t remain;
	char *dataPtr = data;

	/*
	 * Now write out the data.  The current raw IO bounceIO implementation
	 * here limits to MAX_BOUNCEIO_LENGTH which is the page length of 4K.
	 * Consequently the write of the configuration database must be broken
	 * up into chunks.
	 */
	retval_seek = lseek(sharedPartitionFD[partition], offset, SEEK_SET);
	if (retval_seek != offset) {
		clulog(LOG_ERR, "%s: cant seek to offset %d on fd %d: %s\n",
		       __FUNCTION__, (int)offset,
		       sharedPartitionFD[partition], strerror(errno));
		return (-1);
	}

	remain = count;
	while (remain > 0) {
		if (remain >= sysconf(_SC_PAGESIZE))
			write_amount = sysconf(_SC_PAGESIZE);
		else
			write_amount = remain;

		retval_write =
		    diskRawWrite(sharedPartitionFD[partition], dataPtr,
				 write_amount);
		if (retval_write != write_amount) {
			clulog(LOG_ERR, "%s: header write returned %d.\n",
			       __FUNCTION__, (int)retval_write);
			return (-1);
		}
		dataPtr += write_amount;
		remain -= write_amount;
	}
	/*
	 * Now update the header to indicate the number of bytes written.
	 */

	return 0;
}

/*
 * Called as part of the read/repair facility to read in the entire
 * database contents.
 */
/*
int
readScanWholeLarge(void)
{
	int ret = 0;
	char *buf;

	buf = malloc(SPACE_FOR_CONFIG_DATABASE);
	if (buf == NULL) {
		clulog(LOG_ERR, "readScanWholeLarge: Unable to malloc.\n");
		return -1;
	}
	if (clu_try_lock() == LOCK_SUCCESS) {
		ret = readLarge(buf, SPACE_FOR_CONFIG_DATABASE);
		clu_un_lock();
	}
	free(buf);

	if (ret < 0)
		return -1;
	else
		return 0;
}
*/

/*
 * Read in the configuration database off the shared disk and
 * populate the user's buffer with the data.  This of course requires
 * that the user's buffer is big enough to hold the contents of the
 * database.  In order to facilitate this, the user can first call
 * getLargeLength().  Just to avoid any buffer overflows, the 'count' 
 * parameter describes the user buffer size.
 * Note: the database is read in as a single "blob", there are no facilities
 * for retrieving a portion of the database (i.e. records).
 * 
 * Returns: -1 on IO error, the number of bytes read on success.
 */

#define roundpage(x) ((x + (pageSize - 1)) & ~(pageSize - 1))

size_t
diskRawReadLarge(__off64_t offset, char *data_in, size_t count)
{
	size_t size[2];
	int i;
        uint32_t check_ret[2], crc[2];
	char *data[2];
	int pageSize, good_part;
	size_t mmap_size;

	/* From bugzilla #120085 */
	size[0] = size[1] = 0;

	pageSize = sysconf(_SC_PAGESIZE);
	mmap_size = roundpage(count);
	for (i = 0; i < 2; i++) {
		data[i] = allocAlignedBuf(mmap_size);
		if (data[i] == MAP_FAILED) {
			if (i)
				munmap(data[0], mmap_size);
			return -1;
		}
		check_ret[i] = diskLseekRawReadLarge(i, offset, data[i],
						     count,
						     (uint32_t *)&size[i],
						     &crc[i]);
	}

	if (!check_ret[0] && !check_ret[1] && (crc[0] == crc[1])) {
		good_part = 0;
		goto good;
	}

	if (check_ret[0] && check_ret[1]) {
		clulog(LOG_CRIT,
		       "readLarge: data corrupt on primary and shadow partitions.\n");
		return -1;
	}

	if ((!check_ret[0] && !check_ret[1] && (crc[0] != crc[1]))
	    || check_ret[1]) {
		if (diskLseekRawWriteLarge(1, offset, data[0], size[0]))
			clulog(LOG_CRIT, "readLarge: unable to fix database\n");
		good_part = 0;
		goto good;
	}

	if (check_ret[0]) {
		if (diskLseekRawWriteLarge(0, offset, data[1], size[1]))
			clulog(LOG_CRIT, "readLarge: unable to fix database\n");
		good_part = 1;
		goto good;
	}

	return -1;


      good:
	memcpy(data_in, data[good_part], size[good_part]);
	/* Copy out */
	swab_SharedHeader(((SharedHeader *)data_in));
	for (i = 0; i < 2; i++)
		freeAlignedBuf(data[i], mmap_size);
	return size[good_part];
}


int
diskLseekRawReadLarge(int partition, __off64_t readOffset, char *data,
		      size_t count, uint32_t *length, uint32_t *crc)
{
	char *dataPtr;
	__off64_t retval_seek;
	size_t read_amount;
	size_t retval_read;
	size_t remain;
	SharedHeader *hdrp;
	char *realdata;
	uint32_t datalen;
	

	/*
	 * Now read in the data.  The current raw IO bounceIO implementation
	 * here limits to MAX_BOUNCEIO_LENGTH which is the page length of 4K.
	 * Consequently the read of the configuration database must be broken
	 * up into chunks.
	 */

	retval_seek = lseek(sharedPartitionFD[partition], readOffset, SEEK_SET);
	if (retval_seek != readOffset) {
		clulog(LOG_DEBUG, "%s: cant seek to offset %d.\n",
		       __FUNCTION__, (int)readOffset);
		errno = EACCES;
		return -1;
	}
	remain = count;
	dataPtr = data;
	while (remain > 0) {
		if (remain >= sysconf(_SC_PAGESIZE))
			read_amount = sysconf(_SC_PAGESIZE);
		else
			read_amount = remain;

		retval_read =
		    diskRawRead(sharedPartitionFD[partition], dataPtr,
				read_amount);
		if (retval_read != read_amount) {
			clulog(LOG_DEBUG, "%s: read returned %d.\n",
			       __FUNCTION__, (int)retval_read);
			errno = EACCES;
			return -1;
		}
		dataPtr += read_amount;
		remain -= read_amount;
	}

	/* Decode the header portion so we can run a checksum on it. */
	hdrp = (SharedHeader *)data;
	realdata = (char *)data + sizeof(*hdrp);
	swab_SharedHeader(hdrp);
	datalen = hdrp->h_length;

	if (header_verify(hdrp, realdata, datalen)) {
		swab_SharedHeader(hdrp);
		clulog(LOG_EMERG, "diskLseekRawReadLarge: bad check sum, "
		       "part = %d offset = %d len = %d\n", partition,
		       (int)readOffset, (int)count);
		return -1;
	}

	*length = ((SharedHeader *)data)->h_length + sizeof(SharedHeader);
	if (*length > count)
		*length = count;
	*crc = ((SharedHeader *)data)->h_dcrc;
	swab_SharedHeader(hdrp);

	if (*crc != clu_crc32(realdata, datalen)) {
		/* Bad CRC */
		return -1;
	}
	
	return 0;
}
