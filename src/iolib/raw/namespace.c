/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Cluster Namespace -> Shared SCSI/FC Offset Translation.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <offsets.h>
#include <errno.h>
#include <ctype.h>

#include <namespace.h>
#include <cm_api.h>

#define OFF_INVAL ((__off64_t)-1)


/*
 * In our cluster namespace, we have four items:
 * - The configuration database header, which contains its size and checksum. 
 *   This is similar to an inode on a filesystem, except inodes do not update
 *   with the relevant information we need.
 * - The configuration database itself.  This is an exact copy of
 *   /etc/cluster.xml
 * - Last, but most importantly, the header for the shared storage.
 */
static __off64_t
offset_cluster(char *name, size_t *maxsize)
{
	name++;

	if (strcmp(name, NS_F_CONFIG) == 0) {
		*maxsize = SPACE_FOR_CONFIG_DATABASE;
	       	return OFFSET_CONFIG_DB_DATA;
	} else if (strcmp(name, NS_F_CLUHDR) == 0) {
		*maxsize = HEADER_SIZE;
		return OFFSET_HEADER;
	}

	errno = ENOENT;
	return OFF_INVAL;
}


/*
 * Our service namespace only contains status for each of the services for
 * now.  SERVICE_BLOCK_COUNT is currently the same as MAX_SERVICES, so 100
 * service blocks are valid, named:
 *
 * /services/0/status   to
 * /services/99/status
 */
static __off64_t
offset_services(char *name, size_t *maxsize)
{
	char *slash;
	int32_t svcid = -1;

	if (name[0] != '/') {
		errno = ENOENT;
		return OFF_INVAL;
	}

	/* Skip past leading '/' */
	name++;

	/* Ensure the first character is now a digit */
	if (!isdigit(name[0])) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	slash = strchr(name,'/');
	if (!slash) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	*slash = 0;
	svcid = atoi(name);
	*slash = '/';

	if (strcmp(slash, NS_F_STATUS)) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	if ((svcid < 0) || (svcid > (SERVICE_BLOCK_COUNT - 1))) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	*maxsize = SPACE_PER_SERVICE_BLOCK;
	return (__off64_t)(OFFSET_FIRST_SERVICE_BLOCK +
			 (svcid * SPACE_PER_SERVICE_BLOCK));
	
}


/*
 * Similar to the above.
 * The node blocks are labeled:
 *
 * /partition/0/status  to
 * /partition/1/status
 */
static __off64_t
offset_parts(char *name, size_t *maxsize)
{
	char *slash;
	int32_t nodeid = -1;

	if (name[0] != '/') {
		errno = ENOENT;
		return OFF_INVAL;
	}

	/* Skip past leading '/' */
	name++;

	/* Ensure the first character is now a digit */
	if (!isdigit(name[0])) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	slash = strchr(name,'/');
	if (!slash) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	*slash = 0;
	nodeid = atoi(name);
	*slash = '/';

	if (strcmp(slash, NS_F_STATUS)) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	if ((nodeid < 0) || (nodeid > (STATUS_BLOCK_COUNT - 1))) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	*maxsize = SPACE_PER_STATUS_BLOCK;
	return (__off64_t)(OFFSET_FIRST_STATUS_BLOCK +
			 (nodeid * SPACE_PER_STATUS_BLOCK));
}


/*
 * /locks/0/status
 */
static __off64_t
offset_locks(char *name, size_t *maxsize)
{
	char *slash;
	int32_t lockid = -1;

	if (name[0] != '/') {
		errno = ENOENT;
		return OFF_INVAL;
	}

	/* Skip past leading '/' */
	name++;

	/* Ensure the first character is now a digit */
	if (!isdigit(name[0])) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	slash = strchr(name,'/');
	if (!slash) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	*slash = 0;
	lockid = atoi(name);
	*slash = '/';

	if (strcmp(slash, NS_F_STATUS)) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	if ((lockid < 0) || (lockid > (LOCK_BLOCK_COUNT - 1))) {
		errno = ENOENT;
		return OFF_INVAL;
	}

	*maxsize = SPACE_PER_LOCK_BLOCK;
	return (__off64_t)(OFFSET_FIRST_LOCK_BLOCK +
			 (lockid * SPACE_PER_LOCK_BLOCK));
}


/*
 * This is our only exported function in this file.
 */
__off64_t
name2offset(const char *realname, size_t *maxsize)
{
	__off64_t rv = OFF_INVAL;
	int esv = 0;
	char *name;

	if (!realname || !maxsize) {
		errno = EINVAL;
		return OFF_INVAL;
	}
       
	name = strdup(realname);
	*maxsize = 0;

	if (!name) {
		errno = ENOMEM;
		return OFF_INVAL;
	}

	if (strncmp(name, NS_D_CLUSTER, strlen(NS_D_CLUSTER)) == 0)
		rv = offset_cluster(name + strlen(NS_D_CLUSTER), maxsize);
	else if (strncmp(name, NS_D_SERVICE, strlen(NS_D_SERVICE)) == 0)
		rv = offset_services(name + strlen(NS_D_SERVICE), maxsize);
	else if (strncmp(name, NS_D_PARTITION, strlen(NS_D_PARTITION)) == 0)
		rv = offset_parts(name + strlen(NS_D_PARTITION), maxsize);
	else if (strncmp(name, NS_D_LOCK, strlen(NS_D_LOCK)) == 0)
		rv = offset_locks(name + strlen(NS_D_LOCK), maxsize);
	else { 
		errno = ENOENT;
	}

	esv = errno;
	free(name);
	errno = esv;

	return rv;
}


#ifdef DEBUG
#ifdef STANDALONE
int
main(int argc, char **argv)
{
	__off64_t rv = -1;
	size_t maxsize;

	if (argc < 2) {
		printf("usage: %s <namespace id>\n", argv[0]);
		exit(1);
	}

	rv = name2offset(argv[1], &maxsize);

	if (rv == OFF_INVAL) {
		perror("name2offset");
	} else {
		fprintf(stderr,"%s @ 0x%08x (%d), max size = %d\n", argv[1],
			(int)(rv), (int)(rv), maxsize);
	}

	return 0;
}
#endif
#endif
