/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
 */
/** @file
 * Offsets for use in shared raw/O_DIRECT flat files.
 */
#include <clusterdefs.h>
#include <clu_lock.h>

#define SHADOW_SUCCESS 0
#define SHADOW_NO_ACCESS 1
#define SHADOW_BAD_CHECKSUM 2

/* partition values */

#define PRIMARY 0
#define SHADOW 1

/*
 * When DISKSTATE_DEBUG is defined it enables a set of debug validation checks.
 */
//#define DISKSTATE_DEBUG 1
//
#define MAX_BOUNCEIO_LENGTH		4096		// 1 page

/*
 * Each of the shared state data structures resides at fixed offsets
 * within the partition.  The following are the offsets in bytes.
 */
#define OFFSET_HEADER	0
#define HEADER_SIZE	8192

/*
 * The node specific status structures will be after the shared state header.
 * Specify an offset which will safely exceed the size of the header by
 * an amount likely to meet disk block granularity sizes (which is assumed
 * to be of 512 byte units.
 * XXX - actually these offsets should be dynamically calculated.
 * Following that give each node specific status structure a comfortable
 * amount of space.  The node will write its own status at a location
 * equal to (OFFSET_FIRST_STATUS_BLOCK + (nodeNumber * SPACE_PER_STATUS_BLOCK))
 */
 
#define OFFSET_FIRST_STATUS_BLOCK	(OFFSET_HEADER + HEADER_SIZE)
#define SPACE_PER_STATUS_BLOCK		2048
#define STATUS_BLOCK_COUNT		MAX_NODES_DISK

/*
#define OFFSET_FIRST_STATUS_BLOCK	(OFFSET_HEADER + HEADER_SIZE)
#define SPACE_PER_STATUS_BLOCK		0
#define STATUS_BLOCK_COUNT		0
 */

/*
 * Lots of things.
 */
#define OFFSET_FIRST_LOCK_BLOCK		((OFFSET_FIRST_STATUS_BLOCK) + \
					  (SPACE_PER_STATUS_BLOCK * \
					   STATUS_BLOCK_COUNT))
#define SPACE_PER_LOCK_BLOCK		512
#define LOCK_BLOCK_COUNT		MAX_LOCKS

/*
 * The service descriptions are kept separate from the timestamp info
 * used by the quorum daemon.  There is a separate service description block
 * per service.
 */
#define OFFSET_FIRST_SERVICE_BLOCK	((OFFSET_FIRST_LOCK_BLOCK) + \
					 (SPACE_PER_LOCK_BLOCK * \
					  LOCK_BLOCK_COUNT))
#define SPACE_PER_SERVICE_BLOCK		512
#define SERVICE_BLOCK_COUNT		MAX_SERVICES


/*
 * The cluster configuration "database" (formerly /etc/cluster.cfg) is
 * stored on the shared state partition to avoid synchronization issues
 * inherent in a filesystem based scheme.  Place this after the service
 * descriptions with a little buffer zone for safety.
 * Arbitrarily chose a size of 1MB for the max length.
 */
#define OFFSET_CONFIG_DB_DATA		((OFFSET_FIRST_SERVICE_BLOCK) + \
					 (SPACE_PER_SERVICE_BLOCK * \
					  SERVICE_BLOCK_COUNT))
#define SPACE_FOR_CONFIG_DATABASE	(1024 * 1024)

/*
 * This define describes how big the shared state partition must be.
 * Since the config database is the last thing on the partition, it defines
 * the sizing requirements.
 */
#define END_OF_DISK			((OFFSET_CONFIG_DB_DATA) + \
					 (SPACE_FOR_CONFIG_DATABASE))

/*
 * Defining DO_RAW_BOUNCEIO will cause support for buffer alignment
 * on read and write operations as required by RAW IO. (gack)
 */
#define DO_RAW_BOUNCEIO
#ifdef DO_RAW_BOUNCEIO
#define BOUNCEIO_READ diskRawRead
#define BOUNCEIO_WRITE diskRawWrite
#else // DO_RAW_BOUNCEIO
#define BOUNCEIO_READ read
#define BOUNCEIO_WRITE write
#endif // DO_RAW_BOUNCEIO

