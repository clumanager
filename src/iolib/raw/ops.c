/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Shared Raw Device "Driver"
 */
#include <clushared.h>
#include <offsets.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sharedstate.h>
#include <sharedraw.h>
#include <sys/mman.h>

/* WARNING: diskRawReadShadow decodes the header. */
/* WARNING: diskRawWriteShadow does NOT encode the header. */
/* WARNING: NOT THREAD SAFE. */

#define MODULE_DESCRIPTION "Shared Raw Device Driver v1.2"
#define MODULE_AUTHOR      "Tim Burke, Lon Hohberger"

/*
 * Grab the version from the header file so we don't cause API problems
 */
IMPORT_API_VERSION();

#define OFF_INVAL ((__off64_t)-1)

static int
shared_raw_null(void)
{
	printf(MODULE_DESCRIPTION " NULL function called\n");
	return 0;
}


static int
shared_raw_stat(const char *pathname, SharedHeader *hdr)
{
	__off64_t offset;
	size_t maxsize;
	char *myAlignedBuf;

	offset = name2offset(pathname, &maxsize);
	if (offset == OFF_INVAL) {
		errno = ENOENT;
		return -1;
	}

	myAlignedBuf = allocAlignedBuf(512);
	if (diskRawReadShadow(offset, myAlignedBuf, 512, 1) == -1)
		return -1;

	memcpy(hdr, myAlignedBuf, sizeof(*hdr));
	freeAlignedBuf(myAlignedBuf, 512);

	return 0;
}


static int
shared_raw_read_atomic(const char *pathname, void *buf, size_t count)
{
	__off64_t offset;
	size_t maxsize;
	SharedHeader *hdrp;
	char *data;
	size_t total;
	int rv;

	offset = name2offset(pathname, &maxsize);
	if ((offset == OFF_INVAL) || (count > maxsize)) {
		return -1;
	}

	/*
	 * Calculate the total length of the buffer, including the header.
	 * Raw blocks are 512 byte aligned.
	 */
	total = count + sizeof(SharedHeader);
	if (total < 512)
		total = 512;

	/* Round it up */
	if (total % 512) 
		total = total + (512 * !!(total % 512)) - (total % 512);

	hdrp = (void *)allocAlignedBuf(total);
	if (hdrp == MAP_FAILED) 
		return -1;

	data = (char *)hdrp + sizeof(SharedHeader);

	if (total == 512) {
		rv = diskRawReadShadow(offset, (char *)hdrp, total, 1);
	} else {
		/* XXX large locking function? Additional block for data? */
		rv = diskRawReadLarge(offset, (char *)hdrp, total);
	}
	
	if (rv == -1) {
		return -1;
	}
	
	/* Copy out the data */
	memcpy(buf, data, hdrp->h_length);

	/* Zero out the remainder. */
	if (hdrp->h_length < count) {
		memset(buf + hdrp->h_length, 0,
		       count - hdrp->h_length);
	}

	freeAlignedBuf((char *)hdrp, total);
	return count;
}


static int
shared_raw_write_atomic(const char *pathname, const void *buf, size_t count)
{
	__off64_t offset;
	size_t maxsize;
	SharedHeader *hdrp;
	char *data;
	size_t total = 0, rv = -1;

	offset = name2offset(pathname, &maxsize);
	if (offset == OFF_INVAL)
		return -1;

	if (count >= (maxsize + sizeof(SharedHeader))) {
		errno = ENOSPC;
		return -1;
	}

	/*
	 * Calculate the total length of the buffer, including the header.
	 * Raw blocks are 512 byte aligned.
	 */
	total = count + sizeof(SharedHeader);
	if (total < 512)
		total = 512;

	/* Round it up */
	if (total % 512) 
		total = total + (512 * !!(total % 512)) - (total % 512);

	hdrp = (void *)allocAlignedBuf(total);
	if (hdrp == MAP_FAILED) {
		errno = ENOMEM;
		return -1;
	}

	/* 
	 * Copy the data into our new buffer
	 */
	data = (char *)hdrp + sizeof(SharedHeader);
	memcpy(data, buf, count);

	if (header_generate(hdrp, buf, count) == -1) {
		freeAlignedBuf((char *)hdrp, total);
		return -1;
	}
	swab_SharedHeader(hdrp);

	/* 
	 * Locking must be performed elsewhere.  We make no assumptions
	 * about locking here.  (XXX But we should! It's called 'atomic!')
	 */
	if (total == 512) {
		rv = diskRawWriteShadow(offset, (char *)hdrp, 512);
	} else {
		/* XXX lock? */
		rv = diskRawWriteLarge(offset, (char *)hdrp, total);
	}
	
	freeAlignedBuf((char *)hdrp, total);
	if (rv == -1)
		return -1;
	return count;
}


char *
shared_raw_version(void)
{
	static char version[100];

	snprintf(version, sizeof(version), "%s",
		 MODULE_DESCRIPTION);

	return version;
}


int
cluster_storage_module_load(ClusterStorageDriver *driver)
{
	if (!driver) {
		errno = EINVAL;
		return -1;
	}

	strncpy(driver->cs_desc, MODULE_DESCRIPTION, sizeof(driver->cs_desc));
	strncpy(driver->cs_author, MODULE_AUTHOR, sizeof(driver->cs_author));

	driver->cs_fops.s_null = shared_raw_null;
	driver->cs_fops.s_stat = shared_raw_stat;
	driver->cs_fops.s_read_atomic = shared_raw_read_atomic;
	driver->cs_fops.s_write_atomic = shared_raw_write_atomic;
	driver->cs_fops.s_version = shared_raw_version;

	return 0;
}


int
cluster_storage_module_init(ClusterStorageDriver *driver, void *priv,
			    size_t privlen)
{
	if (!driver) {
		errno = EINVAL;
		return -1;
	}

	if (initAlignedBufSubsys() == -1)
		return -1;

	/*
	 * I don't suppose someone's already initialized our shared file
	 * descriptors, so generally private data passed into this function
	 * should be NULL.  But ok, if someone really thinks they're cool...
	 */
	if (priv && (privlen == (sizeof(int) * 2))) {
		driver->cs_private.p_data = malloc(privlen);
		memcpy(driver->cs_private.p_data, priv, privlen);
		driver->cs_private.p_datalen = privlen;
		return 0;
	}

	/*
	 * We need space for two shared file descriptors.
	 */
	driver->cs_private.p_data = malloc(sizeof(int) * 2);
	((int *)driver->cs_private.p_data)[0] = -1;
	((int *)driver->cs_private.p_data)[1] = -1;
	driver->cs_private.p_datalen = sizeof(int) * 2;

	return initSharedFD((int *)driver->cs_private.p_data);
}


/*
 * Clear out the private data, if it exists.
 */
int
cluster_storage_module_unload(ClusterStorageDriver *driver)
{
	if (driver->cs_private.p_data) {
		close(((int *)driver->cs_private.p_data)[0]);
		close(((int *)driver->cs_private.p_data)[1]);
		free(driver->cs_private.p_data);
	}

	deinitAlignedBufSubsys();
	return 0;
}
