/*
  Copyright Red Hat, Inc. 2002-2003
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR lgPURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Single-block Raw I/O Functions
 */
/*
 *  $Id: rawio.c,v 1.5 2003/08/26 21:39:54 lhh Exp $
 *
 *  author: Tim Burke <tburke at redhat.com>
 *  description: Raw IO Interfaces.
 *
 * The RAW IO code we are using from 2.2.13 requires user buffers and
 * disk offsets to be 512 byte aligned.  So this code consists of a 
 * read and write routine which check to see if the user buffer is 
 * aligned.  If it isn't a temporary aligned buffer is allocated, a data
 * copy is performed along with the IO operation itself.
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <offsets.h>
#include <sharedstate.h>
#include <sharedraw.h>

#define clulog(lvl, args...) fprintf(stderr, ##args)

static int pageSize = MAX_BOUNCEIO_LENGTH;
ulong bounceioReads = 0;
ulong bounceioWrites = 0;
ulong alignedReads = 0;
ulong alignedWrites = 0;
extern int sharedPartitionFDinited;
extern int *sharedPartitionFD;
int preferredReadPartition = -1;

/*
 * Forward routine declarations.
 */
int initSharedFD(int *fd_array);
int diskRawRead(int fd, char *buf, int len);
int diskRawWrite(int fd, char *buf, int len);

int diskLseekRawReadChecksum(int partition, off_t readOffset, char *buf,
			     int len);
int diskRawWriteShadow(__off64_t writeOffset, char *buf, int len);


void
setPreferredReadPartition(int partition)
{
	if (partition == 0 || partition == 1 || partition == -1)
		preferredReadPartition = partition;
}

void
flipPreferredReadPartition(void)
{
	if (preferredReadPartition == -1)
		preferredReadPartition = 0;
	else if (preferredReadPartition == 0 || preferredReadPartition == 1)
		preferredReadPartition ^= 1;
	else
		preferredReadPartition = 0;
}

/*
 * Read in the requested chunk of data.  Since the quorum partition is 
 * shadowed there are 2 possible sources.  Perform a read and if it fails
 * issue a read to the other partition and re-write the data onto the 
 * failed partition as a means of self-healing.  In order to spread around
 * the reads across multiple partitions, we read randomly from either
 * partition first.
 *
 * If it is not safe to make the repair, repair_ok set to 0.
 * This is used for the lock code, which calls diskRawReadShadow().
 * Since the lock is read inside diskRawReadShadow(), there is an unending
 * recursion.
 *
 *  Repair_ok set to 1 means that it is safe, if the lock is held.  For
 *  example, the service manager might read the service state in a lazy way
 *  (no lock) or in a serious way (lock held).
 *  For the former it is not safe to make the repair as it might corrupt the
 *  state for a lock holder.  For the latter it is safe, as the only one to
 *  get corrupt is the lazy reader.
 */
int
diskRawReadShadow(__off64_t readOffset, char *buf, int len, int repair_ok)
{
	int part, tries;
	int status, ret;
	SharedHeader *hdrp;
	
	if ((readOffset < 0) || (len < 0)) {
		clulog(LOG_ERR,
		       "diskRawReadShadow: readOffset=%08x, len=%08x.\n",
		       (int)readOffset, len);
		return (-1);
	}
	if (!sharedPartitionFDinited)
		return -1;

	// First decide with of the 2 partitions to start with.
	if (preferredReadPartition != -1)
		part = preferredReadPartition;
	else
		part = random() & 1;
	ret = diskLseekRawReadChecksum(part, readOffset, buf, len);
	if (ret == SHADOW_NO_ACCESS) {
		clulog(LOG_ERR, "diskRawReadShadow: shadow read failed.\n");
		return -1;
	}
	if (ret == SHADOW_SUCCESS) {
		return 0;
	}

	/* Checksum failure.  */
#if 0
	if (repair_ok && !test_clu_lock_held())
		repair_ok = 0;
#endif

	tries = 0;

	part = part ^ 1;
      top:
	// Read from the other partition
	status =
	    diskLseekRawReadChecksum(part, readOffset, buf, len);
	if (status == SHADOW_NO_ACCESS) {
		clulog(LOG_EMERG,
		       "diskRawReadShadow: no acces to quorum device.\n");
		goto fail;
	}
	if (status == SHADOW_SUCCESS && !repair_ok) {
		clulog(LOG_EMERG, "diskRawReadShadow: skipping repair.\n");
		return 0;
	}

	if (status != SHADOW_SUCCESS) {
		tries++;
		if (tries > 1) {
			clulog(LOG_EMERG,
			       "diskRawReadShadow: checksums bad on both partitions");
			goto fail;
		}
		part = part ^ 1;
		goto top;
	}

	// Repair damaged partition.
	hdrp = (SharedHeader *)buf;
	swab_SharedHeader(hdrp);
	ret = diskRawWriteShadow(readOffset, buf, len);

	if (ret) {
		clulog(LOG_EMERG,
		       "diskRawReadShadow: failed repair offset %d, length %d.\n",
		       (int) readOffset, len);
	}
	
	swab_SharedHeader(hdrp);
	return 0;

      fail:
	return -1;
}


int
diskLseekRawReadChecksum(int partition, off_t readOffset, char *buf, int len)
{
	int ret;
	SharedHeader *hdrp;
	char *data;
	int datalen;

	ret = lseek(sharedPartitionFD[partition], readOffset, SEEK_SET);
	if (ret != readOffset) {
		clulog(LOG_ERR,
		       "diskLseekRawReadChecksum: can't seek to offset %d.\n",
		       (int) readOffset);
		return (SHADOW_NO_ACCESS);
	}

	ret = diskRawRead(sharedPartitionFD[partition], buf, len);
	if (ret != len) {
		clulog(LOG_ERR, "diskLseekRawReadChecksum: aligned read "
		       "returned %d, not %d.\n", ret, len);
		return (SHADOW_NO_ACCESS);
	}

	/* Decode the header portion so we can run a checksum on it. */
	hdrp = (SharedHeader *)buf;
	data = (char *)buf + sizeof(*hdrp);
	swab_SharedHeader(hdrp);
	datalen = hdrp->h_length;

	if (header_verify(hdrp, data, len)) {
		clulog(LOG_EMERG, "diskLseekRawReadChecksum: bad check sum, "
		       "part = %d offset = %d len = %d\n", partition,
		       (int) readOffset, len);
		return SHADOW_BAD_CHECKSUM;
	}

	return SHADOW_SUCCESS;
}


/*
 * The RAW IO implementation requires buffers to be 512 byte aligned.
 * Here we check for alignment and do a bounceio if necessary.
 */
int
diskRawRead(int fd, char *buf, int len)
{
	char *alignedBuf;
	int readret;
	int extraLength;
	int readlen;
	int bounceNeeded = 1;

	if ((((unsigned long) buf & (unsigned long) 0x3ff) == 0) &&
	    ((len % 512) == 0)) {
		bounceNeeded = 0;
	}

	if (bounceNeeded == 0) {
		/* Already aligned and even multiple of 512, no bounceio
		 * required. */
		alignedReads++;
		return (read(fd, buf, len));
	}

	if (len > pageSize) {
		clulog(LOG_ERR,
		       "diskRawRead: not setup for reads larger than %d.\n",
		       pageSize);
		return (-1);
	}
	/*
	 * All IOs must be of size which is a multiple of 512.  Here we
	 * just add in enough extra to accommodate.
	 * XXX - if the on-disk offsets don't provide enough room we're cooked!
	 */
	extraLength = 0;
	if (len % 512) {
		extraLength = 512 - (len % 512);
	}

	readlen = len;
	if (extraLength) {
		readlen += extraLength;
	}

	bounceioReads++;
	alignedBuf = allocAlignedBuf(512);
	if (alignedBuf == MAP_FAILED) {
		return (-1);
	}

	readret = read(fd, alignedBuf, readlen);
	if (readret > 0) {
		if (readret > len) {
			bcopy(alignedBuf, buf, len);
			readret = len;
		} else {
			bcopy(alignedBuf, buf, readret);
		}
	}

	freeAlignedBuf(alignedBuf, 512);
	if (readret != len) {
		clulog(LOG_ERR,
		       "diskRawRead: read err, len=%d, readret=%d\n",
		       len, readret);
	}

	return (readret);
}


int
diskRawWriteShadow(__off64_t writeOffset, char *buf, int len)
{
	off_t retval_seek;
	ssize_t retval_write;
	int i;

	if ((writeOffset < 0) || (len < 0)) {
		clulog(LOG_ERR,
		       "diskRawWriteShadow: writeOffset=%08x, "
		       "len=%08x.\n", (int)writeOffset, len);
		return (-1);
	}

	if (!sharedPartitionFDinited)
		return -1;

	for (i = 0; i < 2; i++) {
		retval_seek =
		    lseek(sharedPartitionFD[i], writeOffset, SEEK_SET);
		if (retval_seek != writeOffset) {
			clulog(LOG_ERR,
			       "diskRawWriteShadow: can't seek to offset %d\n",
			       (int) writeOffset);
			return (-1);
		}
		retval_write = diskRawWrite(sharedPartitionFD[i], buf, len);
		if (retval_write != len) {
			if (retval_write == -1) {
				clulog(LOG_err, "%s: %s\n", __FUNCTION__,
				       strerror(errno));
			}
			clulog(LOG_ERR,
			       "diskRawWriteShadow: aligned write returned %d"
			       ", not %d\n", (int)retval_write, (int)len);
			return (-1);
		}
	}
	return 0;
}

/* Same as diskRawWriteShadow, except to named partition.  Only used by
 * config database, which handles writes to shadow.  Config database is treated
 * differently because its size is greater than one block and hence, not atomic.
 */
int
diskRawWritePartition(int partition, __off64_t writeOffset, char *buf, int len)
{
	off_t retval_seek;
	ssize_t retval_write;
	SharedHeader *hdrp;
	char *data;
	int datalen;

	if ((writeOffset < 0) || (len < 0)) {
		clulog(LOG_ERR,
		       "diskRawWriteShadow: writeOffset=%08x, "
		       " len=%08x.\n", (int)writeOffset, len);
		return (-1);
	}
	if (!sharedPartitionFDinited)
		return -1;

	hdrp    = (SharedHeader *)buf;
	data    = (char *)buf + sizeof(*hdrp);
	datalen = len - (sizeof(*hdrp));

	if (header_generate(hdrp, data, datalen)) {
		clulog(LOG_ERR,
		       "diskRawWriteShadow: unable to write check sum.\n");
		return (-1);
	}
	swab_SharedHeader(hdrp);

	retval_seek = lseek(sharedPartitionFD[partition], writeOffset,
			    SEEK_SET);
	if (retval_seek != writeOffset) {
		clulog(LOG_ERR,
		       "diskRawWriteShadow: can't seek to offset %d\n",
		       (int) writeOffset);
		return (-1);
	}

	retval_write = diskRawWrite(sharedPartitionFD[partition], buf, len);
	if (retval_write != len) {
		clulog(LOG_ERR,
		       "diskRawWriteShadow: aligned write returned %d, not "
		       "%d.\n", (int)retval_write, (int)len);
		return (-1);
	}

	return 0;
}

/*
 * The RAW IO implementation requires buffers to be 512 byte aligned.
 * Here we check for alignment and do a bounceio if necessary.
 */

int
diskRawWrite(int fd, char *buf, int len)
{
	char *alignedBuf;
	int ret;
	int extraLength;
	int writelen;
	int bounceNeeded = 1;

	if ((((unsigned long) buf & (unsigned long) 0x3ff) == 0) &&
	    ((len % 512) == 0)) {
		bounceNeeded = 0;
	}
	if (bounceNeeded == 0) {
		/* Already aligned and even multiple of 512, no bounceio
		 * required. */
		alignedWrites++;
		return (write(fd, buf, len));
	}

	if (len > pageSize) {
		clulog(LOG_ERR,
		       "diskRawWrite: not setup for larger than %d.\n",
		       pageSize);
		return (-1);
	}

	/*
	 * All IOs must be of size which is a multiple of 512.  Here we
	 * just add in enough extra to accommodate.
	 * XXX - if the on-disk offsets don't provide enough room we're cooked!
	 */
	extraLength = 0;
	if (len % 512) {
		extraLength = 512 - (len % 512);
	}

	writelen = len;
	if (extraLength) {
		writelen += extraLength;
	}

	bounceioWrites++;
	alignedBuf = allocAlignedBuf(512);
	if (alignedBuf == MAP_FAILED) {
		return (-1);
	}

	bcopy(buf, alignedBuf, len);
	ret = write(fd, alignedBuf, writelen);
	if (ret > len) {
		ret = len;
	}

	freeAlignedBuf(alignedBuf, 512);
	if (ret != len) {
		clulog(LOG_ERR, "diskRawWrite: write err, len=%d, ret=%dn",
		       len, ret);
	}

	return (ret);
}
