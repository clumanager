/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
 */
/** @file
 * Header used when building the Shared Raw SCSI/FC Driver.
 */
#ifndef __SHAREDRAW_H
#define __SHAREDRAW_H

#include <sys/types.h>
#include <stdint.h>

int diskRawReadShadow(__off64_t readOffset, char *buf, int len, int repair_ok);
int diskRawWriteShadow(__off64_t writeOffset, char *buf, int len);

size_t diskRawWriteLarge(__off64_t offset, char *data, size_t count);
size_t diskRawReadLarge(__off64_t, char *data_in, size_t count);

int initAlignedBufSubsys(void);
int deinitAlignedBufSubsys(void);
char *allocAlignedBuf(size_t count);
int freeAlignedBuf(char *buf, size_t count);

__off64_t name2offset(const char *name, size_t *maxsize);
int initSharedFD(int *fd_array);

#endif
