/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Cluster Namespace -> Shared File System Translation.
 */
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <namespace.h>
#include <sharedstate.h>

#ifndef MAXPATHLEN
#define MAXPATHLEN 1024
#endif

int write_header(int fd, SharedHeader *hdr);

extern char *namespace_path;

int
namespace_create(void)
{
	char buf[MAXPATHLEN];

	if ((mkdir(namespace_path, 0700) == -1) && (errno != EEXIST))
		return -1;

	snprintf(buf, sizeof(buf), "%s%s", namespace_path, NS_D_SERVICE);
	if ((mkdir(buf, 0700) == -1) &&
	    (errno != EEXIST))
		return -1;

	snprintf(buf, sizeof(buf), "%s%s", namespace_path, NS_D_PARTITION);
	if ((mkdir(buf, 0700) == -1) &&
	    (errno != EEXIST))
		return -1;
		
	snprintf(buf, sizeof(buf), "%s%s", namespace_path, NS_D_LOCK);
	if ((mkdir(buf, 0700) == -1) &&
	    (errno != EEXIST))
		return -1;

	snprintf(buf, sizeof(buf), "%s%s", namespace_path, NS_D_CLUSTER);
	if ((mkdir(buf, 0700) == -1) &&
	    (errno != EEXIST))
		return -1;

	return 0;
}


/*
 * Open the file.
 */
int
namespace_open(const char *filename, int flags)
{
	SharedHeader hdr;
	char *realfn, *curdir, *nextdir;
	char fpath[MAXPATHLEN];
	int fd;

	if (!filename || (strlen(filename) == 0)) {
		errno = EINVAL;
		return -1;
	}

	if (strlen(filename) > (sizeof(fpath)-1)) {
		errno = ENAMETOOLONG;
		return -1;
	}

       	realfn = strdup(filename);
	if (!realfn)
		return -1;

	curdir = realfn;
	while (1) {
		nextdir = strchr(curdir,'/');
		if (!nextdir) {
			/*curdir is the filename*/
			break;
		}

		*nextdir = (char)0;
		snprintf(fpath, sizeof(fpath), "%s%s", namespace_path, realfn);

		if ((mkdir(fpath, 0700) == -1) && (errno != EEXIST)) {
			free(realfn);
			return -1;
		}
		*nextdir = '/';
		nextdir++;

		curdir = nextdir;
	}

	free(realfn);
	snprintf(fpath, sizeof(fpath), "%s%s", namespace_path, filename);

	while (1) {
		fd = open(fpath, flags, 0600);
		if (fd != -1)
			return fd;

		if (errno != ENOENT)
			return -1;

		/* 
		 * XXX potential race here ;(
	 	 * Generate new header + file
		 */
		fd = open(fpath, O_WRONLY | O_CREAT, 0600);
		if (fd == -1) {
			return -1;
		}

		if (header_generate(&hdr, NULL, 0) == -1) {
			close(fd);
			return -1;
		}

		header_encode(&hdr);

		if (write_header(fd, &hdr) == -1) {
			close(fd);
			return -1;
		}

		close(fd);
	}

	return fd;
}


#if 0
int
main(int argc, char **argv)
{
	int fd;

	if (argc < 2) {
		printf("usage: %s <namespace_name>\n", );
		return 1;
	}


	if (namespace_create() == -1) {
		perror("namespace_create");
		return 1;
	}

	fd = namespace_open(argv[1], O_RDWR);

	if (fd == -1) {
		perror("namespace_open");
		return 1;
	}

	close(fd);
	return 0;
}
#endif
