/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Shared File System "Driver"
 */
#include <clushared.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <namespace.h>
#include <sharedstate.h>
#include <xmlconfig.h>

int namespace_open(const char *filename, int flags);
#define namespace_close close

#define MODULE_DESCRIPTION "Shared FS Driver v1.0"
#define MODULE_AUTHOR      "Lon Hohberger"

#define SLEEP_TIME	   10000

char *namespace_path = NULL;

/*
 * Grab the version from the header file so we don't cause API problems
 */
IMPORT_API_VERSION();


static int
try_lock(int fd, int type)
{
	struct flock flock;

	memset(&flock,0,sizeof(flock));
	flock.l_type = type;

	if (fcntl(fd, F_SETLK, &flock) == -1)
		return -1;

	return 0;
}


static int
sfs_null(void)
{
	printf(MODULE_DESCRIPTION " NULL function called\n");
	return 0;
}


int
write_header(int fd, SharedHeader *hdrp)
{
	if (write(fd, hdrp, sizeof(*hdrp)) < sizeof(*hdrp))
		return -1;
	return 0;
}


int
read_header(int fd, SharedHeader *hdrp)
{
	if (read(fd, hdrp, sizeof(*hdrp)) < sizeof(*hdrp))
		return -1;

	return 0;
}


static int
sfs_read_atomic(const char *pathname, void *buf, size_t count)
{
	int fd, n, c;
	SharedHeader hdr;

	if (!count)
		return -1;

	((char *)buf)[0] = 0;

	while (1) {
		fd = namespace_open(pathname, O_RDONLY);
		if (fd == -1)
			return -1;

		if (try_lock(fd, F_RDLCK) != -1)
			break;

		if ((errno == EAGAIN) || (errno == EACCES)) {
			namespace_close(fd);
			usleep(SLEEP_TIME);
			continue;
		}
		return -1;
	}

	if (read_header(fd, &hdr)) {
		namespace_close(fd);
		return -1;
	}

	swab_SharedHeader((&hdr));

	c = hdr.h_length;
	if (c > count)
		c = count;

	if (c) {
		n = read(fd, (char *)buf, c);
		if ((n == -1) || (n != c)) {
			namespace_close(fd);
			return -1;
		}
	}
	namespace_close(fd);

	if (header_verify(&hdr, buf, c) == -1) {
		return -1;
	}

	/* 
	 * Even if the file is too small, we fill the user-specified buffer
	 * with NULLs and return the user's requested amount of data.
	 */
	if (c < count)
		memset(buf + c, 0, (size_t)(count - c));

	return count;
}


static int
sfs_write_atomic(const char *pathname, const void *buf, size_t count)
{
	int fd, fd_tmp, n;
	char tmppath[1024];
	char fullpath[1024];
	SharedHeader hdr;

	if (header_generate(&hdr, buf, count) == -1) {
		return -1;
	}

	while (1) {
		fd = namespace_open(pathname, O_WRONLY);
		if (fd == -1)
			return -1;

		if (try_lock(fd, F_WRLCK) != -1)
			break;

		if ((errno == EAGAIN) || (errno == EACCES)) {
			namespace_close(fd);
			usleep(SLEEP_TIME);
			continue;
		}
		return -1;
	}

	snprintf(fullpath, sizeof(fullpath), "%s%s", namespace_path,
		 pathname);
	snprintf(tmppath, sizeof(tmppath), "%s%s.XXXXXX", namespace_path,
		 pathname);
	fd_tmp = mkstemp(tmppath);
	if (fd_tmp == -1) {
		namespace_close(fd);
		return -1;
	}

	header_encode(&hdr);
	if (write_header(fd_tmp, &hdr) == -1) {
		unlink(tmppath);
		close(fd_tmp);
		namespace_close(fd);
		return -1;
	}

	n = write(fd_tmp, buf, count);
	if ((n == -1) || (n != count)) {
		unlink(tmppath);
		close(fd_tmp);
		namespace_close(fd);
		return -1;
	}

	fsync(fd_tmp);
	close(fd_tmp);
	close(fd);
	rename(tmppath, fullpath);
	return 0;
}


static int
sfs_stat(const char *pathname, SharedHeader *hdrp)
{
	int fd;
	SharedHeader hdr;

	fd = namespace_open(pathname, O_RDONLY);
	if (fd == -1) {
		return -1;
	}

	if (read_header(fd, &hdr)) {
		namespace_close(fd);
		return -1;
	}

	namespace_close(fd);
	
	swab_SharedHeader((&hdr));
	if (header_verify(&hdr, NULL, 0) == -1) {
		namespace_close(fd);
		return -1;
	}

	memcpy(hdrp, &hdr, sizeof(*hdrp));

	return 0;
}


char *
sfs_version(void)
{
	return MODULE_DESCRIPTION;
}


int
cluster_storage_module_load(ClusterStorageDriver *driver)
{
	if (!driver) {
		errno = EINVAL;
		return -1;
	}

	strcpy(driver->cs_desc, MODULE_DESCRIPTION);
	strcpy(driver->cs_author, MODULE_AUTHOR);

	driver->cs_fops.s_null = sfs_null;
	driver->cs_fops.s_stat = sfs_stat;
	driver->cs_fops.s_read_atomic = sfs_read_atomic;
	driver->cs_fops.s_write_atomic = sfs_write_atomic;
	driver->cs_fops.s_version = sfs_version;

	return 0;
}


int
cluster_storage_module_init(ClusterStorageDriver *driver, void *priv,
			    size_t privlen)
{
	xmlDocPtr xtree = NULL;
	char *path = NULL;
	struct stat sb;

	if (!driver) {
		errno = EINVAL;
		return -1;
	}

	if (priv && privlen) {
		/*
		 * Path name passed in
		 */
		if (stat(priv, &sb) == -1)
			return -1;

		driver->cs_private.p_data = malloc(privlen + 1);
		driver->cs_private.p_datalen = privlen + 1;
		memset(driver->cs_private.p_data, 0, privlen + 1);
		memcpy(driver->cs_private.p_data, priv, privlen);
		namespace_path = driver->cs_private.p_data;
		return 0;
	}

	/*
	 * Read path from /etc/cluster.xml
	 */
	if (xtree_readfile(CLU_CONFIG_FILE, &xtree) == -1) {
		driver->cs_private.p_datalen = privlen + 1;
		driver->cs_private.p_data = strdup(NAMESPACE_ROOT);
		namespace_path = driver->cs_private.p_data;
		return 0;
	}
	
	if (xtree_get(xtree, "sharedstate%sfspath", NULL, &path) != 0) {
		driver->cs_private.p_datalen = privlen + 1;
		driver->cs_private.p_data = strdup(NAMESPACE_ROOT);
		namespace_path = driver->cs_private.p_data;
		xmlFreeDoc(xtree);
		return 0;
	}

	if (stat(path, &sb) == -1) {
		xmlFreeDoc(xtree);
		errno = ENOENT;
		return -1;
	}

	driver->cs_private.p_datalen = strlen(path) + 1;
	driver->cs_private.p_data = strdup(path);
	namespace_path = driver->cs_private.p_data;
	xmlFreeDoc(xtree);

	return 0;
}


/*
 * Clear out the private data, if it exists.
 */
int
cluster_storage_module_unload(ClusterStorageDriver *driver)
{
	if (driver->cs_private.p_data) {
		if (namespace_path == driver->cs_private.p_data)
			namespace_path = NULL;
		free(driver->cs_private.p_data);
	}

	return 0;
}
