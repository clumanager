/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Cluster Namespace Test Program.
 *
 * XXX Needs Doxygenification.
 */
#include <clushared.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


void
perform_op(char *nsname, char *op, char *data)
{
	char buf[256];
	char bigbuf[1048576];

	if (strcmp(op, "sh_read_atomic") == 0) {
		if (sh_size(nsname) < 256) {
			if (sh_read_atomic(nsname, (void *)buf, sizeof(buf)) == -1) {
				perror("sh_read_atomic");
			}
			fprintf(stderr, "+++DATA\n%s\n---DATA\n", buf);
		} else {
			if (sh_read_atomic(nsname, (void *)bigbuf, sizeof(bigbuf)) == -1) {
				perror("sh_read_atomic");
			}
			fprintf(stderr, "+++DATA\n%s\n---DATA\n", bigbuf);
		}

	} else if (strcmp(op, "sh_write_atomic") == 0) {
		printf("%s (%d bytes [%x])\n",data, strlen(data),strlen(data));
		if (sh_write_atomic(nsname, data, strlen(data)) == -1) {
			perror("sh_write_atomic");
		}
	} else if (strcmp(op, "sh_size") == 0) {
		printf("Size of %s = %d bytes\n", nsname, sh_size(nsname));
	} else {
		printf("not yet...\n");
	}
}


int
main(int argc, char **argv)
{
	ClusterStorageDriver *csdp;

	if (argc < 2) {
		printf("usage: %s <libname> [<nsname> <op> <data>]\n",
		       argv[0]);
		return 1;
	}

	csdp = csd_load(argv[1], NULL, 0);
	if (!csdp) {
		perror("csd_load");
		return 1;
	}

	csd_set_default(csdp);
	if (argc == 4) {
		perform_op(argv[2],argv[3], NULL);
	} else if (argc == 5) {
		perform_op(argv[2],argv[3], argv[4]);
	}

	csd_unload(csdp);

	return 0;
}
