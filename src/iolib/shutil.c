/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Shared State Utility Program.
 */
#include <clushared.h>
#include <cm_api.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sharedstate.h>
#include <namespace.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>
#include <platform.h>
#include <time.h>

#define TIMESTR_LEN 128

char *
human_readable_time(uint64_t *stamp)
{
	struct tm *tm;
	static char timestr[TIMESTR_LEN];
	
	memset(timestr, 0, TIMESTR_LEN);
	tm = localtime((time_t *)stamp);
	if (!tm)
		return NULL;

	strftime(timestr, TIMESTR_LEN, "%H:%M:%S %b %d %Y", tm);
	return timestr;
}
 

/**
 * Print a SharedStateHeader structure.
 *
 * @param hdr		Pointer to header data.
 */
void
print_SharedStateHeader(SharedStateHeader *hdr)
{
	uint64_t u64;

	printf("SharedStateHeader {\n");
	printf("\tss_magic = 0x%08x\n", hdr->ss_magic);

	u64 = hdr->ss_timestamp;
	printf("\tss_timestamp = 0x%08x%08x (%s)\n",
	       (uint32_t)((u64 >> 32)&((uint32_t)~0)),
	       (uint32_t)( u64       &((uint32_t)~0)),
	       human_readable_time(&u64));

	printf("\tss_updateHost = %s\n", hdr->ss_updateHost);
	printf("}\n");
}


/**
 * Print a PartStatusBlock structure.
 *
 * @param nsb		Pointer to status data.
 */
void
print_PartStatusBlock(PartStatusBlock *nsb)
{
	uint64_t u64;

	printf("PartStatusBlock {\n");
	printf("\tps_magic = 0x%08x\n", nsb->ps_magic);
	printf("\tps_nodename = %s\n", nsb->ps_nodename);

	u64 = nsb->ps_timestamp;
	printf("\tps_timestamp = 0x%08x%08x (%s)\n",
	       (uint32_t)((u64 >> 32)&((uint32_t)~0)),
	       (uint32_t)( u64       &((uint32_t)~0)),
	       human_readable_time(&u64));

	printf("\tps_updateNodeNum = %d\n", nsb->ps_updateNodeNum);

	u64 = nsb->ps_incarnationNumber;
	printf("\tps_incarnationNumber = 0x%08x%08x\n",
	       (uint32_t)((u64 >> 32)&((uint32_t)~0)),
	       (uint32_t)( u64       &((uint32_t)~0)));

	printf("\tps_state = %d\n", nsb->ps_state);
	printf("\tps_substate = %d\n", nsb->ps_substate);

	printf("}\n");
}


/**
 * Print a SharedServiceBlock structure.
 *
 * @param sbk		Pointer to service data.
 */
void
print_SharedServiceBlock(SharedServiceBlock *sbk)
{
	uint64_t u64;
	
	printf("SharedServiceBlock {\n");
	printf("\tsb_magic = 0x%08x\n", sbk->sb_magic);
	printf("\tServiceBlock sb_svcblk {\n");
	printf("\t\tsb_id = %d\n", sbk->sb_svcblk.sb_id);
	printf("\t\tsb_owner = %d\n", sbk->sb_svcblk.sb_owner);
	printf("\t\tsb_last_owner = %d\n", sbk->sb_svcblk.sb_last_owner);
	printf("\t\tsb_state = %d\n", sbk->sb_svcblk.sb_state);
	printf("\t\tsb_false_starts = %d\n", sbk->sb_svcblk.sb_false_starts);
	printf("\t\tsb_checks = %d\n", sbk->sb_svcblk.sb_checks);
	printf("\t\tsb_restarts = %d\n", sbk->sb_svcblk.sb_restarts);

	u64 = sbk->sb_svcblk.sb_transition;
	printf("\t\tsb_transition = 0x%08x%08x (%s)\n",
	       (uint32_t)((u64 >> 32)&((uint32_t)~0)),
	       (uint32_t)( u64       &((uint32_t)~0)),
	       human_readable_time(&u64));

	printf("\t}\n");
	printf("}\n");
}


/**
 * Print a SharedLockBlock structure.
 *
 * @param lb		Pointer to lock data.
 */
void
print_SharedLockBlock(SharedLockBlock *lb)
{
	uint64_t u64;

	printf("SharedLockBlock {\n");
	printf("\tlb_magic = 0x%08x\n", lb->lb_magic);
	printf("\tlb_state = %d\n", lb->lb_state);
	printf("\tlb_incarnation = %d\n", lb->lb_incarnation);
	printf("\tlb_holder {\n");
	printf("\t\th_node = %d\n", lb->lb_holder.h_node);
	printf("\t\th_pid = %d\n", lb->lb_holder.h_pid);
	printf("\t\th_master = %d\n", lb->lb_holder.h_master);

	u64 = lb->lb_holder.h_pc;
	printf("\t\th_pc = 0x%08x%08x\n",
	       (uint32_t)((u64 >> 32)&((uint32_t)~0)),
	       (uint32_t)((u64      )&((uint32_t)~0)));

	printf("\t\th_crc = 0x%08x\n", lb->lb_holder.h_crc);
	printf("\t\th_exe = %s\n", lb->lb_holder.h_exe);
	printf("\t}\n");
	printf("}\n");
}


/**
 * Figure out what kind of data we're dealing with and print it out.
 *
 * @param datablock	Data read from shared state.
 * @param length	Length data read.
 */
void
print_contents(char *datablock, size_t length)
{
	uint32_t magic;

	magic = be_swap32(*(uint32_t *)datablock);
	switch (magic) {
	case SHARED_HEADER_MAGIC:
		PRINT_SHARED_HEADER((SharedHeader *)datablock);
		break;
	case SHARED_STATE_MAGIC_NUMBER:
		swab_SharedStateHeader(((SharedStateHeader *)datablock));
		print_SharedStateHeader((SharedStateHeader *)datablock);
		break;
	case STATUS_BLOCK_MAGIC_NUMBER:
		swab_PartStatusBlock(((PartStatusBlock *)datablock));
		print_PartStatusBlock((PartStatusBlock *)datablock);
		break;
	case SERVICE_BLOCK_MAGIC_NUMBER:
		swab_SharedServiceBlock(((SharedServiceBlock *)datablock));
		print_SharedServiceBlock((SharedServiceBlock *)datablock);
		break;
	case LOCK_BLOCK_MAGIC_NUMBER:
		swab_SharedLockBlock(((SharedLockBlock *)datablock));
		print_SharedLockBlock((SharedLockBlock *)datablock);
		break;
	default:
		printf("Raw Data {\n");
		fflush(stdout);
		write(STDOUT_FILENO, datablock, length);
		fflush(stdout);
		printf("}\n");
		fflush(stdout);
	}
}


/**
 * Calls into the shared state driver to initialize shared storage.
 * @return 0
 */
int
initialize_shared_storage(void)
{
	char name[256];
	char buf[16384];
	int x;

	/* Service status blocks */
	for (x=0; x < MAX_SERVICES; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_SERVICE, x,
			 NS_F_STATUS);
		printf("Initializing %s...\n", name);

		/* Build the block */
		memset(buf,0,sizeof(buf));
		((SharedServiceBlock *)buf)->sb_magic =
			SERVICE_BLOCK_MAGIC_NUMBER;
		((SharedServiceBlock*)buf)->sb_svcblk.sb_id = x;
		((SharedServiceBlock*)buf)->sb_svcblk.sb_owner = NODE_ID_NONE;
		((SharedServiceBlock*)buf)->sb_svcblk.sb_state =
			SVC_UNINITIALIZED;

		/* Ensure we swap the bytes around! */
		swab_SharedServiceBlock(((SharedServiceBlock *)buf));
		sh_write_atomic(name, buf, sizeof(SharedServiceBlock));
	}

	/* Shared lock blocks */
	for (x=0; x< MAX_LOCKS; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_LOCK, x,
			 NS_F_STATUS);
		printf("Initializing %s...\n", name);

		/* Build the block */
		memset(buf,0,sizeof(buf));
		((SharedLockBlock *)buf)->lb_magic = LOCK_BLOCK_MAGIC_NUMBER;
		((SharedLockBlock *)buf)->lb_holder.h_node = -1;

		/* Ensure we swap the bytes around! */
		swab_SharedLockBlock(((SharedLockBlock *)buf));
		sh_write_atomic(name, buf, sizeof(SharedLockBlock));
	}

	/* Configuration file */
	snprintf(name, sizeof(name), "%s/%s", NS_D_CLUSTER, NS_F_CONFIG);
	printf("Initializing %s...\n", name);
	sh_write_atomic(name, NULL, 0);

	/* Shared state header - not really used anymore */
	snprintf(name, sizeof(name), "%s/%s", NS_D_CLUSTER, NS_F_CLUHDR);
	printf("Initializing %s...\n", name);

	memset(buf,0,sizeof(buf));
	((SharedStateHeader *)buf)->ss_magic = SHARED_STATE_MAGIC_NUMBER;
	((SharedStateHeader *)buf)->ss_timestamp = (uint64_t)time(NULL);
	gethostname(((SharedStateHeader *)buf)->ss_updateHost,
		    sizeof(((SharedStateHeader *)buf)->ss_updateHost));

	swab_SharedStateHeader(((SharedStateHeader *)buf));
	sh_write_atomic(name, buf, sizeof(SharedStateHeader));
	
	/* Partition status blocks */
	for (x=0; x < MAX_PARTS_DISK; x++) {
		snprintf(name, sizeof(name), "%s/%d%s", NS_D_PARTITION, x,
			 NS_F_STATUS);
		printf("Initializing %s...\n", name);

		memset(buf,0,sizeof(buf));
		/* Build the block */
		((PartStatusBlock *)buf)->ps_magic = STATUS_BLOCK_MAGIC_NUMBER;

		/* Write the block out */
		swab_PartStatusBlock(((PartStatusBlock *)buf));
		sh_write_atomic(name, buf, sizeof(PartStatusBlock));
	}

	return 0;
}


/**
 * Prints metadata about filename.
 *
 */
int
print_info(char *filename)
{
	int rv;
	SharedHeader hdr;
	uint64_t u64;

	rv = sh_stat(filename, &hdr);
	if (rv == -1) {
		perror("sh_stat");
		return -1;
	}

	printf("Metadata information for %s\n\n", filename);
	printf("Data Length:   %d bytes\n", hdr.h_length);
	printf("Data CRC:      0x%08x\n", hdr.h_dcrc);
	printf("Header CRC:    0x%08x\n", hdr.h_hcrc);

	/*
	u64 = hdr.h_view;
	printf("Version:       0x%08x%08x\n",
	       (uint32_t)((u64 >> 32)&((uint32_t)~0)),
	       (uint32_t)((u64      )&((uint32_t)~0)));
	 */

	u64 = hdr.h_timestamp;
	printf("Last modified: %s\n", human_readable_time(&u64));

	return 0;
}


/**
 * Reads a configuration file and stores it into shared state.
 *
 * @param filename	File to import to shared state.
 */
int
store_config(char *filename)
{
	int fd;
	struct stat st;
	char *buf;

	fd = open(filename, O_RDONLY);
	if (fd == -1) {
		perror("open");
		return -1;
	}

	fstat(fd, &st);

	buf = malloc(st.st_size+1);
	memset(buf,0,st.st_size+1);
	if (read(fd, buf, st.st_size) != st.st_size) {
		fprintf(stderr,"Couldn't read %s in its entirety\n", filename);
		free(buf);
		return -1;
	}
	close(fd);

	if (sh_write_atomic(NS_D_CLUSTER "/" NS_F_CONFIG, buf,
			    st.st_size) == -1){
		fprintf(stderr, "Failed writing to shared storage: %s\n",
			strerror(errno));
		free(buf);
		return -1;
	}

	free(buf);
	return 0;
}


/**
 * Reads shared config and exports it to local storage.
 *
 * @param filename	File to export.
 */
int
retrieve_config(char *filename)
{
	int fd = STDOUT_FILENO;
	SharedHeader hdr;
	uint32_t sz;
	char *buf;

	if (strcmp(filename, "-")) {
		fd = open(filename, O_WRONLY|O_CREAT|O_TRUNC, 0600);
		if (fd == -1) {
			perror("open");
			return -1;
		}
	}

	sh_stat(NS_D_CLUSTER "/" NS_F_CONFIG, &hdr);
	sz = hdr.h_length;
	buf = malloc(sz);
	memset(buf,0,sz);
	if (sh_read_atomic(NS_D_CLUSTER "/" NS_F_CONFIG, buf, sz) != sz) {
		fprintf(stderr,
		        "Couldn't read shared config in its entirety\n");
		free(buf);
		return -1;
	}

	if (write(fd, buf, sz) == -1){
		fprintf(stderr, "Failed writing to %s: %s\n",
			filename,
			strerror(errno));
		free(buf);
		return -1;
	}

	fsync(fd);
	close(fd);
	free(buf);
	return 0;
}



/**
 * Display usage.
 */
void
usage(char *name)
{
	fprintf(stderr,
"usage: %s [options]\n"
" -v                 Show the driver version\n"
" -f <library_path>  Display information about library_path\n"
" -p <namespace_id>  Print data contents in namespace_id\n"
" -d <namespace_id>  Dump raw data in namespace_id to stdout\n"
" -m <namespace_id>  Print metadata information about namespace_id\n"
" -s <filename>      Store contents of filename into shared configuration\n"
" -r <filename>      Copy shared configuration contents into filename\n"
" -i                 Initialize contents of shared state\n"
" -?, -h             Help!\n\n", name);
}


/**
 * Driver function.  Parses command line arguments, etc.
 */
int
main(int argc, char **argv)
{
	int opt;
	int sz;
	extern char *optarg;
	char *buf;
	ClusterStorageDriver *csd = NULL;
	SharedHeader hdr;

	if (argc < 2) {
		usage(basename(argv[0]));
		return 1;
	}

	while ((opt = getopt(argc, argv, "r:vif:d:m:p:s:?hH-")) != EOF) {
		switch (opt) {
		case 'v':
			/* Initialize */
			if (shared_storage_init() == -1) {
				perror("shared_storage_init");
				return -1;
			}
			/* Print the version */
			printf(sh_version());
			printf("\n");
			break;
		case 'i':
			/* Initialize */
			if (shared_storage_init() == -1) {
				perror("shared_storage_init");
				return -1;
			}
			/* Initialize the shared storage */
			initialize_shared_storage();
			store_config(CLU_CONFIG_FILE);
			break;
		case 'm':
			/* Initialize */
			if (shared_storage_init() == -1) {
				perror("shared_storage_init");
				return -1;
			}

			print_info(optarg);
			break;
			
		case 'p':
		case 'd':
			/* Initialize */
			if (shared_storage_init() == -1) {
				perror("shared_storage_init");
				return -1;
			}
			/* Print contents */
			sz = sh_stat(optarg, &hdr);
			if (sz == -1) {
				perror("sh_stat");
				return -1;
			}
			sz = hdr.h_length;
			buf = malloc(sz+1);
			memset(buf,0,sz);
			sh_read_atomic(optarg, buf, sz);

			if (opt == 'p') {
				printf("%s is %d bytes long\n", optarg, sz);
				print_contents(buf, sz);
			} else {
				write(STDOUT_FILENO, buf, sz);
			}
			free(buf);
			break;
		case 's':
			/* Initialize */
			if (shared_storage_init() == -1) {
				perror("shared_storage_init");
				return -1;
			}
			/* store config from file */
			store_config(optarg);
			break;
		case 'r':
			/* Initialize */
			if (shared_storage_init() == -1) {
				perror("shared_storage_init");
				return -1;
			}
			/* retrieve config from shared db */
			retrieve_config(optarg);
			break;
		case 'f':
			/* 
			 * Display stuff from a specific file
			 */
			csd = csd_load(optarg);
			if (!csd) {
				perror("csd_load");
				return -1;
			}
			printf("Path:        %s\n", optarg);
			printf("Description: %s\n", csd->cs_desc);
			printf("Author:      %s\n", csd->cs_author);
			csd_unload(csd);
			return 0;
		case '?':
		case '-':
		case 'h':
		case 'H':
		default:
			usage(basename(argv[0]));
			return 1;
		}
	}
	
	shared_storage_deinit();
	return 0;

}
