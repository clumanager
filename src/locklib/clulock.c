/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Upper layer in the locking synchronization primitives.
 *
 *  $Id: clulock.c,v 1.26 2009/02/17 14:46:31 lhh Exp $
 *
 *  author:   Dave Winchell <winchell@missioncriticallinux.com>
 *  modified: Lon H. Hohberger <lhh at redhat.com>
 *            - Optimized the clu_lock() code paths
 *            - Eliminated duplicate logic for clu_lock() and clu_try_lock()
 *            - Removed FAULT()s - returns errors in most cases.
 */


#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/syslog.h>
#include <sys/mman.h>
#include <signal.h>
#include <stdlib.h>
#include <errno.h>

#include <xmlwrap.h>
#include <clulog.h>
#include <msgsvc.h>
#include <clushared.h>
#include <sharedstate.h>
#include <namespace.h>
#include <clu_lock.h>
#include <quorumparams.h>
#include <membership.h>
#include <platform.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef CLULOCKDIR
#warn "CLULOCKDIR not defined?!"
#define CLULOCKDIR "/var/lock/clumanager"
#endif

#define CLUSTER_LOCKSTR CLULOCKDIR "/lock.%d"

/* 
 * Function Prototypes - Exported functions 
 */
int	clu_lock(int lockid);
int	clu_try_lock(int lockid);
int	clu_unlock(int lockid);


/*
 * Function Prototypes - Private functions
 */
int	_clu_process_try_lock(void *pc, int lockid);
void	_clu_process_unlock(int lockid);
void	_clu_unlock_node(int lockid);
int	_clu_try_lock_cluster(void *pc, int lockid);
int	_clu_unlock(void *pc, int lockid);
int	_clu_lock_init(int lockid);
int	_clu_lock_init_if_needed(int lockid);

/* Just in case we forget that one microsecond is 0.000001 seconds... */
#define USEC_PER_SEC 1000000

/*
 * Globals
 */
int _clu_node_id = -1;
char _clu_prog_name[MAX_DAEMON_NAME_LEN];
uint32_t _clu_prog_crc = 0;
int _locks_initialized = 0;
int delay_shift = 16;
pid_t _clu_init_pid = -1;
int _keeper = -1;

/*
 * Lock timeout value
 */
static int _clu_lock_timeout;
static uint64_t lock_quorum_view;

static int
lock_keeper(void)
{
	int fd;
	generic_msg_hdr resp;

	fd = msg_open_local(PROCID_CLULOCKD, 10);
	if (fd == -1) {
		fd = msg_open(PROCID_CLULOCKD, -1);
		if (fd == -1) {
			errno = EAGAIN;
			return -1;
		}
	}

	if (msg_send_simple(fd, LOCK_MASTER_QUERY, 0, 0) == -1) {
		msg_close(fd);
		errno = EAGAIN;
		return -1;
	}

	if (msg_receive_timeout(fd, &resp, sizeof(resp), 15) != sizeof(resp)) {
		msg_close(fd);
		errno = EAGAIN;
		return -1;
	}

	msg_close(fd);

	swab_generic_msg_hdr(&resp);
	if (resp.gh_command == LOCK_LOST_QUORUM) {
		errno = ENOLCK;
		return -1;
	}
	
	if (resp.gh_command == LOCK_NO_QUORUM) {
		errno = EAGAIN;
		return -1;
	}

	return resp.gh_arg1;
}


int
__lock_send(lock_request_t *lreq)
{
	generic_msg_hdr response;
	int fd = -1;

	if (_keeper == -1) {
		_keeper = lock_keeper();
		if (_keeper == -1) {
			/* return errno from lock_keeper */
			return -1;
		}
	}

	if (_keeper == _clu_node_id) /* local */ {
		fd = msg_open_local(PROCID_CLULOCKD, 10);
	}

	if (_keeper != _clu_node_id || fd < 0) {
		/* remote keeper or local comms failed */
		fd = msg_open(PROCID_CLULOCKD, _keeper);
	}

	if (fd == -1) {
		if (errno != ECONNREFUSED) {
			/* In ECONNREFUSED case, it's probably just that
			   the lock master was shut down (cleanly) but
			   no quorum transition has taken place yet;
			   it will fix itself in a few seconds */
			clulog(LOG_DEBUG,
			       "Couldn't connect to member #%d: %s\n",
		       		_keeper, strerror(errno));
		}
		errno = EAGAIN;
		return -1; /* retry */
	}

	if (msg_send(fd, lreq, sizeof(*lreq)) != sizeof(*lreq)) {
		clulog(LOG_ERR, "Couldn't send lock message to %d\n");
		msg_close(fd);
		errno = EAGAIN;
		return -1; /* retry */
	}

	if (msg_receive_timeout(fd, &response, sizeof(response), 15) !=
	    sizeof(response)) {
		clulog(LOG_DEBUG, "Invalid reply; retrying\n");
		msg_close(fd);
		errno = EAGAIN;
		return -1; /* retry */
	}

	swab_generic_msg_hdr(&response);
	msg_close(fd);           
	return response.gh_command;
}


void
lock_set_quorum_view(uint64_t quorum_view)
{
	lock_quorum_view = quorum_view;
}


int
lock_send(void *pc, int lockid, int cmd)
{
	lock_request_t req;
	int ret;
	SharedLockBlock *lock = &req.lr_lock;

	memset(&req, 0, sizeof(req));

	req.lr_hdr.gh_magic = GENERIC_HDR_MAGIC;
	req.lr_hdr.gh_command = cmd;
	req.lr_hdr.gh_length = sizeof(req);

	lock->lb_magic = LOCK_BLOCK_MAGIC_NUMBER;
	lock->lb_lockid = lockid;
	lock->lb_state = 0;

	/*
	 * XXX chopping up a 64-bit integer into a 32-bit one.
	 * This will help preserve rolling upgrade.
	 */
	lock->lb_incarnation = (uint32_t)lock_quorum_view;

	lock->lb_holder.h_node = _clu_node_id;
	lock->lb_holder.h_pid = (uint32_t)getpid();
	lock->lb_holder.h_pc = ptrcast_uint64_t(pc);
	lock->lb_holder.h_crc = _clu_prog_crc;
	memset(lock->lb_holder.h_exe, 0, sizeof(lock->lb_holder.h_exe));
	memcpy(lock->lb_holder.h_exe, _clu_prog_name, strlen(_clu_prog_name));

	swab_lock_request_t(&req);

	while (1) {
		ret = __lock_send(&req);
		switch(ret) {
		case LOCK_ACK:
			return 0;

		case LOCK_NOT_MASTER:
			_keeper = lock_keeper();
			sleep(1); /* retry? */
			continue;

        	case LOCK_NACK:
			errno = EBUSY;
			return -1;
		case LOCK_NO_QUORUM:
			errno = EAGAIN;
			return -1;
		case LOCK_LOST_QUORUM:
			errno = ENOLCK;
		case -1:
			if (errno == EAGAIN) {
				/* Maybe keeper changed */
				_keeper = lock_keeper();
				usleep(100000);
				continue;
			}
			return -1;
		default:
			errno = EINVAL;
			return -1;
		}
        }
}


int
lock_lock(void *pc, int lock)
{
	return lock_send(pc, lock, LOCK_TRYLOCK);
}


int
lock_unlock(void *pc, int lock)
{
	int ret;

	do { 
		/* BZ 168665: retry if we time out during unlock.
		   The lock_keeper() will query the lock daemon, which
		   will handle the case that the master has, in fact, died.
		   Otherwise, retry forever. */
		ret = lock_send(pc, lock, LOCK_UNLOCK);

		/* Retry in all cases unless we've lost quorum */
		if (ret < 0 && errno != ENOLCK) {
			/*sleep(1);*/
			continue;
		}
	} while (0);

	return ret;
}


/*
 * Take the cluster lock.  Only returns errors if the lock is uninitialized
 * and we fail to initialize it.  If we time out trying to take the lock,
 * we STONITH...
 */
int
clu_lock(int lockid)
{
	int wait_shoot = 0;
	int wait_cur;
	int rv;
	void *pc;

	if (_clu_lock_init_if_needed(lockid) < 0) {
		return -1;
	}

	pc = __builtin_return_address(0);

	while (((rv = _clu_try_lock_cluster(pc, lockid)) == -1) && 
	       ((errno == EAGAIN) || (errno == EBUSY) || (errno == EACCES))) {
		wait_cur = random() & ((1<<delay_shift)-1);
		usleep(wait_cur);
		wait_shoot += wait_cur;
	
		if (wait_shoot < _clu_lock_timeout)
			continue;

		clulog(LOG_WARNING, "Starvation on Lock #%d!", lockid);
		
		/*
		 * ok, here we need to send a message to clulockd and
		 * find out what the hell is going on with the lock. 
		 *
		 * the lock daemon should print out the lock state!
		 * XXX
		 */
		wait_shoot = 0;
	}

	return rv;
}


int
clu_try_lock(int lockid)
{
	void *pc;
	pc = __builtin_return_address(0);
	return _clu_try_lock_cluster(pc, lockid);
}


int
clu_unlock(int lockid)
{
	void *pc = __builtin_return_address(0);

	return _clu_unlock(pc, lockid);
}


int
_clu_try_lock_cluster(void *pc, int lockid)
{
	/* Clear errno */
	errno = 0;

	if (lock_lock(pc, lockid) != 0) {
		return -1;
	}

	return 0;
}


int
_clu_unlock(void *pc, int lockid)
{
	if (lock_unlock(pc, lockid) == -1)
		return -1;

	/* avoids lock starvation */
	usleep(random() & ((1<<delay_shift)-1));

	return 0;
}


/*
 * [re]configure the lock timeout interval; generally called from a daemon's
 * sighup handler 
 */
void
clu_lock_hup(void)
{
	struct timeval tv = DFLT_INTERVAL;
	int ret,
	    pingint,
	    tko = DFLT_TKO_COUNT;
	char *val;

	pingint = (tv.tv_sec * 1000000 + tv.tv_usec);
	/*
	 * Get our membership intervals so we can calculate a timeout
	 */
	if (CFG_Get((char *)CFG_MEMB_INTERVAL, NULL, &val) == CFG_OK)
		if (val && (ret = atoi(val)))
			pingint = ret;

	if (CFG_Get((char *)CFG_MEMB_TKO_COUNT, NULL, &val) == CFG_OK)
		if (val && (ret = atoi(val)))
			tko = ret;

	_clu_lock_timeout = 2 * (pingint * tko);
}


static void
read_my_data(void)
{
	char fn[MAX_DAEMON_NAME_LEN];
	int rv, fd;
	char *mf = NULL;
	struct stat buf;

	snprintf(fn, sizeof(fn), "/proc/%d/exe", getpid());
	memset(_clu_prog_name, 0, sizeof(_clu_prog_name));
	rv = readlink(fn, _clu_prog_name, (sizeof(_clu_prog_name)-1));

	if (rv == -1) {
		snprintf(_clu_prog_name, sizeof(_clu_prog_name), "ERR:%s",
			 strerror(errno));
		return;
	}

	fd = open(_clu_prog_name, O_RDONLY);
	if (fd == -1) {
		clulog(LOG_ERR, "Couldn't open myself: %s\n", strerror(errno));
		return;
	}

	if (fstat(fd, &buf) != 0) {
		clulog(LOG_ERR, "Couldn't stat myself: %s\n", strerror(errno));
		close(fd);
		return;
	}

	mf = mmap(NULL, buf.st_size, PROT_READ, MAP_PRIVATE, fd,
		  (off_t)0);
	if (mf == MAP_FAILED) {
		clulog(LOG_ERR, "Couldn't mmap myself: %s\n", strerror(errno));
		close(fd);
		return;
	}

	_clu_prog_crc = clu_crc32(mf, buf.st_size);
	munmap(mf, buf.st_size);
	close(fd);
}


/* init code.  */
int
_clu_lock_init(int lockid)
{
	if (_clu_node_id == -1) {
		_clu_node_id = memb_local_id();
		if(_clu_node_id < 0) {
			clulog(LOG_DEBUG, "%s: unable to get local member "
			       "ID; cluster not running?\n", __FUNCTION__);
			return -1;
		}		

		/* Calculate lock timer */
		clu_lock_hup();
	}

	return 0;
}


int
_clu_lock_init_if_needed(int lockid)
{
	pid_t pid;

	/*
	 * full init - XXX pthreads?
	 */
	if (!_locks_initialized) {
		clu_lock_hup();
		read_my_data();	
	}

	if (_clu_lock_init(lockid) < 0)
		return -1;

	pid = getpid();
	/* lock is stripped in the child */
	if (pid != _clu_init_pid) {
		_clu_init_pid = pid;
	}

	_locks_initialized = 1;

	return 0;
}

