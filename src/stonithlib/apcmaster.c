/*
 *
 *  Copyright 2001 Mission Critical Linux, Inc.
 *
 *  All Rights Reserved.
 *
 *  Copyright (c) 2001 Mission Critical Linux, Inc.
 *  Copyright (c) 2003 Red Hat, Inc.
 *  author: mike ledoux <mwl at mclinux.com>
 *  author: Todd Wheeling <wheeling at mclinux.com>
 *  author: Lon Hohberger <lhh at redhat.com>
 *
 *  Based strongly on original code from baytech.c by Alan Robertson.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/** @file
 * Stonith module for APC Master Switch (AP9211).
 */

/*                          Observations/Notes
 * 
 * 1. The APC MasterSwitch, unlike the BayTech network power switch,
 *    accpets only one (telnet) connection/session at a time. When one
 *    session is active, any subsequent attempt to connect to the MasterSwitch 
 *    will result in a connection refused/closed failure. In a cluster 
 *    environment or other environment utilizing polling/monitoring of the 
 *    MasterSwitch (from multiple nodes), this can clearly cause problems. 
 *    Obviously the more nodes and the shorter the polling interval, the more 
 *    frequently such errors/collisions may occur.
 *
 * 2. We observed that on busy networks where there may be high occurances
 *    of broadcasts, the MasterSwitch became unresponsive.  In some 
 *    configurations this necessitated placing the power switch onto a 
 *    private subnet.
 */

/*
 * Version string that is filled in by CVS
 */
static const char *version __attribute__ ((unused)) = "$Revision: 1.10 $"; 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <libintl.h>
#include <sys/wait.h>


#include "expect.h"
#include "stonith.h"

#define	DEVICE	"APC MasterSwitch"

#define N_(text)	(text)
#define _(text)		dgettext(ST_TEXTDOMAIN, text)


/*
 *	I have an AP9211.  This code has been tested with this switch.
 */

/*
 * Flags surrounding the APC MasterSwitch's behavior.
 */
#define FL_ADMIN	0x1
#define FL_MSPLUS	0x2
#define FL_7900		0x4

#ifdef DEBUG
#define dprintf(fmt, args...) printf(fmt, ##args)
#define syslog(level, fmt, args...) printf(fmt, ##args)
#else
#define dprintf(fmt, args...)
#endif


struct APCMS {
	const char *	MSid;
	char *		idinfo;
	char *		unitid;
	pid_t		pid;
	int		rdfd;
	int		wrfd;
	int		config;
	char *		device;
        char *		user;
	char *		passwd;
};

static const char * MSid = "APCMS-Stonith";
static const char * NOTmsid = "Hey dummy, this has been destroyed (APCMS)";

#define	ISAPCMS(i)	(((i)!= NULL && (i)->pinfo != NULL)	\
	&& ((struct APCMS *)(i->pinfo))->MSid == MSid)

#define	ISCONFIGED(i)	(ISAPCMS(i) && ((struct APCMS *)(i->pinfo))->config)

#ifndef MALLOC
#	define	MALLOC	malloc
#endif
#ifndef FREE
#	define	FREE	free
#endif
#ifndef MALLOCT
#	define     MALLOCT(t)      ((t *)(MALLOC(sizeof(t)))) 
#endif

#define DIMOF(a)	(sizeof(a)/sizeof(a[0]))
#define WHITESPACE	" \t\n\r\f"

#define	REPLSTR(s,v)	{					\
			if ((s) != NULL) {			\
				FREE(s);			\
				(s)=NULL;			\
			}					\
			(s) = MALLOC(strlen(v)+1);		\
			if ((s) == NULL) {			\
				syslog(LOG_ERR, _("out of memory"));\
			}else{					\
				memcpy((s),(v),strlen(v)+1);	\
			}					\
			}

/*
 *	Different expect strings that we get from the APC MasterSwitch
 */

#define APCMSSTR	"American Power Conversion"

static struct Etoken EscapeChar[] =	{ {"Escape character is '^]'.", 0, 0}
					,	{NULL,0,0}};
static struct Etoken login[] = 		{ {"User Name :", 0, 0}, {NULL,0,0}};
static struct Etoken password[] =	{ {"Password  :", 0, 0} ,{NULL,0,0}};
static struct Etoken Prompt[] =	{ {"> ", 0, 0} ,{NULL,0,0}};
static struct Etoken LoginOK[] =	{ {APCMSSTR, 0, 0}
                    , {"User Name :", 1, 0} ,{NULL,0,0}};


/*
 * Control Console (Top level menu).  Prompt is also accepted, but used
 * to indicate that the top level menu has not been reached.
 */
static struct Etoken ControlConsole[] = {
	{ "Control Console", 0, 0 },
	{ "> ", 1, 0 },
	{ NULL, 0, 0 } };


/*
 * Master Switch Types
 */
static struct Etoken SwitchType[] = {
	{ "MasterSwitch APP", 0, 0 },
	{ "MasterSwitch plus APP", 1, 0 },
	{ "MSP APP", 1, 0 },
	{ "Rack PDU APP", 2, 0 },
	{ NULL, 0, 0 } };

/*
 * User Types
 */
static struct Etoken UserType[] = {
	{ "User    : Outlet User", 0, 0 },
	{ "User    : Administrator", 1, 0 },
	{ "User : Outlet User", 0, 0 },
	{ "User : Administrator", 1, 0 },
	{ NULL, 0, 0 } };

/* We may get a notice about rebooting, or a request for confirmation */
static struct Etoken Processing[] =	{ {"Press <ENTER> to continue", 0, 0}
				,	{"Enter 'YES' to continue", 1, 0}
				,	{NULL,0,0}};

static int	MSLookFor(struct APCMS* ms, struct Etoken * tlist, int timeout);
static int	MS_connect_device(struct APCMS * ms);
static int	MSLogin(struct APCMS * ms);
static int	MSRobustLogin(struct APCMS * ms);
static int	MSGetFlags(struct APCMS *ms, int *flags);
static int	MSReset(struct APCMS*, const char * port, int flags);
static int	MSLogout(struct APCMS * ms);
static void	MSkillcomm(struct APCMS * ms);

static int	MS_parse_config_info(struct APCMS* ms, const char * info);
#if defined(ST_POWERON) && defined(ST_POWEROFF)
static int MS_onoff(struct APCMS* ms, const char *port, int req,
		     int flags);

#endif


int		st_setconffile(Stonith *, const char * cfgname);
int		st_setconfinfo(Stonith *, const char * info);
const char *	st_getinfo(Stonith * s, int InfoType);
int		st_status(Stonith * );
int		st_reset(Stonith * s, int request, const char * port);
void		st_destroy(Stonith *);
void *		st_new(void);

/*
 *	We do these things a lot.  Here are a few shorthand macros.
 */

#define	SEND(s)         { \
			dprintf("Send: %s\n", s); \
			write(ms->wrfd, (s), strlen(s)); \
			}

#define	EXPECT(p,t)	{						\
			dprintf("Expecting %s [timeout %d]\n", #p, t); \
			if (MSLookFor(ms, p, t) < 0)			\
				return(errno == ETIMEDOUT			\
			?	S_TIMEOUT : S_OOPS);			\
			}

#define	NULLEXPECT(p,t)	{						\
				if (MSLookFor(ms, p, t) < 0)		\
					return(NULL);			\
			}

/* Look for any of the given patterns.  We don't care which */

static int
MSLookFor(struct APCMS* ms, struct Etoken * tlist, int timeout)
{
	int	rc;
	if ((rc = ExpectToken(ms->rdfd, tlist, timeout, NULL, 0)) < 0) {
		/*
		syslog(LOG_DEBUG, _("Did not find string: '%s' from %s.")
		,	tlist[0].string, DEVICE);
		 */
		MSkillcomm(ms);
	}
	return(rc);
}


/* Login to the APC Master Switch */

static int
MSLogin(struct APCMS * ms)
{
        EXPECT(EscapeChar, 10);

  	/* 
	 * We should be looking at something like this:
         *	User Name :
	 */
	EXPECT(login, 10);
	SEND(ms->user);       
	SEND("\r");

	/* Expect "Password  :" */
	EXPECT(password, 10);
	SEND(ms->passwd);
	SEND("\r");
 
	switch (MSLookFor(ms, LoginOK, 30)) {

		case 0:	/* Good! */
			break;

		case 1:	/* Uh-oh - bad password */
			syslog(LOG_ERR, _("Invalid password for " DEVICE "."));
			return(S_ACCESS);

		default:
			MSkillcomm(ms);
			return(errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS);
	} 

	return(S_OK);
}

/* Attempt to login up to 20 times... */

static int
MSRobustLogin(struct APCMS * ms)
{
	int	rc=S_OOPS;
	int	j;
	int	sltime = 62500;

	for (j=0; j < 20 && rc != S_OK; ++j) {

		if (ms->pid > 0)
			MSkillcomm(ms);

		if (MS_connect_device(ms) != S_OK)
		        MSkillcomm(ms);
		else 
			rc = MSLogin(ms);
			
		usleep(sltime);
		if (sltime < 2000000)
			sltime *= 2;
	}
	return rc;
}

/* Log out of the APC Master Switch */

static 
int MSLogout(struct APCMS* ms)
{
	int	rc;
	
	/* Expect "> " */
	rc = MSLookFor(ms, Prompt, 5);

	/* "4" is logout */
	SEND("4\r");

	MSkillcomm(ms);
	return(rc >= 0 ? S_OK : (errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS));
}


static
int MSTopMenu(struct APCMS *ms)
{
	int rc, retries = 0;
	/*
	 * Make sure we're in the top level menu
	 */
	while (1) {
		SEND("\033");
		rc = MSLookFor(ms, ControlConsole, 3);
		if (rc == 0) {
			dprintf("At top level menu\n");
			return S_OK;
		}
		if (rc == -1)
			return S_OOPS;
		if (++retries > 20)
			return S_OOPS;
	}
}


static void
MSkillcomm(struct APCMS* ms)
{
        if (ms->rdfd >= 0) {
		close(ms->rdfd);
		ms->rdfd = -1;
	}
	if (ms->wrfd >= 0) {
		close(ms->wrfd);
		ms->wrfd = -1;
	}
	if (ms->pid > 0) {
		kill(ms->pid, SIGKILL);
		(void)waitpid(ms->pid, NULL, 0);
		ms->pid = -1;
	}
}


static int
MSControlOutlet(struct APCMS *ms, const char *p, int flags)
{
	char unum[32], tmp[32];
	char *pp = (char *)p;
	
	if (MSTopMenu(ms) != S_OK)
		return S_OOPS;

	/* Request menu 1 (Device Control / Device Manager) */
	SEND("1\r");

	/*
	 * APC 9225 can be daisy chained with multiple units
	 * Ok, we're an administrator.  See if the person put the port
	 * as "unit:port" in the config file.  Default to unit #1.
	 */
	if ((flags & (FL_ADMIN | FL_MSPLUS)) == (FL_ADMIN | FL_MSPLUS)) {

		strncpy(unum, "1\r", sizeof(unum));

		/* See if the user has a specific device in mind */
		if ((pp = strchr(p, ':')) != NULL) {
			if (!pp[1]) {
				pp = (char *)p;
			} else {
				/* If so, we need to separate port/device */
				memset(tmp, 0, sizeof(tmp));
				memcpy(tmp, p, (pp - p));
				snprintf(unum, sizeof(unum), "%s\r",
					 tmp);
				pp++;
			}
		} else
			pp = (char *)p;

		EXPECT(Prompt, 3);
		SEND(unum);
	}

	/*
	 * APC 7900 specific stuff.
	 */
	if (flags & FL_7900) {
		EXPECT(Prompt, 3);
		if (flags & FL_ADMIN) {
			/* Admin: 3- Outlet Control/Configuration */
			SEND("3\r");
		} else {
			/* Outlet User: 2- Outlet Control */
			SEND("2\r");
		}
	}

	/* Select requested outlet */
	EXPECT(Prompt, 5);
	snprintf(unum, sizeof(unum), "%s\r", pp);
  	SEND(unum);

	if (flags & FL_ADMIN) {
		/* Select menu 1 (Control Outlet) - Same for 9225 and 7900 */
		EXPECT(Prompt, 5);
		SEND("1\r");
	}

	return S_OK;
}



/*
 * Reset (power-cycle) the given outlets
 */
static int
MSReset(struct APCMS* ms, const char *p, int flags)
{
	if (MSControlOutlet(ms, p, flags) != S_OK)
		return S_OOPS;

	if (flags & FL_MSPLUS) {
		/*
		 * APC MasterSwitch plus: '4' is reboot
		 */
		EXPECT(Prompt, 5);
		SEND("4\r");
	} else if (flags & FL_7900) {
		/*
		 * APC 7900: '6' is a delayed reboot
		 */
		EXPECT(Prompt, 5);
		SEND("6\r");
	} else {
		/*
		 * APC MasterSwitch: '3' is reboot
		 */
		EXPECT(Prompt, 5);
		SEND("3\r");
	}

	/*
	 * Expect "Press <ENTER> " or "Enter 'YES'"
	 * (if confirmation turned on)
	 */
retry:
	switch (MSLookFor(ms, Processing, 5)) {
	case 0: /* Got "Press <ENTER>" Do so */
		SEND("\r");
		break;

	case 1: /* Got that annoying command confirmation :-( */
		SEND("YES\r");
		goto retry;

	default: 
		return(errno == ETIMEDOUT ? S_RESETFAIL : S_OOPS);
	}
	syslog(LOG_INFO, _("Port %s being rebooted."), p);

	/* Expect ">" */
	if (MSLookFor(ms, Prompt, 10) < 0) {
		return(errno == ETIMEDOUT ? S_RESETFAIL : S_OOPS);
	}

	/* All Right!  Power is back on.  Life is Good! */

	syslog(LOG_INFO, _("Power restored to port %s."), p);

	/* Return to top level menu */
	MSTopMenu(ms);
	SEND("\033");
	EXPECT(Prompt, 5);

	return(S_OK);
}


#if defined(ST_POWERON) && defined(ST_POWEROFF)
static int
MS_onoff(struct APCMS* ms, const char *p, int req, int flags)
{
	char *whatdone = "OFF";

	if (MSControlOutlet(ms, p, flags) != S_OK)
		return S_OOPS;

	EXPECT(Prompt, 5);

	if (req == ST_POWERON) {
		whatdone = "ON";
		dprintf("Debug: Port %s ON\n", p);
		SEND("1\r"); /* BOTH - immediate on */
	} else if (flags & FL_MSPLUS) {
		dprintf("Debug: [msplus] Port %s OFF\n", p);
		SEND("3\r"); /* MS plus - immediate off */
	} else {
		dprintf("Debug: Port %s OFF\n", p);
		SEND("2\r"); /* MS - immediate off */
	}

	/*
	 * Expect "Press <ENTER> " or "Enter 'YES'"
	 * (if confirmation turned on)
	 */
retry:
	switch (MSLookFor(ms, Processing, 5)) {
	case 0: /* Got "Press <ENTER>" Do so */
		dprintf("Debug: Complete\n");
		SEND("\r");
		break;

	case 1: /* Got that annoying command confirmation :-( */
		dprintf("Debug: Got confirmation...\n");
		SEND("YES\r");
		goto retry;

	default: 
		dprintf("Debug: Invalid?\n");
		return(errno == ETIMEDOUT ? S_RESETFAIL : S_OOPS);
	}

	/* Expect ">" */
	if (MSLookFor(ms, Prompt, 10) < 0) {
		return(errno == ETIMEDOUT ? S_RESETFAIL : S_OOPS);
	}

	/* All Right! Command done.  Life is Good! */

	syslog(LOG_INFO, _("Power to port %s turned %s\n"), p,
	       whatdone);

	/* Return to top level menu */
	MSTopMenu(ms);
	SEND("\033");
	EXPECT(Prompt, 5);

	return(S_OK);
}
#endif /* defined(ST_POWERON) && defined(ST_POWEROFF) */


int
st_status(Stonith  *s)
{
	struct APCMS*	ms;
	int	rc;

	if (!ISAPCMS(s)) {
		syslog(LOG_ERR, "invalid argument to MS_status");
		return(S_OOPS);
	}
	if (!ISCONFIGED(s)) {
		syslog(LOG_ERR
		,	"unconfigured stonith object in MS_status");
		return(S_OOPS);
	}
	ms = (struct APCMS*) s->pinfo;

	if ((rc = MSRobustLogin(ms) != S_OK)) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
		return(rc);
	}

	MSTopMenu(ms);
	/* Expect ">" */
	SEND("\033\r");
	EXPECT(Prompt, 5);

	return(MSLogout(ms));
}


/*
 *	Parse the given configuration information, and stash it away...
 */

static int
MS_parse_config_info(struct APCMS* ms, const char * info)
{
	static char dev[1024];
	static char user[1024];
	static char passwd[1024];

	if (ms->config) {
		return(S_OOPS);
	}


	/* 
	 * the 1023's come from the sizes above, leaving space for trailing
	 * NULL character
	 */
	if (sscanf(info, "%1023s %1023s %1023[^\n\r\t]", dev, user, passwd) == 3
	&&	strlen(passwd) > 1) {

		if ((ms->device = (char *)MALLOC(strlen(dev)+1)) == NULL) {
			syslog(LOG_ERR, "out of memory");
			return(S_OOPS);
		}
		if ((ms->user = (char *)MALLOC(strlen(user)+1)) == NULL) {
			free(ms->device);
			ms->device=NULL;
			syslog(LOG_ERR, "out of memory");
			return(S_OOPS);
		}
		if ((ms->passwd = (char *)MALLOC(strlen(passwd)+1)) == NULL) {
			free(ms->device);
			ms->device=NULL;
			free(ms->user);
			ms->user=NULL;
			syslog(LOG_ERR, "out of memory");
			return(S_OOPS);
		}
		memcpy(ms->device, dev, strlen(dev)+1);
		memcpy(ms->user, user, strlen(user)+1);
		memcpy(ms->passwd, passwd, strlen(passwd)+1);
		ms->config = 1;
		return(S_OK);
	}
	return(S_BADCONFIG);
}


/*
 *	Connect to the given MS device.  We should add serial support here
 *	eventually...
 */
static int
MS_connect_device(struct APCMS * ms)
{
	char	TelnetCommand[256];

	snprintf(TelnetCommand, sizeof(TelnetCommand)
	,	"exec telnet %s 2>/dev/null", ms->device);

	ms->pid=StartProcess(TelnetCommand, &ms->rdfd, &ms->wrfd, 0);
	if (ms->pid <= 0) {
		return(S_OOPS);
	}
	return(S_OK);
}


static int
MSGetFlags(struct APCMS *ms, int *flags)
{
	*flags = 0;

	/*
	 * Make sure we're in the top level menu
	 */
	if (MSTopMenu(ms) != S_OK) {
		return S_OOPS;
	}

	/*
	 * Determine user-menu behavior
	 */
	SEND("\033");
	switch (MSLookFor(ms, UserType, 3)) {
	case 0:
		break;
	case 1: /* Admin */
		(*flags) |= FL_ADMIN;
		break;
	default:
		return S_OOPS;
	}

	/*
	 * Determine MasterSwitch type
	 */
	SEND("\033");
	switch (MSLookFor(ms, SwitchType, 3)) {
	case 0:	/* Standard APC MasterSwitch */
		dprintf("Switch Type: APC 9211, 9212 or compatible\n");
		break;
	case 1: /* APC MasterSwitch Plus */
		dprintf("Switch Type: APC 9225 or compatible\n");
		(*flags) |= FL_MSPLUS;
		break;
	case 2: /* APC 7900 */
		dprintf("Switch Type: APC 7900 switch or compatible\n");
		(*flags) |= FL_7900;
		break;
	default:
		return S_OOPS;
	}

	if (*flags & FL_ADMIN)
		dprintf("Mode: Administrator\n");
	else
		dprintf("Mode: Outlet User\n");

	return S_OK;
}


/*
 *	Reset the given host on this Stonith device.  
 */
int
st_reset(Stonith * s, int request, const char * port)
{
	int rc = 0;
	int lorc = 0;
	int flags = 0;


	struct APCMS*	ms;

	if (!ISAPCMS(s)) {
		syslog(LOG_ERR, "invalid argument to MS_reset_port");
		return(S_OOPS);
	}
	if (!ISCONFIGED(s)) {
		syslog(LOG_ERR
		,	"unconfigured stonith object in MS_reset_port");
		return(S_OOPS);
	}
	ms = (struct APCMS*) s->pinfo;

	if ((rc = MSRobustLogin(ms)) != S_OK) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
		return(rc);
	}

	if (MSGetFlags(ms, &flags) != S_OK)
		return S_OOPS;


	/*
	 * OK! We're ready to go!
	 */
	SEND("\033");
	EXPECT(Prompt, 5);

	switch(request) {
#if defined(ST_POWERON) && defined(ST_POWEROFF)
	case ST_POWERON:
	case ST_POWEROFF:
	        rc = MS_onoff(ms, port, request, flags);
		break;
#endif
	case ST_GENERIC_RESET:
		rc = MSReset(ms, port, flags);
		break;
	default:
		rc = S_INVAL;
		break;
	}

	lorc = MSLogout(ms);
	return(rc != S_OK ? rc : lorc);
}

/*
 *	Parse the information in the given configuration file,
 *	and stash it away...
 */
int
st_setconffile(Stonith* s, const char * configname)
{
	FILE *	cfgfile;

	char	APCMSid[256];

	struct APCMS*	ms;

	if (!ISAPCMS(s)) {
		syslog(LOG_ERR, "invalid argument to MS_set_configfile");
		return(S_OOPS);
	}
	ms = (struct APCMS*) s->pinfo;

	if ((cfgfile = fopen(configname, "r")) == NULL)  {
		syslog(LOG_ERR, _("Cannot open %s"), configname);
		return(S_BADCONFIG);
	}
	while (fgets(APCMSid, sizeof(APCMSid), cfgfile) != NULL){
		if (*APCMSid == '#' || *APCMSid == '\n' || *APCMSid == EOS) {
			continue;
		}
		fclose(cfgfile);
		return(MS_parse_config_info(ms, APCMSid));
	}
	fclose(cfgfile);
	return(S_BADCONFIG);
}

/*
 *	Parse the config information in the given string, and stash it away...
 */
int
st_setconfinfo(Stonith* s, const char * info)
{
	struct APCMS* ms;

	if (!ISAPCMS(s)) {
		syslog(LOG_ERR, "MS_provide_config_info: invalid argument");
		return(S_OOPS);
	}
	ms = (struct APCMS *)s->pinfo;

	return(MS_parse_config_info(ms, info));
}
const char *
st_getinfo(Stonith * s, int reqtype)
{
	struct APCMS* ms;
	char *		ret;

	if (!ISAPCMS(s)) {
		syslog(LOG_ERR, "MS_idinfo: invalid argument");
		return NULL;
	}
	/*
	 *	We look in the ST_TEXTDOMAIN catalog for our messages
	 */
	ms = (struct APCMS *)s->pinfo;

	switch (reqtype) {
		case ST_DEVICEID:
			ret = ms->idinfo;
			break;

		case ST_CONF_INFO_SYNTAX:
			ret = _("IP-address login password\n"
			"The IP-address, login and password are white-space delimited.");
			break;

		case ST_CONF_FILE_SYNTAX:
			ret = _("IP-address login password\n"
			"The IP-address, login and password are white-space delimited.  "
			"All three items must be on one line.  "
			"Blank lines and lines beginning with # are ignored");
			break;

		default:
			ret = NULL;
			break;
	}
	return ret;
}

/*
 *	APC MasterSwitch Stonith destructor...
 */
void
st_destroy(Stonith *s)
{
	struct APCMS* ms;

	if (!ISAPCMS(s)) {
		syslog(LOG_ERR, "apcms_del: invalid argument");
		return;
	}
	ms = (struct APCMS *)s->pinfo;

	ms->MSid = NOTmsid;
	MSkillcomm(ms);
	if (ms->device != NULL) {
		FREE(ms->device);
		ms->device = NULL;
	}
	if (ms->user != NULL) {
		FREE(ms->user);
		ms->user = NULL;
	}
	if (ms->passwd != NULL) {
		FREE(ms->passwd);
		ms->passwd = NULL;
	}
	if (ms->idinfo != NULL) {
		FREE(ms->idinfo);
		ms->idinfo = NULL;
	}
	if (ms->unitid != NULL) {
		FREE(ms->unitid);
		ms->unitid = NULL;
	}
}

/* Create a new APC Master Switch Stonith device. */

void *
st_new(void)
{
	struct APCMS*	ms = MALLOCT(struct APCMS);

	if (ms == NULL) {
		syslog(LOG_ERR, "out of memory");
		return(NULL);
	}
	memset(ms, 0, sizeof(*ms));
	ms->MSid = MSid;
	ms->pid = -1;
	ms->rdfd = -1;
	ms->wrfd = -1;
	ms->config = 0;
	ms->user = NULL;
	ms->device = NULL;
	ms->passwd = NULL;
	ms->idinfo = NULL;
	ms->unitid = NULL;
	REPLSTR(ms->idinfo, DEVICE);
	REPLSTR(ms->unitid, "unknown");

	return((void *)ms);
}














