/*
 *
 *  Copyright 2001 Mission Critical Linux, Inc.
 *
 *  All Rights Reserved.
 */
/*
 *	Copyright (c) 2000 Alan Robertson <alanr@unix.sh>
 *	Copyright (c) 2003 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/** @file
 * Stonith module for BayTech Remote Power Controllers (RPC-x devices).
 */


/*
 * Version string that is filled in by CVS
 */
static const char *version __attribute__ ((unused)) = "$Revision: 1.7 $"; 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <libintl.h>
#include <sys/wait.h>

#include "expect.h"
#include "stonith.h"

#define	DEVICE	"BayTech power switch"

#define N_(text)	(text)
#define _(text)		dgettext(ST_TEXTDOMAIN, text)

/*
 *	I have an RPC-5.  This code has been tested with this switch.
 *
 *	The BayTech switches are quite nice, but the dialogues are a bit of a
 *	pain for mechanical parsing.
 */

struct BayTech {
	const char *			BTid;
	char *				idinfo;
	char *				unitid;
	const struct BayTechModelInfo*	modelinfo;
	pid_t				pid;
	int				rdfd;
	int				wrfd;
	int				config;
	char *				device;
	char *				user;
	char *				passwd;
};

struct BayTechModelInfo {
		const char *	type;	/* Baytech model info */
		int		socklen;/* Length of socket name string */
		struct Etoken *	expect;	/* Expect string before outlet list */
};

static const char * BTid = "BayTech-Stonith";
static const char * NOTbtid = "Hey, dummy this has been destroyed (BayTech)";

#define	ISBAYTECH(i)	(((i)!= NULL && (i)->pinfo != NULL)	\
	&& ((struct BayTech *)(i->pinfo))->BTid == BTid)

#define	ISCONFIGED(i)	(ISBAYTECH(i) && ((struct BayTech *)(i->pinfo))->config)

#ifndef MALLOC
#	define	MALLOC	malloc
#endif
#ifndef FREE
#	define	FREE	free
#endif
#ifndef MALLOCT
#	define     MALLOCT(t)      ((t *)(MALLOC(sizeof(t))))
#endif

#define DIMOF(a)	(sizeof(a)/sizeof(a[0]))
#define WHITESPACE	" \t\n\r\f"

#define	REPLSTR(s,v)	{					\
			if ((s) != NULL) {			\
				FREE(s);			\
				(s)=NULL;			\
			}					\
			(s) = MALLOC(strlen(v)+1);		\
			if ((s) == NULL) {			\
				syslog(LOG_ERR, _("out of memory"));\
			}else{					\
				memcpy((s),(v),strlen(v)+1);	\
			}					\
			}

/*
 *	Different expect strings that we get from the Baytech
 *	Remote Power Controllers...
 */

#define BAYTECHASSOC	"Bay Technical Associates"

static struct Etoken EscapeChar[] =	{ {"Escape character is '^]'.", 0, 0}
					,	{NULL,0,0}};
static struct Etoken BayTechAssoc[] =	{ {BAYTECHASSOC, 0, 0}, {NULL,0,0}};
static struct Etoken UnitId[] =		{ {"Unit ID: ", 0, 0}, {NULL,0,0}};
static struct Etoken login[] =		{ {"username>", 0, 0} ,{NULL,0,0}};
static struct Etoken password[] =	{ {"password>", 0, 0}
					, {"username>", 0, 0} ,{NULL,0,0}};
static struct Etoken Selection[] =	{ {"election>", 0, 0} ,{NULL,0,0}};
static struct Etoken RPC[] =		{ {"RPC", 0, 0},
					  {"(Y/N)>", 1, 0},
					  {NULL,0,0}};
static struct Etoken LoginOK[] =	{ {"RPC", 0, 0}, {"Invalid password", 1, 0}
					,	{NULL,0,0}};
static struct Etoken GTSign[] =		{ {">", 0, 0} ,{NULL,0,0}};
static struct Etoken Menu[] =		{ {"Menu:", 0, 0} ,{NULL,0,0}};
static struct Etoken Temp[] =		{ {"emperature: ", 0, 0}
					,	{NULL,0,0}};
static struct Etoken Break[] =		{ {"reaker: ", 0, 0}
					,	{NULL,0,0}};
static struct Etoken PowerApplied[] =	{ {"ower applied to outlet", 0, 0}
					,	{NULL,0,0}};
static struct Etoken OutletStatus[] =	{ {"tatus", 0, 0}
					,	{NULL,0,0}};
/* Accept either a CR/NL or an NL/CR */
static struct Etoken CRNL[] =		{ {"\n\r",0,0},{"\r\n",0,0},{NULL,0,0}};

/* We may get a notice about rebooting, or a request for confirmation */
static struct Etoken Rebooting[] =	{ {"ebooting selected outlet", 0, 0}
				,	{"(Y/N)>", 1, 0}
				,	{"already off.", 2, 0}
				,	{NULL,0,0}};


/*
 * Attempt to isolate model specific dependencies.
 * 
 * TIMXXX - originally, the RPC-3 entry appeared as follows:
 * {"RPC-3", 10, Break},
 * but that wasn't working with the new switch they sent me.  (Perhaps the
 * menu changed some.  In response to outlet status commands I get the
 * following now:
 * RPC-3>status
 * True RMS current:  0.3 Amps
 * Maximum Detected:  0.4 Amps
 *
 * Internal Temperature: 32.5 C
 *
 * Circuit Breaker: On 
 *
 * Selection   Outlet    Outlet   Power
 *   Number     Name     Number   Status
 *     1       clu1        1       On 
 *     2       clu2        2       On 
 *
 * Consequently I have to strip up to the last "Status" rather than ending
 * with the temperature in order to derive outlet status.
 */
static struct BayTechModelInfo ModelInfo [] = {
		{"RPC-5", 18, Temp},
		/* This first model will be the default */
		{"RPC-3", 10, OutletStatus},	
		{"RPC-3A", 10, Break},
		{NULL, 0, NULL},
};

static int	RPCLookFor(struct BayTech* bt, struct Etoken * tlist, int timeout);
static int	RPC_connect_device(struct BayTech * bt);
static int	RPCLogin(struct BayTech * bt);
static int	RPCRobustLogin(struct BayTech * bt);
static int	RPCReset(struct BayTech*, const char * port);
static int	RPCScanLine(struct BayTech* bt, int timeout, char * buf, int max);
static int	RPCLogout(struct BayTech * bt);
static void	RPCkillcomm(struct BayTech * bt);

static int	RPC_parse_config_info(struct BayTech* bt, const char *info);
#if defined(ST_POWERON) && defined(ST_POWEROFF)
static int	RPC_onoff(struct BayTech*, const char *port, int request);
#endif


int		st_setconffile(Stonith *, const char * cfgname);
int		st_setconfinfo(Stonith *, const char * info);
const char *	st_getinfo(Stonith * s, int InfoType);
int		st_status(Stonith * );
int		st_reset(Stonith * s, int request, const char *host);
void		st_destroy(Stonith *);
void *		st_new(void);


/*
 *	We do these things a lot.  Here are a few shorthand macros.
 */
#define	SEND(s)	(write(bt->wrfd, (s), strlen(s)))

#define	EXPECT(p,t)	{						\
			if (RPCLookFor(bt, p, t) < 0)			\
				return(errno == ETIMEDOUT			\
			?	S_TIMEOUT : S_OOPS);			\
			}

#define	NULLEXPECT(p,t)	{						\
				if (RPCLookFor(bt, p, t) < 0)		\
					return(NULL);			\
			}

#define	SNARF(s, to)	{						\
				if (RPCScanLine(bt,to,(s),sizeof(s))	\
				!=	S_OK)				\
					return(S_OOPS);			\
			}

#define	NULLSNARF(s, to)	{					\
				if (RPCScanLine(bt,to,(s),sizeof(s))	\
				!=	S_OK)				\
					return(NULL);			\
				}

/* Look for any of the given patterns.  We don't care which */

#define	MAXSAVE	512
static int
RPCLookFor(struct BayTech* bt, struct Etoken * tlist, int timeout)
{
	int	rc;
	char	savebuf[MAXSAVE];
	savebuf[MAXSAVE-1] = EOS;
	savebuf[0] = EOS;

	if ((rc = ExpectToken(bt->rdfd, tlist, timeout, savebuf, MAXSAVE)) < 0){
		syslog(LOG_ERR, _("Did not find string: '%s' from " DEVICE ".")
		,	tlist[0].string);
		syslog(LOG_ERR, _("Got '%s' from " DEVICE " instead.")
		,	savebuf);
		RPCkillcomm(bt);
		return(-1);
	}
	return(rc);
}

/* Read and return the rest of the line */

static int
RPCScanLine(struct BayTech* bt, int timeout, char * buf, int max)
{
	if (ExpectToken(bt->rdfd, CRNL, timeout, buf, max) < 0) {
		syslog(LOG_ERR, ("Could not read line from " DEVICE "."));
		RPCkillcomm(bt);
		bt->pid = -1;
		return(S_OOPS);
	}
	return(S_OK);
}

/* Login to the Baytech Remote Power Controller (RPC) */

static int
RPCLogin(struct BayTech * bt)
{
	char		IDinfo[128];
	static char	IDbuf[128];
	char *		idptr = IDinfo;
	char *		delim;
	int		j;


	EXPECT(EscapeChar, 10);
	/* Look for the unit type info */
	if (ExpectToken(bt->rdfd, BayTechAssoc, 2, IDinfo
	,	sizeof(IDinfo)) < 0) {
		syslog(LOG_ERR, _("No initial response from " DEVICE "."));
		RPCkillcomm(bt);
		return(errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS);
	}
	idptr += strspn(idptr, WHITESPACE);
	/*
	 * We should be looking at something like this:
         *	RPC-5 Telnet Host
    	 *	Revision F 4.22, (C) 1999
    	 *	Bay Technical Associates
	 */

	/* Truncate the result after the RPC-5 part */
	if ((delim = strchr(idptr, ' ')) != NULL) {
		*delim = EOS;
	}
	snprintf(IDbuf, sizeof(IDbuf), "BayTech %s", idptr);
	REPLSTR(bt->idinfo, IDbuf);

	bt->modelinfo = &ModelInfo[0];

	for (j=0; ModelInfo[j].type != NULL; ++j) {
		/*
		 * TIMXXX - need to submit this back to stonith.  They
		 * were looking at the unit # for model info. This doesn't
		 * work as unit # is configurable.  Need to look at device
		 * ID as this really describes the model.
		 */
		if (strcasecmp(ModelInfo[j].type, idptr) == 0) {
			bt->modelinfo = &ModelInfo[j];
			break;
		}
	}

	/* Look for the unit id info */
	EXPECT(UnitId, 10);
	SNARF(IDbuf, 2);
	delim = IDbuf + strcspn(IDbuf, WHITESPACE);
	*delim = EOS;
	REPLSTR(bt->unitid, IDbuf);

	/* Expect "username>" */
	EXPECT(login, 2);

	SEND(bt->user);
	SEND("\r");

	/* Expect "password>" */

	switch (RPCLookFor(bt, password, 5)) {
		case 0:	/* Good! */
			break;

		case 1:	/* OOPS!  got another username prompt */
			syslog(LOG_ERR, _("Invalid username for " DEVICE "."));
			return(S_ACCESS);

		default:
			return(errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS);
	}

	SEND(bt->passwd);
	SEND("\r");

	/* Expect "RPC-x Menu" */

	switch (RPCLookFor(bt, LoginOK, 5)) {

		case 0:	/* Good! */
			break;

		case 1:	/* Uh-oh - bad password */
			syslog(LOG_ERR, _("Invalid password for " DEVICE "."));
			return(S_ACCESS);

		default:
			RPCkillcomm(bt);
			return(errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS);
	}
	EXPECT(Menu, 2);

	return(S_OK);
}

static int
RPCRobustLogin(struct BayTech * bt)
{
	int	rc=S_OOPS;
	int	j;

	for (j=0; j < 20 && rc != S_OK; ++j) {

		if (bt->pid > 0) {
			RPCkillcomm(bt);
		}

		if (RPC_connect_device(bt) != S_OK) {
			RPCkillcomm(bt);
			continue;
		}

		rc = RPCLogin(bt);
	}
	return rc;
}

/* Log out of the Baytech RPC */

static int
RPCLogout(struct BayTech* bt)
{
	int	rc;

	/* Make sure we're in the right menu... */
	SEND("\r");

	/* Expect "Selection>" */
	rc = RPCLookFor(bt, Selection, 5);

	/* Option 6 is Logout */
	SEND("6\r");

	RPCkillcomm(bt);
	return(rc >= 0 ? S_OK : (errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS));
}
static void
RPCkillcomm(struct BayTech* bt)
{
        if (bt->rdfd >= 0) {
		close(bt->rdfd);
		bt->rdfd = -1;
	}
	if (bt->wrfd >= 0) {
		close(bt->wrfd);
		bt->wrfd = -1;
	}
	if (bt->pid > 0) {
		kill(bt->pid, SIGKILL);
		(void)waitpid(bt->pid, NULL, 0);
		bt->pid = -1;
	}
}


/* Reset (power-cycle) the given outlet number */
static int
RPCReset(struct BayTech* bt, const char *port)
{
	char		unum[32];


	SEND("\r");

	/* Make sure we're in the top level menu */

	/* Expect "RPC-x Menu" */
	EXPECT(RPC, 5);
	EXPECT(Menu, 5);

	/* OK.  Request sub-menu 1 (Outlet Control) */
	SEND("1\r");

	/* Verify that we're in the sub-menu */

	/* Expect: "RPC-x>" */
	EXPECT(RPC, 5);
	EXPECT(GTSign, 5);


	/* Send REBOOT command for given outlet */
	snprintf(unum, sizeof(unum), "REBOOT %s\r", port);
	SEND(unum);

	/* Expect "ebooting "... or "(Y/N)" (if confirmation turned on) */

	retry:
	switch (RPCLookFor(bt, Rebooting, 5)) {
		case 0: /* Got "Rebooting" Do nothing */
			break;

		case 1: /* Got that annoying command confirmation :-( */
			SEND("Y\r");
			goto retry;

		case 2:	/* Outlet is turned off */
			syslog(LOG_ERR, _("Port %s is OFF."), port);
			return(S_ISOFF);

		default:
			return(errno == ETIMEDOUT ? S_RESETFAIL : S_OOPS);
	}
	syslog(LOG_INFO, _("Port %s being rebooted."), port);

	/* Expect "ower applied to outlet" */
	if (RPCLookFor(bt, PowerApplied, 30) < 0) {
		return(errno == ETIMEDOUT ? S_RESETFAIL : S_OOPS);
	}

	/* All Right!  Power is back on.  Life is Good! */

	syslog(LOG_INFO, _("Power restored to port %s."), port);

	/* Expect: "RPC-x>" */
	EXPECT(RPC,5);
	EXPECT(GTSign, 5);

	/* Pop back to main menu */
	SEND("MENU\r");
	return(S_OK);
}


#if defined(ST_POWERON) && defined(ST_POWEROFF)
static int
RPC_onoff(struct BayTech* bt, const char * port, int req)
{
	char		unum[32];

	const char *	onoff = (req == ST_POWERON ? "on" : "off");
	int	rc;


	if ((rc = RPCRobustLogin(bt) != S_OK)) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
		return(rc);
	}
	SEND("\r");

	/* Make sure we're in the top level menu */

	/* Expect "RPC-x Menu" */
	EXPECT(RPC, 5);
	EXPECT(Menu, 5);

	/* OK.  Request sub-menu 1 (Outlet Control) */
	SEND("1\r");

	/* Verify that we're in the sub-menu */

	/* Expect: "RPC-x>" */
	EXPECT(RPC, 5);
	EXPECT(GTSign, 5);

	/* Send ON/OFF command for given outlet */
	snprintf(unum, sizeof(unum), "%s %s\r", onoff, port);
	SEND(unum);

	/* Expect "RPC->x "... or "(Y/N)" (if confirmation turned on) */

	if (RPCLookFor(bt, RPC, 5) == 1) {
		/* They've turned on that annoying command confirmation :-( */
		SEND("Y\r");
		EXPECT(RPC, 5);
	}

	EXPECT(GTSign, 5);

	/* All Right!  Command done. Life is Good! */
	syslog(LOG_NOTICE, _("Power to port %s turned %s."), port, onoff);
	/* Pop back to main menu */
	SEND("MENU\r");
	return(S_OK);
}
#endif /* defined(ST_POWERON) && defined(ST_POWEROFF) */


int
st_status(Stonith  *s)
{
	struct BayTech*	bt;
	int	rc;

	if (!ISBAYTECH(s)) {
		syslog(LOG_ERR, "invalid argument to RPC_status");
		return(S_OOPS);
	}
	if (!ISCONFIGED(s)) {
		syslog(LOG_ERR
		,	"unconfigured stonith object in RPC_status");
		return(S_OOPS);
	}
	bt = (struct BayTech*) s->pinfo;

	if ((rc = RPCRobustLogin(bt) != S_OK)) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
		return(rc);
	}

	/* Verify that we're in the top-level menu */
	SEND("\r");

	/* Expect "RPC-x Menu" */
	EXPECT(RPC, 5);
	EXPECT(Menu, 5);

	return(RPCLogout(bt));
}


/*
 *	Parse the given configuration information, and stash it away...
 */
static int
RPC_parse_config_info(struct BayTech* bt, const char * info)
{
	static char dev[1024];
	static char user[1024];
	static char passwd[1024];

	if (bt->config) {
		return(S_OOPS);
	}

	/* 
	 * the 1023's come from the sizes above, leaving space for trailing
	 * NULL character
	 */
	if (sscanf(info, "%1023s %1023s %1023[^\n\r\t]", dev, user, passwd) == 3
	&&	strlen(passwd) > 1) {

		if ((bt->device = (char *)MALLOC(strlen(dev)+1)) == NULL) {
			syslog(LOG_ERR, "out of memory");
			return(S_OOPS);
		}
		if ((bt->user = (char *)MALLOC(strlen(user)+1)) == NULL) {
			free(bt->device);
			bt->device=NULL;
			syslog(LOG_ERR, "out of memory");
			return(S_OOPS);
		}
		if ((bt->passwd = (char *)MALLOC(strlen(passwd)+1)) == NULL) {
			free(bt->user);
			bt->user=NULL;
			free(bt->device);
			bt->device=NULL;
			syslog(LOG_ERR, "out of memory");
			return(S_OOPS);
		}
		memcpy(bt->device, dev, strlen(dev)+1);
		memcpy(bt->user, user, strlen(user)+1);
		memcpy(bt->passwd, passwd, strlen(passwd)+1);
		bt->config = 1;
		return(S_OK);
	}
	return(S_BADCONFIG);
}


/*
 *	Connect to the given BayTech device.  We should add serial support here
 *	eventually...
 */
static int
RPC_connect_device(struct BayTech * bt)
{
	char	TelnetCommand[256];

	snprintf(TelnetCommand, sizeof(TelnetCommand)
	,	"exec telnet %s 2>/dev/null", bt->device);

	bt->pid=StartProcess(TelnetCommand, &bt->rdfd, &bt->wrfd, 0);
	if (bt->pid <= 0) {
		return(S_OOPS);
	}
	return(S_OK);
}

/*
 *	Reset the given host on this Stonith device.
 */
int
st_reset(Stonith * s, int request, const char * port)
{
	int	rc = 0;
	int	lorc = 0;
	struct BayTech*	bt;

	if (!ISBAYTECH(s)) {
		syslog(LOG_ERR, "invalid argument to RPC_reset_port");
		return(S_OOPS);
	}
	if (!ISCONFIGED(s)) {
		syslog(LOG_ERR
		,	"unconfigured stonith object in RPC_reset_port");
		return(S_OOPS);
	}
	bt = (struct BayTech*) s->pinfo;


	if ((rc = RPCRobustLogin(bt)) != S_OK) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
	} else {
		switch(request) {

#if defined(ST_POWERON) && defined(ST_POWEROFF)
		case ST_POWERON:
		case ST_POWEROFF:
			rc = RPC_onoff(bt, port, request);
			break;
#endif
		case ST_GENERIC_RESET:
			rc = RPCReset(bt, port);
			break;
		default:
			rc = S_INVAL;
			break;
		}
	}

	lorc = RPCLogout(bt);
	RPCkillcomm(bt);

	return(rc != S_OK ? rc : lorc);
}

/*
 *	Parse the information in the given configuration file,
 *	and stash it away...
 */
int
st_setconffile(Stonith* s, const char * configname)
{
	FILE *	cfgfile;

	char	RPCid[256];

	struct BayTech*	bt;

	if (!ISBAYTECH(s)) {
		syslog(LOG_ERR, "invalid argument to RPC_set_configfile");
		return(S_OOPS);
	}
	bt = (struct BayTech*) s->pinfo;

	if ((cfgfile = fopen(configname, "r")) == NULL)  {
		syslog(LOG_ERR, _("Cannot open %s"), configname);
		return(S_BADCONFIG);
	}
	while (fgets(RPCid, sizeof(RPCid), cfgfile) != NULL){
		if (*RPCid == '#' || *RPCid == '\n' || *RPCid == EOS) {
			continue;
		}
		fclose(cfgfile);
		return(RPC_parse_config_info(bt, RPCid));
	}
	fclose(cfgfile);
	return(S_BADCONFIG);
}

/*
 *	Parse the config information in the given string, and stash it away...
 */
int
st_setconfinfo(Stonith* s, const char * info)
{
	struct BayTech* bt;

	if (!ISBAYTECH(s)) {
		syslog(LOG_ERR, "RPC_provide_config_info: invalid argument");
		return(S_OOPS);
	}
	bt = (struct BayTech *)s->pinfo;

	return(RPC_parse_config_info(bt, info));
}


const char *
st_getinfo(Stonith * s, int reqtype)
{
	struct BayTech* bt;
	char *		ret;

	if (!ISBAYTECH(s)) {
		syslog(LOG_ERR, "RPC_idinfo: invalid argument");
		return NULL;
	}
	/*
	 *	We look in the ST_TEXTDOMAIN catalog for our messages
	 */
	bt = (struct BayTech *)s->pinfo;

	switch (reqtype) {
		case ST_DEVICEID:
			ret = bt->idinfo;
			break;

		case ST_CONF_INFO_SYNTAX:
			ret = _("IP-address login password\n"
			"The IP-address and login are white-space delimited.");
			break;

		case ST_CONF_FILE_SYNTAX:
			ret = _("IP-address login password\n"
			"The IP-address and login are white-space delimited.  "
			"All three items must be on one line.  "
			"Blank lines and lines beginning with # are ignored");
			break;

		default:
			ret = NULL;
			break;
	}
	return ret;
}


/*
 *	Baytech Stonith destructor...
 */
void
st_destroy(Stonith *s)
{
	struct BayTech* bt;

	if (!ISBAYTECH(s)) {
		syslog(LOG_ERR, "baytech_del: invalid argument");
		return;
	}
	bt = (struct BayTech *)s->pinfo;

	bt->BTid = NOTbtid;
	RPCkillcomm(bt);
	if (bt->device != NULL) {
		FREE(bt->device);
		bt->device = NULL;
	}
	if (bt->user != NULL) {
		FREE(bt->user);
		bt->user = NULL;
	}
	if (bt->passwd != NULL) {
		FREE(bt->passwd);
		bt->passwd = NULL;
	}
	if (bt->idinfo != NULL) {
		FREE(bt->idinfo);
		bt->idinfo = NULL;
	}
	if (bt->unitid != NULL) {
		FREE(bt->unitid);
		bt->unitid = NULL;
	}
}


/* Create a new BayTech Stonith device. */
void *
st_new(void)
{
	struct BayTech*	bt = MALLOCT(struct BayTech);

	if (bt == NULL) {
		syslog(LOG_ERR, "out of memory");
		return(NULL);
	}
	memset(bt, 0, sizeof(*bt));
	bt->BTid = BTid;
	bt->pid = -1;
	bt->rdfd = -1;
	bt->wrfd = -1;
	bt->config = 0;
	bt->user = NULL;
	bt->device = NULL;
	bt->passwd = NULL;
	bt->idinfo = NULL;
	bt->unitid = NULL;
	REPLSTR(bt->idinfo, DEVICE);
	bt->modelinfo = &ModelInfo[0];

	return((void *)bt);
}
