/** @file
 * Stonith module for IBM Bladecenter
 *
 *  Copyright 2001 Mission Critical Linux, Inc.
 *  Copyright 2003, 2005 Red Hat, Inc.
 *  author: mike ledoux <mwl@mclinux.com>
 *  author: Todd Wheeling <wheeling@mclinux.com>
 *  author: Lon Hohberger <lhh at redhat.com>
 *
 *  lhh's modifications:
 *    - Blade Center driver
 *
 *  Based on original wti_nps.c by mike ledoux.
 *  Which was based strongly on original code from baytech.c by
 *    Alan Robertson.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <libintl.h>
#include <sys/wait.h>

#include "expect.h"
#include "stonith.h"
#include "stelnet.h"

#define	DEVICE	"IBM Blade Center"

#define N_(text)	(text)
#define _(text)		dgettext(ST_TEXTDOMAIN, text)

struct BC {
	const char *	stid;
	char *		idinfo;
	char *		unitid;
	int		config;
	char *		address;
	telnet_t	t;
};

static const char * stid = "WTI";
static const char * NOTstid = "Hey, dummy this has been destroyed (BC)";

#define	ISBC(i)	(((i)!= NULL && (i)->pinfo != NULL)	\
	&& ((struct BC *)(i->pinfo))->stid == stid)

#define	ISCONFIGED(i)	(ISBC(i) && ((struct BC *)(i->pinfo))->config)

#ifndef MALLOC
#	define	MALLOC	malloc
#endif
#ifndef FREE
#	define	FREE	free
#endif
#ifndef MALLOCT
#	define     MALLOCT(t)      ((t *)(MALLOC(sizeof(t)))) 
#endif

#define	REPLSTR(s,v)	{					\
			if ((s) != NULL) {			\
				FREE(s);			\
				(s)=NULL;			\
			}					\
			(s) = MALLOC(strlen(v)+1);		\
			if ((s) == NULL) {			\
				syslog(LOG_ERR, _("out of memory"));\
			}else{					\
				memcpy((s),(v),strlen(v)+1);	\
			}					\
			}

/*
 *	Different expect strings that we get from the WTI
 *	Network Power Switch
 */

#define BCSTR "IBM Blade Center"

static struct Etoken username[] = 	{ {"username:", 0, 0},
					  {NULL,0,0}};

static struct Etoken password[] =	{ {"password:", 0, 0},
					  {NULL,0,0}};


static struct Etoken loginok[] =	{ {"system>", 0, 0}, 
	                                  {"Invalid login!", 1, 0},
					  {NULL,0,0}};

static struct Etoken Prompt[] =	        { {"system>", 0, 0},
	                                  {NULL,0,0}};

static struct Etoken blade_prompt[] =	{ {"system:blade[", 0, 0},
					  {"Invalid target path", 1, 0},
					  {"The target bay is empty", 2, 0},
					  {NULL, 0, 0} };

static struct Etoken power_state[] =	{ {"On", 1, 0},
					  {"Off", 0, 0},
					  {NULL, 0, 0} };

static struct Etoken okay[] =		{ {"OK", 0, 0},
					  {NULL, 0, 0} };


/* We may get a notice about rebooting, or a request for confirmation */

int		BC_power_state(struct BC *bc);
int		BC_wait_for_state(struct BC *bc, int exp, int timeout);
static int	select_blade(struct BC *bc, char *port);
static int	BCReset(struct BC*, char *port);
static int	BCLogout(struct BC * bc);
static int	BC_parse_config_info(struct BC* bc, const char * info);
#if defined(ST_POWERON) && defined(ST_POWEROFF)
static int	BC_onoff(struct BC*, char *port, int request);
#endif

/*
 * Our shared symbols
 */
int		st_setconffile(Stonith *, const char * cfgname);
int		st_setconfinfo(Stonith *, const char * info);
const char *	st_getinfo(Stonith * s, int InfoType);
int		st_status(Stonith * );
int		st_reset(Stonith * s, int request, char *port);
void		st_destroy(Stonith *);
void *		st_new(void);


/*
 * Log out of the IBM Blade Center
 */
static int
BCLogout(struct BC* bc)
{
	int	rc, e;
	
	telnet_send(&bc->t, "env -T system\r");
	rc = telnet_expect(&bc->t, Prompt, 5);
	e = errno;
	telnet_send(&bc->t, "exit\r");
	telnet_disconnect(&bc->t);
	errno = e;

	return(rc >= 0 ? S_OK : (e == ETIMEDOUT ? S_TIMEOUT : S_OOPS));
}


/*
 * Reset (power-cycle) the given outlets 
 */
static int
BCReset(struct BC* bc, char *port)
{
	int state;
	/* Sets up our blade prompt for expects too */
	if (select_blade(bc, port) != 0) {
		syslog(LOG_ERR, "Failed to select blade %s", port);
		return S_BADCONFIG;
	}

	/* At blade prompt */
	/* Grab state */
	state = BC_power_state(bc);

	if (!state)
		return S_OK;

	telnet_send(&bc->t, "power -cycle\r");

	/* Wait for the OK */
	if (telnet_expect(&bc->t, okay, 30) != 0) {
		return S_RESETFAIL;
	}	
	
	/* Wait for the blade prompt */
	if (telnet_expect(&bc->t, blade_prompt, 10) != 0)
		return S_RESETFAIL;

	if (!BC_wait_for_state(bc, 0, 20)) {
		syslog(LOG_WARNING,
		       "Power state still off after power-cycle.\n");
	}

	return S_OK;
}


static int
select_blade(struct BC *bc, char *port)
{
	char bladename[128];

	snprintf(bladename, sizeof(bladename), "system:blade[%s]",
		 port);

	telnet_send(&bc->t, "env -T ");
	telnet_send(&bc->t, bladename);
	telnet_send(&bc->t, "\r");

	/* Wait for the blade prompt */
	if (telnet_expect(&bc->t, blade_prompt, 2) == 0) {
		//printf("Blade %s selected\n", bladename);
		return 0;
	}

	return 1;
}


int
BC_power_state(struct BC *bc)
{
	telnet_send(&bc->t, "power -state\r");
	return telnet_expect(&bc->t, power_state, 2);
}


int
BC_wait_for_state(struct BC *bc, int exp, int timeout)
{
	int state, retries = 0;

	state = BC_power_state(bc);
	while (state != exp && ++retries < timeout) {
		usleep(1000000);
		state = BC_power_state(bc);
		if (state == exp)
			break;
	}

	return state;
}


#if defined(ST_POWERON) && defined(ST_POWEROFF)
static int
BC_onoff(struct BC* bc, char *port, int req)
{
	int state, exp, tries = 0;
	/* Sets up our blade prompt for expects too */
	if (select_blade(bc, port) != 0) {
		syslog(LOG_ERR, "Failed to select blade %s", port);
		return S_BADCONFIG;
	}

	/* At blade prompt */

	/*
	   XXX
	   The Blade Center doesn't like quick power-state changes for
	   the blades.  Basically, if you do a "power -off" and a 
	   "power -on" too quickly, one of them gets thrown out (it does
	   not seem to matter what the ordering is.
	 */
	while (++tries < 3) {
		/* Grab state */
		state = BC_power_state(bc);

		if (req == ST_POWERON) {
			if (state) {
				//printf("Already on!\n");
				return S_OK;
			}
			telnet_send(&bc->t, "power -on\r");
			exp = 1;
		} else if (req == ST_POWEROFF) {
			if (!state) {
				//printf("Already off!\n");
				return S_OK;
		}
			telnet_send(&bc->t, "power -off\r");
			exp = 0;
		}

		/* Wait for the OK */
		if (telnet_expect(&bc->t, okay, 20) != 0) {
			syslog(LOG_ERR, "No response from Blade Center\n");
			return S_RESETFAIL;
		}
	
		/* Wait for the blade prompt */
		if (telnet_expect(&bc->t, blade_prompt, 10) != 0) {
			syslog(LOG_ERR, "No response from Blade Center\n");
			return S_RESETFAIL;
		}

		/* XXX mantis said so. */
		usleep(5000000);

		if (BC_wait_for_state(bc, exp, 10) == exp) {
			/* We're done */
			return S_OK;
		}
		printf("State wrong than expected; retrying\n");
	}

	syslog(LOG_ERR, "Retries exceeded waiting for power state %d\n",
	       exp);
	return S_RESETFAIL;
}
#endif /* defined(ST_POWERON) && defined(ST_POWEROFF) */


int
st_status(Stonith  *s)
{
	struct BC*	bc;
	int	rc = 0;

	if (!ISBC(s)) {
		syslog(LOG_ERR, "invalid argument to BC_status");
		return(S_OOPS);
	}
	if (!ISCONFIGED(s)) {
		syslog(LOG_ERR
		,	"unconfigured stonith object in BC_status");
		return(S_OOPS);
	}
	bc = (struct BC*) s->pinfo;

	if (telnet_robust_login(bc->address, &bc->t, username, password,
				loginok) < 0) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
		return(rc);
	}

	return(BCLogout(bc));
}


/*
 *	Parse the given configuration information, and stash it away...
 */
static int
BC_parse_config_info(struct BC* bc, const char * info)
{
	static char addr[128];
	static char login[128];
	static char passwd[128];

	if (bc->config)
		return(S_OOPS);

	/* 
	 * the 1023's come from the sizes above, leaving space for trailing
	 * NULL character
	 */
	if (sscanf(info, "%127s %127s %127[^\n\r\t]",
		   addr, login, passwd) != 3)
		return S_BADCONFIG;

	if ((bc->address = (char *)MALLOC(strlen(addr)+1)) == NULL) {
		syslog(LOG_ERR, "out of memory");
		return(S_OOPS);
	}
	if ((bc->t.t_login = (char *)MALLOC(strlen(login)+1)) == NULL) {
		free(bc->address);
		syslog(LOG_ERR, "out of memory");
		return(S_OOPS);
	}
	if ((bc->t.t_password = (char *)MALLOC(strlen(passwd)+1)) == NULL) {
		free(bc->address);
		free(bc->t.t_login);
		bc->address=NULL;
		syslog(LOG_ERR, "out of memory");
		return(S_OOPS);
	}
	memcpy(bc->address, addr, strlen(addr)+1);
	memcpy(bc->t.t_login, login, strlen(login)+1);
	memcpy(bc->t.t_password, passwd, strlen(passwd)+1);

	bc->config = 1;
	return S_OK;
}


/*
 *	Reset the given host on this Stonith device.  
 */
int
st_reset(Stonith *s, int request, char *port)
{
	int	rc = 0;
	int	lorc = 0;
	struct BC*	bc;

	if (!ISBC(s)) {
		syslog(LOG_ERR, "invalid argument to BC_reset_port");
		return(S_OOPS);
	}
	if (!ISCONFIGED(s)) {
		syslog(LOG_ERR
		,	"unconfigured stonith object in BC_reset_port");
		return(S_OOPS);
	}
	bc = (struct BC*) s->pinfo;

	if (telnet_robust_login(bc->address, &bc->t, username, password,
				loginok) != 0) {
		
		rc = errno;
		syslog(LOG_ERR, "Connect to %s failed: %s\n",
		       bc->address, strerror(errno));
		if (rc == EACCES) 
			return S_BADCONFIG;
		return S_OOPS;
	}

	switch(request) {

#if defined(ST_POWERON) && defined(ST_POWEROFF)
	case ST_POWERON:
	case ST_POWEROFF:
		rc = BC_onoff(bc, port, request);
		break;
#endif
	case ST_GENERIC_RESET:
		rc = BCReset(bc, port);
		break;
	default:
		rc = S_INVAL;			
		break;
	}

	lorc = BCLogout(bc);
	return(rc != S_OK ? rc : lorc);
}


/*
 *	Parse the information in the given configuration file,
 *	and stash it away...
 */
int
st_setconffile(Stonith* s, const char * configname)
{
	FILE *	cfgfile;
	char	sid[256];

	struct BC*	bc;

	if (!ISBC(s)) {
		syslog(LOG_ERR, "invalid argument to BC_set_configfile");
		return(S_OOPS);
	}
	bc = (struct BC*) s->pinfo;

	if ((cfgfile = fopen(configname, "r")) == NULL)  {
		syslog(LOG_ERR, _("Cannot open %s"), configname);
		return(S_BADCONFIG);
	}
	while (fgets(sid, sizeof(sid), cfgfile) != NULL){
		if (*sid == '#' || *sid == '\n' || *sid == EOS) {
			continue;
		}
		fclose(cfgfile);
		return(BC_parse_config_info(bc, sid));
	}
	fclose(cfgfile);
	return(S_BADCONFIG);
}

/*
 *	Parse the config information in the given string, and stash it away...
 */
int
st_setconfinfo(Stonith* s, const char * info)
{
	struct BC* bc;

	if (!ISBC(s)) {
		syslog(LOG_ERR, "BC_provide_config_info: invalid argument");
		return(S_OOPS);
	}
	bc = (struct BC *)s->pinfo;

	return(BC_parse_config_info(bc, info));
}


const char *
st_getinfo(Stonith * s, int reqtype)
{
	struct BC* bc;
	char *		ret;

	if (!ISBC(s)) {
		syslog(LOG_ERR, "BC_idinfo: invalid argument");
		return NULL;
	}
	/*
	 *	We look in the ST_TEXTDOMAIN catalog for our messages
	 */
	bc = (struct BC *)s->pinfo;

	switch (reqtype) {
		case ST_DEVICEID:
			ret = bc->idinfo;
			break;

		case ST_CONF_INFO_SYNTAX:
			ret = _("IP-address username password\n"
			"The IP-address and password are white-space delimited.");
			break;

		case ST_CONF_FILE_SYNTAX:
			ret = _("IP-address username password\n"
			"The IP-address and password are white-space delimited.  "
			"All three items must be on one line.  "
			"Blank lines and lines beginning with # are ignored");
			break;

		default:
			ret = NULL;
			break;
	}
	return ret;
}

/*
 *	Baytech Stonith destructor...
 */
void
st_destroy(Stonith *s)
{
	struct BC* bc;

	if (!ISBC(s)) {
		syslog(LOG_ERR, "wtibc_del: invalid argument");
		return;
	}
	bc = (struct BC *)s->pinfo;

	bc->stid = NOTstid;
	telnet_cleanup(&bc->t);
	if (bc->address != NULL) {
		FREE(bc->address );
		bc->address = NULL;
	}
	if (bc->idinfo != NULL) {
		FREE(bc->idinfo);
		bc->idinfo = NULL;
	}
	if (bc->unitid != NULL) {
		FREE(bc->unitid);
		bc->unitid = NULL;
	}

	telnet_cleanup(&bc->t);
}

/* Create a new BayTech Stonith device. */

void *
st_new(void)
{
	struct BC*	bc = MALLOCT(struct BC);

	if (bc == NULL) {
		syslog(LOG_ERR, "out of memory");
		return(NULL);
	}
	memset(bc, 0, sizeof(*bc));
	bc->stid = stid;
	bc->config = 0;
	bc->address = NULL;
	bc->idinfo = NULL;
	bc->unitid = NULL;
	telnet_init(&bc->t);
	REPLSTR(bc->idinfo, DEVICE);
	REPLSTR(bc->unitid, "unknown");

	return((void *)bc);
}
