/** @file
 * STONITH module which asks GuLM for status.
 *
 * Copyright (c) 2003 Red Hat, Inc.
 * Lon Hohberger <lhh at redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgulm.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include "stonith.h"

#define	DEVICE	"STONITH<->GuLM Bridge"

struct GuLM {
	char *myid;
	char *nodename;
	int seen;
};

struct node_misc {
	uint8_t done;
	uint8_t nodestate;
	char *nodename;
};

static char * GuLMid = DEVICE;
static char * NOTGuLMid = "OBJECT DESTROYED: (" DEVICE ")";

int		st_setconffile(Stonith *, const char * cfgname);
int		st_setconfinfo(Stonith *, const char * info);
const char *	st_getinfo(Stonith * s, int InfoType);
int		st_status(Stonith * );
int		st_reset(Stonith * s, int request, const char * host);
void		st_destroy(Stonith *);
void *		st_new(void);

/**
 * GuLM no-ops protos
 */
static int null_login_reply(void *misc, uint64_t gen, uint32_t error,
                            uint32_t rank, uint8_t corestate);
static int null_logout_reply(void *misc);
static int null_nodelist(void *misc, lglcb_t type, char *name, uint32_t ip,
                         uint8_t state);
static int null_statechange(void *misc, uint8_t corestate, uint32_t masterip,
                            char *mastername);
static int null_nodechange(void *misc, char *nodename, uint32_t nodeip,
                           uint8_t nodestate);
static int null_service_list(void *misc, lglcb_t type, char *service);
static int null_status(void *misc, lglcb_t type, char *key, char *value);
static int null_error(void *misc, uint32_t err);
                                                                                 
static lg_core_callbacks_t gulm_callbacks_initializer = {
        null_login_reply,
        null_logout_reply,
        null_nodelist,
        null_statechange,
        null_nodechange,
        null_service_list,
        null_status,
        null_error
};

#ifndef dprintf
#ifdef DEBUG
#define dprintf(fmt, args...) printf(fmt, ##args)
#else
#define dprintf(fmt, args...)
#endif
#endif
                                                                                 
/**
 * GuLM reply-no-ops
 */
static int
null_login_reply(void *misc, uint64_t gen, uint32_t error, uint32_t rank,
                 uint8_t corestate)
{
        dprintf("GuLM: %s called\n", __FUNCTION__);
        return 0;
}
                                                                                 
                                                                                 
static int
null_logout_reply(void *misc)
{
        dprintf("GuLM: %s called\n", __FUNCTION__);
        return 0;
}
                                                                                 
                                                                                 
static int
null_nodelist(void *misc, lglcb_t type, char *name, uint32_t ip,
              uint8_t state)
{
        dprintf("GuLM: %s called\n", __FUNCTION__);
        return 0;
}
                                                                                 
                                                                                 
static int
null_statechange(void *misc, uint8_t corestate, uint32_t masterip,
                 char *mastername)
{
        dprintf("GuLM: %s called: corestate: %d, mastername: %s\n",
		__FUNCTION__, corestate, mastername);
        return 0;
 }
                                                                                 
                                                                                 
static int
null_nodechange(void *misc, char *nodename, uint32_t nodeip,
                uint8_t nodestate)
{
        dprintf("GuLM: %s called: nodename: %s nodestate: %d\n", __FUNCTION__,
		nodename, nodestate);
        return 0;
}
                                                                                 
                                                                                 
static int
null_service_list(void *misc, lglcb_t type, char *service)
{
        dprintf("GuLM: %s called\n", __FUNCTION__);
        return 0;
}
                                                                                 
                                                                                 
static int
null_status(void *misc, lglcb_t type, char *key, char *value)
{
        dprintf("GuLM: %s called\n", __FUNCTION__);
        return 0;
}
                                                                                 
                                                                                 
static int
null_error(void *misc, uint32_t err)
{
        dprintf("GuLM: %s called, error = %d\n", __FUNCTION__, err);
	if (err)
		dprintf("error = %d (0x%x)\n", err, err);
        return 0;
}
/** END DEBUGGING STUFF */


/**
 * Handle a login reply from GuLM.
 *
 * @param misc		(Flag code) We use this to indicate that this
 *			function was actually called, so we know we got the
 *			message we wanted from GuLM.
 * @return		S_OK
 */
static int
gulm_login_reply(void *misc, uint64_t gen, uint32_t error, uint32_t rank,
	    uint8_t corestate)
{
	int *flag = (int *)misc;

	*flag = 1;

	switch(error) {
	case lg_err_Ok:
		return 0;

	case lg_err_BadConfig:
		return -1;

	case lg_err_BadLogin:
		return -1;
	}

	return -1;
}


/**
 * Log in to GuLM.
 *
 * @param pg		gulm_interface_p opaque structure
 * @return		S_OOPS on error, S_OK on success, others correspond
 *			to libgulm error flags.
 */
static int
gulm_login(gulm_interface_p pg)
{
	int flag = 0, ret = S_OOPS;

	lg_core_callbacks_t cb = { gulm_login_reply, NULL, NULL, NULL, NULL,
				   NULL, NULL, NULL };

	if (lg_core_login(pg, 0) != 0)
		return S_OOPS;
	
	/*
	 * gulm_login_reply modifies ret.  WATCH SIZING conflicts for
	 * ret here and *ret in gulm_login_reply above!!!
	 */
	ret = lg_core_handle_messages(pg, &cb, (void *)&flag);
	if (!flag) {
		/* Should NEVER be reached; gulm returns the login reply
		   first - this is guaranteed */
		return S_OOPS;
	}

	return ret;
}


/**
 * Handle a logout reply from GuLM.
 *
 * @param misc		(Return code) We use this to indicate that this
 *			function was actually called, so we know we got the
 *			message we wanted from GuLM.
 * @return		S_OK
 */
static int
gulm_logout_reply(void *misc)
{
	int *flag = (int *)misc;

	/*
	 * Ok, we got our logout message.
	 */
	*flag = 1;

	return 0;
}


/**
 * Log out from GuLM core.
 *
 * @param pg		GuLM interface opaque structure
 * @return		S_OK
 */
static int
gulm_logout(gulm_interface_p pg)
{
	int flag = 0;
	lg_core_callbacks_t cb = gulm_callbacks_initializer;

	cb.logout_reply = gulm_logout_reply;

	if (lg_core_logout(pg) < 0)
		return S_OOPS;
	
	while (flag == 0) {
		/*
		 * Chew up messages.
		 */
		lg_core_handle_messages(pg, &cb, (void *)&flag);
	}

	return S_OK;
}


/**
 * Handle nodelist callbacks; look for fence flag
 *
 * @param misc		Return data (see #defines)
 * @oaran type		Callback type
 * @param name		Callback name
 * @param ip		Unused; IP address of member
 * @param state		State of member
 * @return		0: not applicable (This wasn't a lglcb_item),
 *			otherwise node states (lg_core_Expired/etc.)
 */
static int
gulm_nodelist(void *misc, lglcb_t type, char *name, uint32_t ip,
	      uint8_t state)
{
	struct node_misc *nm = misc;

	nm->done = 0;

	switch(type) {
	case lglcb_start:
		return 0;

	case lglcb_item:
		nm->nodestate = state;
		return 0;

	case lglcb_stop:
		nm->done = 1;
		return 0;
	}
	return -1;
}


/**
 * Handle nodechange callbacks; look for fence flag
 *
 * @param misc		Return data; see above.
 * @oaran type		Callback type
 * @param name		Callback name
 * @param ip		Unused; IP address of member
 * @param state		State of member
 * @return		0: not applicable (This wasn't a lglcb_item),
 *			otherwise node states (lg_core_Expired/etc.)
 */
static int
gulm_nodechange(void *misc, char *nodename, uint32_t nodeip,
                uint8_t nodestate)
{
	struct node_misc *nm = misc;

	if (!strcmp(nodename, nm->nodename)) {
		nm->done = 1;
		nm->nodestate = nodestate;
	}
        return 0;
}
                                                                                 


/**
 * Get the status of a host.  XXX should do gethostbyname first
 *
 * @param pg		GuLM structure
 * @param nodename	Node to check status of
 */
static int
gulm_status(gulm_interface_p pg, const char *nodename)
{
	int state = -1, ret = 0;
	struct node_misc nm;
	lg_core_callbacks_t cb = gulm_callbacks_initializer;

	cb.nodelist = gulm_nodelist;
	cb.nodechange = gulm_nodechange;

	nm.done = 0;
	nm.nodestate = 0;
	nm.nodename = (char *)nodename;

	if (lg_core_nodeinfo(pg, (char *)nodename) != 0)
		return -1;

	do {
		ret = lg_core_handle_messages(pg, &cb, (void *)&nm);

		/* GuLM callbacks need to return 0 or libgulm doesn't like
		   it ... so, use negative values for gulm_nodelist. */
		if (nm.done)
			state = nm.nodestate;
	} while (ret >= 0 && !nm.done);

	if (ret < 0)
		return -1;

	return state;
}


/**
 * Check the status and/or fence a node using GuLM.  This relies on the
 * fact that clumanager monitors status of power switches.  In the
 * case of the GuLM-bridge, we monitor GuLM's visibility of a node.
 * In the case that a node never logs in to GuLM, we can't fence it, ever.
 *
 * If a node was not recently seen (e.g. the st_status failed or GuLM
 * didn't know about the member), it's unsafe to fail over services,
 * so we send S_RESETFAIL back to the caller.  In the case that GuLM
 * has not expired a node and we need to fence it, we send GuLM a
 * forceexpire command.  In the case that the node was previously in
 * GuLM's membership list (e.g. st_reset saw the node), and no longer is,
 * we assume the node has been fenced for lack of a better idea.
 *
 * Deficiencies: Clumanager must monitor lock_gulmd and shut down if
 * lock_gulmd stops running when the gulm-bridge driver is in use.
 *
 * @param pg		Pointer to opaque gulm structure thing.
 * @return		S_RESETFAIL (node online; not fenced)
 */
static int
gulm_fence(gulm_interface_p pg, const char *nodename, struct GuLM *gulm)
{
	int timeout = 120;
	int state = lg_core_Logged_in, ret, forced=0;

	dprintf("%s: Telling gulm to fence %s\n", __FUNCTION__, nodename);

	while ((state != 0) && (timeout > 0)) {

		state = gulm_status(pg, nodename);

		dprintf("%s: state of %s is %d; timeout = %d\n", __FUNCTION__,
			nodename, state, timeout);
	
		switch(state) {
		case -1:
			break;

		case lg_core_Logged_in:	/* Discrepancy! Tell Gulm to expire */
			gulm->seen = 1;

			if (forced)
				break;

			dprintf("%s: calling lg_core_forceexpire\n",
				__FUNCTION__);
			ret = lg_core_forceexpire(pg, (char *)nodename);
			if (ret != 0) {
				dprintf("%s: lg_core_forceexpire returned %d\n",
					__FUNCTION__, ret );
				return S_RESETFAIL;
			}

			forced = 1;
			break;

		case lg_core_Expired:	/* Not quite dead yet... */
			gulm->seen = 1;
			break;

		case lg_core_Fenced:	/* Fenced message */
		case lg_core_Logged_out:/* No longer member. */
			gulm->seen = 0; /* Reset our seen flag until gulm
					   comes back online */
			return S_OK;
		case 0:			/* state unavailable. assume dead. */
			if (gulm->seen) {
				gulm->seen = 0;
				return S_OK;
			}

			dprintf("%s: Can not fence member %s: No recent "
				"status from GuLM\n", __FUNCTION__,
				gulm->nodename);

			syslog(LOG_ERR, "Can not fence member %s: (No recent"
			       " status from GuLM)", gulm->nodename);
			syslog(LOG_ERR, "Are you sure lock_gulmd was "
			       "running on %s?", gulm->nodename);
			return S_RESETFAIL;

		default:
			dprintf("%s: Unhandled state %d\n", __FUNCTION__,
				state);
			return S_OOPS;
		}

		sleep(1);
		--timeout;
	}

	if (state == -1) {
		dprintf("Unable to determine state of member %s\n", nodename);
	} else {
		dprintf("Unable to verify that member %s was fenced\n",
		       nodename);
	}

	/* Timed out.  No failover for you. */
	return S_RESETFAIL;
}


/**
 * Set and read the configuration file.
 *
 * @param s		Stonith structure
 * @param cfgname	Config filename
 * @return		S_BADCONFIG on failure; S_OK on success.
 */
int
st_setconffile(Stonith *s, const char *cfgname)
{
	FILE *cfgfile;
	char data[256];
	int ret = S_BADCONFIG;

	struct GuLM *gulm;

	if (!s || !s->pinfo || ((struct GuLM *)(s->pinfo))->myid != GuLMid)
		return S_OOPS;

	gulm = (struct GuLM *)s->pinfo;

	if ((cfgfile = fopen(cfgname, "r")) == NULL)
		return S_BADCONFIG;

	do {
		memset(data,0,sizeof(data));
		if (fgets(data, sizeof(data)-1, cfgfile) == NULL)
			break;
	} while (*data == '#' || *data == '\n' || *data == '\000');

	if (*data)
		ret = st_setconfinfo(s, data);

	fclose(cfgfile);
	return ret;
}


/**
 * Set the configuration info.
 *
 * @param s		Stonith structure
 * @param info		Config information (e.g. the nodename)
 * @return		S_BADCONFIG on failure; S_OK on success.
 */
int
st_setconfinfo(Stonith *s, const char *info)
{
	char data[256];
	int x,y;
	struct GuLM *gulm;

	if (!s || !s->pinfo || ((struct GuLM *)(s->pinfo))->myid != GuLMid)
		return S_OOPS;

	if (!info || !strlen(info))
		return S_BADCONFIG;

	gulm = (struct GuLM *)s->pinfo;

	memset(data,0,sizeof(data));
	for (x = 0, y = 0; x < strlen(info) ; x++) {
		if (info[x] && info[x] > 31)
			data[y++] = info[x];
	}

	gulm->nodename = strdup(data);
	dprintf("Nodename = %s\n", gulm->nodename);

	return S_OK;
}

const char *
st_getinfo(Stonith __attribute__ ((unused)) *s,
	       int __attribute__ ((unused)) InfoType)
{
	return S_OK;
}


/**
 * Since we don't have a real power switch, we need to keep track of
 * GuLM's visibility of a given node.
 *
 * @param s	Stonith object
 * @return	S_OOPS on failure; S_OK on success.
 */
int
st_status(Stonith *s)
{
	gulm_interface_p pg = NULL;
	int ret = S_OOPS;
	struct GuLM *gulm;

	if (!s || !s->pinfo || ((struct GuLM *)(s->pinfo))->myid != GuLMid)
		return S_OOPS;

	gulm = (struct GuLM *)s->pinfo;

	/* Use DEVICE for our GuLM service name */
	/* 4/20 commit to gulm changes behavior; we want it to be empty string
	   so that lock_gulmd allows our connection */
	if (lg_initialize(&pg, "", (char *)DEVICE) != 0) {
		gulm->seen = 0;
		return S_OOPS;
	}

	if (!pg) {
		gulm->seen = 0;
		return S_OOPS;
	}

	if (gulm_login(pg) == S_OK) {
		ret = S_OK;
	} else {
		gulm->seen = 0;
		goto out;
	}

	/*
	 * Get the status of the node this particular struct controls
	 * This is so we know when a node was online and transitions to
	 * offline.
	 */
	if (gulm_status(pg, gulm->nodename) > 0) {
		dprintf("Member %s seen by gulm; fencing enabled!\n",
		        gulm->nodename);

		/*
		 * Two tries before we forget we saw the member
		 * This handles the case of:
		 *
		 * st_status = ok (gulm->seen = 2)
		 * (time interval elapses)
		 * 
		 * st_status = bad (gulm->seen = 1)
		 *
		 * membership change; st_reset { st_status = bad }
		 *
		 * If we don't do this (e.g. set to 1 or 0), set this to 1,
		 * then the second call to st_status would make it 0,
		 * and the st_reset would fail because gulm->seen == 0.
		 */
		gulm->seen = 2;

	} else if (gulm->seen) {
		gulm->seen--;
		if (gulm->seen < 0)
			gulm->seen = 0;
	}

	gulm_logout(pg);
out:
	lg_release(pg);
	return ret;
}


/**
 * Fence a host using GuLM.
 *
 * @param s		Stonith structure
 * @param host		Host to reboot
 *
 */
int
st_reset(Stonith *s, int request, const char *host)
{
	struct GuLM *gulm;
	gulm_interface_p pg = NULL;
	int ret = S_RESETFAIL;

	if (!s || !s->pinfo || ((struct GuLM *)(s->pinfo))->myid != GuLMid)
		return S_OOPS;

	gulm = (struct GuLM *)s->pinfo;

#ifdef ST_POWERON
	if (request == ST_POWERON)	/* No-op for power-on */
		return S_OK;
#endif

	/* Use DEVICE for our GuLM service name */
	if (lg_initialize(&pg, "", DEVICE) != 0)
		return S_OOPS;

	if (!pg)
		return S_OOPS;

	if (gulm_login(pg) == 0) {
		ret = gulm_fence(pg, host, gulm);
	} else {
		goto out;
	}

	gulm_logout(pg);
out:
	lg_release(pg);
	return ret;
}


/**
 * Clean up this module
 * @param s		Stonith structure.
 */
void
st_destroy(Stonith *s)
{
	struct GuLM *foo;

	if (!s || !s->pinfo || ((struct GuLM *)(s->pinfo))->myid != GuLMid)
		return;

	foo = s->pinfo;
	foo->myid = NOTGuLMid;
	if (foo->nodename) {
		free(foo->nodename);
		foo->nodename = NULL;
	}
}


/**
 * Initialize this module
 *
 * @return		NULL on failure, or pointer to newly allocated struct
 *			on success.
 */
void *
st_new(void)
{
	struct GuLM *foo;

	foo = malloc(sizeof(*foo));
	if (!foo)
		return NULL;

	foo->myid = GuLMid;
	foo->nodename = NULL;
	foo->seen = 0;

	return foo;
}
