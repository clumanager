/*
 *
 *  Copyright 2001 Mission Critical Linux, Inc.
 *  Copyright 2004 Bull
 *  Copyright 2005 Red Hat, Inc.
 *
 *  All Rights Reserved. ()
 */
/*
 * Cluster Manager 1.2 STONITH module for Bull NovaScale 'fame' hardware
 * using PAP/PAM manager.
 *
 * Tested with NSMasterHW-3.0-1 RPM from Bull.
 *
 * author: Lon Hohberger   <lhh@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <libintl.h>
#include <sys/wait.h>

#include "expect.h"
#include "stonith.h"

#ifndef NSMASTER_HW
#define NSMASTER_HW "/usr/local/bull/NSMasterHW/bin"
#endif

#define DEVICE  "NovaScale STONITH module for Fame"

#define N_(text)        (text)
#define _(text)         dgettext(ST_TEXTDOMAIN, text)

struct Fame64 {
        const char *    NSid;
        char *          idinfo;
        char *          unitid;
        pid_t           pid;
        int             config;
        char *          nshost;
        char *          user;
        char *          passwd;
};


/*
   lhh - Power states, or as best as I can figure them out from
   return codes from nsmpower.sh:

   0 - Powered on
   1 - Powered off
   2 - Powering off/powering on transitional state
 */
#define STATE_ON	0
#define STATE_OFF	1
#define STATE_TRANS	2



static const char * NSid = "NovaScale FAME";
static const char * NOTnsid = "Hey dummy, this has been destroyed (NS-Fame)";

#define ISNS(i) (((i)!= NULL && (i)->pinfo != NULL)     \
        && ((struct Fame64 *)(i->pinfo))->NSid == NSid)

#define ISCONFIGED(i)   (ISNS(i) && ((struct Fame64 *)(i->pinfo))->config)

#ifndef MALLOC
#       define  MALLOC  malloc
#endif
#ifndef FREE
#       define  FREE    free
#endif
#ifndef MALLOCT
#       define     MALLOCT(t)      ((t *)(MALLOC(sizeof(t)))) 
#endif

#define DIMOF(a)        (sizeof(a)/sizeof(a[0]))
#define WHITESPACE      " \t\n\r\f"

#define REPLSTR(s,v)    {                                       \
                        if ((s) != NULL) {                      \
                                FREE(s);                        \
                                (s)=NULL;                       \
                        }                                       \
                        (s) = MALLOC(strlen(v)+1);              \
                        if ((s) == NULL) {                      \
                                syslog(LOG_ERR, _("out of memory"));\
                        }else{                                  \
                                memcpy((s),(v),strlen(v)+1);            \
                        }                                       \
                        }

static int      NS_Reset(struct Fame64 *st, const char *domain);
static int      NS_Off(struct Fame64 *st, const char *domain);
static int      NS_On(struct Fame64 *st, const char *domain);
static int      NS_Status(struct Fame64 *st);
static int      NS_parse_config_info(struct Fame64 *st, const char *info);

int             st_setconffile(Stonith *, const char *cfgname);
int             st_setconfinfo(Stonith *, const char *info);
const char *    st_getinfo(Stonith * s, int InfoType);
int             st_status(Stonith * );
int             st_reset(Stonith * s, int request, const char * port);
void            st_destroy(Stonith *);
void *          st_new(void);


/*
   Reset the specified fame domain.  This isn't used by Cluster Manager.
 */
static int
NS_Reset(struct Fame64* ns, const char *domain)
{
        int             my_ok;

#if defined(ST_POWERON) && defined(ST_POWEROFF)
	my_ok = NS_Off(ns, domain);
	if (my_ok != S_OK)
		return my_ok;
	my_ok = NS_On(ns, domain);
	return my_ok;
         
#else /* if defined(ST_POWERON) && defined(ST_POWEROFF) */

        char            cmd[1024], cmd0[256];
	/* 
	   Use a single reboot operation.
	   XXX WARNING: Untested, not supported...
	 */
        snprintf(cmd0, sizeof(cmd0),
		 NSMASTER_HW "/nsmreset.sh -m fame -M %s -D \"%s\"",
	 	 ns->nshost, domain);

        /* nsmreset -H host -m fame -M outletName -u user -p password */ 
	snprintf(cmd, sizeof(cmd),
		 "%s -u %s -p %s", cmd0, ns->user, ns->passwd);
#ifdef DEBUG
	syslog(LOG_DEBUG, _("NovaScale:  %s\n"), cmd);
#endif

	my_ok = system(cmd);
	if (!WIFEXITED(my_ok) || (WEXITSTATUS(my_ok) != 0)) {
                syslog(LOG_CRIT, _("Failed to reboot %s: %d\n"), host, my_ok);
                return S_RESETFAIL;
        }
        /* Bigrement sommaire et risque! */

        return S_OK;
#endif /* if defined(ST_POWERON) && defined(ST_POWEROFF) */
}


#if defined(ST_POWERON) || defined(ST_POWEROFF)
static int
wait_for_state(struct Fame64* ns, const char *domain, int state,
	       int timeout)
{
        int             my_ok;
        char            cmd[1024], cmd0[256];

        snprintf(cmd0, sizeof(cmd0),
		 NSMASTER_HW "/nsmpower.sh -m fame -M %s -D \"%s\"",
	 	 ns->nshost, domain);
        snprintf(cmd, sizeof(cmd),
		 "%s -a status -u %s -p %s", cmd0, ns->user, ns->passwd);

	while (timeout > 0) {
		/*
		   Step 1: Wait awhile
		 */
		sleep(3);
		timeout -= 3;

		/*
		   Step 2: Issue the command
		 */
		my_ok = system(cmd);

		/* If we didn't exit normally, try again */
		if (!WIFEXITED(my_ok))
			continue;

		my_ok = WEXITSTATUS(my_ok);
		if (my_ok == state)
			break;
	}

	return my_ok;
}
#endif


#if defined(ST_POWEROFF)
/*
   Power off the given domain using the manager console
 */
static int
NS_Off(struct Fame64* ns, const char *domain)
{
        int             my_ok;
        char            cmd[1024], cmd0[256];

	/*
	   Step 1: Force power off to the given domain.

	   -M -> Management console IP address or host name
	   -D -> Domain name of the host we're power cycling
	 */
        snprintf(cmd0, sizeof(cmd0),
		 NSMASTER_HW "/nsmpower.sh -m fame -M %s -D \"%s\"",
	 	 ns->nshost, domain);
        snprintf(cmd, sizeof(cmd),
		 "%s -a off_force -u %s -p %s", cmd0, ns->user, ns->passwd);
#ifdef DEBUG
        syslog(LOG_DEBUG, _("NovaScale:  %s\n"), cmd);
#endif

        my_ok = system(cmd);
	if (!WIFEXITED(my_ok) || (WEXITSTATUS(my_ok) != 0)) {
                syslog(LOG_CRIT, _("Failed to force off domain %s: %d\n"),
		       domain, my_ok);
                return S_RESETFAIL;
        }

	/*
	   Step 2: Wait for the PAP/PAM manager to tell us the state is 
	   powered off.
	 */
	if (wait_for_state(ns, domain, STATE_OFF, 60) != STATE_OFF) {
		syslog(LOG_CRIT, _("Domain %s status is not OFF.\n"), domain);
		return S_RESETFAIL;
	}

	return S_OK;
}
#endif /* if defined(ST_POWEROFF) */


#if defined(ST_POWERON)
/*
   Power on the given domain via the manager
 */
static int
NS_On(struct Fame64* ns, const char *domain)
{
        int             my_ok;
        char            cmd[1024], cmd0[256];

	/*
	   Step 1: turn on domain.

	   -M -> Management console IP address or host name
	   -D -> Domain name of the host we're power cycling
	 */
        snprintf(cmd0, sizeof(cmd0),
		 NSMASTER_HW "/nsmpower.sh -m fame -M %s -D \"%s\"",
		 ns->nshost, domain);
        snprintf(cmd, sizeof(cmd),
		 "%s -a on -u %s -p %s", cmd0, ns->user, ns->passwd);
#ifdef DEBUG
        syslog(LOG_DEBUG, _("NovaScale:  %s\n"), cmd);
#endif

        my_ok = system(cmd);
	if (!WIFEXITED(my_ok) || (WEXITSTATUS(my_ok) != 0)) {
                syslog(LOG_CRIT, _("Failed to power on domain %s: %d\n"),
		       domain, my_ok);
                return S_OOPS;
        }

	/*
	   Step 2: Wait for the PAP/PAM manager to tell us the state is 
	   powered on.
	 */
	if (wait_for_state(ns, domain, STATE_ON, 120) != STATE_ON) {
		syslog(LOG_CRIT, _("Domain %s status is not ON\n"), domain);
	}

	return S_OK;
}
#endif /* if defined(ST_POWERON)*/


/*
   Check to see if we can communicate with the fame hardware.  If we can,
   we would also then hope that we can issue commands. (This is not meant
   to figure out the power state of a given port).  Note that the status
   function doesn't take a port/domain argument in Cluster Manager 1.2.

   This is for periodic status checks by cluquorumd.  Regardless of the
   reported status, we always try to power cycle on node failure/hang.
 */
static inline int
NS_Status(struct Fame64* ns)
{
        int             my_ok;
        char            cmd[1024], cmd0[256];

        snprintf(cmd0, sizeof(cmd0),
		 NSMASTER_HW "/nsminfo.sh -m fame -M %s", ns->nshost);
        snprintf(cmd, sizeof(cmd),
		 "%s -i domainlist -u %s -p %s >/dev/null",
		 cmd0, ns->user, ns->passwd);
#ifdef DEBUG
        syslog(LOG_DEBUG, _("NovaScale:  %s\n"), cmd);
#endif

        my_ok = system(cmd);
	if (!WIFEXITED(my_ok) || (WEXITSTATUS(my_ok) != 0)) {
		/* cluquorumd will tell everyone that the fame device
		   isn't working; no need to log it here. */
                return S_OOPS;
	}

	return S_OK;
}


/*
 * My French ability is non existent.  The below function is not supposed
 * to check the status of the port controlling a cluster node; rather, it
 * is supposed to check for communication with the STONITH device.  In this
 * case, the PAP/PAM manager.
 *
 * Cette fonction est pour savoir si le power switch est operationnel
 * Elle est appelee par shoot_partner via clu_power_check_msg_uncached pour
 * decider si le 'services takeover' peut etre realise et comment
 * DONC si le partner est OFF ou doit etre reboote
 * DONC il faut recuperer le status du node 
 * PB: sans modifier soit la structure Stonith pour stocker le port name
 *     soit l'appel de la fonction st_status pour passer le host et recuperer
 *     l'adresse par ST_Name2Outlet, c'est pas possible!
 */
int
st_status(Stonith  *s)
{
        struct Fame64*  ns;
        int     rc;

        if (!ISNS(s)) {
                syslog(LOG_ERR, "invalid argument to ST_status");
                return(S_OOPS);
        }
        if (!ISCONFIGED(s)) {
                syslog(LOG_ERR
                ,       "unconfigured stonith object in ST_status");
                return(S_OOPS);
        }

        ns = (struct Fame64*) s->pinfo;

        rc = NS_Status(ns);
        return(rc);
}


/*
 *      Parse the given configuration information, and stash it away...
 */
static int
NS_parse_config_info(struct Fame64* ns, const char * info)
{
        static char host[1024];
        static char user[1024];
        static char passwd[1024];

        if (ns->config) {
                return(S_OOPS);
        }

        if (sscanf(info, "%1023s %1023s %1023[^\n\r\t]",
		   host, user, passwd) == 3 && strlen(passwd) > 1) {

                if ((ns->nshost = (char *)MALLOC(strlen(host)+1)) == NULL) {
                        syslog(LOG_ERR, "out of memory");
                        return(S_OOPS);
                }

                if ((ns->user = (char *)MALLOC(strlen(user)+1)) == NULL) {
                        free(ns->nshost);
                        ns->nshost=NULL;
                        syslog(LOG_ERR, "out of memory");
                        return(S_OOPS);
                }

                if ((ns->passwd = (char *)MALLOC(strlen(passwd)+1)) == NULL) {
                        free(ns->nshost);
                        ns->nshost=NULL;
                        free(ns->user);
                        ns->user=NULL;
                        syslog(LOG_ERR, "out of memory");
                        return(S_OOPS);
                }
                memcpy(ns->nshost, host, strlen(host)+1);
                memcpy(ns->user, user, strlen(user)+1);
                memcpy(ns->passwd, passwd, strlen(passwd)+1);
                ns->config = 1;
                return(S_OK);
        }
        return(S_BADCONFIG);
}

/*
 *      Reset the given host on this Stonith device.  
 */
int
st_reset(Stonith * s, int request, const char * domain)
{
        struct Fame64*  ns;
        int     rc = 0;

        if (!ISNS(s)) {
                syslog(LOG_ERR, "invalid argument to st_reset");
                return(S_OOPS);
        }
        if (!ISCONFIGED(s)) {
                syslog(LOG_ERR
                ,       "unconfigured stonith object in reset");
                return(S_OOPS);
        }
        ns = (struct Fame64*) s->pinfo;

        switch(request) {
#if defined(ST_POWERON) 
                case ST_POWERON:
                        rc = NS_On(ns, domain);
                        break;
#endif
#if defined(ST_POWEROFF)
                case ST_POWEROFF:
                        rc = NS_Off(ns, domain);
                        break;
#endif
                case ST_GENERIC_RESET:
                        rc = NS_Reset(ns, domain);
                        break;
                default:
                        rc = S_INVAL;
                        break;
        }

        return(rc);
}


/*
 *      Parse the information in the given configuration file,
 *      and stash it away...
 */
int
st_setconffile(Stonith* s, const char * configname)
{
        FILE *  cfgfile;
        char    NSbuf[256];
        struct Fame64*  st;
        int     rv;

        if (!ISNS(s)) {
                syslog(LOG_ERR, "invalid argument to ST_set_configfile");
                return(S_OOPS);
        }
        st = (struct Fame64*) s->pinfo;

        if ((cfgfile = fopen(configname, "r")) == NULL)  {
                syslog(LOG_ERR, _("Cannot open %s"), configname);
                return(S_BADCONFIG);
        }
        while (fgets(NSbuf, sizeof(NSbuf), cfgfile) != NULL){
                if (*NSbuf == '#' || *NSbuf == '\n' || *NSbuf == EOS) {
                        continue;
                }
                rv = NS_parse_config_info(st, NSbuf);
                fclose(cfgfile);
                return rv;
        }
        fclose(cfgfile);
        return(S_BADCONFIG);
}


/*
 *      Parse the config information in the given string, and stash it away...
 */
int
st_setconfinfo(Stonith* s, const char * info)
{
        struct Fame64* st;

        if (!ISNS(s)) {
                syslog(LOG_ERR, "ST_provide_config_info: invalid argument");
                return(S_OOPS);
        }
        st = (struct Fame64 *)s->pinfo;

        return(NS_parse_config_info(st, info));
}


const char *
st_getinfo(Stonith * s, int reqtype)
{
        struct Fame64*  st;
        char *          ret;

        if (!ISNS(s)) {
                syslog(LOG_ERR, "st_getinfo: invalid argument");
                return NULL;
        }
        /*
         *      We look in the ST_TEXTDOMAIN catalog for our messages
         */
        st = (struct Fame64 *)s->pinfo;
        
        switch (reqtype) {
        case ST_DEVICEID:
                ret = st->idinfo;
                break;
                
        case ST_CONF_INFO_SYNTAX:
                ret = _("IP-address login password\n"
                        "The IP-address, login and password are white-space delimited.");
                break;
                
        case ST_CONF_FILE_SYNTAX:
                ret = _("IP-address login password\n"
                        "The IP-address, login and password are white-space delimited.  "
                        "All three items must be on one line.  "
                        "Blank lines and lines beginning with # are ignored");
                break;
                
        default:
                ret = NULL;
                break;
        }
        return ret;
}

/*
 *      Fame64 Sentry Stonith destructor...
 */
void
st_destroy(Stonith *s)
{
        struct Fame64* ns;

        if (!ISNS(s)) {
                syslog(LOG_ERR, "st_destroy: invalid argument");
                return;
        }
        ns = (struct Fame64 *)s->pinfo;

        ns->NSid = NOTnsid;
        if (ns->nshost != NULL) {
                FREE(ns->nshost);
                ns->nshost = NULL;
        }
        if (ns->user != NULL) {
                FREE(ns->user);
                ns->user = NULL;
        }
        if (ns->passwd != NULL) {
                FREE(ns->passwd);
                ns->passwd = NULL;
        }
        if (ns->idinfo != NULL) {
                FREE(ns->idinfo);
                ns->idinfo = NULL;
        }
        if (ns->unitid != NULL) {
                FREE(ns->unitid);
                ns->unitid = NULL;
        }
}

/* Create a new Fame64 Sentry Stonith device. */

void *
st_new(void)
{
        struct Fame64 * st = MALLOCT(struct Fame64);

#ifdef DEBUG
        syslog(LOG_NOTICE, "Testing the Fame64 Sentry Unit..\n");
#endif
        if (st == NULL) {
                syslog(LOG_ERR, "out of memory");
                return(NULL);
        }
        memset(st, 0, sizeof(*st));
        st->NSid = NSid;
        st->pid = -1;
        st->config = 0;
        st->user = NULL;
        st->nshost = NULL;
        st->passwd = NULL;
        st->idinfo = NULL;
        st->unitid = NULL;
        REPLSTR(st->idinfo, DEVICE);
        REPLSTR(st->unitid, "unknown");

        return((void *)st);
}
