/*
 *
 *  Copyright 2001 Mission Critical Linux, Inc.
 *
 *  All Rights Reserved.
 */
/** @file
 * Stonith module for ServerTech Sentry (Models 4820, R-2000).
 *
 *  author: Aashu Virmani      <avirmani@winphoria.com>
 *          Lon H. Hohberger   <lhh at redhat.com>
 *
 *  Based strongly on apcmaster.c code by Mike Ledoux, since 
 *  servertech is closest in behavior to the above.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*                          Observations/Notes
 * 
 * 1. The ServerTech Sentry unit, much like the APC MasterSwitch and unlike 
 *    the BayTech network power switch, accepts only one (telnet) session
 *    at a time. When one session is active, any subsequent attempt to connect
 *    to connect to the MasterSwitch will result in a connection refused/closed 
 *    failure. In a cluster environment utilizing polling/monitoring of the 
 *    ServerTech Switch (from multiple nodes), this can clearly cause problems. 
 *    Obviously the more nodes and the shorter the polling interval, the more 
 *    frequently such errors/collisions may occur.
 *
 * 2. We observed that on busy networks where there may be high occurances
 *    of broadcasts, the ServerTech switch became unresponsive.  In some 
 *    configurations this necessitated placing the power switch onto a 
 *    private subnet.
 *
 * 3. The Sentry's network interface is a separate (but internal) module 
 *    called the MSSLite which connects to the Sentry unit (internally) via
 *    a serial cable.  The Sentry unit itself does not support multiple
 *    logins - that is, two people may NOT telnet to it at the same time.
 *    It is also not possible to log in via the console port and the MSSLite
 *    simultaneously.  One of the two (at least on the R-2000) can get into 
 *    a state where the network port no longer work, producing the following
 *    error upon a connect attempt:
 *
 *    Port 1 (Sentry): Port not available.
 *
 *    This error is most easily fixed via telnetting to the MSSLite, setting
 *    privileged mode, and rebooting the Sentry (using "init delay 0"). Anyway,
 *    this all implies that NSPF is broken while using this switch.
 *
 * 4. The Sentry sometimes seems to enter "Screen" mode on its own, apparently
 *    depending on exactly when the socket is closed.  When a user reconnects,
 *    (s)he is presented with this mode and not asked for a password or
 *    user name.
 */

/*
 * ChangeLog:
 *
 * 2003-02-21: lhh at redhat.com
 *	- removed st_hostlist and st_freehostlist from ops.  All power
 *	cycle operations are done with port names; not host names.
 */

/*
 * Version string that is filled in by CVS
 */
static const char *version __attribute__ ((unused)) = "$Revision: 1.8 $"; 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <libintl.h>
#include <sys/wait.h>

#include "expect.h"
#include "stonith.h"

#define	DEVICE	"ServerTech Sentry"

#define N_(text)	(text)
#define _(text)		dgettext(ST_TEXTDOMAIN, text)

/*
 *  Tested on:  ServerTech Sentry 4820
 *              ServerTech Sentry R-2000
 */

struct ServerTech {
        const char *	STid;
	char *		idinfo;
	char *		unitid;
	pid_t		pid;
	int		rdfd;
	int		wrfd;
	int		config;
	char *		device;
        char *		user;
	char *		passwd;
};


static const char * STid = "Sentry";
static const char * NOTstid = "Hey dummy, this has been destroyed (ST Sentry)";

#define	ISST(i)	(((i)!= NULL && (i)->pinfo != NULL)	\
	&& ((struct ServerTech *)(i->pinfo))->STid == STid)

#define	ISCONFIGED(i)	(ISST(i) && ((struct ServerTech *)(i->pinfo))->config)

#ifndef MALLOC
#	define	MALLOC	malloc
#endif
#ifndef FREE
#	define	FREE	free
#endif
#ifndef MALLOCT
#	define     MALLOCT(t)      ((t *)(MALLOC(sizeof(t)))) 
#endif

#define DIMOF(a)	(sizeof(a)/sizeof(a[0]))
#define WHITESPACE	" \t\n\r\f"

#define	REPLSTR(s,v)	{					\
			if ((s) != NULL) {			\
				FREE(s);			\
				(s)=NULL;			\
			}					\
			(s) = MALLOC(strlen(v)+1);		\
			if ((s) == NULL) {			\
				syslog(LOG_ERR, _("out of memory"));\
			}else{					\
				memcpy((s),(v),strlen(v)+1);		\
			}					\
			}

/*
 *	Different expect strings that we get from the ServerTech Sentry
 */


static struct Etoken EscapeChar[] =	{ {"Escape character is '^]'.", 0, 0}
					,	{NULL,0,0}};

static struct Etoken login[] = 		{ {"Username:", 0, 0}, 
	/* Left session around */	  {"Sentry:", 1, 0},
	/* Busy */			  {"Please try again later.", 2, 0},
	/* Screen mode (?) XXX */	  {"[", 3, 0},
	/* BAD BAD BAD BAD */		  {"Port not available.",4,0},
					  {NULL,0,0}};
static struct Etoken password[] =	{ {"Password:", 0, 0},
					  {NULL,0,0}};
static struct Etoken Prompt[] =	        { {"Sentry:", 0, 0} , {NULL,0,0}};
static struct Etoken LoginOK[] =	{ {"Sentry:", 0, 0},
					  {"word entered is NOT valid",1,0},
					  {NULL,0,0}};
/* We may get a notice about rebooting, or a request for confirmation */
static struct Etoken Processing[] =	{ {"1 port(s) rebooted", 0, 0},
					  {"1 port(s) turned off", 1, 0},
					  {"1 port(s) turned on", 2, 0},
					  {"0 port(s) rebooted", 3, 0},
					  {"0 port(s) turned off", 4, 0},
					  {"0 port(s) turned on", 5, 0},
					  {NULL,0,0}};
#if 0
static struct Etoken Goodbye[] =	{ {"Session ended", 0, 0},
					  {NULL,0,0}};
#endif

static int	ST_LookFor(struct ServerTech* st, struct Etoken * tlist, int timeout);
static int	ST_connect_device(struct ServerTech * st);
static int	ST_Login(struct ServerTech * st);
static int	ST_RobustLogin(struct ServerTech * st);
static int	ST_Reset(struct ServerTech*, const char * port);
static int	ST_Logout(struct ServerTech * st);
static void	ST_killcomm(struct ServerTech * st);
static int	ST_parse_config_info(struct ServerTech* st, const char * info);

#if defined(ST_POWERON) && defined(ST_POWEROFF)
static int	ST_onoff(struct ServerTech*, const char * port, int request);
#endif

int             st_setconffile(Stonith *, const char * cfgname);
int	        st_setconfinfo(Stonith *, const char * info);
const char *	st_getinfo(Stonith * s, int InfoType);
int	        st_status(Stonith * );
int	        st_reset(Stonith * s, int request, const char * host);
void	        st_destroy(Stonith *);
void *	        st_new(void);


/*
 *	We do these things a lot.  Here are a few shorthand macros.
 */

#define	SEND(s)         (write(st->wrfd, (s), strlen(s)))

#define	EXPECT(p,t)	{						\
			        if (ST_LookFor(st, p, t) < 0)		\
				        return(errno == ETIMEDOUT	\
			        ?	S_TIMEOUT : S_OOPS);		\
			}

#define	NULLEXPECT(p,t)	{						\
				if (ST_LookFor(st, p, t) < 0)		\
					return(NULL);			\
			}


/* Look for any of the given patterns.  We don't care which */
static int
ST_LookFor(struct ServerTech* st, struct Etoken * tlist, int timeout)
{
	int	rc;
	char    saw[1024];

	if ((rc = ExpectToken(st->rdfd, tlist, timeout, saw, 1024)) < 0) {
		/*
		syslog(LOG_DEBUG, _("Did not find string: '%s' from %s.")
		,	tlist[0].string, DEVICE);
		 */
		ST_killcomm(st);
	}
	return(rc);
}


/* Login to the ServerTech Unit */
static int
ST_Login(struct ServerTech * st)
{
	static int bad_state = 0;

	/*syslog(LOG_DEBUG, _("Attempting to Login to Sentry unit now..\n"));*/

        EXPECT(EscapeChar, 10);

  	/* 
	 * We should be looking at something like this:
         *	Username:
	 */
	switch (ST_LookFor(st, login, 10)) {
	case 0:
		if (bad_state) {
			syslog(LOG_NOTICE, _("Sentry returned to nominal "
					     "state."));
			bad_state = 0;
		}
		break;
	case 1:
		/*
		 * lhh - Sometimes, the Sentry doesn't reset fast enough,
		 * or something...  Anyway, it, on rare occasions,
		 * doesn't present the login prompt and heads strait
		 * to the command prompt!
		 */
		syslog(LOG_DEBUG, _("debug: (Anomaly) Session restored?!\n"));
		goto out;
	case 2:
		/* lhh - someone else is logged in.  No matter, retry.  */
		return S_ACCESS;
	case 3:
		/* lhh - ... yes, sometimes, it can get it here.  */
		syslog(LOG_DEBUG, _("debug: (Anomaly) In screen mode?!\n"));
		SEND("c\r");
		goto out;
	case 4:
		/*
		 * lhh - Sometimes the Sentry gets into an anomalous state
		 * where we can no longer telnet to it... hence, no reboot.
		 * XXX Sometimes, you can get out of it by telnetting to
		 * the management port (23) and rebooting the network card
		 * (init delay 0) from privileged mode.
		 */
		if (!bad_state) {
			syslog(LOG_ALERT, _("Sentry in anomalous state. "
					    "Power cycling may not work."));
			bad_state = 1;
		}
	default:
		ST_killcomm(st);
		return(errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS);
	}
 
	/* Got the login stuff right.  Send the username */
	SEND(st->user);       
	SEND("\r");

	/* Expect "Password:" */
	EXPECT(password, 10);
	SEND(st->passwd);
	SEND("\r");

	/* Great, we entered the login + password - now wait for the "ok" */
	switch (ST_LookFor(st, LoginOK, 30)) {
	case 0:	/* Good! */
		break;
		
	case 1:	/* Uh-oh - bad password */
		syslog(LOG_ERR, _("Invalid password for " DEVICE "."));
		return(S_ACCESS);
		
	default:
		ST_killcomm(st);
		return(errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS);
	} 

out:
	/*syslog(LOG_DEBUG, _("Sentry: Successful login!\n"));*/
	return(S_OK);
}


/* Attempt to login up to 20 times... */
/*
 * lhh - the manual says it will reset in a max of 15 seconds, assuming
 * modem initialization strings, etc.  So, we will use 1 second delays,
 * and try 25 times -- this should be plenty.
 */
static int
ST_RobustLogin(struct ServerTech * st)
{
	int	rc=S_OOPS;
	int	j;
	int	sltime = 62500;
	

	for (j=0; j < 25 && rc != S_OK; ++j) {

	  if (st->pid > 0) {
			ST_killcomm(st);
		}

		if (ST_connect_device(st) != S_OK)
		        ST_killcomm(st);
		else
			rc = ST_Login(st);
			
		usleep(sltime);
		if (sltime < 2000000)
			sltime *= 2;
	}
	return rc;
}


#if 0 /* lhh - maybe later */
static void
wait_for_close(struct ServerTech *st, int secs)
{
	fd_set rfds;
	int buf[256];
	struct timeval tv;

	tv.tv_sec = secs;
	tv.tv_usec = 0;

	FD_ZERO(&rfds);
	FD_SET(st->rdfd, &rfds);

	while (select(st->rdfd+1, &rfds, NULL, NULL, &tv) > 0) {
		if (read(st->rdfd,buf,sizeof(buf)) == 0) {
			break;
		}
	}

	close(st->rdfd);
	st->rdfd = -1;
}
#endif


/* Log out of the ServerTech Sentry unit */
static 
int ST_Logout(struct ServerTech* st)
{
	int	rc;

	/* Make sure we're in the right menu... */
        SEND("c\r");
	
	/* Expect "Sentry: " */
	rc = ST_LookFor(st, Prompt, 5);

	/* "QUIT" is logout */
	SEND("logout\r");

	/*
	ST_LookFor(st, Goodbye, 5);

	wait_for_close(st, 7);
	*/

	ST_killcomm(st);
	return(rc >= 0 ? S_OK : (errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS));
}

static void
ST_killcomm(struct ServerTech* st)
{
        if (st->rdfd >= 0) {
		close(st->rdfd);
		st->rdfd = -1;
	}
	if (st->wrfd >= 0) {
		close(st->wrfd);
		st->wrfd = -1;
	}
	if (st->pid > 0) {
		kill(st->pid, SIGKILL);
		(void)waitpid(st->pid, NULL, 0);
		st->pid = -1;
	}
}


/* Reset (power-cycle) the given outlets */
static int
ST_Reset(struct ServerTech* st, const char *port)
{
  	char		unum[32];
	int             my_ok;

	/* Expect "Sentry: " */
	SEND("c\r");
	EXPECT(Prompt, 5);

	/* Select requested outlet */
	sprintf(unum, "reboot %s\r", port);
  	SEND(unum);
	syslog(LOG_INFO, _("Port %s being power cycled."), port);

	/* See if this was successful */
	switch (ST_LookFor(st, Processing, 5)) {
	case 0: /* Got "1 port(s) rebooted" */	  
		syslog(LOG_INFO, _("Power restored to host %s."), port);
		my_ok = S_OK;
		break;
	case 3: /* Got "0 port(s) rebooted" */	  
		syslog(LOG_INFO, _("Reset of host %s failed."), port);
		my_ok = S_RESETFAIL;
		break;
	} 

	/* Expect "Sentry: " */
	if (ST_LookFor(st, Prompt, 10) < 0) {
		return(errno == ETIMEDOUT ? S_RESETFAIL : S_OOPS);
	}

	/* ok.. we got the prompt back.. return the proper return code */
	return(my_ok);
}


#if defined(ST_POWERON) && defined(ST_POWEROFF)
static int
ST_onoff(struct ServerTech* st, const char * port, int req)
{
	char		unum[32];
	int             my_ok;

	const char *	onoff = (req == ST_POWERON ? "ON " : "OFF ");
	int	rc;

	if ((rc = ST_RobustLogin(st) != S_OK)) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
		return(rc);
	}
	
	/* Make sure we're in the top level menu */
        SEND("c\r");
	EXPECT(Prompt, 5);

	/* Select requested outlet */
	sprintf(unum, "%s %s\r", onoff, port);
  	SEND(unum);
	syslog(LOG_INFO, _("Port %s being powered %s."), port, onoff);

	/* See if this was successful */
	switch (ST_LookFor(st, Processing, 5)) {
	case 1: /* Got "1 port(s) turned off" */	  
	case 2: /* Got "1 port(s) turned on" */	  
		syslog(LOG_INFO,
		       _("Port %s successfully powered %s."), port, onoff);
		my_ok = S_OK;
		break;
	case 4: /* Got "0 port(s) turned off" */	  
	case 5: /* Got "0 port(s) turned on" */	  
		syslog(LOG_INFO, _("Power %s of port %s failed."), onoff, port);
		my_ok = S_RESETFAIL;
		break;
	} 

	/* Expect "Sentry: " */
	if (ST_LookFor(st, Prompt, 10) < 0) {
		return(errno == ETIMEDOUT ? S_RESETFAIL : S_OOPS);
	}
	
	/* ok.. we got the prompt back.. return the proper return code */
	return(my_ok);
	
}
#endif /* defined(ST_POWERON) && defined(ST_POWEROFF) */


int
st_status(Stonith  *s)
{
	struct ServerTech*	st;
	int	rc;

	if (!ISST(s)) {
		syslog(LOG_ERR, "invalid argument to ST_status");
		return(S_OOPS);
	}
	if (!ISCONFIGED(s)) {
		syslog(LOG_ERR
		,	"unconfigured stonith object in ST_status");
		return(S_OOPS);
	}
	st = (struct ServerTech*) s->pinfo;

	if ((rc = ST_RobustLogin(st) != S_OK)) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
		return(rc);
	}

	return(ST_Logout(st));
}


/*
 *	Parse the given configuration information, and stash it away...
 */
static int
ST_parse_config_info(struct ServerTech* st, const char * info)
{
	static char dev[1024];
	static char user[1024];
	static char passwd[1024];

	if (st->config) {
		return(S_OOPS);
	}

	if (sscanf(info, "%1023s %1023s %1023[^\n\r\t]", dev, user, passwd) == 3
	&&	strlen(passwd) > 1) {

		if ((st->device = (char *)MALLOC(strlen(dev)+1)) == NULL) {
			syslog(LOG_ERR, "out of memory");
			return(S_OOPS);
		}
		if ((st->user = (char *)MALLOC(strlen(user)+1)) == NULL) {
			free(st->device);
			st->device=NULL;
			syslog(LOG_ERR, "out of memory");
			return(S_OOPS);
		}
		if ((st->passwd = (char *)MALLOC(strlen(passwd)+1)) == NULL) {
			free(st->device);
			st->device=NULL;
			free(st->user);
			st->user=NULL;
			syslog(LOG_ERR, "out of memory");
			return(S_OOPS);
		}
		memcpy(st->device, dev, strlen(dev)+1);
		memcpy(st->user, user, strlen(user)+1);
		memcpy(st->passwd, passwd, strlen(passwd)+1);
		st->config = 1;
		return(S_OK);
	}
	return(S_BADCONFIG);
}

/*
 *	Connect to the given ST device.  
 */
static int
ST_connect_device(struct ServerTech * st)
{
	char	TelnetCommand[256];

	snprintf(TelnetCommand, sizeof(TelnetCommand)
	,	"exec telnet -K -c %s 2001 2>/dev/null", st->device);

	/*syslog(LOG_DEBUG, "starting process %s\n", TelnetCommand);*/
	st->pid=StartProcess(TelnetCommand, &st->rdfd, &st->wrfd, 0);
	if (st->pid <= 0) {
		return(S_OOPS);
	}
	return(S_OK);
}


/*
 *	Reset the given host on this Stonith device.  
 */
int
st_reset(Stonith * s, int request, const char * port)
{
	int	rc = 0;
	int	lorc = 0;
	struct ServerTech*	st;

	if (!ISST(s)) {
		syslog(LOG_ERR, "invalid argument to ST_reset_host");
		return(S_OOPS);
	}
	if (!ISCONFIGED(s)) {
		syslog(LOG_ERR
		,	"unconfigured stonith object in ST_reset_host");
		return(S_OOPS);
	}
	st = (struct ServerTech*) s->pinfo;

	if ((rc = ST_RobustLogin(st)) != S_OK) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
		return(rc);
	}else{
		switch(request) {

#if defined(ST_POWERON) && defined(ST_POWEROFF)
		case ST_POWERON:
		        rc = ST_onoff(st, port, request);
			break;
		case ST_POWEROFF:
			rc = ST_onoff(st, port, request);
			break;
#endif
		case ST_GENERIC_RESET:
			rc = ST_Reset(st, port);
			break;
		default:
			rc = S_INVAL;
			break;
		}
	}

	lorc = ST_Logout(st);
	return(rc != S_OK ? rc : lorc);
}

/*
 *	Parse the information in the given configuration file,
 *	and stash it away...
 */
int
st_setconffile(Stonith* s, const char * configname)
{
	FILE *	cfgfile;
	char	STbuf[256];
	struct ServerTech*	st;

	if (!ISST(s)) {
		syslog(LOG_ERR, "invalid argument to ST_set_configfile");
		return(S_OOPS);
	}
	st = (struct ServerTech*) s->pinfo;

	if ((cfgfile = fopen(configname, "r")) == NULL)  {
		syslog(LOG_ERR, _("Cannot open %s"), configname);
		return(S_BADCONFIG);
	}
	while (fgets(STbuf, sizeof(STbuf), cfgfile) != NULL){
		if (*STbuf == '#' || *STbuf == '\n' || *STbuf == EOS) {
			continue;
		}
		fclose(cfgfile);
		return(ST_parse_config_info(st, STbuf));
	}
	fclose(cfgfile);
	return(S_BADCONFIG);
}

/*
 *	Parse the config information in the given string, and stash it away...
 */
int
st_setconfinfo(Stonith* s, const char * info)
{
	struct ServerTech* st;

	if (!ISST(s)) {
		syslog(LOG_ERR, "ST_provide_config_info: invalid argument");
		return(S_OOPS);
	}
	st = (struct ServerTech *)s->pinfo;

	return(ST_parse_config_info(st, info));
}
const char *
st_getinfo(Stonith * s, int reqtype)
{
	struct ServerTech* st;
	char *		ret;

	if (!ISST(s)) {
		syslog(LOG_ERR, "st_getinfo: invalid argument");
		return NULL;
	}
	/*
	 *	We look in the ST_TEXTDOMAIN catalog for our messages
	 */
	st = (struct ServerTech *)s->pinfo;
	
	switch (reqtype) {
	case ST_DEVICEID:
		ret = st->idinfo;
		break;
		
	case ST_CONF_INFO_SYNTAX:
		ret = _("IP-address login password\nThe IP-address, login "
			"and password are white-space delimited.");
		break;
		
	case ST_CONF_FILE_SYNTAX:
		ret = _("IP-address login password\nThe IP-address, login "
			"and password are white-space delimited.  All three "
			"items must be on one line.  Blank lines and lines "
			"beginning with # are ignored");
		break;
		
	default:
		ret = NULL;
		break;
	}
	return ret;
}


/*
 *	ServerTech Sentry Stonith destructor...
 */
void
st_destroy(Stonith *s)
{
	struct ServerTech* st;

	if (!ISST(s)) {
		syslog(LOG_ERR, "st_destroy: invalid argument");
		return;
	}
	st = (struct ServerTech *)s->pinfo;

	st->STid = NOTstid;
	ST_killcomm(st);
	if (st->device != NULL) {
		FREE(st->device);
		st->device = NULL;
	}
	if (st->user != NULL) {
		FREE(st->user);
		st->user = NULL;
	}
	if (st->passwd != NULL) {
		FREE(st->passwd);
		st->passwd = NULL;
	}
	if (st->idinfo != NULL) {
		FREE(st->idinfo);
		st->idinfo = NULL;
	}
	if (st->unitid != NULL) {
		FREE(st->unitid);
		st->unitid = NULL;
	}
}


/*
 * Create a new ServerTech Sentry Stonith device.
 */
void *
st_new(void)
{
	struct ServerTech*	st = MALLOCT(struct ServerTech);

	if (st == NULL) {
		syslog(LOG_ERR, "out of memory");
		return(NULL);
	}
	memset(st, 0, sizeof(*st));
	st->STid = STid;
	st->pid = -1;
	st->rdfd = -1;
	st->wrfd = -1;
	st->config = 0;
	st->user = NULL;
	st->device = NULL;
	st->passwd = NULL;
	st->idinfo = NULL;
	st->unitid = NULL;
	REPLSTR(st->idinfo, DEVICE);
	REPLSTR(st->unitid, "unknown");

	return((void *)st);
}
