/** @file
 *  Generic Telnet functions which are more ore less common to STONITH modules
 *  which use telnet.  Designed to replace the tons of replicated code in
 *  apcmaster.c, wti_nps.c, etc.  However, those modules work as-is, so
 *  this will probably only be used for new modules.
 *
 *  Copyright 2001 Mission Critical Linux, Inc.
 *  Copyright 2003, 2005 Red Hat, Inc.
 *  author: Lon Hohberger <lhh at redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <libintl.h>
#include <sys/wait.h>

#include "expect.h"
#include "stonith.h"
#include "stelnet.h"

#ifndef dprintf
#ifdef DEBUG
#define dprintf(fmt, args...) printf(fmt, ##args)
#else
#define dprintf(fmt, args...)
#endif
#endif

static struct Etoken telnet_r[] =	{ {"Escape character is '^]'.", 0, 0},
					  {"Connection refused", 1, 0 },
					  {"No route to host", 2, 0 },
					  {"Name or service not known", 2, 0 },
					  {"Unknown host", 2, 0 },
		 			  {NULL,0,0}};

void telnet_disconnect(telnet_t *t);

void
telnet_init(telnet_t *t)
{
	memset(t, 0, sizeof(telnet_t));
	t->t_rfd = -1;
	t->t_wfd = -1;
	t->t_pid = -1;
}


void
telnet_cleanup(telnet_t *t)
{
	telnet_disconnect(t);
	if (t->t_login) {
		free(t->t_login);
		t->t_password = NULL;
	}

	if (t->t_password) {
		free(t->t_password);
		t->t_password = NULL;
	}
}


int
telnet_connect(const char *addr, telnet_t *t)
{
	char cmd[1024];
	int rv;

	snprintf(cmd, sizeof(cmd), "exec telnet %s 2>/dev/null", addr);
	
	t->t_pid = StartProcess(cmd, &t->t_rfd, &t->t_wfd, 0);
	if (t->t_pid <= 0) {
		t->t_pid = 0;
		dprintf("%s: Failed to spawn telnet: %s\n",
			__FUNCTION__, strerror(errno));
		return -1;
	}

	dprintf("%s: Telnet spawned, pid %d\n", __FUNCTION__,
		(int)t->t_pid);
	memset(cmd, 0, sizeof(cmd));

	rv = ExpectToken(t->t_rfd, telnet_r, 10, cmd, sizeof(cmd));
	switch(rv) {
	case 0: /* Connected okay */
		dprintf("%s: Connected\n", __FUNCTION__);
		return 0;
	case 1: /* Connection refused */
		rv = ECONNREFUSED;
		break;
	case 2: /* No route to host */
		rv = EHOSTUNREACH;
		break;
	default:
		rv = errno;
		break;
	}

	dprintf("%s: %s\n", __FUNCTION__, strerror(rv));
	errno = rv;
	return -1;
}


/**
  @param t		Telnet info (file descriptors, pid, etc).
 */
void
telnet_disconnect(telnet_t *t)
{
	int e = errno;

	if (!t) {
		dprintf("%s: t == NULL\n", __FUNCTION__);
	}

	if (t->t_pid >= 0) {
		kill(t->t_pid, SIGKILL);
		waitpid(t->t_pid, NULL, 0);
		dprintf("%s: Killed/reaped %d\n", __FUNCTION__, (int)t->t_pid);
		t->t_pid = -1;
	}

	if (t->t_rfd >= 0) {
		close(t->t_rfd);
		t->t_rfd = -1;
	}

	if (t->t_wfd >= 0) {
		close(t->t_wfd);
		t->t_wfd = -1;
	}

	/* Restore errno */
	errno = e;
}


/**
  Log in to the specified telnet-based fencing device.

  @param addr		Address to which we want to connect
  @param t		Telnet structure from STONITH module
  @param login		Etoken describing what to look for WRT a login/userid.
  			If NULL, no login name is necessary, and this is 
			ignored.  A value of 0 means that the proper login
			string was found.  Other values indicate an error.
  @param password	Etoken describing what to look for WRT a password.
  			If NULL, no password is necessary, and this is
			ignored.  A value of 0 means that the proper password
			was found, a value of 1 means that the password is
			disabled.  Other values indicate an error.
  @param loginok	Etoken describing what we'll see if we've either
  			logged in ok (0) or not (nonzero).  The Etoken MUST
			have a value of 0 to describe a successful login.
			This value is required.
*/
int
telnet_login(telnet_t *t, struct Etoken *login, struct Etoken *password,
	     struct Etoken *loginok)
{
	char buf[128];
	int rv;

	if (!loginok || !t) {
		dprintf("%s: No t/loginok specified\n", __FUNCTION__);
		errno = EINVAL;
		return -1;
	}

	/* Already connected, so don't wait for long for this */
	if (login) {
		memset(buf, 0, sizeof(buf));
		rv = ExpectToken(t->t_rfd, login, 2, buf, sizeof(buf));
		switch(rv) {
		case 0:
			snprintf(buf, sizeof(buf), "%s\r", t->t_login);
			rv = telnet_send(t, buf);
			break;
		default:
			break;
		}

		if (rv == -1) {
			rv = errno;
			dprintf("%s: login %s\n", __FUNCTION__,
				strerror(rv));
			errno = rv;
			return -1;
		}

		dprintf("%s: username sent\n", __FUNCTION__);
	}

	/* Do the password dance */
	if (password) {
		memset(buf, 0, sizeof(buf));
		rv = ExpectToken(t->t_rfd, password, 2, buf, sizeof(buf));
		switch(rv) {
		case 0:
			snprintf(buf, sizeof(buf), "%s\r", t->t_password);
			rv = telnet_send(t, buf);
			break;
		case 1:
			/* Password disabled (some WTI switches) */
			break;
		default:
			break;
		}

		if (rv == -1) {
			rv = errno;
			dprintf("%s: passwd %s\n", __FUNCTION__,
				strerror(rv));
			printf("{%s}\n", buf);
			errno = rv;
			return -1;
		}

		dprintf("%s: password sent\n", __FUNCTION__);
	}

	memset(buf, 0, sizeof(buf));
	rv = ExpectToken(t->t_rfd, loginok, 2, buf, sizeof(buf));
	if (rv == 0) 
		return 0;

	if (rv > 0) {
		dprintf("%s: Wrong authentication info\n", __FUNCTION__);
		errno = EACCES;
		return -1;
	}

	return -1;
}


int
telnet_robust_login(char *addr, telnet_t *t,
		    struct Etoken *login,	/*Username/login (if needed)*/
		    struct Etoken *password,	/*Password (if needed)*/
		    struct Etoken *loginok)	/*Login ok/failed (if needed)*/
{
	int x, sltime = 62500, rv = 1;

	for (x=0; x < 20 && rv != 0; x++) {

		if (t->t_pid >= 0)
			telnet_disconnect(t);

		if (telnet_connect(addr, t) != 0) {
			telnet_disconnect(t);
		} else {
			rv = telnet_login(t, login, password, loginok);
			if (rv == -1 && errno == EACCES)
				return -1;
			if (rv == 0)
				return 0;
		}
			
		usleep(sltime);
		if (sltime < 2000000)
			sltime *= 2;
	}

	return rv;
}


int
telnet_expect(telnet_t *t, struct Etoken *tlist, int timeout)
{
	return ExpectToken(t->t_rfd, tlist, timeout, NULL, 0);
}


int
telnet_send(telnet_t *t, const char *buf)
{
	return write(t->t_wfd, buf, strlen(buf));
}
