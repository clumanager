#include "expect.h"

/* Telnet structure for STONITH modules */
typedef struct _telnet {
	pid_t t_pid;
	int t_rfd;
	int t_wfd;
	char *t_login;
	char *t_password;
} telnet_t;


void telnet_init(telnet_t *t);
void telnet_cleanup(telnet_t *t);

int telnet_login(telnet_t *t, struct Etoken *login,
		 struct Etoken *password, struct Etoken *loginok);
int telnet_robust_login(char *addr, telnet_t *t, struct Etoken *login,
			struct Etoken *password, struct Etoken *loginok);
void telnet_disconnect(telnet_t *t);

int telnet_expect(telnet_t *t, struct Etoken *tlist, int timeout);
int telnet_send(telnet_t *t, const char *buf);
