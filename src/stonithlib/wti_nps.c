/*
 *  Copyright 2001 Mission Critical Linux, Inc.
 *
 *  All Rights Reserved.
 *
 */
/** @file
 * Stonith module for WTI Network Power Switch Devices.
 *
 *  Copyright 2001 Mission Critical Linux, Inc.
 *  Copyright 2003 Red Hat, Inc.
 *  author: mike ledoux <mwl@mclinux.com>
 *  author: Todd Wheeling <wheeling@mclinux.com>
 *  author: Lon Hohberger <lhh at redhat.com>
 *
 *  lhh's modifications:
 *  - Removal of parsing hosts from power switch port names.  This, although
 *    nice, is not possible on many power switches; hence, the cluster
 *    software needs to know what the power switch port name is anyway.
 *    What this means is that instead of calling st_reset(host), the
 *    cluster software will call st_reset(port)...  Pretty simple, really.
 *
 *  Supports the following WTI power controllers:
 *  NPS-115 NPS-230 TPS-2 TPS-2CE RPC-4840N IPS-15
 * 
 *  *Tested* on:
 *  NPS-115
 *  TPS-2
 *  IPS-15
 *
 *  DOES NOT SUPPORT THE WTI RPB SERIES!!!
 *
 *  Based strongly on original code from baytech.c by Alan Robertson.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*                          Observations/Notes
 * 
 * 1. The WTI Network Power Switch, unlike the BayTech network power switch,
 *    accpets only one (telnet) connection/session at a time. When one
 *    session is active, any subsequent attempt to connect to the NPS will
 *    result in a connection refused/closed failure. In a cluster environment
 *    or other environment utilizing polling/monitoring of the NPS
 *    (from multiple nodes), this can clearly cause problems. Obviously the
 *    more nodes and the shorter the polling interval, the more frequently such
 *    errors/collisions may occur.
 *
 * 2. We observed that on busy networks where there may be high occurances
 *    of broadcasts, the NPS became unresponsive.  In some 
 *    configurations this necessitated placing the power switch onto a 
 *    private subnet.
 */
/*
 * Version string that is filled in by CVS
 */
static const char *version __attribute__ ((unused)) = "$Revision: 1.11 $"; 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <libintl.h>
#include <sys/wait.h>

#include "expect.h"
#include "stonith.h"

#define	DEVICE	"WTI Network/Telnet Power Switch"

#define N_(text)	(text)
#define _(text)		dgettext(ST_TEXTDOMAIN, text)

struct WTINPS {
	const char *	NPSid;
	char *		idinfo;
	char *		unitid;
	pid_t		pid;
	int		rdfd;
	int		wrfd;
	int		config;
	char *		device;
	char *		passwd;
};

static const char * NPSid = "WTINPS-Stonith";
static const char * NOTnpsid = "Hey, dummy this has been destroyed (WTINPS)";

#define	ISWTINPS(i)	(((i)!= NULL && (i)->pinfo != NULL)	\
	&& ((struct WTINPS *)(i->pinfo))->NPSid == NPSid)

#define	ISCONFIGED(i)	(ISWTINPS(i) && ((struct WTINPS *)(i->pinfo))->config)

#ifndef MALLOC
#	define	MALLOC	malloc
#endif
#ifndef FREE
#	define	FREE	free
#endif
#ifndef MALLOCT
#	define     MALLOCT(t)      ((t *)(MALLOC(sizeof(t)))) 
#endif

#define DIMOF(a)	(sizeof(a)/sizeof(a[0]))
#define WHITESPACE	" \t\n\r\f"

#define	REPLSTR(s,v)	{					\
			if ((s) != NULL) {			\
				FREE(s);			\
				(s)=NULL;			\
			}					\
			(s) = MALLOC(strlen(v)+1);		\
			if ((s) == NULL) {			\
				syslog(LOG_ERR, _("out of memory"));\
			}else{					\
				memcpy((s),(v),strlen(v)+1);	\
			}					\
			}

/*
 *	Different expect strings that we get from the WTI
 *	Network Power Switch
 */

#define WTINPSSTR	"Network Power Switch"
#define WTITPSSTR	"Telnet Power Switch"
#define WTIIPSSTR	"Internet Power Switch"
#define WTIRPCSTR	"Remote Power Controller"

static struct Etoken EscapeChar[] =	{ {"Escape character is '^]'.", 0, 0},
//					  {"Connection refused", 1, 0 },
					  {NULL,0,0}};
static struct Etoken password[] =	{ {"Password:", 0, 0},
					  {WTINPSSTR, 1, 0},	/*no password*/
					  {WTITPSSTR, 1, 0},
					  {WTIIPSSTR, 1, 0},
					  {WTIRPCSTR, 1, 0},
					  {NULL,0,0}};
static struct Etoken Prompt[] =	        { {"NPS>", 0, 0},
					  {"TPS>", 0, 0},
					  {"IPS>", 0, 0},
					  {"RPC>", 0, 0},
	                                  {NULL,0,0}};
static struct Etoken LoginOK[] =	{ {WTINPSSTR, 0, 0}, 
					  {WTITPSSTR, 0, 0}, 
					  {WTIIPSSTR, 0, 0},
					  {WTIRPCSTR, 0, 0},
	                                  {"Invalid password", 1, 0},
	                                  {"Invalid Password", 1, 0},
					  {NULL,0,0}};

/* We may get a notice about rebooting, or a request for confirmation */
static struct Etoken Processing[] =	{ {"rocessing - please wait", 0, 0}
				,	{"(Y/N):", 1, 0}
				,	{NULL,0,0}};

static int	NPSLookFor(struct WTINPS* nps, struct Etoken * tlist,
			   int timeout);
static int	NPS_connect_device(struct WTINPS * nps);
static int	NPSLogin(struct WTINPS * nps);
static int	NPSReset(struct WTINPS*, char *port);
//static int	NPSScanLine(struct WTINPS* nps, int timeout, char * buf,
			    //int max);
static int	NPSLogout(struct WTINPS * nps);
static void	NPSkillcomm(struct WTINPS * nps);
static int	NPS_parse_config_info(struct WTINPS* nps, const char * info);
#if defined(ST_POWERON) && defined(ST_POWEROFF)
static int	NPS_onoff(struct WTINPS*, char *port, int request);
#endif

/*
 * Our shared symbols
 */
int		st_setconffile(Stonith *, const char * cfgname);
int		st_setconfinfo(Stonith *, const char * info);
const char *	st_getinfo(Stonith * s, int InfoType);
int		st_status(Stonith * );
int		st_reset(Stonith * s, int request, char *port);
void		st_destroy(Stonith *);
void *		st_new(void);


/*
 * We do these things a lot.  Here are a few shorthand macros.
 */
#define	SEND(s) (write(nps->wrfd, (s), strlen(s)))

#define	EXPECT(p,t)	{						\
			if (NPSLookFor(nps, p, t) < 0)			\
				return(errno == ETIMEDOUT		\
			?	S_TIMEOUT : S_OOPS);			\
			}

#define	NULLEXPECT(p,t)	{						\
				if (NPSLookFor(nps, p, t) < 0)		\
					return(NULL);			\
			}

#define	SNARF(s, to)	{						\
				if (NPSScanLine(nps,to,(s),sizeof(s))	\
				!=	S_OK)				\
					return(S_OOPS);			\
			}

#define	NULLSNARF(s, to)	{					\
				if (NPSScanLine(nps,to,(s),sizeof(s))	\
				!=	S_OK)				\
					return(NULL);			\
				}


/*
 * Look for any of the given patterns.  We don't care which
 */
static int
NPSLookFor(struct WTINPS* nps, struct Etoken * tlist, int timeout)
{
	int	rc;
	if ((rc = ExpectToken(nps->rdfd, tlist, timeout, NULL, 0)) < 0) {
		/*
		syslog(LOG_DEBUG, _("Did not find string: '%s' from %s.")
		,      tlist[0].string, DEVICE);
		 */
		NPSkillcomm(nps);
	}
	return(rc);
}


/*
 * Read and return the rest of the line
 */
#if 0
static int
NPSScanLine(struct WTINPS* nps, int timeout, char * buf, int max)
{
	if (ExpectToken(nps->rdfd, CRNL, timeout, buf, max) < 0) {
		syslog(LOG_ERR, ("Could not read line from " DEVICE "."));
		NPSkillcomm(nps);
		return(S_OOPS);
	}
	return(S_OK);
}
#endif


/*
 * Attempt to login up to 20 times...
 */
static int
NPSRobustLogin(struct WTINPS * nps)
{
	int	rc = S_OOPS;
	int	j;
	int	sltime = 62500;

	for (j=0; j < 20 && rc != S_OK; ++j) {

		if (nps->pid > 0)
			NPSkillcomm(nps);

		if (NPS_connect_device(nps) != S_OK)
			NPSkillcomm(nps);
		else
			rc = NPSLogin(nps);
			
		usleep(sltime);
		if (sltime < 2000000)
			sltime *= 2;
	}
	return rc;
}


/*
 * Login to the WTI Network Power Switch (NPS)
 */
static int
NPSLogin(struct WTINPS * nps)
{
	char		IDinfo[128];
	char *		idptr = IDinfo;
	int		rv = 0;

	EXPECT(EscapeChar, 10);

	/*
	 * We should be looking at something like this:
	 *	Enter Password: 
	 */
	if ((rv = ExpectToken(nps->rdfd, password, 2, IDinfo, 
	 		      sizeof(IDinfo))) < 0) {
		syslog(LOG_ERR, _("No initial response from " DEVICE "."));
		NPSkillcomm(nps);
 		return(errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS);
	}

	/* Don't send the password if there wasn't one required */
	if (rv == 1) {
		syslog(LOG_WARNING,
		       _(DEVICE ": No password configured!"));
		return S_OK;
	}

	idptr += strspn(idptr, WHITESPACE);
	SEND(nps->passwd);
	SEND("\r");

	/* Look for the unit type info */
	/* Expect "Network Power Switch vX.YY" */
	switch (NPSLookFor(nps, LoginOK, 5)) {

		case 0:	/* Good! */
			break;

		case 1:	/* Uh-oh - bad password */
			syslog(LOG_ERR, _("Invalid password for " DEVICE "."));
			return(S_ACCESS);

		default:
			NPSkillcomm(nps);
			return(errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS);
	}
	return(S_OK);
}


/*
 * Log out of the WTI NPS
 */
static int
NPSLogout(struct WTINPS* nps)
{
	int	rc;
	
	SEND("/h\r");
	/* Expect "NPS>" */
	rc = NPSLookFor(nps, Prompt, 5);

	/* "/x" is Logout, "/x,y" auto-confirms */
	SEND("/x,y\r");

	NPSkillcomm(nps);

	return(rc >= 0 ? S_OK : (errno == ETIMEDOUT ? S_TIMEOUT : S_OOPS));
}


static void
NPSkillcomm(struct WTINPS* nps)
{
        if (nps->rdfd >= 0) {
	        close(nps->rdfd);
	        nps->rdfd = -1;
	}
	if (nps->wrfd >= 0) {
	        close(nps->wrfd);
  	        nps->wrfd = -1;
	}
        if (nps->pid > 0) {
	        kill(nps->pid, SIGKILL);		
		(void)waitpid(nps->pid, NULL, 0);
		nps->pid = -1;
	}
}


/*
 * Reset (power-cycle) the given outlets 
 */
static int
NPSReset(struct WTINPS* nps, char *port)
{
	char		unum[32];

	/* Send "/h" help command and expect back prompt */
	SEND("/h\r");
	/* Expect "NPS>" */
	EXPECT(Prompt, 5);
	
	/* Send REBOOT command for given outlets */
	snprintf(unum, sizeof(unum), "/BOOT %s,y\r", port);
	SEND(unum);
	
	/* Expect "Processing "... or "(Y/N)" (if confirmation turned on) */

	retry:
	switch (NPSLookFor(nps, Processing, 5)) {
		case 0: /* Got "Processing" Do nothing */
			break;

		case 1: /* Got that annoying command confirmation :-( */
			SEND("Y\r");
			goto retry;

		default: 
			return(errno == ETIMEDOUT ? S_RESETFAIL : S_OOPS);
	}
	syslog(LOG_INFO, _("Port %s being rebooted."), port);

	/* Expect "NPS>" */
	if (NPSLookFor(nps, Prompt, 10) < 0) {
		return(errno == ETIMEDOUT ? S_RESETFAIL : S_OOPS);
	}

	/* All Right!  Power is back on.  Life is Good! */

	syslog(LOG_INFO, _("Power restored to port %s."), port);
	SEND("/h\r");
	return(S_OK);
}


#if defined(ST_POWERON) && defined(ST_POWEROFF)
static int
NPS_onoff(struct WTINPS* nps, char *port, int req)
{
	char		unum[32];

	const char *	onoff = (req == ST_POWERON ? "/On" : "/Off");
	int	rc;

	if ((rc = NPSRobustLogin(nps) != S_OK)) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
		return(rc);
	}
       
	/* Send "/h" help command and expect prompt back */
	SEND("/h\r");
	/* Expect "NPS>" */
	EXPECT(Prompt, 5);

	/* Send ON/OFF command for given outlet */
	snprintf(unum, sizeof(unum), "%s %s,y\r", onoff, port);
	SEND(unum);

	/* Expect "Processing"... or "(Y/N)" (if confirmation turned on) */

	retry:
	switch (NPSLookFor(nps, Processing, 5)) {
		case 0: /* Got "Processing" Do nothing */
			break;

		case 1: /* Got that annoying command confirmation :-( */
			SEND("Y\r");
			goto retry;

		default: 
			return(errno == ETIMEDOUT ? S_RESETFAIL : S_OOPS);
	}

	EXPECT(Prompt, 10);

	/* All Right!  Command done. Life is Good! */
	syslog(LOG_NOTICE, _("Power to NPS outlet(s) %s turned %s."), port,
	       onoff);
	return(S_OK);
}
#endif /* defined(ST_POWERON) && defined(ST_POWEROFF) */


int
st_status(Stonith  *s)
{
	struct WTINPS*	nps;
	int	rc;

	if (!ISWTINPS(s)) {
		syslog(LOG_ERR, "invalid argument to NPS_status");
		return(S_OOPS);
	}
	if (!ISCONFIGED(s)) {
		syslog(LOG_ERR
		,	"unconfigured stonith object in NPS_status");
		return(S_OOPS);
	}
	nps = (struct WTINPS*) s->pinfo;

       	if ((rc = NPSRobustLogin(nps) != S_OK)) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
		return(rc);
	}

	/* Send "/h" help command and expect back prompt */
	SEND("/h\r");
	/* Expect "NPS>" */
	EXPECT(Prompt, 5);

	return(NPSLogout(nps));
}


/*
 *	Parse the given configuration information, and stash it away...
 */
static int
NPS_parse_config_info(struct WTINPS* nps, const char * info)
{
	static char dev[1024];
	static char passwd[1024];

	if (nps->config) {
		return(S_OOPS);
	}

	/* 
	 * the 1023's come from the sizes above, leaving space for trailing
	 * NULL character
	 */
	if (sscanf(info, "%1023s %1023[^\n\r\t]", dev, passwd) == 2
	&&	strlen(passwd) > 0) {

		if ((nps->device = (char *)MALLOC(strlen(dev)+1)) == NULL) {
			syslog(LOG_ERR, "out of memory");
			return(S_OOPS);
		}
		if ((nps->passwd = (char *)MALLOC(strlen(passwd)+1)) == NULL) {
			free(nps->device);
			nps->device=NULL;
			syslog(LOG_ERR, "out of memory");
			return(S_OOPS);
		}
		memcpy(nps->device, dev, strlen(dev)+1);
		memcpy(nps->passwd, passwd, strlen(passwd)+1);
		nps->config = 1;
		return(S_OK);
	}
	return(S_BADCONFIG);
}

/*
 *	Connect to the given NPS device.  We should add serial support here
 *	eventually...
 */
static int
NPS_connect_device(struct WTINPS * nps)
{
	char	TelnetCommand[256];

	snprintf(TelnetCommand, sizeof(TelnetCommand)
	,	"exec telnet %s 2>/dev/null", nps->device);
	
	nps->pid=StartProcess(TelnetCommand, &nps->rdfd, &nps->wrfd, 0);
	if (nps->pid <= 0) {	
		return(S_OOPS);
	}
	return(S_OK);
}

/*
 *	Reset the given host on this Stonith device.  
 */
int
st_reset(Stonith *s, int request, char *port)
{
	int	rc = 0;
	int	lorc = 0;
	struct WTINPS*	nps;

	if (!ISWTINPS(s)) {
		syslog(LOG_ERR, "invalid argument to NPS_reset_port");
		return(S_OOPS);
	}
	if (!ISCONFIGED(s)) {
		syslog(LOG_ERR
		,	"unconfigured stonith object in NPS_reset_port");
		return(S_OOPS);
	}
	nps = (struct WTINPS*) s->pinfo;

        if ((rc = NPSRobustLogin(nps)) != S_OK) {
		syslog(LOG_ERR, _("Cannot log into " DEVICE "."));
        }else{
		switch(request) {

#if defined(ST_POWERON) && defined(ST_POWEROFF)
		case ST_POWERON:
		case ST_POWEROFF:
			rc = NPS_onoff(nps, port, request);
			break;
#endif
		case ST_GENERIC_RESET:
			rc = NPSReset(nps, port);
			break;
		default:
			rc = S_INVAL;			
			break;
		}
	}

	lorc = NPSLogout(nps);
	return(rc != S_OK ? rc : lorc);
}


/*
 *	Parse the information in the given configuration file,
 *	and stash it away...
 */
int
st_setconffile(Stonith* s, const char * configname)
{
	FILE *	cfgfile;

	char	WTINPSid[256];

	struct WTINPS*	nps;

	if (!ISWTINPS(s)) {
		syslog(LOG_ERR, "invalid argument to NPS_set_configfile");
		return(S_OOPS);
	}
	nps = (struct WTINPS*) s->pinfo;

	if ((cfgfile = fopen(configname, "r")) == NULL)  {
		syslog(LOG_ERR, _("Cannot open %s"), configname);
		return(S_BADCONFIG);
	}
	while (fgets(WTINPSid, sizeof(WTINPSid), cfgfile) != NULL){
		if (*WTINPSid == '#' || *WTINPSid == '\n' || *WTINPSid == EOS) {
			continue;
		}
		fclose(cfgfile);
		return(NPS_parse_config_info(nps, WTINPSid));
	}
	fclose(cfgfile);
	return(S_BADCONFIG);
}

/*
 *	Parse the config information in the given string, and stash it away...
 */
int
st_setconfinfo(Stonith* s, const char * info)
{
	struct WTINPS* nps;

	if (!ISWTINPS(s)) {
		syslog(LOG_ERR, "NPS_provide_config_info: invalid argument");
		return(S_OOPS);
	}
	nps = (struct WTINPS *)s->pinfo;

	return(NPS_parse_config_info(nps, info));
}
const char *
st_getinfo(Stonith * s, int reqtype)
{
	struct WTINPS* nps;
	char *		ret;

	if (!ISWTINPS(s)) {
		syslog(LOG_ERR, "NPS_idinfo: invalid argument");
		return NULL;
	}
	/*
	 *	We look in the ST_TEXTDOMAIN catalog for our messages
	 */
	nps = (struct WTINPS *)s->pinfo;

	switch (reqtype) {
		case ST_DEVICEID:
			ret = nps->idinfo;
			break;

		case ST_CONF_INFO_SYNTAX:
			ret = _("IP-address password\n"
			"The IP-address and password are white-space delimited.");
			break;

		case ST_CONF_FILE_SYNTAX:
			ret = _("IP-address password\n"
			"The IP-address and password are white-space delimited.  "
			"All three items must be on one line.  "
			"Blank lines and lines beginning with # are ignored");
			break;

		default:
			ret = NULL;
			break;
	}
	return ret;
}

/*
 *	Baytech Stonith destructor...
 */
void
st_destroy(Stonith *s)
{
	struct WTINPS* nps;

	if (!ISWTINPS(s)) {
		syslog(LOG_ERR, "wtinps_del: invalid argument");
		return;
	}
	nps = (struct WTINPS *)s->pinfo;

	nps->NPSid = NOTnpsid;
	NPSkillcomm(nps);
	if (nps->device != NULL) {
		FREE(nps->device);
		nps->device = NULL;
	}
	if (nps->passwd != NULL) {
		FREE(nps->passwd);
		nps->passwd = NULL;
	}
	if (nps->idinfo != NULL) {
		FREE(nps->idinfo);
		nps->idinfo = NULL;
	}
	if (nps->unitid != NULL) {
		FREE(nps->unitid);
		nps->unitid = NULL;
	}
}

/* Create a new BayTech Stonith device. */

void *
st_new(void)
{
	struct WTINPS*	nps = MALLOCT(struct WTINPS);

	if (nps == NULL) {
		syslog(LOG_ERR, "out of memory");
		return(NULL);
	}
	memset(nps, 0, sizeof(*nps));
	nps->NPSid = NPSid;
	nps->pid = -1;
	nps->rdfd = -1;
	nps->wrfd = -1;
	nps->config = 0;
	nps->device = NULL;
	nps->passwd = NULL;
	nps->idinfo = NULL;
	nps->unitid = NULL;
	REPLSTR(nps->idinfo, DEVICE);
	REPLSTR(nps->unitid, "unknown");

	return((void *)nps);
}





