/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Cluster lock test program.
 */
#include <stdio.h>
#include <clusterdefs.h>
#include <clu_lock.h>


int
main(int argc, char **argv)
{
	if (argc < 3) {
		printf("usage: %s <id> <delay>\n", argv[0]);
		return -1;
	}

	CFG_ReadFile("/etc/cluster.xml");

	if (clu_try_lock(atoi(argv[1])) == -1) {
		perror("clu_try_lock");
		return 1;
	}

	sleep(atoi(argv[2]));

	clu_unlock(atoi(argv[1]));

	return 0;
}
