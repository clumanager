/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
int cluConfigChangeNotification(void);

/** @file
 * Utility to modify keys within the cluster configuration file.
 *
 * author: Ron Lawrence <lawrence@missioncriticallinux.com>
 */
#include <unistd.h>
#include <stdlib.h>
#include <clushared.h>
#include <sharedstate.h>
#include <namespace.h>
#include <xmlwrap.h>
#include <argp.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <malloc.h>
#include <clu_lock.h>

/* Program documentation. */
static char doc[] = "cludb:  read or manipulate cluster parameters\v"
    "If no output file is given, writes to the shared database.\n"
    "If no input file is given, reads from the shared database.\n"
    "If file input, or output file are given as '-', \n"
    "\tthen reading or writing is to/from stdin/stdout.\n"
    "The default separator is '%', and cannot be modified.\n"
    "Note that match takes precedence over expunge, which "
    "takes precedence over get and put.\n\n";

/********************************************************************/
/* A description of the arguments we accept. */
static char args_doc[] = "[KEY] [VALUE]";

/* The options we understand. */
static struct argp_option options[] = {
	{"verbose", 'v', 0, 0, "Produce verbose output", 0},
	{"output", 'o', "OUTFILE", 0, "Write to FILE", 0},
	{"put", 'p', 0, 0, "Set key to value", 0},
	{"get", 'g', 0, 0, "Get value for key", 0},
	{"match", 'm', "PATTERN", 0, "Get entries that match pattern", 0},
	{"file", 'f', "INFILE", 0, "The config file to parse", 0},
	{"remove", 'r', 0, 0, "Remove a key", 0},
	{"expunge", 'x', "PATTERN", 0, "Remove all keys that match pattern", 0},
	{"default", 'd', "DEFAULT", 0, "The default if a value is not found",
	 0},
	{"version", 'V', 0, 0, "Print version number and exit", 0},
//{"init",     'i', 0,          0, "Copy cluster config file from the quorum partition", 0},
	{0, 0, 0, 0, 0, 0}
};

/* Used by `main' to communicate with `parse_opt'. */
struct arguments {
	char *args[2];		/* KEY & VALUE */
	int verbose;
	char *output_file;
	char *input_file;
	int read_from_shared;
	char *match;
	char *expunge;
	int get, put, remove;
	char *dflt;
};

#define STREQ(s1,s2) (!strcmp((s1),(s2)))

int
bump_viewnumber(void)
{
	char *val = NULL;
	int vn = 0;
	char buf[80];

	if (CFG_Get("cluster%config_viewnumber", NULL, &val) == CFG_OK)
		vn = atoi(val);

	snprintf(buf, sizeof (buf), "%d", ++vn);
	if (CFG_Set("cluster%config_viewnumber", buf) != CFG_OK)
		return -1;
	return 0;
}

/* Parse a single option. */
static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
/* Get the INPUT argument from `argp_parse', which we
   know is a pointer to our arguments structure. */
	struct arguments *arguments = state->input;

	switch (key) {
	case 'V':
		fprintf(stderr, "cludb version 1.2\n");
		exit(0);
		break;
	case 'v':
		arguments->verbose = 1;
		break;
	case 'o':
		arguments->output_file = arg;
		break;
	case 'p':
		arguments->put = 1;
		arguments->get = 0;
		arguments->remove = 0;
		break;
	case 'g':
		arguments->get = 1;
		arguments->put = 0;
		arguments->remove = 0;
		break;
	case 'm':
		arguments->match = arg;
		break;
	case 'x':
		arguments->expunge = arg;
		break;
	case 'r':
		arguments->put = 0;
		arguments->get = 0;
		arguments->remove = 1;
		break;
	case 'f':
		arguments->input_file = arg;
		break;
	case 'd':
		arguments->dflt = arg;
		break;
	case 'i':
		arguments->read_from_shared = 1;
		break;
	case ARGP_KEY_ARG:
		/* Should it be possible to specify multiple queries or multiple
		   key value pairs on the command line? */
		if (STREQ("", arguments->match) &&
		    STREQ("", arguments->expunge)) {
			if (arguments->put && state->arg_num >= 2)
				/* Too many arguments. */
				argp_usage(state);
			if (arguments->get && state->arg_num >= 1)
				/* Too many arguments. */
				argp_usage(state);
		}
		arguments->args[state->arg_num] = arg;

		break;

	case ARGP_KEY_END:
		break;

	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

int
copy_shared_database_file(void)
{
	ssize_t length;
	char *buffer;
	ssize_t res;
	SharedHeader hdr;

	shared_storage_init();

	if (sh_stat(NS_D_CLUSTER NS_F_CONFIG, &hdr) == -1) {
		goto failed;
	}
	length = hdr.h_length;
	buffer = (char *) malloc(length + 1);
	if (NULL == buffer)
		goto failed;
	memset(buffer, 0, length);

	res = sh_read_atomic(NS_D_CLUSTER NS_F_CONFIG, buffer, length);

	if (res >= 0) {
		FILE *f;
		f = fopen(CLU_CONFIG_FILE, "w");
		if (NULL == f)
			goto failed;
		fwrite(buffer, sizeof (char), strlen(buffer), f);
		fclose(f);
		free(buffer);
	} else {
		free(buffer);
		goto failed;
	}
	return CFG_OK;
      failed:
	return CFG_FAILED;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc, 0, 0, 0 };

static int must_write = 1;

int
main(int argc, char **argv)
{
	char *result;
	int rc = CFG_OK, shared_ok = 0;
	struct arguments arguments;

	if (geteuid() != (uid_t) 0) {
		fprintf(stderr, "%s must be run as the user root\n", argv[0]);
		exit(1);
	}

	/* Default values. */
	arguments.args[0] = "";
	arguments.args[1] = "";
	arguments.verbose = 0;
	arguments.output_file = "";
	arguments.input_file = "";
	arguments.read_from_shared = 0;
	arguments.get = 0;
	arguments.put = 0;
	arguments.match = "";
	arguments.expunge = "";
	arguments.remove = 0;
	arguments.dflt = "not found";

	/* Parse our arguments; every option seen by `parse_opt' will be
	   reflected in `arguments'. */
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	if (arguments.verbose) {
		printf("ARG1 = %s\nARG2 = %s\nOUTPUT_FILE = %s\n"
		       "VERBOSE = %s\n"
		       "INPUT_FILE = %s\nGET = %s\nPUT = %s\n"
		       "MATCH = %s\nEXPUNGE = %s\n"
		       "REMOVE = %s\nDEFAULT = %s\n",
		       arguments.args[0], arguments.args[1],
		       arguments.output_file,
		       arguments.verbose ? "yes" : "no",
		       arguments.input_file,
		       arguments.get ? "yes" : "no",
		       arguments.put ? "yes" : "no",
		       arguments.match,
		       arguments.expunge,
		       arguments.remove ? "yes" : "no", arguments.dflt);
	}

  /*********************************************************************/
	/*  Always read in the database. */
	/* choose the input channel... */
	if (arguments.read_from_shared) {
		/* Refuse if the cluster is running. */

		//The cluster_member_check() call below is commented
		//out for v2.0 because the check depends upon the
		//the /etc/cluster.conf file being in a
		//"good" state. Specifically, the check depends upon
		//being able to determine the node id.
		//Since the purpose of the --init is
		//restore the /etc/cluster.conf file b/c
		//something may have happen to it, it does not make
		//sense to perform this check.
		//CRMs : LW1J21UM LW97FKLY

		/*
		   if(1 == cluster_member_check()) {
		   fprintf(stderr, "%s --init cannot be run on a running cluster member\n", argv[0]);
		   exit(2);
		   }
		 */
		/* Get the contents of the shared database on the quorum disk, and
		   blast them out to the config file on disk. */
		if (CFG_OK == copy_shared_database_file()) {

			/* After writing out the local copy of the cluster database, we
			   are finished. */
			return (0);
		} else {
			fprintf(stderr,
				"Failed to copy from shared storage.\n");
			return (1);
		}
	} else if (STREQ("-", arguments.input_file)) {	/* read from stdin. */
#if 0
		/* Will need to read stdin into a buffer, until eof. Then pass the
		   resulting buffer to CFG_get. */
		rc = CFG_ReadFD(stdin);
		if (CFG_OK != rc) {
			fprintf(stderr, "%s: failed to read stdin\n", argv[0]);
			return 1;
		}
#endif
		fprintf(stderr, "TODO\n");
	} else if (STREQ("", arguments.input_file)) {	/* Read from shared storage. */

		/*shared_storage_init(); */
		if (clu_config_lock() == 0) {
			shared_ok = 1;
			shared_storage_init();
		}
		
		rc = CFG_ReadFile(CLU_CONFIG_FILE);
		if (CFG_OK != rc) {
			fprintf(stderr,
				"%s: failed to read from shared database\n",
				argv[0]);
			return 1;
		}
	} else {		/* Read from file specified. */
		rc = CFG_ReadFile(arguments.input_file);
		if (CFG_OK != rc) {
			fprintf(stderr, "%s: failed to read file %s\n",
				argv[0], arguments.input_file);
			return 1;
		}
	}
  /*********************************************************************/
	/* Database is read in, look up item(s). */

	/* Lookup keys that match the given pattern. */
	if (!STREQ("", arguments.match)) {
		CFG_List *list;
		CFG_status res;

		list = CFG_ListCreate(arguments.match, CFG_REGEX);
		res = (list != NULL);
		while (res) {
			if (arguments.verbose)
				printf("key: [%s], value: [%s]\n",
				       CFG_ListToken(list),
				       CFG_ListValue(list));
			else
				printf("%s = %s\n", CFG_ListToken(list),
				       CFG_ListValue(list));
			res = CFG_ListNext(list);
		}
		CFG_ListDestroy(list);
		/* This is a lookup, so, we don't need to write anything out. */
		must_write = 0;
	} else if (!STREQ("", arguments.expunge)) {
#if 0
		CFG_List *list;
		CFG_status res;

		list = CFG_ListCreate(arguments.expunge, CFG_REGEX);
		while (CFG_ListMore(list)) {
			res = CFG_ListNext(list);
			if (res)
				CFG_Remove(CFG_ListToken(list));
		}
		CFG_ListDestroy(list);
#endif
		CFG_RemoveMatch(arguments.expunge, CFG_REGEX);
	} else if (arguments.get) {
		/* Look up one key. */
		rc = CFG_Get(arguments.args[0], arguments.dflt, &result);

		if (CFG_OK == rc) {
			printf("%s\n", result);
			/* On a get we don't need to write out the database. */
			must_write = 0;
		} else {
			switch (rc) {
			case CFG_NO_SUCH_SECTION:
				if (arguments.verbose) {
					fprintf(stderr, "No such path: %s\n",
						arguments.args[0]);
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_DEFAULT:
				if (arguments.verbose) {
					fprintf(stderr, "Using default: %s\n",
						result);
				} else {
					printf("%s\n", arguments.dflt);
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_FAILED:
				if (arguments.verbose) {
					fprintf(stderr, "Failed\n");
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_LINE_TOO_LONG:
				if (arguments.verbose) {
					fprintf(stderr, "Line too long\n");
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_PARSE_FAILED:
				if (arguments.verbose) {
					fprintf(stderr, "Parse Failed\n");
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			}
		}
	} else if (arguments.put) {
		rc = CFG_Set(arguments.args[0], arguments.args[1]);
		bump_viewnumber();
		if (CFG_OK == rc) {
			if (arguments.verbose)
				printf("%s\n", arguments.args[1]);
		} else {
			switch (rc) {
			case CFG_NO_SUCH_SECTION:
				if (arguments.verbose) {
					fprintf(stderr, "No such path: %s\n",
						arguments.args[0]);
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_DEFAULT:
				if (arguments.verbose) {
					fprintf(stderr, "Using default: %s\n",
						result);
				} else {
					printf("%s\n", arguments.dflt);
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_FAILED:
				if (arguments.verbose) {
					fprintf(stderr, "Failed\n");
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_LINE_TOO_LONG:
				if (arguments.verbose) {
					fprintf(stderr, "Line too long\n");
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_PARSE_FAILED:
				if (arguments.verbose) {
					fprintf(stderr, "Parse Failed\n");
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			}
		}
	} else if (arguments.remove) {
		rc = CFG_Remove(arguments.args[0]);
		bump_viewnumber();
		if (CFG_OK == rc) {
			if (arguments.verbose)
				printf("removed: %s\n", arguments.args[0]);
		} else {
			switch (rc) {
			case CFG_NO_SUCH_SECTION:
				if (arguments.verbose) {
					fprintf(stderr, "No such path: %s\n",
						arguments.args[0]);
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_DEFAULT:
				if (arguments.verbose) {
					fprintf(stderr, "Using default: %s\n",
						result);
				} else {
					printf("%s\n", arguments.dflt);
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_FAILED:
				if (arguments.verbose) {
					fprintf(stderr, "Failed\n");
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_LINE_TOO_LONG:
				if (arguments.verbose) {
					fprintf(stderr, "Line too long\n");
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			case CFG_PARSE_FAILED:
				if (arguments.verbose) {
					fprintf(stderr, "Parse Failed\n");
				}
				if (shared_ok)
					clu_config_unlock();
				return (-rc);
				break;
			}
		}
	} else {
		/* no action specified, ok? */
	}
  /********************************************************************/
	/* Choose the output channel... */
	if (STREQ("-", arguments.output_file)) {	/* Write results to stdout. */
#if 0
		rc = CFG_WriteFD(stdout);
		if (CFG_OK != rc) {
			fprintf(stderr, "%s: failed to write to stdout\n",
				argv[0]);
			return 1;
		} else {
			return 0;
		}
#endif
		fprintf(stderr, "TODO\n");
	} else if (STREQ("", arguments.output_file)) {	/* Write to shared storage. */

		if (!must_write) {
			/* We didn't write anything out; return any result code that was
			   set by a prior operation. Shell scripts expect a zero return
			   result on success. So, here, we subtract one from the rc, and
			   ten return that. This way, CFG_OK, which is normally 1, is
			   returned as zero. Any other resturn result is non-zero. */
			if (shared_ok)
				clu_config_unlock();
			return (rc - 1);
		}
			
		/* We weren't asked to write the
		   database anywhere, so only write it
		   if it has been changed. */
		printf("Writing %s...",CLU_CONFIG_FILE);
		rc = CFG_WriteFile(CLU_CONFIG_FILE);
		
		if (CFG_OK != rc) {
			printf("FAILED\n");
			fprintf(stderr, "%s: failed to write to %s\n",
				argv[0], CLU_CONFIG_FILE);
			if (shared_ok)
				clu_config_unlock();
			return 1;
		}
		printf("OK\n");

		if (shared_ok) {
			printf("Writing to shared state...");
			rc = CFG_Write();
			if (CFG_OK != rc) {
				printf("FAILED\n");
				clu_config_unlock();
				return 1;
			}
			printf("OK\n");
		}
		clu_config_unlock();
		return 0;
	} else {		/* Write to the specified file. */
		rc = CFG_WriteFile(arguments.output_file);
		if (CFG_OK != rc) {
			fprintf(stderr, "%s: failed to write file %s\n",
				argv[0], arguments.output_file);
			return 1;
		} else {
			fprintf(stderr,
				"Warning: You must run 'service clumanager reload'\n"
				"         for changes to take effect!\n");
			return 0;
		}
	}

	if (shared_ok)
		clu_config_unlock();

	CFG_Destroy();
	exit(0);
}
