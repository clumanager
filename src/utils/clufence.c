/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Utility to test/fence members within a cluster.
 */
#include <stonithapis.h>
#include <clusterdefs.h>
#include <xmlwrap.h>
#include <clulog.h>
#include <errno.h>
#include <svcmgr.h>
#include <nodeid.h>


static void
set_facility(void)
{
	char *p;
	if (CFG_Get((char *) "cluster%logfacility", NULL, &p) == CFG_OK) {
		if (p)
			clu_set_facility(p);
	}
}


int
main(int argc, char **argv)
{
	extern char *optarg;
	char *nodename = NULL;
	int opt, help = 0, me = -1, target = -1;
	int (*fence_func)(int) = NULL;

	clu_set_loglevel(LOG_INFO);
	clu_log_console(1);

	while ((opt = getopt(argc, argv, "df:u:r:s:?hH")) != -1) {
		switch (opt) {
		case 'd':
			clu_set_loglevel(LOG_DEBUG);
			break;
		case 'f': 
			nodename = optarg;
			fence_func = clu_stonith_fence;
			break;
		case 'u':
			nodename = optarg;
			fence_func = clu_stonith_unfence;
			break;
		case 'r':
			nodename = optarg;
			fence_func = clu_stonith_fence_cycle;
			break;
		case 's':
			nodename = optarg;
			fence_func = clu_stonith_status;
			break;
		default:
			help = 1;
		}
	}

	if (help || !nodename) {
		printf("usage: %s [-d] [-[furs] <member>]\n"
		       "  -d	  	  Turn on debugging\n"
		       "  -f <member>     Fence (power off) <member>\n"
		       "  -u <member>     Unfence (power on) <member>\n"
		       "  -r <member>     Reboot (power cycle) <member>\n"
		       "  -s <member>     Check status of all switches "
		       "controlling <member>\n",
		       argv[0]);
		return 1;
	}

	if (CFG_ReadFile(CLU_CONFIG_FILE) != CFG_OK) {
		fprintf(stderr, "%s: %s\n", CLU_CONFIG_FILE, strerror(errno));
		return -1;
	}

	set_facility();
	clulog(LOG_DEBUG, "Debugging enabled\n");

	me = cluGetLocalNodeId();
	target = getNodeID(nodename);

	if (me == -1) {
		printf("Error: Failed to determine my member number.\n");
		return -1;
	}

	if (target == -1) {
		printf("Error: Failed to determine target member number.\n");
		return -1;
	}

	if (me == target) {
		printf("Error: Can not perform STONITH operations on myself.\n");
		return -1;
	}

	if (clu_stonith_init() != 0) {
		printf("Error: Failed STONITH initialization.\n");
		return -1;
	}

	return (fence_func)(target);
}
