/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Tell the quorum daemon to allow itself to form a quorum using the upstream
 * router.  We disallow this because of our inability to reproduce, yet
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/param.h>
#include <msgsvc.h>
#include <clusterdefs.h>
#include <libgen.h>
#include <quorum.h>
#include <clushared.h>
#include <sharedstate.h>
#include <xmlwrap.h>


void
build_message(generic_msg_hdr *msgp)
{
	msgp->gh_magic = GENERIC_HDR_MAGIC;
	msgp->gh_command = QUORUM_FORCE;
	msgp->gh_length = sizeof(*msgp);
}


int
main(int argc, char **argv)
{
	generic_msg_hdr msg;
	int msgfd;

	if (CFG_ReadFile(CLU_CONFIG_FILE) != CFG_OK)
		return 1;

	printf("Enabling soft quorum...");
	fflush(stdout);

	build_message(&msg);
	msgfd = msg_open(PROCID_CLUQUORUMD, -1);
	if (msgfd < 0) {
		perror("msg_open");
		fprintf(stderr,
			"Could not connect to quorum daemon!\n");
		return 1;
	}

	/* encode */
	swab_generic_msg_hdr(&msg);

	if (msg_send(msgfd, &msg, sizeof(msg)) != sizeof(msg)) {
		perror("msg_send");
		fprintf(stderr, "Could not send entire message!\n");
		return 1;
	}

	msg_close(msgfd);

	printf("done\n");

	return 0;
}
