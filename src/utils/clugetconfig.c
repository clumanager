/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Execute a CFG_Get() given a token; print the result.
 */

 
/*

   Parse a configuration file.  Print the result of looking up the
   command line arguments in the file.
 
   Fri Mar 24 13:49:46 2000 RAL Altered to parse command line
   arguments, parse the given config file, do the lookup, print the
   result and exit.  Returns codes to the shell: 0 for success,
   various others for failures.
  
   Fri Mar 31 09:57:10 2000 RAL note the tablesize argument was added
   because the hsearch hash table is, apparently, not very goo at
   resizing its tables.  When configuration files get really, really,
   large, it fails to do the lookups properly.  The default given here
   is sized for the expected usual cases.

   Sun May 7 14:39:33 2000 Removed all traces of argtable.  Using argp
   from libc instead.  This to take advantage of lgpl in libc.
   Argtable is gpl.

   author: Ron Lawrence <lawrence@missioncriticallinux.com>

*/

#include <unistd.h>
#include <stdlib.h>
#include <argp.h>
#include <xmlwrap.h>
#include <clusterdefs.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>

static const char *version __attribute__ ((unused)) = "$Id: clugetconfig.c,v 1.3 2003/05/12 21:04:42 lhh Exp $";
     
/* Program documentation. */
static char doc[] = 
"clugetconfig - parse a configuration file." 
" lookup an item and print it.";
     
/* A description of the arguments we accept. */
static char args_doc[] = "KEYPATH";

/* The options we understand. */
static struct argp_option options[] = {
  {"verbose",  'v', 0,          0,  "Produce verbose output",  0 },
  {"quiet",    'q', 0,          0,  "Don't produce any output",0 },
  {"separator",'s', "SEPARATOR",0,  "Path separator",          0 },
  {"default",  'd', "DEFAULT",  0,  "Default if key not found",0 },
  {"file",     'f', "FILE",     0,  "File to parse",           0 },
  {"version",  'V', 0,          0,  "Print version and exit",  0 },
  { 0,0,0,0,0,0 }
};

/* Used by `main' to communicate with `parse_opt'. */
struct arguments
{
  char *args[1];                /* 1 ARG */
  int silent, verbose;
  char separator;
  char *dflt;
  char *filename;
};

/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  /* Get the INPUT argument from `argp_parse', which we
     know is a pointer to our arguments structure. */
  struct arguments *arguments = state->input;
  
  switch (key)
  {
    case 'V':
      fprintf(stdout,"clugetconfig version: %s\n", version);
      exit(0);
      break;
    case 'q':
      arguments->silent = 1;
      break;
    case 'v':
      arguments->verbose = 1;
      break;
    case 's':
      arguments->separator = arg[0];
      break;
    case 'd':
      arguments->dflt = arg;
      break;
    case 'f':
      arguments->filename = arg;
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num >= 1)
        /* Too many arguments. */
        argp_usage (state);
      
      arguments->args[state->arg_num] = arg;
      
      break;
      
    case ARGP_KEY_END:
      if (state->arg_num < 1)
        /* Not enough arguments. */
        argp_usage (state);
      break;
      
    default:
      return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc,0,0,0 };
     
int main (int argc, char **argv)
{
  struct arguments arguments;
  char * result;
  int rc;

  if (geteuid() != (uid_t)0) {
    fprintf(stderr, "%s must be run as the user root\n", argv[0]);
    exit(1);    
  }
  
  /* Default values. */
  arguments.silent = 0;
  arguments.verbose = 0;
  arguments.filename = "";
  arguments.dflt = "not found";
  arguments.separator = '%';
  
  /* Parse our arguments; every option seen by `parse_opt' will
     be reflected in `arguments'. */
  argp_parse (&argp, argc, argv, 0, 0, &arguments);
     
  if(arguments.verbose) {
    printf ("KEY = %s\nCONFIG FILE = %s\n"
            "VERBOSE = %s\nSILENT = %s\nSEPARATOR= %c\n",
            arguments.args[0],
            arguments.filename,
            arguments.verbose ? "yes" : "no",
            arguments.silent ? "yes" : "no",
            arguments.separator);
  }


  /* Only read the config database from the given file if a filename
     was specified on the command line. */
  if(strcmp("", arguments.filename)) {
    rc = CFG_ReadFile(arguments.filename);

    if(CFG_OK != rc) {
      fprintf(stderr,"%s: failed to read file %s\n",argv[0],arguments.filename);
      return rc;
    }
  } else {
    rc = CFG_ReadFile(CLU_CONFIG_FILE);

    if(CFG_OK != rc) {
      fprintf(stderr,"%s: failed to read file %s\n",argv[0],arguments.filename);
      return rc;
    }
  }

  rc = CFG_Get(arguments.args[0],arguments.dflt,&result);

  if(CFG_OK == rc) {
    printf("%s\n",result);
  } else {
    switch(rc) {
    case CFG_NO_SUCH_SECTION:
      if(arguments.verbose) {
        fprintf(stderr,"No such path: %s\n", arguments.args[0]);
      }
      return(-rc);
      break; 
    case CFG_DEFAULT:
      if(arguments.verbose) {
        fprintf(stderr,"Using default: %s\n", result);
      } else {
        printf("%s\n",arguments.dflt);
      }
      return(-rc);
      break;
    case CFG_FAILED:
      if(arguments.verbose) {
        fprintf(stderr,"Failed\n");
      }
      return(-rc);
      break;
    case CFG_LINE_TOO_LONG:
      if(arguments.verbose) {
        fprintf(stderr,"Line too long\n");
      }
      return(-rc);
      break;
    case CFG_PARSE_FAILED:
      if(arguments.verbose) {
        fprintf(stderr,"Parse Failed\n");
      }
      return(-rc);
      break;
    }
  }
  CFG_Destroy();

  exit (0);
}

