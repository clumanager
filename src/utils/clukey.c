/*
  Copyright Red Hat, Inc. 2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Generate/Remove the cluster net authentication key.
 */
#include <stdio.h>
#include <stdlib.h>
#include <xmlwrap.h>
#include <fcntl.h>
#include <auth_md5.h>
#include <clu_lock.h>

int bin2hex(char *in, int inlen, char *out, int outlen);

int
generatekey(void)
{
	int fd;
	char buf[16];
	char hexbuf[33];

	fd = open("/dev/random", O_RDONLY);
	if (fd == -1) {
		perror("open(/dev/random)");
		return -1;
	}

	if (read(fd, buf, sizeof(buf)) == -1) {
		perror("read");
		close(fd);
		return -1;
	}

	close(fd);
	memset(hexbuf,0,sizeof(hexbuf));
	if (bin2hex(buf,16,hexbuf,32) == -1) {
		perror("bin2hex");
		return -1;
	}

	if (CFG_Set("cluster%key", hexbuf) != CFG_OK) {
		perror("CFG_Set");
		return -1;
	}
	return 0;
}


int
deletekey(void)
{
	if (CFG_Remove("cluster%key") != CFG_OK) {
		return -1;
	}
	return 0;
}


int
bump_viewnumber(void)
{
	char *val = NULL;
	int vn = 0;
	char buf[80];

	if (CFG_Get("cluster%config_viewnumber", NULL, &val) == CFG_OK)
		vn = atoi(val);

	snprintf(buf, sizeof(buf), "%d", ++vn);
	if (CFG_Set("cluster%config_viewnumber", buf) != CFG_OK)
		return -1;
	return 0;
}


int
main(int argc, char **argv)
{
	char *oldkey = NULL, *newkey = NULL, *val;
	int shared = 0;
	
	if (argc != 2) {
		fprintf(stderr,
			"WARNING: Running '%s' is extremely "
			"dangerous and can render the cluster "
			"inoperable.\n\n",
			argv[0]);
		fprintf(stderr, "usage: %s [generate|delete]\n",
			argv[0]);
		return 1;
	}
	
	if (CFG_ReadFile(CLU_CONFIG_FILE) != CFG_OK) {
		fprintf(stderr, "Could not read %s\n", CLU_CONFIG_FILE);
		return 1;
	}

	if (clu_config_lock() == 0) {
		printf("Cluster configuration lock obtained.\n");
		shared = 1;
		shared_storage_init();
	}
	
	if (CFG_Get("cluster%key", NULL, &val) == CFG_OK)
		oldkey = strdup(val);

	if (!strcmp(argv[1], "generate")) {
		printf("Generating new Cluster Key...");
		if (generatekey() != 0) {
			printf("FAILED\n");
			return -1;
		}
	} else if (!strcmp(argv[1], "delete")) {
		printf("Deleting Cluster Key...");
		if (deletekey() != 0) {
			printf("FAILED\n");
			return -1;
		}
	} else
		return 0;

	if (bump_viewnumber() != 0) {
		perror("bump_viewnumber");
		return -1;
	}
	printf("OK\n");

	if (CFG_Get("cluster%key", NULL, &val) != CFG_OK)
		return 0;
	newkey = strdup(val);

	if (shared && oldkey && newkey) {
		printf("Writing to shared state...");
		if (CFG_Write() != CFG_OK) {
			printf("FAILED\n");
			clu_config_unlock();
			return -1;
		}
		printf("OK\n");
	}

	printf("Writing new %s...", CLU_CONFIG_FILE);
	if (CFG_WriteFile(CLU_CONFIG_FILE) != CFG_OK) {
		printf("FAILED\n");
		return -1;
	}
	printf("OK\n");

	if (shared && oldkey && newkey) {
		CFG_Set("cluster%key", oldkey);
		clu_config_unlock();
	}

	return 0;
}
	
