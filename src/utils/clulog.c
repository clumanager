/*
  Copyright Red Hat, Inc. 2002
  Copyright Mission Critical Linux, 2000

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * Utility for logging arbitrary strings to the cluster log file via syslog.
 *
 * $Id: clulog.c,v 1.4 2004/10/04 13:57:26 lhh Exp $
 *
 * Author: Jeff Moyer <moyer@mclinux.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <getopt.h>
#include <string.h>
#include <sys/syslog.h>

#include <clulog.h>
#include <msgsvc.h>
#include <xmlwrap.h>
#include <clusterdefs.h>
#include <svcmgr.h>

static char *config_file = NULL;

#define MAX_TOKENLEN   64
#define DFLT_LOGLEVEL  LOG_NOTICE   /* taken from msgsvc.h */

static const char *version __attribute__ ((unused)) = "$Revision: 1.4 $";

void
usage(char *progname)
{
    fprintf(stdout, "%s -s severity [-l priority_filter] [-n program name] \n"
	    "\t\t [-p pid] \"message text\"\n", progname);
    exit(0);
}


static void
set_facility(void)
{
	char *p;
	if (CFG_Get((char *) "cluster%logfacility", NULL, &p) == CFG_OK) {
		if (p)
			clu_set_facility(p);
	}
}


int
main(int argc, char **argv)
{
    int  opt;
    int  severity = 7,
	 cmdline_loglevel = 0;/* set if we should not use the config file val*/
    char *logmsg, *value;
    char token[MAX_TOKENLEN];
    int  pid = 0;
    char *progname = NULL;
    int result;
    size_t len;

    if (argc < 4)
	usage(argv[0]);

    while ((opt = getopt(argc, argv, "l:s:hp:n:")) != -1) {
	switch (opt) {
	case 'l':
	    clu_set_loglevel(atoi(optarg));
	    cmdline_loglevel = 1;
	    break;
	case 's':
	    severity = atoi(optarg);
	    break;
	case 'p':
	    pid = atoi(optarg);
	    break;
	case 'n':
	    progname = strdup(optarg);
	    break;
	case 'h':
	    usage(argv[0]);
	default:
	    usage(argv[0]);
	}
    }

    /* Add two bytes for linefeed and NULL terminator */
    len = strlen(argv[argc-1]) + 2;
    logmsg = (char*)malloc(strlen(argv[argc-1])+2);
    snprintf(logmsg, len, "%s\n", argv[argc-1]);

    if (!config_file) {
	config_file = strdup(CLU_CONFIG_FILE);
    }

    if (CFG_ReadFile((char *) config_file) != CFG_OK) {
	fprintf(stderr, "clulog: could not read config file %s\n", config_file);
	exit(1);
    }
    free(config_file);

    set_facility();

    if (!cmdline_loglevel) {
	/*
	 * Let's see what loglevel the SM is running at.
	 */
	snprintf(token, MAX_TOKENLEN, "%s%c%s",
		 SVCMGR_STR, CLU_CONFIG_SEPARATOR, SVCMGR_LOGLEVEL_STR);
	switch (CFG_Get(token, NULL, &value)) {
	case CFG_OK:
	    clu_set_loglevel(atoi(value));
	    break;
	case CFG_DEFAULT:
	default:
	    clu_set_loglevel(DFLT_LOGLEVEL);
	    break;
	}
    }
    result = clulog_pid(severity, pid, progname, logmsg);
    free(progname);
    return(result);
}
/*
 * Local variables:
 *  c-basic-offset: 4
 *  c-indent-level: 4
 *  tab-width: 8
 *  compile-command: "gcc -I../include -L../lib  clulog.c -o clulog -lmetaconfig -lmsg -llogger "
 * End:
 */

