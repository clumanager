/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge,
  MA 02139, USA.
*/
/** @file
 * Displays cluster status in a terminal window.  Deplorable design.
 *
 * Alternatively can cycle (using the -i <n> parameter) or display status
 * in XML to standard-output (this is used by redhat-config-cluster).
 */
#include <stdio.h>
#include <clushared.h>
#include <sharedstate.h>
#include <cm_api.h>
#include <svcmgr.h>
#include <clusterdefs.h>
#include <errno.h>
#include <arpa/inet.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <xmlwrap.h>
#include <nodeid.h>
#include <netdb.h>
#include <ncurses.h>
#include <term.h>
#include <termios.h>

static int myNodeID = -1;
int quorum_status(int print, int force_tb);

#define TIMESTR_LEN 128

/**
 * Print out the member header.
 */
void
print_member_header(void)
{
	printf("  %-18.18s %-10s\n", "Member", "Status");
	printf("  %-18.18s %-10s\n", "------------------","----------");
}


/**
 * Print the state of a given node/member ID.
 *
 * @param long_form		Print in long or short format?
 * @param nodeid		Member ID whose status we should print.
 * @return			0 for down, 1 for up
 */
int
print_member_state(int nodeid, int long_form)
{
	char buf[80];
	char *nodename;
	cm_event_t *memb_event = NULL;
	char *state = "Unknown";
	int rv = -1;

	snprintf(buf, sizeof(buf), "members%%member%d%%name", nodeid);
	if (CFG_Get(buf, NULL, &nodename) != CFG_OK)
		return -1;

	if (!memb_query(&memb_event)) {
		if (memb_online(cm_memb_mask(memb_event), nodeid)) {
			state = "Active";
			rv = 1;
		} else {
			state = "Inactive";
			rv = 0;
		}
		cm_ev_free(memb_event);
	}

	if (long_form)
		printf("  %-18.18s %-10s %-10s\n", nodename,
		       state, nodeid == myNodeID ? "<-- You are here" : "");
	else
		printf("Member: %s  Status: %s (%d)\n", nodename, state, rv);

	return rv;
}


int
print_member_state_byname(char *name)
{
	int node = getNodeID(name);
	
	if (node != FAIL)
		return print_member_state(node, 0);

	fprintf(stderr,"Unknown member: %s\n", name);

	return -1;
}


/**
 * Retrieve the member states from the membership daemon and place them
 * in the config database so that we can pass it up to redhat-config-cluster.
 *
 * @return		-1 on failure; 0 on success.
 */
int
xml_get_store_member_states(void)
{
	char buf[80];
	cm_event_t *memb_event = NULL;
	char *memb_name;
	int x;

	if (memb_query(&memb_event) == -1) {
		return -1;
	}

	for (x=0; x<MAX_NODES; x++) {
		snprintf(buf, sizeof(buf), "members%%member%d%%name", x);
		if (CFG_Get(buf, NULL, &memb_name) != CFG_OK)
			continue;

		snprintf(buf, sizeof(buf), "members%%member%d%%state", x);
		if (memb_online(cm_memb_mask(memb_event), x)) {
			CFG_Set(buf, "1");
		} else {
			CFG_Set(buf, "0");
		}
	}

	cm_ev_free(memb_event);

	return 0;
}


/**
 * Print out the service header.
 */
void                               
print_service_header(void)
{
	printf("  %-14s %-8s %-16s %-15s %-3s %-8s\n", "Service", "Status",
	       "Owner (Last)", "Last Transition", "Chk", "Restarts");
        printf("  %-14s %-8s %-16s %-15s %-3s %-8s\n", "--------------",
    	       "--------", "----------------", "---------------",
	       "---", "--------");
}


/**
 * Print the state of a given serviceID.
 *
 * @param serviceid	Service ID whose status we should print.
 * @param long_form	Do we want to print the whole line, or just the state?
 */
int
print_service_state(int serviceid, int long_form)
{
	SharedServiceBlock sb;
	char buf[80];
	struct tm *tm;
	char timestr[TIMESTR_LEN];
	char ownerstr[64];
	char *node, *servicename;
	int interval;
	char *intervalStr;

	snprintf(buf, sizeof(buf), "services%%service%d%%name", serviceid);
	if (CFG_Get(buf, NULL, &servicename) != CFG_OK)
		return -1;

	snprintf(buf, sizeof(buf), "/service/%d/status", serviceid);
	if (sh_read_atomic(buf, (void *)&sb, sizeof(sb)) == -1) {
		fprintf(stderr,"Error reading %s from shared state: %s\n",
			buf, strerror(errno));
		return -1;
	}

	swab_SharedServiceBlock(&sb);

	memset(timestr, 0, TIMESTR_LEN);
    	tm = localtime((time_t *)&sb.sb_svcblk.sb_transition);
	if (tm)
		strftime(timestr, TIMESTR_LEN, "%H:%M:%S %b %d", tm);

	if (sb.sb_svcblk.sb_owner == NODE_ID_NONE)
		node = "None";
	else
		getNodeName(sb.sb_svcblk.sb_owner, &node);
	
	if (getSvcCheckInterval(serviceid, &intervalStr) == SUCCESS)
		interval = atoi(intervalStr);
	else
		interval = 0;

	switch(sb.sb_svcblk.sb_state) {
	default:
		if (sb.sb_svcblk.sb_last_owner != NODE_ID_NONE)
			getNodeName(sb.sb_svcblk.sb_last_owner, &node);
		else
			node = "None";
			
		if (node) {
			snprintf(ownerstr, sizeof(ownerstr), "(%s)",
			         node);
			break;
		}
	case SVC_UNINITIALIZED:
		strcpy(ownerstr, "(None)");
		break;
	case SVC_STARTED:
	case SVC_STOPPING:
		snprintf(ownerstr, sizeof(ownerstr), "%s", node);
		break;
	}

	if (long_form)
		printf("  %-14.14s %-8.8s %-16.16s %-15.15s %3d %8d\n",
		       servicename, serviceStateStrings[sb.sb_svcblk.sb_state],
		       ownerstr, timestr, interval, sb.sb_svcblk.sb_restarts);
	else
		printf("Service: %s  Status: %s (%d)  Owner (Last): %s\n",
		       servicename, serviceStateStrings[sb.sb_svcblk.sb_state],
		       sb.sb_svcblk.sb_state, ownerstr);

	return 0;
}


int
print_service_state_byname(char *name)
{
	int rv, svc;
	
	rv = getSvcID(name, &svc);
	if ((rv == SUCCESS) && (quorum_status(0,0))) {
		if (shared_storage_init() != 0) {
			perror("shared_storage_init");
			return -1;
		}
		return print_service_state(svc, 0);
	}

	fprintf(stderr,"Unknown service: %s\n", name);

	return -1;
}


/**
 * Retrieve the service states from shared storage and place them in the 
 * config database so that we can pass it up to redhat-config-cluster.
 *
 * @return		-1 on failure; 0 on success.
 */
int
xml_get_store_service_states(void)
{
	struct tm *tm;
	char timestr[TIMESTR_LEN];
	char buf[80], buf2[20];
	int x;
	char *servicename, *ownername;
	SharedServiceBlock sb;

	for (x=0; x<MAX_SERVICES; x++) {
		snprintf(buf, sizeof(buf), "services%%service%d%%name", x);
		if (CFG_Get(buf, NULL, &servicename) != CFG_OK)
			continue;

		snprintf(buf, sizeof(buf), "/service/%d/status", x);
		if (sh_read_atomic(buf, (void *)&sb, sizeof(sb)) == -1) {
			fprintf(stderr,
				"Error reading %s from shared state: %s\n",
				buf, strerror(errno));
			continue;
		}
		
		swab_SharedServiceBlock(&sb);
		
		/*
		 * Store state
		 */
		snprintf(buf, sizeof(buf), "services%%service%d%%state", x);
		/* Fudge service state to not break redhat-config-cluster */
		if (sb.sb_svcblk.sb_state == SVC_STOPPING)
			sb.sb_svcblk.sb_state = SVC_STARTED;
		snprintf(buf2, sizeof(buf2), "%d", sb.sb_svcblk.sb_state);
		CFG_Set(buf,buf2);

		/*
		 * Store owner node ID
		 */
		snprintf(buf, sizeof(buf), "services%%service%d%%ownerid", x);
		snprintf(buf2, sizeof(buf2), "%d", sb.sb_svcblk.sb_owner);
		CFG_Set(buf,buf2);

		/*
		 * Store owner name
		 */
		snprintf(buf, sizeof(buf), "members%%member%d%%name",
			 sb.sb_svcblk.sb_owner);
		if (CFG_Get(buf, NULL, &ownername) == CFG_OK) {
			snprintf(buf, sizeof(buf), "services%%service%d%%owner",
				 x);
			CFG_Set(buf,ownername);
		}

		/*
		 * Store last owner node ID
		 */
		snprintf(buf, sizeof(buf), "services%%service%d%%lastownerid", x);
		snprintf(buf2, sizeof(buf2), "%d", sb.sb_svcblk.sb_last_owner);
		CFG_Set(buf,buf2);

		/*
		 * Store owner name
		 */
		snprintf(buf, sizeof(buf), "members%%member%d%%name",
			 sb.sb_svcblk.sb_last_owner);
		if (CFG_Get(buf, NULL, &ownername) == CFG_OK) {
			snprintf(buf, sizeof(buf), "services%%service%d%%lastowner",
				 x);
			CFG_Set(buf,ownername);
		}

		/*
		 * Store restart count
		 */
		snprintf(buf, sizeof(buf),
			 "services%%service%d%%restart_count", x);
		snprintf(buf2, sizeof(buf2), "%d", sb.sb_svcblk.sb_restarts);
		CFG_Set(buf,buf2);

		/*
		 * Store the last transition date/time
		 */
		memset(timestr, 0, TIMESTR_LEN);
		tm = localtime((time_t *)&sb.sb_svcblk.sb_transition);
		if (tm)
			strftime(timestr, TIMESTR_LEN, "%H:%M:%S %b %d", tm);

		snprintf(buf, sizeof(buf),
			 "services%%service%d%%last_transition", x);
		CFG_Set(buf,timestr);
	}

	return 0;
}


inline void
print_cluforce_info(void)
{
	printf("\n"
"NOTE: If you are sure that it is safe to run services, you may enable\n"
"them at this point by using the 'cluforce' command.  See the cluforce(8)\n"
"man page ('man cluforce') for more details.\n");
}


/**
 * Get and print the quorum status of the local member.
 *
 * @param print		If set to nonzero, printf() the status.
 * @return		0 no/invalid quorum or 1 for quorum.
 */
int
quorum_status(int print, int force_tb)
{
	cm_event_t *quorum_event = NULL;
	int rv, connect_ok=1, tbf, qv;
	char *msg;

	if (quorum_query(&quorum_event) == -1) {
		if (print) {
			connect_ok = 0;
			msg = (errno == ECONNREFUSED) ?
			      "Cluster not running?" : 
			      strerror(errno);

			printf("Cluster Quorum Status Unknown (%s)\n",
			       msg);
		}
		return 0;
	} 

	rv = (cm_ev_event(quorum_event) == EV_QUORUM ||
	      cm_ev_event(quorum_event) == EV_QUORUM_GAINED);

	qv = (int)cm_quorum_view(quorum_event);

	if (print) {
		if (rv) {
			printf("Cluster Quorum Incarnation #%d\n",
			       (int)cm_quorum_view(quorum_event));
		} else {
			if (cm_quorum_view(quorum_event) == 0) {
				printf("Initializing...\n");
			} else {
				printf("Incarnation #%d\n",
				       (int)cm_quorum_view(quorum_event));
				printf("(This member is not part of the "
				       "cluster quorum)\n");
			}
		}
	}

	cm_ev_free(quorum_event);

	/* Print cluster quorum information if we're not quorate,
	   for debugging/helpful hints for users. */
	if (connect_ok && print && (!rv || force_tb)) {
		if (quorum_query_tb(&quorum_event) == -1) {
			msg = (errno == ECONNREFUSED) ?
			      "Cluster not running?" : 
			      strerror(errno);

			printf("Tiebreaker Status Unknown (%s)\n",
			       msg);

			return rv;
		}

		tbf = cm_ev_event(quorum_event);
		cm_ev_free(quorum_event);

		if (tbf & EV_TBF_NONE)
			return rv;

		if (tbf & EV_TBF_DISK) {
			printf("Disk Tiebreaker: ");

			if (tbf & EV_TBF_ONLINE)
				printf("Online\n");
			else
				printf("Offline\n");

			/* Not reporting on foreign node status */

		} else if (tbf & EV_TBF_NET) {
			printf("IP Tiebreaker: ");

			if (tbf & EV_TBF_ONLINE)
				printf("Online ");
			else 
				printf("Offline ");

			if (tbf & EV_TBF_NET_SOFT) {
				printf("(Soft quorum enabled)\n");
			} else {
				printf("\n");
				if (qv && (tbf & EV_TBF_ONLINE)) {
					print_cluforce_info();
				}
			}
		} else {
			return rv;
		}
	}

	return rv;
}


/**
 * Store the local member's view of quorum in the cluster config database
 * so we can pass it up to redhat-config-cluster.
 */
int
xml_get_store_quorum_state(void)
{
	char buf2[20];
	cm_event_t *quorum_event = NULL;
	int rv = 0;

	if (quorum_query(&quorum_event) == 0) {
		rv = (cm_ev_event(quorum_event) == EV_QUORUM ||
	      	      cm_ev_event(quorum_event) == EV_QUORUM_GAINED);

		snprintf(buf2, sizeof(buf2), "%d",
			 (int)cm_quorum_view(quorum_event));
		CFG_Set("cluquorumd%quorum_view", buf2);

		cm_ev_free(quorum_event);
	} else {
		CFG_Set("cluquorumd%quorum_view", "-1");
	}

	snprintf(buf2, sizeof(buf2), "%d", rv);
	CFG_Set("cluquorumd%have_quorum", buf2);

	return (!rv);
}


/**
 * Print out the current cluster status to a terminal.
 */
void
display_cluster_status(int force_tb)
{
	struct tm *tm = NULL;
	char timestr[TIMESTR_LEN];
	time_t clk;
	int c, q, ss = -1, ss_error = 0;
	char title[80];
	char *val = NULL;

	if (CFG_ReadFile(CLU_CONFIG_FILE) != CFG_OK) {
		fprintf(stderr,"Couldn't open %s: %s\n", CLU_CONFIG_FILE, strerror(errno));
		return;
	}

	myNodeID = memb_local_id();

	time(&clk);
	memset(timestr,0,TIMESTR_LEN);
	tm = localtime(&clk);
	if (tm)
	    strftime(timestr, TIMESTR_LEN, "%H:%M:%S", tm);

	if (CFG_Get("cluster%name", NULL, &val) != CFG_OK) {
		snprintf(title, sizeof(title), "Cluster Status");
	} else {
		snprintf(title, sizeof(title), "Cluster Status - %s", val);
	}

        printf("%-70s %8s\n", title, timestr);

	q = quorum_status(1, force_tb);

	if (q) {
		if ((ss = shared_storage_init()) == -1) {
			ss_error = errno;
		} else {
			printf("Shared State: %s\n", sh_version());
		}
	}

	printf("\n");

	print_member_header();
	for (c=0; c<MAX_NODES; c++) {
		print_member_state(c, 1);
	}

	printf("\n");
	if (q) {
		if (ss == 0) {
			print_service_header();
			for (c=0; c<MAX_SERVICES; c++) {
				print_service_state(c, 1);
			}

			shared_storage_deinit();
		} else {
			printf("Shared State Failure - %s\n",
			       strerror(ss_error));
		}
	} else {
		printf("No Quorum - Service States Unknown\n");
	}

	CFG_Destroy();
}


/**
 * Get/Store info necessary to output XML status later.
 */
void
build_xml_info(void)
{
	xml_get_store_member_states();
	if (xml_get_store_quorum_state() == 0) { 
		shared_storage_init();
		xml_get_store_service_states();
		shared_storage_deinit();
	}
}


/**
 * Print our XML status.
 */
void
dump_xml_info(void)
{
	char *buffer = NULL;
	size_t buflen = 0;

	if (CFG_WriteBuffer(&buffer, &buflen) == CFG_OK) {
		write(STDOUT_FILENO, buffer, buflen);
	}
}


/**
 * Print out the local machine's cluster member id
 */
int
print_node_id(void)
{
	int x = memb_local_id();

	if (x == -1)
		return -1;
	
	printf("%d\n",x);
	return 0;
}


/**
 * Display usage.
 */
void
usage(void)
{
	printf("Arguments:\n");
	printf("  -I           Display the member ID of this member.\n");
	printf("  -i <n>       Refresh every n seconds.\n");
	printf("  -m <member>  Display the status of the designated member.\n");
	printf("  -s <service> Display the status of the designated service.\n");
	printf("  -x           Output XML of cluster configuration + status.\n");
	printf("  -Q           Return quorum status immediately.\n");
	printf("  -v           Display version\n");
	printf("\n");
}


/**
 * Main driver
 */
int
main(int argc, char **argv)
{
	int opt;
	extern char *optarg;
	int refresh_sec = 0, errors = 0, args = 0, force_tb = 0;

	/* Quick hack to get version information */
        if (argc >= 2 && !strcmp(argv[1], "-v")) {
		printf("%s\n", VERSION);
		return 0;
	}
	
	if (CFG_ReadFile(CLU_CONFIG_FILE) != CFG_OK) {
		fprintf(stderr, "Warning: Could not read %s: %s\n",
			CLU_CONFIG_FILE, strerror(errno));
		return 1;
	}

	while ((opt = getopt(argc, argv, "tIs:m:i:xvQ")) != EOF) {
		switch(opt) {
		case 'v':
			printf("%s\n", VERSION);
			return 0;

		case 't':
			force_tb = 1;
			break;

		case 'I':
			return print_node_id();

		case 'i':
			args++;
			refresh_sec = atoi(optarg);
			if (refresh_sec <= 0)
				refresh_sec = 1;
			break;

		case 'm':
			return print_member_state_byname(optarg);

		case 'Q':
			/* Return to shell: 0 true, 1 false... */
			return !quorum_status(0,0);

		case 's':
			return print_service_state_byname(optarg);

		case 'x':
			if (refresh_sec || force_tb) {
				printf("Error: Options '-x' cannot be used "
				       "with '-i' or '-t'.\n");
				return 1;
			}

			build_xml_info();
			dump_xml_info();
			return 0;
		default:
			errors++;
			break;
		}
	}

	if (errors) {
		usage();
		return 0;
	}

	if (refresh_sec)
		setupterm((char *) 0, STDOUT_FILENO, (int *) 0);

	do {
		if (refresh_sec)
			tputs(clear_screen, lines > 0 ? lines : 1, putchar);

		display_cluster_status(force_tb);

		if (refresh_sec)
			sleep(refresh_sec);
	} while(refresh_sec);

	return 0;
}
