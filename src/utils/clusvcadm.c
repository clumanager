/*
  Copyright Red Hat, Inc. 2002-2003

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; see the file COPYING.  If not, write to the
  Free Software Foundation, Inc.,  675 Mass Ave, Cambridge, 
  MA 02139, USA.
*/
/** @file
 * The New And Improved Cluster Service Admin Utility.
 * TODO Clean up the code.
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/param.h>
#include <msgsvc.h>
#include <clusterdefs.h>
#include <libgen.h>
#include <svcmgr.h>
#include <clushared.h>
#include <sharedstate.h>
#include <xmlwrap.h>
#include <nodeid.h>
#include <errno.h>

int wait_forever = 0;

int getServiceStatus(int, ServiceBlock *);

void
build_message(SmMessageSt *msgp, int action, int svcid, int target)
{
	msgp->sm_hdr.gh_magic = GENERIC_HDR_MAGIC;
	msgp->sm_hdr.gh_command = SVC_ACTION_REQUEST;
	msgp->sm_hdr.gh_length = sizeof(*msgp);
	msgp->sm_data.d_action = action;
	msgp->sm_data.d_svcID = svcid;
	msgp->sm_data.d_svcOwner = target;
	msgp->sm_data.d_ret = 0;
}


int
notify_all(uint32_t cmd)
{
	int x, fd;
	int rv, errors = 0;

	for (x=0; x<MAX_NODES; x++) {
		fd = msg_open(PROCID_CLUSVCMGRD, x);
		if (fd == -1)
			continue;

		rv = msg_send_simple(fd, cmd, 0, 0);
		if (rv < 0)
			errors++;
		msg_close(fd);
	}

	return !!errors;
}


void
display_lock_states(void)
{
	int t = 0, l = 0, x = 0, fd;
	generic_msg_hdr foo;

	printf("Service Manager Shutdown-Lock States\n");

	for (x=0; x<MAX_NODES; x++) {
		fd = msg_open(PROCID_CLUSVCMGRD, x);
		if (fd == -1)
			continue;
	
		t++;
		if (msg_send_simple(fd, SVC_QUERY_LOCK, 0, 0) < 0) {
			printf("Couldn't send message to member #%d\n",
			       x);

			continue;
		}

		if (msg_receive(fd, &foo, sizeof(foo)) != sizeof(foo)) {
			printf("Size mismatch in reply from member #%d\n",
			       x);
			continue;
		}

		swab_generic_msg_hdr(&foo);

		printf("Member #%d: ", x);
		if (foo.gh_command == SVC_LOCK) {
			printf("locked\n");
			l++;
		} else {
			printf("unlocked\n");
		}

		msg_close(fd);
	}

	if (l == t) {
		printf("All service managers are prepared for shutdown\n");
	}
}


int
getServiceOwnerID(int svcid)
{
	ServiceBlock sb;

	if (getServiceStatus(svcid, &sb) != SUCCESS)
		return -1;

	return sb.sb_owner;
}


void
usage(char *name)
{
printf("usage: %s -d <service>             Disable <service>\n", name);
printf("       %s -e <service>             Enable <service>\n",
       name);
printf("       %s -e <service> -m <member> Enable <service> on <member>\n",
       name);
printf("       %s -l                       Lock service managers in preparation\n"
       "                                   for cluster-wide shutdown.\n",name);
printf("       %s -R <service>             Restart a service in place.\n",
       name);
printf("       %s -r <service> -m <member> Relocate <service> [to <member>]\n",
	       name);
printf("       %s -q                       Quiet operation\n", name);
printf("       %s -S                       Query shutdown state of service \n"
       "                                   managers\n", name);
printf("       %s -s <service>             Stop <service>\n", name);
printf("       %s -u                       Unlock service managers (cluster-wide "
       "                                   shutdown has been cancelled)\n", name);
printf("       %s -v                       Display version and exit\n",name);
}


int
main(int argc, char **argv)
{
	extern char *optarg;
	char *svcname=NULL, *nodename=NULL, *destnode=NULL;
	int opt;
	int msgfd = -1;
	SmMessageSt msg;
	int action, svcid, svctarget = -1, msgtarget;
	char *actionstr = NULL;
	struct timeval tout = {30, 0};

	if (geteuid() != (uid_t) 0) {
		fprintf(stderr, "%s must be run as the root user.\n", argv[0]);
		return 1;
	}

	while ((opt = getopt(argc, argv, "e:d:r:n:m:vR:s:luqSw")) != EOF) {
		switch (opt) {
		case 'w':
			/* Wait for clusvcmgrd if it's blocked on something
			 * rather than timing out after 10 seconds like normal
			 */
			wait_forever = 1;
			break;
		case 'e':
			/* ENABLE */
			actionstr = "trying to enable";
			action = SVC_START;
			svcname = optarg;
			break;
		case 'd':
			/* DISABLE */
			actionstr = "disabling";
			action = SVC_DISABLE;
			svcname = optarg;
			break;
		case 'r':
			/* RELOCATE */
			actionstr = "trying to relocate";
			action = SVC_RELOCATE;
			svcname = optarg;
			break;
		case 's':
			/* stop */
			actionstr = "stopping";
			action = SVC_STOP;
			svcname = optarg;
			break;
		case 'R':
			actionstr = "trying to restart";
			action = SVC_RESTART;
			svcname = optarg;
			break;
		case 'm': /* member ... */
		case 'n': /* node .. same thing */
			nodename = optarg;
			break;
		case 'u':
			/* Unlock all service states */
			action = SVC_UNLOCK;
			break;
		case 'S':
			action = SVC_QUERY_LOCK;
			break;
		case 'l':
			/* Lock all service states */
			action = SVC_LOCK;
			break;
		case 'v':
			printf("%s\n",VERSION);
			return 0;
		case 'q':
			close(STDOUT_FILENO);
			break;
		default:
			usage(basename(argv[0]));
			return 1;
		}
	}

	/* Fun */
	shared_storage_init();
	CFG_Read();

	switch(action) {
	case SVC_QUERY_LOCK:
		display_lock_states();
		return 0;

	case SVC_START:
		if (!svcname) {
			usage(basename(argv[0]));
			return 1;
		}

		if (getSvcID(svcname, &svcid) != SUCCESS) {
			fprintf(stderr, "Unknown service: %s\n", svcname);
			return 1;
		}

do_start:
		if (nodename) {
			msgtarget = getNodeIDCached(nodename);
			if (msgtarget == -1) {
				fprintf(stderr, "Unknown node: %s\n",
				       	nodename);
				return 1;
			}
		}
		break;

	case SVC_LOCK:
	case SVC_UNLOCK:
		return notify_all(action);

	default:
		if (!svcname) {
			usage(basename(argv[0]));
			return 1;
		}

		if (getSvcID(svcname, &svcid) != SUCCESS) {
			fprintf(stderr, "Unknown service: %s\n", svcname);
			return 1;
		}

		msgtarget = getServiceOwnerID(svcid);
		if (msgtarget == -1) {
			fprintf(stderr,
				"Could not determine service owner!\n");
			return 1;
		}

		/* Disabled? Stopped? */
		if (msgtarget == NODE_ID_NONE && action != SVC_DISABLE
		    && action != SVC_STOP) {
			actionstr = "enabling";
			action = SVC_START;
			goto do_start;
		}

		/* Relocate to next-highest prio node */
		if (nodename && (action == SVC_RELOCATE)) {
			destnode = nodename;
			svctarget = getNodeIDCached(nodename);
			if (svctarget == -1) {
				fprintf(stderr, "Unknown node: %s\n",
				       	nodename);
				return 1;
			}
		}

		getNodeName(msgtarget, &nodename);
	}

	/* XXX dirty. */
	if (!nodename) {
		msgtarget = cluGetLocalNodeId();
		getNodeName(msgtarget, &nodename);
	}

	/* handle relocation to owner as 'restart' */
	if (destnode && nodename && !strcmp(destnode,nodename)) {
		action = SVC_RESTART;
		actionstr = "trying to restart";
	}

	printf("Member %s %s %s", nodename, actionstr, svcname);
	if (action == SVC_RELOCATE && destnode) {
		printf(" to %s", destnode);
	}
	printf("...");
	fflush(stdout);

	build_message(&msg, action, svcid, svctarget);

	do {
		msgfd = msg_open_timeout(PROCID_CLUSVCMGRD, msgtarget, &tout);
		if (msgfd < 0) {
			if (errno == ETIMEDOUT && wait_forever)
				continue;
			perror("msg_open");
			fprintf(stderr,
				"Could not connect to service manager!\n");
			return 1;
		}
	} while (0);

	/* encode */
	swab_SmMessageSt(&msg);

	if (msg_send(msgfd, &msg, sizeof(msg)) != sizeof(msg)) {
		perror("msg_send");
		fprintf(stderr, "Could not send entire message!\n");
		return 1;
	}

	if (msg_receive(msgfd, &msg, sizeof(msg)) != sizeof(msg)) {
		perror("msg_receive");
		fprintf(stderr, "Error receiving reply!\n");
		return 1;
	}

	/* Decode */
	swab_SmMessageSt(&msg);

	if (msg.sm_data.d_ret == SUCCESS) {
		printf("success\n");

		if (svctarget == -1) {
			/*
			 * Relocate w/ unspecified target: Just display
			 * where the service went.
			 */
			if (action == SVC_RELOCATE) {
				getNodeName(msg.sm_data.d_svcOwner, &nodename);
				printf("Service %s is now running on %s.\n",
				       svcname, nodename);
				return msg.sm_data.d_ret;
			}

			/*
			 * Enable/restart w/ unspecified target: expect 
			 * service starts on member we sent message to.
			 */
			svctarget = msgtarget;
		}
		
		if ((action == SVC_START || action == SVC_RELOCATE ||
		     action == SVC_RESTART) &&
		    (msg.sm_data.d_svcOwner != svctarget)) {
			getNodeName(svctarget, &destnode);
			printf("Note: Service %s failed to start on %s.\n",
			       svcname, nodename);
			getNodeName(msg.sm_data.d_svcOwner, &nodename);
			printf("Service %s is now running on %s.\n", svcname,
			       destnode);
		}
	} else
		printf("failed\n");

	return msg.sm_data.d_ret;
}
